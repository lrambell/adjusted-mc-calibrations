# Adjusted-MC calibrations
Workflow for obtaining Scale Factor values starting by TDD h5 output files.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.cern.ch/lrambell/adjusted-mc-calibrations.git
git branch -M master
git push -uf origin master
```

## Usage
After the TDD h5 files production in the dump-retag-fatjet mode a couple of steps are needed for obtaining the SF values for different working points as a function of the pT: 

**Variables extrapolation for different final state categories** with ```variables_extrapolation.py```

After a selection done using the VR variables ```HadronConeExclTruthLabelID``` and ```valid```, the interesting variables from the h5 files are extracted for each final state, merged for 6 categories (0b0c, 0b1c, 0b2c, 1b1c, 1b0c, 2b0c) and saved in another h5 file. The reported script have to be used passing each time the path of the directory which contains the TDD output files and defining the name of the output h5 file. In it also the total sum of the weights of the sample is saved as it is needed for single event weighting.

**Separate SF contribution computation** with ```separate_sf_computation.py```

Taking as input the h5 output files from the 1st step, events are weighted and mistag efficiencies computed as selected/(selected+non_selected) events for each configuration (nominal and with track systematics applied), then the ratio between the nominal and the adjusted case is computed with its error. The reported script works only with the systematics related to the impact parameter, but can be modified as needed. Within the script is it possible to select the working point of interest, which will define the name of all the created output files. In the end another h5 file which contains the SF values with their error for each considered case is returned.



**Final SF computation with errors for each WP** with ```sf_rel_unc_computation.py``` 

Taking all the h5 files containing the SF values with errors for each systematic variation in this script they are combined for obtaining the scale factor central values (defined by the data-driven variations) and the error (in which all the MC-driven systematics are included). 
It returns a txt file with all the values with errors, a plot for each selected WP and an overall plot which shows for each category the SF values for each WP.


## Other useful plotting scripts
With the ``` mistag_for_categories.py ``` script it is possible to produce plots of the mistag rate as a function of the jet pT for different categories and configurations. Here the contribution given by events in each slice is firstly weighted and then combined to obtain an overall value for each pT bin which takes into account separate slice values and errors.