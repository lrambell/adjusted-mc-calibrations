import numpy as np
import h5py
import matplotlib.pyplot as plt

#load datas 
bias_file_50 = h5py.File('sf_bias50.h5', 'r')
dead_file_50 = h5py.File('sf_dead50.h5', 'r')
trkd0_file_50 = h5py.File('sf_trkd050.h5', 'r')
trkz0_file_50 = h5py.File('sf_trkz050.h5', 'r')
effloose_file_50 = h5py.File('sf_effloose50.h5', 'r')
effrate_file_50 = h5py.File('sf_effrate50.h5', 'r')
fakerate_file_50 = h5py.File('sf_fakerate50.h5', 'r')


#TRK_BIAS_QOVERP_SAGITTA_WM
sf_0b0c_qoverp_50 = bias_file_50['sf_0b0c_qoverp_50'][:]
sf_0b1c_qoverp_50 = bias_file_50['sf_0b1c_qoverp_50'][:]
sf_0b2c_qoverp_50 = bias_file_50['sf_0b2c_qoverp_50'][:]
sf_1b0c_qoverp_50 = bias_file_50['sf_1b0c_qoverp_50'][:]
sf_2b0c_qoverp_50 = bias_file_50['sf_2b0c_qoverp_50'][:]
sf_1b1c_qoverp_50 = bias_file_50['sf_1b1c_qoverp_50'][:]

err_sf_0b0c_qoverp_50 = bias_file_50['err_sf_0b0c_qoverp_50'][:]
err_sf_0b1c_qoverp_50 = bias_file_50['err_sf_0b1c_qoverp_50'][:]
err_sf_0b2c_qoverp_50 = bias_file_50['err_sf_0b2c_qoverp_50'][:]
err_sf_1b0c_qoverp_50 = bias_file_50['err_sf_1b0c_qoverp_50'][:]
err_sf_2b0c_qoverp_50 = bias_file_50['err_sf_2b0c_qoverp_50'][:]
err_sf_1b1c_qoverp_50 = bias_file_50['err_sf_1b1c_qoverp_50'][:]

#TRK_BIAS_D0_WM
sf_0b0c_trkd0bias_50 = bias_file_50['sf_0b0c_trkd0bias_50'][:]
sf_0b1c_trkd0bias_50 = bias_file_50['sf_0b1c_trkd0bias_50'][:]
sf_0b2c_trkd0bias_50 = bias_file_50['sf_0b2c_trkd0bias_50'][:]
sf_1b0c_trkd0bias_50 = bias_file_50['sf_1b0c_trkd0bias_50'][:]
sf_2b0c_trkd0bias_50 = bias_file_50['sf_2b0c_trkd0bias_50'][:]
sf_1b1c_trkd0bias_50 = bias_file_50['sf_1b1c_trkd0bias_50'][:]

err_sf_0b0c_trkd0bias_50 = bias_file_50['err_sf_0b0c_trkd0bias_50'][:]
err_sf_0b1c_trkd0bias_50 = bias_file_50['err_sf_0b1c_trkd0bias_50'][:]
err_sf_0b2c_trkd0bias_50 = bias_file_50['err_sf_0b2c_trkd0bias_50'][:]
err_sf_1b0c_trkd0bias_50 = bias_file_50['err_sf_1b0c_trkd0bias_50'][:]
err_sf_2b0c_trkd0bias_50 = bias_file_50['err_sf_2b0c_trkd0bias_50'][:]
err_sf_1b1c_trkd0bias_50 = bias_file_50['err_sf_1b1c_trkd0bias_50'][:]

#TRK_RES_D0_DEAD
sf_0b0c_trkd0dead_50 = dead_file_50['sf_0b0c_trkd0dead_50'][:]
sf_0b1c_trkd0dead_50 = dead_file_50['sf_0b1c_trkd0dead_50'][:]
sf_0b2c_trkd0dead_50 = dead_file_50['sf_0b2c_trkd0dead_50'][:]
sf_1b0c_trkd0dead_50 = dead_file_50['sf_1b0c_trkd0dead_50'][:]
sf_2b0c_trkd0dead_50 = dead_file_50['sf_2b0c_trkd0dead_50'][:]
sf_1b1c_trkd0dead_50 = dead_file_50['sf_1b1c_trkd0dead_50'][:]

err_sf_0b0c_trkd0dead_50 = dead_file_50['err_sf_0b0c_trkd0dead_50'][:]
err_sf_0b1c_trkd0dead_50 = dead_file_50['err_sf_0b1c_trkd0dead_50'][:]
err_sf_0b2c_trkd0dead_50 = dead_file_50['err_sf_0b2c_trkd0dead_50'][:]
err_sf_1b0c_trkd0dead_50 = dead_file_50['err_sf_1b0c_trkd0dead_50'][:]
err_sf_2b0c_trkd0dead_50 = dead_file_50['err_sf_2b0c_trkd0dead_50'][:]
err_sf_1b1c_trkd0dead_50 = dead_file_50['err_sf_1b1c_trkd0dead_50'][:]

#TRK_RES_Z0_DEAD
sf_0b0c_trkz0dead_50 = dead_file_50['sf_0b0c_trkz0dead_50'][:]
sf_0b1c_trkz0dead_50 = dead_file_50['sf_0b1c_trkz0dead_50'][:]
sf_0b2c_trkz0dead_50 = dead_file_50['sf_0b2c_trkz0dead_50'][:]
sf_1b0c_trkz0dead_50 = dead_file_50['sf_1b0c_trkz0dead_50'][:]
sf_2b0c_trkz0dead_50 = dead_file_50['sf_2b0c_trkz0dead_50'][:]
sf_1b1c_trkz0dead_50 = dead_file_50['sf_1b1c_trkz0dead_50'][:]

err_sf_0b0c_trkz0dead_50 = dead_file_50['err_sf_0b0c_trkz0dead_50'][:]
err_sf_0b1c_trkz0dead_50 = dead_file_50['err_sf_0b1c_trkz0dead_50'][:]
err_sf_0b2c_trkz0dead_50 = dead_file_50['err_sf_0b2c_trkz0dead_50'][:]
err_sf_1b0c_trkz0dead_50 = dead_file_50['err_sf_1b0c_trkz0dead_50'][:]
err_sf_2b0c_trkz0dead_50 = dead_file_50['err_sf_2b0c_trkz0dead_50'][:]
err_sf_1b1c_trkz0dead_50 = dead_file_50['err_sf_1b1c_trkz0dead_50'][:]

#TRK_RES_D0_MEAS
sf_0b0c_trkd0_50 = trkd0_file_50['sf_0b0c_trkd0_50'][:]
sf_0b1c_trkd0_50 = trkd0_file_50['sf_0b1c_trkd0_50'][:]
sf_0b2c_trkd0_50 = trkd0_file_50['sf_0b2c_trkd0_50'][:]
sf_1b0c_trkd0_50 = trkd0_file_50['sf_1b0c_trkd0_50'][:]
sf_2b0c_trkd0_50 = trkd0_file_50['sf_2b0c_trkd0_50'][:]
sf_1b1c_trkd0_50 = trkd0_file_50['sf_1b1c_trkd0_50'][:]

err_sf_0b0c_trkd0_50 = trkd0_file_50['err_sf_0b0c_trkd0_50'][:]
err_sf_0b1c_trkd0_50 = trkd0_file_50['err_sf_0b1c_trkd0_50'][:]
err_sf_0b2c_trkd0_50 = trkd0_file_50['err_sf_0b2c_trkd0_50'][:]
err_sf_1b0c_trkd0_50 = trkd0_file_50['err_sf_1b0c_trkd0_50'][:]
err_sf_2b0c_trkd0_50 = trkd0_file_50['err_sf_2b0c_trkd0_50'][:]
err_sf_1b1c_trkd0_50 = trkd0_file_50['err_sf_1b1c_trkd0_50'][:]

#TRK_RES_D0_MEAS_UP
sf_0b0c_trkd0up_50 = trkd0_file_50['sf_0b0c_trkd0up_50'][:]
sf_0b1c_trkd0up_50 = trkd0_file_50['sf_0b1c_trkd0up_50'][:]
sf_0b2c_trkd0up_50 = trkd0_file_50['sf_0b2c_trkd0up_50'][:]
sf_1b0c_trkd0up_50 = trkd0_file_50['sf_1b0c_trkd0up_50'][:]
sf_2b0c_trkd0up_50 = trkd0_file_50['sf_2b0c_trkd0up_50'][:]
sf_1b1c_trkd0up_50 = trkd0_file_50['sf_1b1c_trkd0up_50'][:]

err_sf_0b0c_trkd0up_50 = trkd0_file_50['err_sf_0b0c_trkd0up_50'][:]
err_sf_0b1c_trkd0up_50 = trkd0_file_50['err_sf_0b1c_trkd0up_50'][:]
err_sf_0b2c_trkd0up_50 = trkd0_file_50['err_sf_0b2c_trkd0up_50'][:]
err_sf_1b0c_trkd0up_50 = trkd0_file_50['err_sf_1b0c_trkd0up_50'][:]
err_sf_2b0c_trkd0up_50 = trkd0_file_50['err_sf_2b0c_trkd0up_50'][:]
err_sf_1b1c_trkd0up_50 = trkd0_file_50['err_sf_1b1c_trkd0up_50'][:]

#TRK_RES_D0_MEAS_DOWN
sf_0b0c_trkd0down_50 = trkd0_file_50['sf_0b0c_trkd0down_50'][:]
sf_0b1c_trkd0down_50 = trkd0_file_50['sf_0b1c_trkd0down_50'][:]
sf_0b2c_trkd0down_50 = trkd0_file_50['sf_0b2c_trkd0down_50'][:]
sf_1b0c_trkd0down_50 = trkd0_file_50['sf_1b0c_trkd0down_50'][:]
sf_2b0c_trkd0down_50 = trkd0_file_50['sf_2b0c_trkd0down_50'][:]
sf_1b1c_trkd0down_50 = trkd0_file_50['sf_1b1c_trkd0down_50'][:]
err_sf_0b0c_trkd0down_50 = trkd0_file_50['err_sf_0b0c_trkd0down_50'][:]
err_sf_0b1c_trkd0down_50 = trkd0_file_50['err_sf_0b1c_trkd0down_50'][:]
err_sf_0b2c_trkd0down_50 = trkd0_file_50['err_sf_0b2c_trkd0down_50'][:]
err_sf_1b0c_trkd0down_50 = trkd0_file_50['err_sf_1b0c_trkd0down_50'][:]
err_sf_2b0c_trkd0down_50 = trkd0_file_50['err_sf_2b0c_trkd0down_50'][:]
err_sf_1b1c_trkd0down_50 = trkd0_file_50['err_sf_1b1c_trkd0down_50'][:]

#TRK_RES_Z0_MEAS
sf_0b0c_trkz0_50 = trkz0_file_50['sf_0b0c_trkz0_50'][:]
sf_0b1c_trkz0_50 = trkz0_file_50['sf_0b1c_trkz0_50'][:]
sf_0b2c_trkz0_50 = trkz0_file_50['sf_0b2c_trkz0_50'][:]
sf_1b0c_trkz0_50 = trkz0_file_50['sf_1b0c_trkz0_50'][:]
sf_2b0c_trkz0_50 = trkz0_file_50['sf_2b0c_trkz0_50'][:]
sf_1b1c_trkz0_50 = trkz0_file_50['sf_1b1c_trkz0_50'][:]

err_sf_0b0c_trkz0_50 = trkz0_file_50['err_sf_0b0c_trkz0_50'][:]
err_sf_0b1c_trkz0_50 = trkz0_file_50['err_sf_0b1c_trkz0_50'][:]
err_sf_0b2c_trkz0_50 = trkz0_file_50['err_sf_0b2c_trkz0_50'][:]
err_sf_1b0c_trkz0_50 = trkz0_file_50['err_sf_1b0c_trkz0_50'][:]
err_sf_2b0c_trkz0_50 = trkz0_file_50['err_sf_2b0c_trkz0_50'][:]
err_sf_1b1c_trkz0_50 = trkz0_file_50['err_sf_1b1c_trkz0_50'][:]

#TRK_RES_Z0_MEAS_UP
sf_0b0c_trkz0up_50 = trkz0_file_50['sf_0b0c_trkz0up_50'][:]
sf_0b1c_trkz0up_50 = trkz0_file_50['sf_0b1c_trkz0up_50'][:]
sf_0b2c_trkz0up_50 = trkz0_file_50['sf_0b2c_trkz0up_50'][:]
sf_1b0c_trkz0up_50 = trkz0_file_50['sf_1b0c_trkz0up_50'][:]
sf_2b0c_trkz0up_50 = trkz0_file_50['sf_2b0c_trkz0up_50'][:]
sf_1b1c_trkz0up_50 = trkz0_file_50['sf_1b1c_trkz0up_50'][:]

err_sf_0b0c_trkz0up_50 = trkz0_file_50['err_sf_0b0c_trkz0up_50'][:]
err_sf_0b1c_trkz0up_50 = trkz0_file_50['err_sf_0b1c_trkz0up_50'][:]
err_sf_0b2c_trkz0up_50 = trkz0_file_50['err_sf_0b2c_trkz0up_50'][:]
err_sf_1b0c_trkz0up_50 = trkz0_file_50['err_sf_1b0c_trkz0up_50'][:]
err_sf_2b0c_trkz0up_50 = trkz0_file_50['err_sf_2b0c_trkz0up_50'][:]
err_sf_1b1c_trkz0up_50 = trkz0_file_50['err_sf_1b1c_trkz0up_50'][:]

#TRK_RES_Z0_MEAS_DOWN
sf_0b0c_trkz0down_50 = trkz0_file_50['sf_0b0c_trkz0down_50'][:]
sf_0b1c_trkz0down_50 = trkz0_file_50['sf_0b1c_trkz0down_50'][:]
sf_0b2c_trkz0down_50 = trkz0_file_50['sf_0b2c_trkz0down_50'][:]
sf_1b0c_trkz0down_50 = trkz0_file_50['sf_1b0c_trkz0down_50'][:]
sf_2b0c_trkz0down_50 = trkz0_file_50['sf_2b0c_trkz0down_50'][:]
sf_1b1c_trkz0down_50 = trkz0_file_50['sf_1b1c_trkz0down_50'][:]

err_sf_0b0c_trkz0down_50 = trkz0_file_50['err_sf_0b0c_trkz0down_50'][:]
err_sf_0b1c_trkz0down_50 = trkz0_file_50['err_sf_0b1c_trkz0down_50'][:]
err_sf_0b2c_trkz0down_50 = trkz0_file_50['err_sf_0b2c_trkz0down_50'][:]
err_sf_1b0c_trkz0down_50 = trkz0_file_50['err_sf_1b0c_trkz0down_50'][:]
err_sf_2b0c_trkz0down_50 = trkz0_file_50['err_sf_2b0c_trkz0down_50'][:]
err_sf_1b1c_trkz0down_50 = trkz0_file_50['err_sf_1b1c_trkz0down_50'][:]

#TRK_EFF_LOOSE_GLOBAL
sf_0b0c_efflooseglob_50 = effloose_file_50['sf_0b0c_efflooseglob_50'][:]
sf_0b1c_efflooseglob_50 = effloose_file_50['sf_0b1c_efflooseglob_50'][:]
sf_0b2c_efflooseglob_50 = effloose_file_50['sf_0b2c_efflooseglob_50'][:]
sf_1b0c_efflooseglob_50 = effloose_file_50['sf_1b0c_efflooseglob_50'][:]
sf_2b0c_efflooseglob_50 = effloose_file_50['sf_2b0c_efflooseglob_50'][:]
sf_1b1c_efflooseglob_50 = effloose_file_50['sf_1b1c_efflooseglob_50'][:]

err_sf_0b0c_efflooseglob_50 = effloose_file_50['err_sf_0b0c_efflooseglob_50'][:]   
err_sf_0b1c_efflooseglob_50 = effloose_file_50['err_sf_0b1c_efflooseglob_50'][:]
err_sf_0b2c_efflooseglob_50 = effloose_file_50['err_sf_0b2c_efflooseglob_50'][:]
err_sf_1b0c_efflooseglob_50 = effloose_file_50['err_sf_1b0c_efflooseglob_50'][:]
err_sf_2b0c_efflooseglob_50 = effloose_file_50['err_sf_2b0c_efflooseglob_50'][:]
err_sf_1b1c_efflooseglob_50 = effloose_file_50['err_sf_1b1c_efflooseglob_50'][:]

#TRK_EFF_LOOSE_IBL
sf_0b0c_efflooseibl_50 = effloose_file_50['sf_0b0c_efflooseibl_50'][:]
sf_0b1c_efflooseibl_50 = effloose_file_50['sf_0b1c_efflooseibl_50'][:]
sf_0b2c_efflooseibl_50 = effloose_file_50['sf_0b2c_efflooseibl_50'][:]
sf_1b0c_efflooseibl_50 = effloose_file_50['sf_1b0c_efflooseibl_50'][:]
sf_2b0c_efflooseibl_50 = effloose_file_50['sf_2b0c_efflooseibl_50'][:]
sf_1b1c_efflooseibl_50 = effloose_file_50['sf_1b1c_efflooseibl_50'][:]

err_sf_0b0c_efflooseibl_50 = effloose_file_50['err_sf_0b0c_efflooseibl_50'][:]
err_sf_0b1c_efflooseibl_50 = effloose_file_50['err_sf_0b1c_efflooseibl_50'][:]
err_sf_0b2c_efflooseibl_50 = effloose_file_50['err_sf_0b2c_efflooseibl_50'][:]
err_sf_1b0c_efflooseibl_50 = effloose_file_50['err_sf_1b0c_efflooseibl_50'][:]
err_sf_2b0c_efflooseibl_50 = effloose_file_50['err_sf_2b0c_efflooseibl_50'][:]
err_sf_1b1c_efflooseibl_50 = effloose_file_50['err_sf_1b1c_efflooseibl_50'][:]

#TRK_EFF_LOOSE_TIDE
sf_0b0c_effloosetide_50 = effloose_file_50['sf_0b0c_effloosetide_50'][:]
sf_0b1c_effloosetide_50 = effloose_file_50['sf_0b1c_effloosetide_50'][:]
sf_0b2c_effloosetide_50 = effloose_file_50['sf_0b2c_effloosetide_50'][:]
sf_1b0c_effloosetide_50 = effloose_file_50['sf_1b0c_effloosetide_50'][:]
sf_2b0c_effloosetide_50 = effloose_file_50['sf_2b0c_effloosetide_50'][:]
sf_1b1c_effloosetide_50 = effloose_file_50['sf_1b1c_effloosetide_50'][:]
err_sf_0b0c_effloosetide_50 = effloose_file_50['err_sf_0b0c_effloosetide_50'][:]
err_sf_0b1c_effloosetide_50 = effloose_file_50['err_sf_0b1c_effloosetide_50'][:]
err_sf_0b2c_effloosetide_50 = effloose_file_50['err_sf_0b2c_effloosetide_50'][:]
err_sf_1b0c_effloosetide_50 = effloose_file_50['err_sf_1b0c_effloosetide_50'][:]
err_sf_2b0c_effloosetide_50 = effloose_file_50['err_sf_2b0c_effloosetide_50'][:]
err_sf_1b1c_effloosetide_50 = effloose_file_50['err_sf_1b1c_effloosetide_50'][:]


#TRK_EFF_RATE_LOOSE_TIDE
sf_0b0c_effrateloosetide_50 = effrate_file_50['sf_0b0c_effrateloosetide_50'][:]
sf_0b1c_effrateloosetide_50 = effrate_file_50['sf_0b1c_effrateloosetide_50'][:]
sf_0b2c_effrateloosetide_50 = effrate_file_50['sf_0b2c_effrateloosetide_50'][:]
sf_1b0c_effrateloosetide_50 = effrate_file_50['sf_1b0c_effrateloosetide_50'][:]
sf_2b0c_effrateloosetide_50 = effrate_file_50['sf_2b0c_effrateloosetide_50'][:]
sf_1b1c_effrateloosetide_50 = effrate_file_50['sf_1b1c_effrateloosetide_50'][:]

err_sf_0b0c_effrateloosetide_50 = effrate_file_50['err_sf_0b0c_effrateloosetide_50'][:]
err_sf_0b1c_effrateloosetide_50 = effrate_file_50['err_sf_0b1c_effrateloosetide_50'][:]
err_sf_0b2c_effrateloosetide_50 = effrate_file_50['err_sf_0b2c_effrateloosetide_50'][:]
err_sf_1b0c_effrateloosetide_50 = effrate_file_50['err_sf_1b0c_effrateloosetide_50'][:]
err_sf_2b0c_effrateloosetide_50 = effrate_file_50['err_sf_2b0c_effrateloosetide_50'][:]
err_sf_1b1c_effrateloosetide_50 = effrate_file_50['err_sf_1b1c_effrateloosetide_50'][:]

#TRK_EFF_RATE_LOOSE_ROBUST
sf_0b0c_effratelooserob_50 = effrate_file_50['sf_0b0c_effratelooserob_50'][:]
sf_0b1c_effratelooserob_50 = effrate_file_50['sf_0b1c_effratelooserob_50'][:]
sf_0b2c_effratelooserob_50 = effrate_file_50['sf_0b2c_effratelooserob_50'][:]
sf_1b0c_effratelooserob_50 = effrate_file_50['sf_1b0c_effratelooserob_50'][:]
sf_2b0c_effratelooserob_50 = effrate_file_50['sf_2b0c_effratelooserob_50'][:]
sf_1b1c_effratelooserob_50 = effrate_file_50['sf_1b1c_effratelooserob_50'][:]

err_sf_0b0c_effratelooserob_50 = effrate_file_50['err_sf_0b0c_effratelooserob_50'][:]
err_sf_0b1c_effratelooserob_50 = effrate_file_50['err_sf_0b1c_effratelooserob_50'][:]
err_sf_0b2c_effratelooserob_50 = effrate_file_50['err_sf_0b2c_effratelooserob_50'][:]
err_sf_1b0c_effratelooserob_50 = effrate_file_50['err_sf_1b0c_effratelooserob_50'][:]
err_sf_2b0c_effratelooserob_50 = effrate_file_50['err_sf_2b0c_effratelooserob_50'][:]
err_sf_1b1c_effratelooserob_50 = effrate_file_50['err_sf_1b1c_effratelooserob_50'][:]

#TRK_FAKE_RATE_LOOSE
sf_0b0c_fakerateloose_50 = fakerate_file_50['sf_0b0c_fakerateloose_50'][:]
sf_0b1c_fakerateloose_50 = fakerate_file_50['sf_0b1c_fakerateloose_50'][:]
sf_0b2c_fakerateloose_50 = fakerate_file_50['sf_0b2c_fakerateloose_50'][:]
sf_1b0c_fakerateloose_50 = fakerate_file_50['sf_1b0c_fakerateloose_50'][:]
sf_2b0c_fakerateloose_50 = fakerate_file_50['sf_2b0c_fakerateloose_50'][:]
sf_1b1c_fakerateloose_50 = fakerate_file_50['sf_1b1c_fakerateloose_50'][:]

err_sf_0b0c_fakerateloose_50 = fakerate_file_50['err_sf_0b0c_fakerateloose_50'][:]
err_sf_0b1c_fakerateloose_50 = fakerate_file_50['err_sf_0b1c_fakerateloose_50'][:]
err_sf_0b2c_fakerateloose_50 = fakerate_file_50['err_sf_0b2c_fakerateloose_50'][:]
err_sf_1b0c_fakerateloose_50 = fakerate_file_50['err_sf_1b0c_fakerateloose_50'][:]
err_sf_2b0c_fakerateloose_50 = fakerate_file_50['err_sf_2b0c_fakerateloose_50'][:]
err_sf_1b1c_fakerateloose_50 = fakerate_file_50['err_sf_1b1c_fakerateloose_50'][:]


pt_values = np.asarray([250,300, 350, 450 ,600,800,1000,1500])
pt_bins = (pt_values[1:] + pt_values[:-1])/2
pt_bin_width = pt_values[1:] - pt_values[:-1]

# compute final scale factors for each category 
# cental value is the multiplication of trkd0, trkz0 and fakerateloose sfs

sf_0b0c_50 = sf_0b0c_trkd0_50*sf_0b0c_trkz0_50*sf_0b0c_fakerateloose_50
sf_0b1c_50 = sf_0b1c_trkd0_50*sf_0b1c_trkz0_50*sf_0b1c_fakerateloose_50
sf_0b2c_50 = sf_0b2c_trkd0_50*sf_0b2c_trkz0_50*sf_0b2c_fakerateloose_50
sf_1b0c_50 = sf_1b0c_trkd0_50*sf_1b0c_trkz0_50*sf_1b0c_fakerateloose_50
sf_2b0c_50 = sf_2b0c_trkd0_50*sf_2b0c_trkz0_50*sf_2b0c_fakerateloose_50
sf_1b1c_50 = sf_1b1c_trkd0_50*sf_1b1c_trkz0_50*sf_1b1c_fakerateloose_50



# all the others comes in as systematic uncertainties 

delta_sf_0b0c_trkd0up_50 = 1 - sf_0b0c_trkd0up_50
delta_sf_0b1c_trkd0up_50 = 1 - sf_0b1c_trkd0up_50
delta_sf_0b2c_trkd0up_50 = 1 - sf_0b2c_trkd0up_50
delta_sf_1b0c_trkd0up_50 = 1 - sf_1b0c_trkd0up_50
delta_sf_2b0c_trkd0up_50 = 1 - sf_2b0c_trkd0up_50
delta_sf_1b1c_trkd0up_50 = 1 - sf_1b1c_trkd0up_50

delta_sf_0b0c_trkd0down_50 = 1 - sf_0b0c_trkd0down_50
delta_sf_0b1c_trkd0down_50 = 1 - sf_0b1c_trkd0down_50
delta_sf_0b2c_trkd0down_50 = 1 - sf_0b2c_trkd0down_50
delta_sf_1b0c_trkd0down_50 = 1 - sf_1b0c_trkd0down_50
delta_sf_2b0c_trkd0down_50 = 1 - sf_2b0c_trkd0down_50
delta_sf_1b1c_trkd0down_50 = 1 - sf_1b1c_trkd0down_50

delta_sf_0b0c_trkd0dead_50 = 1 - sf_0b0c_trkd0dead_50
delta_sf_0b1c_trkd0dead_50 = 1 - sf_0b1c_trkd0dead_50
delta_sf_0b2c_trkd0dead_50 = 1 - sf_0b2c_trkd0dead_50
delta_sf_1b0c_trkd0dead_50 = 1 - sf_1b0c_trkd0dead_50
delta_sf_2b0c_trkd0dead_50 = 1 - sf_2b0c_trkd0dead_50
delta_sf_1b1c_trkd0dead_50 = 1 - sf_1b1c_trkd0dead_50

delta_sf_0b0c_trkz0up_50 = 1 - sf_0b0c_trkz0up_50
delta_sf_0b1c_trkz0up_50 = 1 - sf_0b1c_trkz0up_50
delta_sf_0b2c_trkz0up_50 = 1 - sf_0b2c_trkz0up_50
delta_sf_1b0c_trkz0up_50 = 1 - sf_1b0c_trkz0up_50
delta_sf_2b0c_trkz0up_50 = 1 - sf_2b0c_trkz0up_50
delta_sf_1b1c_trkz0up_50 = 1 - sf_1b1c_trkz0up_50

delta_sf_0b0c_trkz0down_50 = 1 - sf_0b0c_trkz0down_50
delta_sf_0b1c_trkz0down_50 = 1 - sf_0b1c_trkz0down_50
delta_sf_0b2c_trkz0down_50 = 1 - sf_0b2c_trkz0down_50
delta_sf_1b0c_trkz0down_50 = 1 - sf_1b0c_trkz0down_50
delta_sf_2b0c_trkz0down_50 = 1 - sf_2b0c_trkz0down_50
delta_sf_1b1c_trkz0down_50 = 1 - sf_1b1c_trkz0down_50

delta_sf_0b0c_trkz0dead_50 = 1 - sf_0b0c_trkz0dead_50
delta_sf_0b1c_trkz0dead_50 = 1 - sf_0b1c_trkz0dead_50
delta_sf_0b2c_trkz0dead_50 = 1 - sf_0b2c_trkz0dead_50
delta_sf_1b0c_trkz0dead_50 = 1 - sf_1b0c_trkz0dead_50
delta_sf_2b0c_trkz0dead_50 = 1 - sf_2b0c_trkz0dead_50
delta_sf_1b1c_trkz0dead_50 = 1 - sf_1b1c_trkz0dead_50

delta_sf_0b0c_efflooseglob_50 = 1 - sf_0b0c_efflooseglob_50
delta_sf_0b1c_efflooseglob_50 = 1 - sf_0b1c_efflooseglob_50
delta_sf_0b2c_efflooseglob_50 = 1 - sf_0b2c_efflooseglob_50
delta_sf_1b0c_efflooseglob_50 = 1 - sf_1b0c_efflooseglob_50
delta_sf_2b0c_efflooseglob_50 = 1 - sf_2b0c_efflooseglob_50
delta_sf_1b1c_efflooseglob_50 = 1 - sf_1b1c_efflooseglob_50

delta_sf_0b0c_efflooseibl_50 = 1 - sf_0b0c_efflooseibl_50
delta_sf_0b1c_efflooseibl_50 = 1 - sf_0b1c_efflooseibl_50
delta_sf_0b2c_efflooseibl_50 = 1 - sf_0b2c_efflooseibl_50
delta_sf_1b0c_efflooseibl_50 = 1 - sf_1b0c_efflooseibl_50
delta_sf_2b0c_efflooseibl_50 = 1 - sf_2b0c_efflooseibl_50
delta_sf_1b1c_efflooseibl_50 = 1 - sf_1b1c_efflooseibl_50

delta_sf_0b0c_effloosetide_50 = 1 - sf_0b0c_effloosetide_50
delta_sf_0b1c_effloosetide_50 = 1 - sf_0b1c_effloosetide_50
delta_sf_0b2c_effloosetide_50 = 1 - sf_0b2c_effloosetide_50
delta_sf_1b0c_effloosetide_50 = 1 - sf_1b0c_effloosetide_50
delta_sf_2b0c_effloosetide_50 = 1 - sf_2b0c_effloosetide_50
delta_sf_1b1c_effloosetide_50 = 1 - sf_1b1c_effloosetide_50

delta_sf_0b0c_effrateloosetide_50 = 1 - sf_0b0c_effrateloosetide_50
delta_sf_0b1c_effrateloosetide_50 = 1 - sf_0b1c_effrateloosetide_50
delta_sf_0b2c_effrateloosetide_50 = 1 - sf_0b2c_effrateloosetide_50
delta_sf_1b0c_effrateloosetide_50 = 1 - sf_1b0c_effrateloosetide_50
delta_sf_2b0c_effrateloosetide_50 = 1 - sf_2b0c_effrateloosetide_50
delta_sf_1b1c_effrateloosetide_50 = 1 - sf_1b1c_effrateloosetide_50

delta_sf_0b0c_effratelooserob_50 = 1 - sf_0b0c_effratelooserob_50
delta_sf_0b1c_effratelooserob_50 = 1 - sf_0b1c_effratelooserob_50
delta_sf_0b2c_effratelooserob_50 = 1 - sf_0b2c_effratelooserob_50
delta_sf_1b0c_effratelooserob_50 = 1 - sf_1b0c_effratelooserob_50
delta_sf_2b0c_effratelooserob_50 = 1 - sf_2b0c_effratelooserob_50
delta_sf_1b1c_effratelooserob_50 = 1 - sf_1b1c_effratelooserob_50

delta_sf_0b0c_fakerateloose_50 = 1 - sf_0b0c_fakerateloose_50
delta_sf_0b1c_fakerateloose_50 = 1 - sf_0b1c_fakerateloose_50
delta_sf_0b2c_fakerateloose_50 = 1 - sf_0b2c_fakerateloose_50
delta_sf_1b0c_fakerateloose_50 = 1 - sf_1b0c_fakerateloose_50
delta_sf_2b0c_fakerateloose_50 = 1 - sf_2b0c_fakerateloose_50
delta_sf_1b1c_fakerateloose_50 = 1 - sf_1b1c_fakerateloose_50

# compute the total systematic uncertainty for each category

err_sf_0b0c_50 = np.sqrt(delta_sf_0b0c_trkd0up_50**2 + delta_sf_0b0c_trkd0down_50**2 + delta_sf_0b0c_trkd0dead_50**2 + delta_sf_0b0c_trkz0up_50**2 + delta_sf_0b0c_trkz0down_50**2 + delta_sf_0b0c_trkz0dead_50**2 + delta_sf_0b0c_efflooseglob_50**2 + delta_sf_0b0c_efflooseibl_50**2 + delta_sf_0b0c_effloosetide_50**2 + delta_sf_0b0c_effrateloosetide_50**2 + delta_sf_0b0c_effratelooserob_50**2 + delta_sf_0b0c_fakerateloose_50**2)
err_sf_0b1c_50 = np.sqrt(delta_sf_0b1c_trkd0up_50**2 + delta_sf_0b1c_trkd0down_50**2 + delta_sf_0b1c_trkd0dead_50**2 + delta_sf_0b1c_trkz0up_50**2 + delta_sf_0b1c_trkz0down_50**2 + delta_sf_0b1c_trkz0dead_50**2 + delta_sf_0b1c_efflooseglob_50**2 + delta_sf_0b1c_efflooseibl_50**2 + delta_sf_0b1c_effloosetide_50**2 + delta_sf_0b1c_effrateloosetide_50**2 + delta_sf_0b1c_effratelooserob_50**2 + delta_sf_0b1c_fakerateloose_50**2)
err_sf_0b2c_50 = np.sqrt(delta_sf_0b2c_trkd0up_50**2 + delta_sf_0b2c_trkd0down_50**2 + delta_sf_0b2c_trkd0dead_50**2 + delta_sf_0b2c_trkz0up_50**2 + delta_sf_0b2c_trkz0down_50**2 + delta_sf_0b2c_trkz0dead_50**2 + delta_sf_0b2c_efflooseglob_50**2 + delta_sf_0b2c_efflooseibl_50**2 + delta_sf_0b2c_effloosetide_50**2 + delta_sf_0b2c_effrateloosetide_50**2 + delta_sf_0b2c_effratelooserob_50**2 + delta_sf_0b2c_fakerateloose_50**2)
err_sf_1b0c_50 = np.sqrt(delta_sf_1b0c_trkd0up_50**2 + delta_sf_1b0c_trkd0down_50**2 + delta_sf_1b0c_trkd0dead_50**2 + delta_sf_1b0c_trkz0up_50**2 + delta_sf_1b0c_trkz0down_50**2 + delta_sf_1b0c_trkz0dead_50**2 + delta_sf_1b0c_efflooseglob_50**2 + delta_sf_1b0c_efflooseibl_50**2 + delta_sf_1b0c_effloosetide_50**2 + delta_sf_1b0c_effrateloosetide_50**2 + delta_sf_1b0c_effratelooserob_50**2 + delta_sf_1b0c_fakerateloose_50**2)
err_sf_2b0c_50 = np.sqrt(delta_sf_2b0c_trkd0up_50**2 + delta_sf_2b0c_trkd0down_50**2 + delta_sf_2b0c_trkd0dead_50**2 + delta_sf_2b0c_trkz0up_50**2 + delta_sf_2b0c_trkz0down_50**2 + delta_sf_2b0c_trkz0dead_50**2 + delta_sf_2b0c_efflooseglob_50**2 + delta_sf_2b0c_efflooseibl_50**2 + delta_sf_2b0c_effloosetide_50**2 + delta_sf_2b0c_effrateloosetide_50**2 + delta_sf_2b0c_effratelooserob_50**2 + delta_sf_2b0c_fakerateloose_50**2)
err_sf_1b1c_50 = np.sqrt(delta_sf_1b1c_trkd0up_50**2 + delta_sf_1b1c_trkd0down_50**2 + delta_sf_1b1c_trkd0dead_50**2 + delta_sf_1b1c_trkz0up_50**2 + delta_sf_1b1c_trkz0down_50**2 + delta_sf_1b1c_trkz0dead_50**2 + delta_sf_1b1c_efflooseglob_50**2 + delta_sf_1b1c_efflooseibl_50**2 + delta_sf_1b1c_effloosetide_50**2 + delta_sf_1b1c_effrateloosetide_50**2 + delta_sf_1b1c_effratelooserob_50**2 + delta_sf_1b1c_fakerateloose_50**2)


# plot final sf values for each category in a different plot 
# adding on each point the central value and the error value

fig = plt.figure(figsize=(20, 18))

plt.errorbar(pt_bins, sf_0b0c_50, xerr = pt_bin_width/2, yerr=err_sf_0b0c_50, fmt='o', label='0b + 0c')
plt.errorbar(pt_bins, sf_0b1c_50, xerr = pt_bin_width/2, yerr=err_sf_0b1c_50, fmt='o', label='0b + >=1c')
plt.errorbar(pt_bins, sf_0b2c_50, xerr = pt_bin_width/2, yerr=err_sf_0b2c_50, fmt='o', label='0b + >=2c')
plt.errorbar(pt_bins, sf_1b0c_50, xerr = pt_bin_width/2, yerr=err_sf_1b0c_50, fmt='o', label='1b + 0c')
plt.errorbar(pt_bins, sf_2b0c_50, xerr = pt_bin_width/2, yerr=err_sf_2b0c_50, fmt='o', label='>=2b + 0c')
plt.errorbar(pt_bins, sf_1b1c_50, xerr = pt_bin_width/2, yerr=err_sf_1b1c_50, fmt='o', label='1b + >=1c')

plt.text(0.02, 0.97, 'ATLAS', fontsize=20, fontweight='bold', fontstyle='italic', color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.09, 0.97, 'Simulation Preliminary', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.95, r'$\sqrt{s}$ = 13 TeV, Anti-$k_t$ R = 1.0 UFO jets', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.92, r'$\Delta$R = 1.0 track-jet association', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.90, r'GN2X Scores, QCD Samples, 50% H($b\bar{b}$) WP', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)


plt.xlabel(r'$p_T$ [GeV]',  ha = 'right', fontsize=18)
plt.ylabel('Scale Factor',  ha = 'right', fontsize=18)
plt.legend(fontsize=18)
plt.grid(True)


plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.ylim(0.4, 1.9)


#add text on the plot with central value and error value
for i in range(len(pt_bins)):
    plt.text(pt_bins[i], sf_0b0c_50[i], str(round(sf_0b0c_50[i],2)) + '+/-' + str(round(err_sf_0b0c_50[i],2)), fontsize=14)
    plt.text(pt_bins[i], sf_0b1c_50[i], str(round(sf_0b1c_50[i],2)) + '+/-' + str(round(err_sf_0b1c_50[i],2)), fontsize=14)
    plt.text(pt_bins[i], sf_0b2c_50[i], str(round(sf_0b2c_50[i],2)) + '+/-' + str(round(err_sf_0b2c_50[i],2)), fontsize=14)
    plt.text(pt_bins[i], sf_1b0c_50[i], str(round(sf_1b0c_50[i],2)) + '+/-' + str(round(err_sf_1b0c_50[i],2)), fontsize=14)
    plt.text(pt_bins[i], sf_2b0c_50[i], str(round(sf_2b0c_50[i],2)) + '+/-' + str(round(err_sf_2b0c_50[i],2)), fontsize=14)
    plt.text(pt_bins[i], sf_1b1c_50[i], str(round(sf_1b1c_50[i],2)) + '+/-' + str(round(err_sf_1b1c_50[i],2)), fontsize=14)
   

plt.savefig('final_sf_50.png', dpi=1000)




bias_file_60 = h5py.File('sf_bias60.h5', 'r')
effloose_file_60 = h5py.File('sf_effloose60.h5', 'r')
effrate_file_60 = h5py.File('sf_effrate60.h5', 'r')
fakerate_file_60 = h5py.File('sf_fakerate60.h5', 'r')
trkd0_file_60 = h5py.File('sf_trkd060.h5', 'r')
trkz0_file_60 = h5py.File('sf_trkz060.h5', 'r')
dead_file_60 = h5py.File('sf_dead60.h5', 'r')

#TRK_D0
sf_0b0c_trkd0_60 = trkd0_file_60['sf_0b0c_trkd0_50'][:]
sf_0b1c_trkd0_60 = trkd0_file_60['sf_0b1c_trkd0_50'][:]
sf_0b2c_trkd0_60 = trkd0_file_60['sf_0b2c_trkd0_50'][:]
sf_1b0c_trkd0_60 = trkd0_file_60['sf_1b0c_trkd0_50'][:]
sf_2b0c_trkd0_60 = trkd0_file_60['sf_2b0c_trkd0_50'][:]
sf_1b1c_trkd0_60 = trkd0_file_60['sf_1b1c_trkd0_50'][:]

err_sf_0b0c_trkd0_60 = trkd0_file_60['err_sf_0b0c_trkd0_50'][:]
err_sf_0b1c_trkd0_60 = trkd0_file_60['err_sf_0b1c_trkd0_50'][:]
err_sf_0b2c_trkd0_60 = trkd0_file_60['err_sf_0b2c_trkd0_50'][:]
err_sf_1b0c_trkd0_60 = trkd0_file_60['err_sf_1b0c_trkd0_50'][:]
err_sf_2b0c_trkd0_60 = trkd0_file_60['err_sf_2b0c_trkd0_50'][:]
err_sf_1b1c_trkd0_60 = trkd0_file_60['err_sf_1b1c_trkd0_50'][:]

#TRK_D0_UP
sf_0b0c_trkd0up_60 = trkd0_file_60['sf_0b0c_trkd0up_50'][:]
sf_0b1c_trkd0up_60 = trkd0_file_60['sf_0b1c_trkd0up_50'][:]
sf_0b2c_trkd0up_60 = trkd0_file_60['sf_0b2c_trkd0up_50'][:]
sf_1b0c_trkd0up_60 = trkd0_file_60['sf_1b0c_trkd0up_50'][:]
sf_2b0c_trkd0up_60 = trkd0_file_60['sf_2b0c_trkd0up_50'][:]
sf_1b1c_trkd0up_60 = trkd0_file_60['sf_1b1c_trkd0up_50'][:]

err_sf_0b0c_trkd0up_60 = trkd0_file_60['err_sf_0b0c_trkd0up_50'][:]
err_sf_0b1c_trkd0up_60 = trkd0_file_60['err_sf_0b1c_trkd0up_50'][:]
err_sf_0b2c_trkd0up_60 = trkd0_file_60['err_sf_0b2c_trkd0up_50'][:]
err_sf_1b0c_trkd0up_60 = trkd0_file_60['err_sf_1b0c_trkd0up_50'][:]
err_sf_2b0c_trkd0up_60 = trkd0_file_60['err_sf_2b0c_trkd0up_50'][:]
err_sf_1b1c_trkd0up_60 = trkd0_file_60['err_sf_1b1c_trkd0up_50'][:]

#TRK_D0_DOWN
sf_0b0c_trkd0down_60 = trkd0_file_60['sf_0b0c_trkd0down_50'][:]
sf_0b1c_trkd0down_60 = trkd0_file_60['sf_0b1c_trkd0down_50'][:]
sf_0b2c_trkd0down_60 = trkd0_file_60['sf_0b2c_trkd0down_50'][:]
sf_1b0c_trkd0down_60 = trkd0_file_60['sf_1b0c_trkd0down_50'][:]
sf_2b0c_trkd0down_60 = trkd0_file_60['sf_2b0c_trkd0down_50'][:]
sf_1b1c_trkd0down_60 = trkd0_file_60['sf_1b1c_trkd0down_50'][:]

err_sf_0b0c_trkd0down_60 = trkd0_file_60['err_sf_0b0c_trkd0down_50'][:]
err_sf_0b1c_trkd0down_60 = trkd0_file_60['err_sf_0b1c_trkd0down_50'][:]
err_sf_0b2c_trkd0down_60 = trkd0_file_60['err_sf_0b2c_trkd0down_50'][:]
err_sf_1b0c_trkd0down_60 = trkd0_file_60['err_sf_1b0c_trkd0down_50'][:]
err_sf_2b0c_trkd0down_60 = trkd0_file_60['err_sf_2b0c_trkd0down_50'][:]
err_sf_1b1c_trkd0down_60 = trkd0_file_60['err_sf_1b1c_trkd0down_50'][:]

#TRK_D0_BIAS
sf_0b0c_trkd0bias_60 = bias_file_60['sf_0b0c_trkd0bias_50'][:]
sf_0b1c_trkd0bias_60 = bias_file_60['sf_0b1c_trkd0bias_50'][:]
sf_0b2c_trkd0bias_60 = bias_file_60['sf_0b2c_trkd0bias_50'][:]
sf_1b0c_trkd0bias_60 = bias_file_60['sf_1b0c_trkd0bias_50'][:]
sf_2b0c_trkd0bias_60 = bias_file_60['sf_2b0c_trkd0bias_50'][:]
sf_1b1c_trkd0bias_60 = bias_file_60['sf_1b1c_trkd0bias_50'][:]

err_sf_0b0c_trkd0bias_60 = bias_file_60['err_sf_0b0c_trkd0bias_50'][:]
err_sf_0b1c_trkd0bias_60 = bias_file_60['err_sf_0b1c_trkd0bias_50'][:]
err_sf_0b2c_trkd0bias_60 = bias_file_60['err_sf_0b2c_trkd0bias_50'][:]
err_sf_1b0c_trkd0bias_60 = bias_file_60['err_sf_1b0c_trkd0bias_50'][:]
err_sf_2b0c_trkd0bias_60 = bias_file_60['err_sf_2b0c_trkd0bias_50'][:]
err_sf_1b1c_trkd0bias_60 = bias_file_60['err_sf_1b1c_trkd0bias_50'][:]

#QOVERP
sf_0b0c_qoverp_60 = bias_file_60['sf_0b0c_qoverp_50'][:]
sf_0b1c_qoverp_60 = bias_file_60['sf_0b1c_qoverp_50'][:]
sf_0b2c_qoverp_60 = bias_file_60['sf_0b2c_qoverp_50'][:]
sf_1b0c_qoverp_60 = bias_file_60['sf_1b0c_qoverp_50'][:]
sf_2b0c_qoverp_60 = bias_file_60['sf_2b0c_qoverp_50'][:]
sf_1b1c_qoverp_60 = bias_file_60['sf_1b1c_qoverp_50'][:]
err_sf_0b0c_qoverp_60 = bias_file_60['err_sf_0b0c_qoverp_50'][:]
err_sf_0b1c_qoverp_60 = bias_file_60['err_sf_0b1c_qoverp_50'][:]
err_sf_0b2c_qoverp_60 = bias_file_60['err_sf_0b2c_qoverp_50'][:]
err_sf_1b0c_qoverp_60 = bias_file_60['err_sf_1b0c_qoverp_50'][:]
err_sf_2b0c_qoverp_60 = bias_file_60['err_sf_2b0c_qoverp_50'][:]
err_sf_1b1c_qoverp_60 = bias_file_60['err_sf_1b1c_qoverp_50'][:]



#TRK_Z0
sf_0b0c_trkz0_60 = trkz0_file_60['sf_0b0c_trkz0_50'][:]
sf_0b1c_trkz0_60 = trkz0_file_60['sf_0b1c_trkz0_50'][:]
sf_0b2c_trkz0_60 = trkz0_file_60['sf_0b2c_trkz0_50'][:]
sf_1b0c_trkz0_60 = trkz0_file_60['sf_1b0c_trkz0_50'][:]
sf_2b0c_trkz0_60 = trkz0_file_60['sf_2b0c_trkz0_50'][:]
sf_1b1c_trkz0_60 = trkz0_file_60['sf_1b1c_trkz0_50'][:]

err_sf_0b0c_trkz0_60 = trkz0_file_60['err_sf_0b0c_trkz0_50'][:]
err_sf_0b1c_trkz0_60 = trkz0_file_60['err_sf_0b1c_trkz0_50'][:]
err_sf_0b2c_trkz0_60 = trkz0_file_60['err_sf_0b2c_trkz0_50'][:]
err_sf_1b0c_trkz0_60 = trkz0_file_60['err_sf_1b0c_trkz0_50'][:]
err_sf_2b0c_trkz0_60 = trkz0_file_60['err_sf_2b0c_trkz0_50'][:]
err_sf_1b1c_trkz0_60 = trkz0_file_60['err_sf_1b1c_trkz0_50'][:]

#TRK_Z0_UP
sf_0b0c_trkz0up_60 = trkz0_file_60['sf_0b0c_trkz0up_50'][:]
sf_0b1c_trkz0up_60 = trkz0_file_60['sf_0b1c_trkz0up_50'][:]
sf_0b2c_trkz0up_60 = trkz0_file_60['sf_0b2c_trkz0up_50'][:]
sf_1b0c_trkz0up_60 = trkz0_file_60['sf_1b0c_trkz0up_50'][:]
sf_2b0c_trkz0up_60 = trkz0_file_60['sf_2b0c_trkz0up_50'][:]
sf_1b1c_trkz0up_60 = trkz0_file_60['sf_1b1c_trkz0up_50'][:]

err_sf_0b0c_trkz0up_60 = trkz0_file_60['err_sf_0b0c_trkz0up_50'][:]
err_sf_0b1c_trkz0up_60 = trkz0_file_60['err_sf_0b1c_trkz0up_50'][:]
err_sf_0b2c_trkz0up_60 = trkz0_file_60['err_sf_0b2c_trkz0up_50'][:]
err_sf_1b0c_trkz0up_60 = trkz0_file_60['err_sf_1b0c_trkz0up_50'][:]
err_sf_2b0c_trkz0up_60 = trkz0_file_60['err_sf_2b0c_trkz0up_50'][:]
err_sf_1b1c_trkz0up_60 = trkz0_file_60['err_sf_1b1c_trkz0up_50'][:]

#TRK_Z0_DOWN
sf_0b0c_trkz0down_60 = trkz0_file_60['sf_0b0c_trkz0down_50'][:]
sf_0b1c_trkz0down_60 = trkz0_file_60['sf_0b1c_trkz0down_50'][:]
sf_0b2c_trkz0down_60 = trkz0_file_60['sf_0b2c_trkz0down_50'][:]
sf_1b0c_trkz0down_60 = trkz0_file_60['sf_1b0c_trkz0down_50'][:]
sf_2b0c_trkz0down_60 = trkz0_file_60['sf_2b0c_trkz0down_50'][:]
sf_1b1c_trkz0down_60 = trkz0_file_60['sf_1b1c_trkz0down_50'][:]

err_sf_0b0c_trkz0down_60 = trkz0_file_60['err_sf_0b0c_trkz0down_50'][:]
err_sf_0b1c_trkz0down_60 = trkz0_file_60['err_sf_0b1c_trkz0down_50'][:]
err_sf_0b2c_trkz0down_60 = trkz0_file_60['err_sf_0b2c_trkz0down_50'][:]
err_sf_1b0c_trkz0down_60 = trkz0_file_60['err_sf_1b0c_trkz0down_50'][:]
err_sf_2b0c_trkz0down_60 = trkz0_file_60['err_sf_2b0c_trkz0down_50'][:]
err_sf_1b1c_trkz0down_60 = trkz0_file_60['err_sf_1b1c_trkz0down_50'][:]



#TRK_D0_DEAD
sf_0b0c_trkd0dead_60 = dead_file_60['sf_0b0c_trkd0dead_50'][:]
sf_0b1c_trkd0dead_60 = dead_file_60['sf_0b1c_trkd0dead_50'][:]
sf_0b2c_trkd0dead_60 = dead_file_60['sf_0b2c_trkd0dead_50'][:]
sf_1b0c_trkd0dead_60 = dead_file_60['sf_1b0c_trkd0dead_50'][:]
sf_2b0c_trkd0dead_60 = dead_file_60['sf_2b0c_trkd0dead_50'][:]
sf_1b1c_trkd0dead_60 = dead_file_60['sf_1b1c_trkd0dead_50'][:]

err_sf_0b0c_trkd0dead_60 = dead_file_60['err_sf_0b0c_trkd0dead_50'][:]
err_sf_0b1c_trkd0dead_60 = dead_file_60['err_sf_0b1c_trkd0dead_50'][:]
err_sf_0b2c_trkd0dead_60 = dead_file_60['err_sf_0b2c_trkd0dead_50'][:]
err_sf_1b0c_trkd0dead_60 = dead_file_60['err_sf_1b0c_trkd0dead_50'][:]
err_sf_2b0c_trkd0dead_60 = dead_file_60['err_sf_2b0c_trkd0dead_50'][:]
err_sf_1b1c_trkd0dead_60 = dead_file_60['err_sf_1b1c_trkd0dead_50'][:]

#TRK_Z0_DEAD
sf_0b0c_trkz0dead_60 = dead_file_60['sf_0b0c_trkz0dead_50'][:]
sf_0b1c_trkz0dead_60 = dead_file_60['sf_0b1c_trkz0dead_50'][:]
sf_0b2c_trkz0dead_60 = dead_file_60['sf_0b2c_trkz0dead_50'][:]
sf_1b0c_trkz0dead_60 = dead_file_60['sf_1b0c_trkz0dead_50'][:]
sf_2b0c_trkz0dead_60 = dead_file_60['sf_2b0c_trkz0dead_50'][:]
sf_1b1c_trkz0dead_60 = dead_file_60['sf_1b1c_trkz0dead_50'][:]

err_sf_0b0c_trkz0dead_60 = dead_file_60['err_sf_0b0c_trkz0dead_50'][:]
err_sf_0b1c_trkz0dead_60 = dead_file_60['err_sf_0b1c_trkz0dead_50'][:]
err_sf_0b2c_trkz0dead_60 = dead_file_60['err_sf_0b2c_trkz0dead_50'][:]
err_sf_1b0c_trkz0dead_60 = dead_file_60['err_sf_1b0c_trkz0dead_50'][:]
err_sf_2b0c_trkz0dead_60 = dead_file_60['err_sf_2b0c_trkz0dead_50'][:]
err_sf_1b1c_trkz0dead_60 = dead_file_60['err_sf_1b1c_trkz0dead_50'][:]

#TRK_EFF_LOOSE_GLOB
sf_0b0c_efflooseglob_60 = effloose_file_60['sf_0b0c_efflooseglob_50'][:]
sf_0b1c_efflooseglob_60 = effloose_file_60['sf_0b1c_efflooseglob_50'][:]
sf_0b2c_efflooseglob_60 = effloose_file_60['sf_0b2c_efflooseglob_50'][:]
sf_1b0c_efflooseglob_60 = effloose_file_60['sf_1b0c_efflooseglob_50'][:]
sf_2b0c_efflooseglob_60 = effloose_file_60['sf_2b0c_efflooseglob_50'][:]
sf_1b1c_efflooseglob_60 = effloose_file_60['sf_1b1c_efflooseglob_50'][:]

err_sf_0b0c_efflooseglob_60 = effloose_file_60['err_sf_0b0c_efflooseglob_50'][:]
err_sf_0b1c_efflooseglob_60 = effloose_file_60['err_sf_0b1c_efflooseglob_50'][:]
err_sf_0b2c_efflooseglob_60 = effloose_file_60['err_sf_0b2c_efflooseglob_50'][:]
err_sf_1b0c_efflooseglob_60 = effloose_file_60['err_sf_1b0c_efflooseglob_50'][:]
err_sf_2b0c_efflooseglob_60 = effloose_file_60['err_sf_2b0c_efflooseglob_50'][:]
err_sf_1b1c_efflooseglob_60 = effloose_file_60['err_sf_1b1c_efflooseglob_50'][:]

#TRK_EFF_LOOSE_IBL
sf_0b0c_efflooseibl_60 = effloose_file_60['sf_0b0c_efflooseibl_50'][:]
sf_0b1c_efflooseibl_60 = effloose_file_60['sf_0b1c_efflooseibl_50'][:]
sf_0b2c_efflooseibl_60 = effloose_file_60['sf_0b2c_efflooseibl_50'][:]
sf_1b0c_efflooseibl_60 = effloose_file_60['sf_1b0c_efflooseibl_50'][:]
sf_2b0c_efflooseibl_60 = effloose_file_60['sf_2b0c_efflooseibl_50'][:]
sf_1b1c_efflooseibl_60 = effloose_file_60['sf_1b1c_efflooseibl_50'][:]

err_sf_0b0c_efflooseibl_60 = effloose_file_60['err_sf_0b0c_efflooseibl_50'][:]
err_sf_0b1c_efflooseibl_60 = effloose_file_60['err_sf_0b1c_efflooseibl_50'][:]
err_sf_0b2c_efflooseibl_60 = effloose_file_60['err_sf_0b2c_efflooseibl_50'][:]
err_sf_1b0c_efflooseibl_60 = effloose_file_60['err_sf_1b0c_efflooseibl_50'][:]
err_sf_2b0c_efflooseibl_60 = effloose_file_60['err_sf_2b0c_efflooseibl_50'][:]
err_sf_1b1c_efflooseibl_60 = effloose_file_60['err_sf_1b1c_efflooseibl_50'][:]

#TRK_EFF_LOOSE_TIDE
sf_0b0c_effloosetide_60 = effloose_file_60['sf_0b0c_effloosetide_50'][:]
sf_0b1c_effloosetide_60 = effloose_file_60['sf_0b1c_effloosetide_50'][:]
sf_0b2c_effloosetide_60 = effloose_file_60['sf_0b2c_effloosetide_50'][:]
sf_1b0c_effloosetide_60 = effloose_file_60['sf_1b0c_effloosetide_50'][:]
sf_2b0c_effloosetide_60 = effloose_file_60['sf_2b0c_effloosetide_50'][:]
sf_1b1c_effloosetide_60 = effloose_file_60['sf_1b1c_effloosetide_50'][:]

err_sf_0b0c_effloosetide_60 = effloose_file_60['err_sf_0b0c_effloosetide_50'][:]
err_sf_0b1c_effloosetide_60 = effloose_file_60['err_sf_0b1c_effloosetide_50'][:]
err_sf_0b2c_effloosetide_60 = effloose_file_60['err_sf_0b2c_effloosetide_50'][:]
err_sf_1b0c_effloosetide_60 = effloose_file_60['err_sf_1b0c_effloosetide_50'][:]
err_sf_2b0c_effloosetide_60 = effloose_file_60['err_sf_2b0c_effloosetide_50'][:]
err_sf_1b1c_effloosetide_60 = effloose_file_60['err_sf_1b1c_effloosetide_50'][:]

#TRK_EFF_RATE_LOOSE_TIDE
sf_0b0c_effrateloosetide_60 = effrate_file_60['sf_0b0c_effrateloosetide_50'][:]
sf_0b1c_effrateloosetide_60 = effrate_file_60['sf_0b1c_effrateloosetide_50'][:]
sf_0b2c_effrateloosetide_60 = effrate_file_60['sf_0b2c_effrateloosetide_50'][:]
sf_1b0c_effrateloosetide_60 = effrate_file_60['sf_1b0c_effrateloosetide_50'][:]
sf_2b0c_effrateloosetide_60 = effrate_file_60['sf_2b0c_effrateloosetide_50'][:]
sf_1b1c_effrateloosetide_60 = effrate_file_60['sf_1b1c_effrateloosetide_50'][:]

err_sf_0b0c_effrateloosetide_60 = effrate_file_60['err_sf_0b0c_effrateloosetide_50'][:]
err_sf_0b1c_effrateloosetide_60 = effrate_file_60['err_sf_0b1c_effrateloosetide_50'][:]
err_sf_0b2c_effrateloosetide_60 = effrate_file_60['err_sf_0b2c_effrateloosetide_50'][:]
err_sf_1b0c_effrateloosetide_60 = effrate_file_60['err_sf_1b0c_effrateloosetide_50'][:]
err_sf_2b0c_effrateloosetide_60 = effrate_file_60['err_sf_2b0c_effrateloosetide_50'][:]
err_sf_1b1c_effrateloosetide_60 = effrate_file_60['err_sf_1b1c_effrateloosetide_50'][:]

#TRK_EFF_RATE_LOOSE_ROB
sf_0b0c_effratelooserob_60 = effrate_file_60['sf_0b0c_effratelooserob_50'][:]
sf_0b1c_effratelooserob_60 = effrate_file_60['sf_0b1c_effratelooserob_50'][:]
sf_0b2c_effratelooserob_60 = effrate_file_60['sf_0b2c_effratelooserob_50'][:]
sf_1b0c_effratelooserob_60 = effrate_file_60['sf_1b0c_effratelooserob_50'][:]
sf_2b0c_effratelooserob_60 = effrate_file_60['sf_2b0c_effratelooserob_50'][:]
sf_1b1c_effratelooserob_60 = effrate_file_60['sf_1b1c_effratelooserob_50'][:]

err_sf_0b0c_effratelooserob_60 = effrate_file_60['err_sf_0b0c_effratelooserob_50'][:]
err_sf_0b1c_effratelooserob_60 = effrate_file_60['err_sf_0b1c_effratelooserob_50'][:]
err_sf_0b2c_effratelooserob_60 = effrate_file_60['err_sf_0b2c_effratelooserob_50'][:]
err_sf_1b0c_effratelooserob_60 = effrate_file_60['err_sf_1b0c_effratelooserob_50'][:]
err_sf_2b0c_effratelooserob_60 = effrate_file_60['err_sf_2b0c_effratelooserob_50'][:]
err_sf_1b1c_effratelooserob_60 = effrate_file_60['err_sf_1b1c_effratelooserob_50'][:]

#FAKE_RATE_LOOSE
sf_0b0c_fakerateloose_60 = fakerate_file_60['sf_0b0c_fakerateloose_50'][:]
sf_0b1c_fakerateloose_60 = fakerate_file_60['sf_0b1c_fakerateloose_50'][:]
sf_0b2c_fakerateloose_60 = fakerate_file_60['sf_0b2c_fakerateloose_50'][:]
sf_1b0c_fakerateloose_60 = fakerate_file_60['sf_1b0c_fakerateloose_50'][:]
sf_2b0c_fakerateloose_60 = fakerate_file_60['sf_2b0c_fakerateloose_50'][:]
sf_1b1c_fakerateloose_60 = fakerate_file_60['sf_1b1c_fakerateloose_50'][:]
err_sf_0b0c_fakerateloose_60 = fakerate_file_60['err_sf_0b0c_fakerateloose_50'][:]
err_sf_0b1c_fakerateloose_60 = fakerate_file_60['err_sf_0b1c_fakerateloose_50'][:]
err_sf_0b2c_fakerateloose_60 = fakerate_file_60['err_sf_0b2c_fakerateloose_50'][:]
err_sf_1b0c_fakerateloose_60 = fakerate_file_60['err_sf_1b0c_fakerateloose_50'][:]
err_sf_2b0c_fakerateloose_60 = fakerate_file_60['err_sf_2b0c_fakerateloose_50'][:]
err_sf_1b1c_fakerateloose_60 = fakerate_file_60['err_sf_1b1c_fakerateloose_50'][:]

## compute final scale factors for each category 
# cental value is the multiplication of trkd0, trkz0 and fakerateloose sfs

sf_0b0c_60 = sf_0b0c_trkd0_60*sf_0b0c_trkz0_60*sf_0b0c_fakerateloose_60
sf_0b1c_60 = sf_0b1c_trkd0_60*sf_0b1c_trkz0_60*sf_0b1c_fakerateloose_60
sf_0b2c_60 = sf_0b2c_trkd0_60*sf_0b2c_trkz0_60*sf_0b2c_fakerateloose_60
sf_1b0c_60 = sf_1b0c_trkd0_60*sf_1b0c_trkz0_60*sf_1b0c_fakerateloose_60
sf_2b0c_60 = sf_2b0c_trkd0_60*sf_2b0c_trkz0_60*sf_2b0c_fakerateloose_60
sf_1b1c_60 = sf_1b1c_trkd0_60*sf_1b1c_trkz0_60*sf_1b1c_fakerateloose_60

# all the others comes in as systematic uncertainties 

delta_sf_0b0c_trkd0up_60 = 1 - sf_0b0c_trkd0up_60
delta_sf_0b1c_trkd0up_60 = 1 - sf_0b1c_trkd0up_60
delta_sf_0b2c_trkd0up_60 = 1 - sf_0b2c_trkd0up_60
delta_sf_1b0c_trkd0up_60 = 1 - sf_1b0c_trkd0up_60
delta_sf_2b0c_trkd0up_60 = 1 - sf_2b0c_trkd0up_60
delta_sf_1b1c_trkd0up_60 = 1 - sf_1b1c_trkd0up_60

delta_sf_0b0c_trkd0down_60 = 1 - sf_0b0c_trkd0down_60
delta_sf_0b1c_trkd0down_60 = 1 - sf_0b1c_trkd0down_60
delta_sf_0b2c_trkd0down_60 = 1 - sf_0b2c_trkd0down_60
delta_sf_1b0c_trkd0down_60 = 1 - sf_1b0c_trkd0down_60
delta_sf_2b0c_trkd0down_60 = 1 - sf_2b0c_trkd0down_60
delta_sf_1b1c_trkd0down_60 = 1 - sf_1b1c_trkd0down_60

delta_sf_0b0c_trkd0dead_60 = 1 - sf_0b0c_trkd0dead_60
delta_sf_0b1c_trkd0dead_60 = 1 - sf_0b1c_trkd0dead_60
delta_sf_0b2c_trkd0dead_60 = 1 - sf_0b2c_trkd0dead_60
delta_sf_1b0c_trkd0dead_60 = 1 - sf_1b0c_trkd0dead_60
delta_sf_2b0c_trkd0dead_60 = 1 - sf_2b0c_trkd0dead_60
delta_sf_1b1c_trkd0dead_60 = 1 - sf_1b1c_trkd0dead_60

delta_sf_0b0c_trkz0up_60 = 1 - sf_0b0c_trkz0up_60
delta_sf_0b1c_trkz0up_60 = 1 - sf_0b1c_trkz0up_60
delta_sf_0b2c_trkz0up_60 = 1 - sf_0b2c_trkz0up_60
delta_sf_1b0c_trkz0up_60 = 1 - sf_1b0c_trkz0up_60
delta_sf_2b0c_trkz0up_60 = 1 - sf_2b0c_trkz0up_60
delta_sf_1b1c_trkz0up_60 = 1 - sf_1b1c_trkz0up_60

delta_sf_0b0c_trkz0down_60 = 1 - sf_0b0c_trkz0down_60
delta_sf_0b1c_trkz0down_60 = 1 - sf_0b1c_trkz0down_60
delta_sf_0b2c_trkz0down_60 = 1 - sf_0b2c_trkz0down_60
delta_sf_1b0c_trkz0down_60 = 1 - sf_1b0c_trkz0down_60
delta_sf_2b0c_trkz0down_60 = 1 - sf_2b0c_trkz0down_60
delta_sf_1b1c_trkz0down_60 = 1 - sf_1b1c_trkz0down_60

delta_sf_0b0c_trkz0dead_60 = 1 - sf_0b0c_trkz0dead_60
delta_sf_0b1c_trkz0dead_60 = 1 - sf_0b1c_trkz0dead_60
delta_sf_0b2c_trkz0dead_60 = 1 - sf_0b2c_trkz0dead_60
delta_sf_1b0c_trkz0dead_60 = 1 - sf_1b0c_trkz0dead_60
delta_sf_2b0c_trkz0dead_60 = 1 - sf_2b0c_trkz0dead_60
delta_sf_1b1c_trkz0dead_60 = 1 - sf_1b1c_trkz0dead_60

delta_sf_0b0c_efflooseglob_60 = 1 - sf_0b0c_efflooseglob_60
delta_sf_0b1c_efflooseglob_60 = 1 - sf_0b1c_efflooseglob_60
delta_sf_0b2c_efflooseglob_60 = 1 - sf_0b2c_efflooseglob_60
delta_sf_1b0c_efflooseglob_60 = 1 - sf_1b0c_efflooseglob_60
delta_sf_2b0c_efflooseglob_60 = 1 - sf_2b0c_efflooseglob_60
delta_sf_1b1c_efflooseglob_60 = 1 - sf_1b1c_efflooseglob_60

delta_sf_0b0c_efflooseibl_60 = 1 - sf_0b0c_efflooseibl_60
delta_sf_0b1c_efflooseibl_60 = 1 - sf_0b1c_efflooseibl_60
delta_sf_0b2c_efflooseibl_60 = 1 - sf_0b2c_efflooseibl_60
delta_sf_1b0c_efflooseibl_60 = 1 - sf_1b0c_efflooseibl_60
delta_sf_2b0c_efflooseibl_60 = 1 - sf_2b0c_efflooseibl_60
delta_sf_1b1c_efflooseibl_60 = 1 - sf_1b1c_efflooseibl_60

delta_sf_0b0c_effloosetide_60 = 1 - sf_0b0c_effloosetide_60
delta_sf_0b1c_effloosetide_60 = 1 - sf_0b1c_effloosetide_60
delta_sf_0b2c_effloosetide_60 = 1 - sf_0b2c_effloosetide_60
delta_sf_1b0c_effloosetide_60 = 1 - sf_1b0c_effloosetide_60
delta_sf_2b0c_effloosetide_60 = 1 - sf_2b0c_effloosetide_60
delta_sf_1b1c_effloosetide_60 = 1 - sf_1b1c_effloosetide_60

delta_sf_0b0c_effrateloosetide_60 = 1 - sf_0b0c_effrateloosetide_60
delta_sf_0b1c_effrateloosetide_60 = 1 - sf_0b1c_effrateloosetide_60
delta_sf_0b2c_effrateloosetide_60 = 1 - sf_0b2c_effrateloosetide_60
delta_sf_1b0c_effrateloosetide_60 = 1 - sf_1b0c_effrateloosetide_60
delta_sf_2b0c_effrateloosetide_60 = 1 - sf_2b0c_effrateloosetide_60
delta_sf_1b1c_effrateloosetide_60 = 1 - sf_1b1c_effrateloosetide_60

delta_sf_0b0c_effratelooserob_60 = 1 - sf_0b0c_effratelooserob_60
delta_sf_0b1c_effratelooserob_60 = 1 - sf_0b1c_effratelooserob_60
delta_sf_0b2c_effratelooserob_60 = 1 - sf_0b2c_effratelooserob_60
delta_sf_1b0c_effratelooserob_60 = 1 - sf_1b0c_effratelooserob_60
delta_sf_2b0c_effratelooserob_60 = 1 - sf_2b0c_effratelooserob_60
delta_sf_1b1c_effratelooserob_60 = 1 - sf_1b1c_effratelooserob_60

delta_sf_0b0c_fakerateloose_60 = 1 - sf_0b0c_fakerateloose_60
delta_sf_0b1c_fakerateloose_60 = 1 - sf_0b1c_fakerateloose_60
delta_sf_0b2c_fakerateloose_60 = 1 - sf_0b2c_fakerateloose_60
delta_sf_1b0c_fakerateloose_60 = 1 - sf_1b0c_fakerateloose_60
delta_sf_2b0c_fakerateloose_60 = 1 - sf_2b0c_fakerateloose_60
delta_sf_1b1c_fakerateloose_60 = 1 - sf_1b1c_fakerateloose_60

delta_sf_0b0c_qoverp_60 = 1 - sf_0b0c_qoverp_60
delta_sf_0b1c_qoverp_60 = 1 - sf_0b1c_qoverp_60
delta_sf_0b2c_qoverp_60 = 1 - sf_0b2c_qoverp_60
delta_sf_1b0c_qoverp_60 = 1 - sf_1b0c_qoverp_60
delta_sf_2b0c_qoverp_60 = 1 - sf_2b0c_qoverp_60
delta_sf_1b1c_qoverp_60 = 1 - sf_1b1c_qoverp_60


# compute the total systematic uncertainty for each category

err_sf_0b0c_60 = np.sqrt(delta_sf_0b0c_qoverp_60**2 + delta_sf_0b0c_trkd0up_60**2 + delta_sf_0b0c_trkd0down_60**2 + delta_sf_0b0c_trkd0dead_60**2 + delta_sf_0b0c_trkz0up_60**2 + delta_sf_0b0c_trkz0down_60**2 + delta_sf_0b0c_trkz0dead_60**2 + delta_sf_0b0c_efflooseglob_60**2 + delta_sf_0b0c_efflooseibl_60**2 + delta_sf_0b0c_effloosetide_60**2 + delta_sf_0b0c_effrateloosetide_60**2 + delta_sf_0b0c_effratelooserob_60**2 + delta_sf_0b0c_fakerateloose_60**2)
err_sf_0b1c_60 = np.sqrt(delta_sf_0b1c_qoverp_60**2 + delta_sf_0b1c_trkd0up_60**2 + delta_sf_0b1c_trkd0down_60**2 + delta_sf_0b1c_trkd0dead_60**2 + delta_sf_0b1c_trkz0up_60**2 + delta_sf_0b1c_trkz0down_60**2 + delta_sf_0b1c_trkz0dead_60**2 + delta_sf_0b1c_efflooseglob_60**2 + delta_sf_0b1c_efflooseibl_60**2 + delta_sf_0b1c_effloosetide_60**2 + delta_sf_0b1c_effrateloosetide_60**2 + delta_sf_0b1c_effratelooserob_60**2 + delta_sf_0b1c_fakerateloose_60**2)
err_sf_0b2c_60 = np.sqrt(delta_sf_0b2c_qoverp_60**2 + delta_sf_0b2c_trkd0up_60**2 + delta_sf_0b2c_trkd0down_60**2 + delta_sf_0b2c_trkd0dead_60**2 + delta_sf_0b2c_trkz0up_60**2 + delta_sf_0b2c_trkz0down_60**2 + delta_sf_0b2c_trkz0dead_60**2 + delta_sf_0b2c_efflooseglob_60**2 + delta_sf_0b2c_efflooseibl_60**2 + delta_sf_0b2c_effloosetide_60**2 + delta_sf_0b2c_effrateloosetide_60**2 + delta_sf_0b2c_effratelooserob_60**2 + delta_sf_0b2c_fakerateloose_60**2)
err_sf_1b0c_60 = np.sqrt(delta_sf_1b0c_qoverp_60**2 + delta_sf_1b0c_trkd0up_60**2 + delta_sf_1b0c_trkd0down_60**2 + delta_sf_1b0c_trkd0dead_60**2 + delta_sf_1b0c_trkz0up_60**2 + delta_sf_1b0c_trkz0down_60**2 + delta_sf_1b0c_trkz0dead_60**2 + delta_sf_1b0c_efflooseglob_60**2 + delta_sf_1b0c_efflooseibl_60**2 + delta_sf_1b0c_effloosetide_60**2 + delta_sf_1b0c_effrateloosetide_60**2 + delta_sf_1b0c_effratelooserob_60**2 + delta_sf_1b0c_fakerateloose_60**2)
err_sf_2b0c_60 = np.sqrt(delta_sf_2b0c_qoverp_60**2 + delta_sf_2b0c_trkd0up_60**2 + delta_sf_2b0c_trkd0down_60**2 + delta_sf_2b0c_trkd0dead_60**2 + delta_sf_2b0c_trkz0up_60**2 + delta_sf_2b0c_trkz0down_60**2 + delta_sf_2b0c_trkz0dead_60**2 + delta_sf_2b0c_efflooseglob_60**2 + delta_sf_2b0c_efflooseibl_60**2 + delta_sf_2b0c_effloosetide_60**2 + delta_sf_2b0c_effrateloosetide_60**2 + delta_sf_2b0c_effratelooserob_60**2 + delta_sf_2b0c_fakerateloose_60**2)
err_sf_1b1c_60 = np.sqrt(delta_sf_1b1c_qoverp_60**2 + delta_sf_1b1c_trkd0up_60**2 + delta_sf_1b1c_trkd0down_60**2 + delta_sf_1b1c_trkd0dead_60**2 + delta_sf_1b1c_trkz0up_60**2 + delta_sf_1b1c_trkz0down_60**2 + delta_sf_1b1c_trkz0dead_60**2 + delta_sf_1b1c_efflooseglob_60**2 + delta_sf_1b1c_efflooseibl_60**2 + delta_sf_1b1c_effloosetide_60**2 + delta_sf_1b1c_effrateloosetide_60**2 + delta_sf_1b1c_effratelooserob_60**2 + delta_sf_1b1c_fakerateloose_60**2)

# plot

fig = plt.figure(figsize=(20, 18))

plt.errorbar(pt_bins, sf_0b0c_60, xerr = pt_bin_width/2, yerr=err_sf_0b0c_60, fmt='o', label='0b + 0c')
plt.errorbar(pt_bins, sf_0b1c_60, xerr = pt_bin_width/2, yerr=err_sf_0b1c_60, fmt='o', label='0b + >=1c')
plt.errorbar(pt_bins, sf_0b2c_60, xerr = pt_bin_width/2, yerr=err_sf_0b2c_60, fmt='o', label='0b + >=2c')
plt.errorbar(pt_bins, sf_1b0c_60, xerr = pt_bin_width/2, yerr=err_sf_1b0c_60, fmt='o', label='1b + 0c')
plt.errorbar(pt_bins, sf_2b0c_60, xerr = pt_bin_width/2, yerr=err_sf_2b0c_60, fmt='o', label='>=2b + 0c')
plt.errorbar(pt_bins, sf_1b1c_60, xerr = pt_bin_width/2, yerr=err_sf_1b1c_60, fmt='o', label='1b + >=1c')

plt.text(0.02, 0.97, 'ATLAS', fontsize=20, fontweight='bold', fontstyle='italic', color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.09, 0.97, 'Simulation Preliminary', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.95, r'$\sqrt{s}$ = 13 TeV, Anti-$k_t$ R = 1.0 UFO jets', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.92, r'$\Delta$R = 1.0 track-jet association', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.90, r'GN2X Scores, QCD Samples, 60% H($b\bar{b}$) WP', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)


plt.xlabel(r'$p_T$ [GeV]',  ha = 'right', fontsize=18)
plt.ylabel('Scale Factor',  ha = 'right', fontsize=18)
plt.legend(fontsize=18)
plt.grid(True)


plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.ylim(0.4, 1.9)


#add text on the plot with central value and error value
for i in range(len(pt_bins)):
 plt.text(pt_bins[i], sf_0b0c_60[i], str(round(sf_0b0c_60[i],2)) + '+/-' + str(round(err_sf_0b0c_60[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_0b1c_60[i], str(round(sf_0b1c_60[i],2)) + '+/-' + str(round(err_sf_0b1c_60[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_0b2c_60[i], str(round(sf_0b2c_60[i],2)) + '+/-' + str(round(err_sf_0b2c_60[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_1b0c_60[i], str(round(sf_1b0c_60[i],2)) + '+/-' + str(round(err_sf_1b0c_60[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_2b0c_60[i], str(round(sf_2b0c_60[i],2)) + '+/-' + str(round(err_sf_2b0c_60[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_1b1c_60[i], str(round(sf_1b1c_60[i],2)) + '+/-' + str(round(err_sf_1b1c_60[i],2)), fontsize=14)
   

plt.savefig('final_sf_60.png', dpi=1000)

bias_file_70 = h5py.File('sf_bias70.h5', 'r')
effloose_file_70 = h5py.File('sf_effloose70.h5', 'r')
effrate_file_70 = h5py.File('sf_effrate70.h5', 'r')
fakerate_file_70 = h5py.File('sf_fakerate70.h5', 'r')
trkd0_file_70 = h5py.File('sf_trkd070.h5', 'r')
trkz0_file_70 = h5py.File('sf_trkz070.h5', 'r')
dead_file_70 = h5py.File('sf_dead70.h5', 'r')

#TRK_D0
sf_0b0c_trkd0_70 = trkd0_file_70['sf_0b0c_trkd0_50'][:]
sf_0b1c_trkd0_70 = trkd0_file_70['sf_0b1c_trkd0_50'][:]
sf_0b2c_trkd0_70 = trkd0_file_70['sf_0b2c_trkd0_50'][:]
sf_1b0c_trkd0_70 = trkd0_file_70['sf_1b0c_trkd0_50'][:]
sf_2b0c_trkd0_70 = trkd0_file_70['sf_2b0c_trkd0_50'][:]
sf_1b1c_trkd0_70 = trkd0_file_70['sf_1b1c_trkd0_50'][:]

err_sf_0b0c_trkd0_70 = trkd0_file_70['err_sf_0b0c_trkd0_50'][:]
err_sf_0b1c_trkd0_70 = trkd0_file_70['err_sf_0b1c_trkd0_50'][:]
err_sf_0b2c_trkd0_70 = trkd0_file_70['err_sf_0b2c_trkd0_50'][:]
err_sf_1b0c_trkd0_70 = trkd0_file_70['err_sf_1b0c_trkd0_50'][:]
err_sf_2b0c_trkd0_70 = trkd0_file_70['err_sf_2b0c_trkd0_50'][:]
err_sf_1b1c_trkd0_70 = trkd0_file_70['err_sf_1b1c_trkd0_50'][:]

#TRK_D0_UP
sf_0b0c_trkd0up_70 = trkd0_file_70['sf_0b0c_trkd0up_50'][:]
sf_0b1c_trkd0up_70 = trkd0_file_70['sf_0b1c_trkd0up_50'][:]
sf_0b2c_trkd0up_70 = trkd0_file_70['sf_0b2c_trkd0up_50'][:]
sf_1b0c_trkd0up_70 = trkd0_file_70['sf_1b0c_trkd0up_50'][:]
sf_2b0c_trkd0up_70 = trkd0_file_70['sf_2b0c_trkd0up_50'][:]
sf_1b1c_trkd0up_70 = trkd0_file_70['sf_1b1c_trkd0up_50'][:]

err_sf_0b0c_trkd0up_70 = trkd0_file_70['err_sf_0b0c_trkd0up_50'][:]
err_sf_0b1c_trkd0up_70 = trkd0_file_70['err_sf_0b1c_trkd0up_50'][:]
err_sf_0b2c_trkd0up_70 = trkd0_file_70['err_sf_0b2c_trkd0up_50'][:]
err_sf_1b0c_trkd0up_70 = trkd0_file_70['err_sf_1b0c_trkd0up_50'][:]
err_sf_2b0c_trkd0up_70 = trkd0_file_70['err_sf_2b0c_trkd0up_50'][:]
err_sf_1b1c_trkd0up_70 = trkd0_file_70['err_sf_1b1c_trkd0up_50'][:]

#TRK_D0_DOWN
sf_0b0c_trkd0down_70 = trkd0_file_70['sf_0b0c_trkd0down_50'][:]
sf_0b1c_trkd0down_70 = trkd0_file_70['sf_0b1c_trkd0down_50'][:]
sf_0b2c_trkd0down_70 = trkd0_file_70['sf_0b2c_trkd0down_50'][:]
sf_1b0c_trkd0down_70 = trkd0_file_70['sf_1b0c_trkd0down_50'][:]
sf_2b0c_trkd0down_70 = trkd0_file_70['sf_2b0c_trkd0down_50'][:]
sf_1b1c_trkd0down_70 = trkd0_file_70['sf_1b1c_trkd0down_50'][:]

err_sf_0b0c_trkd0down_70 = trkd0_file_70['err_sf_0b0c_trkd0down_50'][:]
err_sf_0b1c_trkd0down_70 = trkd0_file_70['err_sf_0b1c_trkd0down_50'][:]
err_sf_0b2c_trkd0down_70 = trkd0_file_70['err_sf_0b2c_trkd0down_50'][:]
err_sf_1b0c_trkd0down_70 = trkd0_file_70['err_sf_1b0c_trkd0down_50'][:]
err_sf_2b0c_trkd0down_70 = trkd0_file_70['err_sf_2b0c_trkd0down_50'][:]
err_sf_1b1c_trkd0down_70 = trkd0_file_70['err_sf_1b1c_trkd0down_50'][:]

#TRK_D0_BIAS
sf_0b0c_trkd0bias_70 = bias_file_70['sf_0b0c_trkd0bias_50'][:]
sf_0b1c_trkd0bias_70 = bias_file_70['sf_0b1c_trkd0bias_50'][:]
sf_0b2c_trkd0bias_70 = bias_file_70['sf_0b2c_trkd0bias_50'][:]
sf_1b0c_trkd0bias_70 = bias_file_70['sf_1b0c_trkd0bias_50'][:]
sf_2b0c_trkd0bias_70 = bias_file_70['sf_2b0c_trkd0bias_50'][:]
sf_1b1c_trkd0bias_70 = bias_file_70['sf_1b1c_trkd0bias_50'][:]

err_sf_0b0c_trkd0bias_70 = bias_file_70['err_sf_0b0c_trkd0bias_50'][:]
err_sf_0b1c_trkd0bias_70 = bias_file_70['err_sf_0b1c_trkd0bias_50'][:]
err_sf_0b2c_trkd0bias_70 = bias_file_70['err_sf_0b2c_trkd0bias_50'][:]
err_sf_1b0c_trkd0bias_70 = bias_file_70['err_sf_1b0c_trkd0bias_50'][:]
err_sf_2b0c_trkd0bias_70 = bias_file_70['err_sf_2b0c_trkd0bias_50'][:]
err_sf_1b1c_trkd0bias_70 = bias_file_70['err_sf_1b1c_trkd0bias_50'][:]


#TRK_Z0
sf_0b0c_trkz0_70 = trkz0_file_70['sf_0b0c_trkz0_50'][:]
sf_0b1c_trkz0_70 = trkz0_file_70['sf_0b1c_trkz0_50'][:]
sf_0b2c_trkz0_70 = trkz0_file_70['sf_0b2c_trkz0_50'][:]
sf_1b0c_trkz0_70 = trkz0_file_70['sf_1b0c_trkz0_50'][:]
sf_2b0c_trkz0_70 = trkz0_file_70['sf_2b0c_trkz0_50'][:]
sf_1b1c_trkz0_70 = trkz0_file_70['sf_1b1c_trkz0_50'][:]

err_sf_0b0c_trkz0_70 = trkz0_file_70['err_sf_0b0c_trkz0_50'][:]
err_sf_0b1c_trkz0_70 = trkz0_file_70['err_sf_0b1c_trkz0_50'][:]
err_sf_0b2c_trkz0_70 = trkz0_file_70['err_sf_0b2c_trkz0_50'][:]
err_sf_1b0c_trkz0_70 = trkz0_file_70['err_sf_1b0c_trkz0_50'][:]
err_sf_2b0c_trkz0_70 = trkz0_file_70['err_sf_2b0c_trkz0_50'][:]
err_sf_1b1c_trkz0_70 = trkz0_file_70['err_sf_1b1c_trkz0_50'][:]

#TRK_Z0_UP
sf_0b0c_trkz0up_70 = trkz0_file_70['sf_0b0c_trkz0up_50'][:]
sf_0b1c_trkz0up_70 = trkz0_file_70['sf_0b1c_trkz0up_50'][:]
sf_0b2c_trkz0up_70 = trkz0_file_70['sf_0b2c_trkz0up_50'][:]
sf_1b0c_trkz0up_70 = trkz0_file_70['sf_1b0c_trkz0up_50'][:]
sf_2b0c_trkz0up_70 = trkz0_file_70['sf_2b0c_trkz0up_50'][:]
sf_1b1c_trkz0up_70 = trkz0_file_70['sf_1b1c_trkz0up_50'][:]

err_sf_0b0c_trkz0up_70 = trkz0_file_70['err_sf_0b0c_trkz0up_50'][:]
err_sf_0b1c_trkz0up_70 = trkz0_file_70['err_sf_0b1c_trkz0up_50'][:]
err_sf_0b2c_trkz0up_70 = trkz0_file_70['err_sf_0b2c_trkz0up_50'][:]
err_sf_1b0c_trkz0up_70 = trkz0_file_70['err_sf_1b0c_trkz0up_50'][:]
err_sf_2b0c_trkz0up_70 = trkz0_file_70['err_sf_2b0c_trkz0up_50'][:]
err_sf_1b1c_trkz0up_70 = trkz0_file_70['err_sf_1b1c_trkz0up_50'][:]

#TRK_Z0_DOWN
sf_0b0c_trkz0down_70 = trkz0_file_70['sf_0b0c_trkz0down_50'][:]
sf_0b1c_trkz0down_70 = trkz0_file_70['sf_0b1c_trkz0down_50'][:]
sf_0b2c_trkz0down_70 = trkz0_file_70['sf_0b2c_trkz0down_50'][:]
sf_1b0c_trkz0down_70 = trkz0_file_70['sf_1b0c_trkz0down_50'][:]
sf_2b0c_trkz0down_70 = trkz0_file_70['sf_2b0c_trkz0down_50'][:]
sf_1b1c_trkz0down_70 = trkz0_file_70['sf_1b1c_trkz0down_50'][:]

err_sf_0b0c_trkz0down_70 = trkz0_file_70['err_sf_0b0c_trkz0down_50'][:]
err_sf_0b1c_trkz0down_70 = trkz0_file_70['err_sf_0b1c_trkz0down_50'][:]
err_sf_0b2c_trkz0down_70 = trkz0_file_70['err_sf_0b2c_trkz0down_50'][:]
err_sf_1b0c_trkz0down_70 = trkz0_file_70['err_sf_1b0c_trkz0down_50'][:]
err_sf_2b0c_trkz0down_70 = trkz0_file_70['err_sf_2b0c_trkz0down_50'][:]
err_sf_1b1c_trkz0down_70 = trkz0_file_70['err_sf_1b1c_trkz0down_50'][:]




#TRK_D0_DEAD
sf_0b0c_trkd0dead_70 = dead_file_70['sf_0b0c_trkd0dead_50'][:]
sf_0b1c_trkd0dead_70 = dead_file_70['sf_0b1c_trkd0dead_50'][:]
sf_0b2c_trkd0dead_70 = dead_file_70['sf_0b2c_trkd0dead_50'][:]
sf_1b0c_trkd0dead_70 = dead_file_70['sf_1b0c_trkd0dead_50'][:]
sf_2b0c_trkd0dead_70 = dead_file_70['sf_2b0c_trkd0dead_50'][:]
sf_1b1c_trkd0dead_70 = dead_file_70['sf_1b1c_trkd0dead_50'][:]

err_sf_0b0c_trkd0dead_70 = dead_file_70['err_sf_0b0c_trkd0dead_50'][:]
err_sf_0b1c_trkd0dead_70 = dead_file_70['err_sf_0b1c_trkd0dead_50'][:]
err_sf_0b2c_trkd0dead_70 = dead_file_70['err_sf_0b2c_trkd0dead_50'][:]
err_sf_1b0c_trkd0dead_70 = dead_file_70['err_sf_1b0c_trkd0dead_50'][:]
err_sf_2b0c_trkd0dead_70 = dead_file_70['err_sf_2b0c_trkd0dead_50'][:]
err_sf_1b1c_trkd0dead_70 = dead_file_70['err_sf_1b1c_trkd0dead_50'][:]

#TRK_Z0_DEAD
sf_0b0c_trkz0dead_70 = dead_file_70['sf_0b0c_trkz0dead_50'][:]
sf_0b1c_trkz0dead_70 = dead_file_70['sf_0b1c_trkz0dead_50'][:]
sf_0b2c_trkz0dead_70 = dead_file_70['sf_0b2c_trkz0dead_50'][:]
sf_1b0c_trkz0dead_70 = dead_file_70['sf_1b0c_trkz0dead_50'][:]
sf_2b0c_trkz0dead_70 = dead_file_70['sf_2b0c_trkz0dead_50'][:]
sf_1b1c_trkz0dead_70 = dead_file_70['sf_1b1c_trkz0dead_50'][:]

err_sf_0b0c_trkz0dead_70 = dead_file_70['err_sf_0b0c_trkz0dead_50'][:]
err_sf_0b1c_trkz0dead_70 = dead_file_70['err_sf_0b1c_trkz0dead_50'][:]
err_sf_0b2c_trkz0dead_70 = dead_file_70['err_sf_0b2c_trkz0dead_50'][:]
err_sf_1b0c_trkz0dead_70 = dead_file_70['err_sf_1b0c_trkz0dead_50'][:]
err_sf_2b0c_trkz0dead_70 = dead_file_70['err_sf_2b0c_trkz0dead_50'][:]
err_sf_1b1c_trkz0dead_70 = dead_file_70['err_sf_1b1c_trkz0dead_50'][:]

#QOVERP 
sf_0b0c_qoverp_70 = bias_file_70['sf_0b0c_qoverp_50'][:]
sf_0b1c_qoverp_70 = bias_file_70['sf_0b1c_qoverp_50'][:]
sf_0b2c_qoverp_70 = bias_file_70['sf_0b2c_qoverp_50'][:]
sf_1b0c_qoverp_70 = bias_file_70['sf_1b0c_qoverp_50'][:]
sf_2b0c_qoverp_70 = bias_file_70['sf_2b0c_qoverp_50'][:]
sf_1b1c_qoverp_70 = bias_file_70['sf_1b1c_qoverp_50'][:]

err_sf_0b0c_qoverp_70 = bias_file_70['err_sf_0b0c_qoverp_50'][:]
err_sf_0b1c_qoverp_70 = bias_file_70['err_sf_0b1c_qoverp_50'][:]
err_sf_0b2c_qoverp_70 = bias_file_70['err_sf_0b2c_qoverp_50'][:]
err_sf_1b0c_qoverp_70 = bias_file_70['err_sf_1b0c_qoverp_50'][:]
err_sf_2b0c_qoverp_70 = bias_file_70['err_sf_2b0c_qoverp_50'][:]
err_sf_1b1c_qoverp_70 = bias_file_70['err_sf_1b1c_qoverp_50'][:]

#TRK_EFF_LOOSE_GLOB
sf_0b0c_efflooseglob_70 = effloose_file_70['sf_0b0c_efflooseglob_50'][:]
sf_0b1c_efflooseglob_70 = effloose_file_70['sf_0b1c_efflooseglob_50'][:]
sf_0b2c_efflooseglob_70 = effloose_file_70['sf_0b2c_efflooseglob_50'][:]
sf_1b0c_efflooseglob_70 = effloose_file_70['sf_1b0c_efflooseglob_50'][:]
sf_2b0c_efflooseglob_70 = effloose_file_70['sf_2b0c_efflooseglob_50'][:]
sf_1b1c_efflooseglob_70 = effloose_file_70['sf_1b1c_efflooseglob_50'][:]

err_sf_0b0c_efflooseglob_70 = effloose_file_70['err_sf_0b0c_efflooseglob_50'][:]
err_sf_0b1c_efflooseglob_70 = effloose_file_70['err_sf_0b1c_efflooseglob_50'][:]
err_sf_0b2c_efflooseglob_70 = effloose_file_70['err_sf_0b2c_efflooseglob_50'][:]
err_sf_1b0c_efflooseglob_70 = effloose_file_70['err_sf_1b0c_efflooseglob_50'][:]
err_sf_2b0c_efflooseglob_70 = effloose_file_70['err_sf_2b0c_efflooseglob_50'][:]
err_sf_1b1c_efflooseglob_70 = effloose_file_70['err_sf_1b1c_efflooseglob_50'][:]

#TRK_EFF_LOOSE_IBL
sf_0b0c_efflooseibl_70 = effloose_file_70['sf_0b0c_efflooseibl_50'][:]
sf_0b1c_efflooseibl_70 = effloose_file_70['sf_0b1c_efflooseibl_50'][:]
sf_0b2c_efflooseibl_70 = effloose_file_70['sf_0b2c_efflooseibl_50'][:]
sf_1b0c_efflooseibl_70 = effloose_file_70['sf_1b0c_efflooseibl_50'][:]
sf_2b0c_efflooseibl_70 = effloose_file_70['sf_2b0c_efflooseibl_50'][:]
sf_1b1c_efflooseibl_70 = effloose_file_70['sf_1b1c_efflooseibl_50'][:]

err_sf_0b0c_efflooseibl_70 = effloose_file_70['err_sf_0b0c_efflooseibl_50'][:]
err_sf_0b1c_efflooseibl_70 = effloose_file_70['err_sf_0b1c_efflooseibl_50'][:]
err_sf_0b2c_efflooseibl_70 = effloose_file_70['err_sf_0b2c_efflooseibl_50'][:]
err_sf_1b0c_efflooseibl_70 = effloose_file_70['err_sf_1b0c_efflooseibl_50'][:]
err_sf_2b0c_efflooseibl_70 = effloose_file_70['err_sf_2b0c_efflooseibl_50'][:]
err_sf_1b1c_efflooseibl_70 = effloose_file_70['err_sf_1b1c_efflooseibl_50'][:]

#TRK_EFF_LOOSE_TIDE
sf_0b0c_effloosetide_70 = effloose_file_70['sf_0b0c_effloosetide_50'][:]
sf_0b1c_effloosetide_70 = effloose_file_70['sf_0b1c_effloosetide_50'][:]
sf_0b2c_effloosetide_70 = effloose_file_70['sf_0b2c_effloosetide_50'][:]
sf_1b0c_effloosetide_70 = effloose_file_70['sf_1b0c_effloosetide_50'][:]
sf_2b0c_effloosetide_70 = effloose_file_70['sf_2b0c_effloosetide_50'][:]
sf_1b1c_effloosetide_70 = effloose_file_70['sf_1b1c_effloosetide_50'][:]

err_sf_0b0c_effloosetide_70 = effloose_file_70['err_sf_0b0c_effloosetide_50'][:]
err_sf_0b1c_effloosetide_70 = effloose_file_70['err_sf_0b1c_effloosetide_50'][:]
err_sf_0b2c_effloosetide_70 = effloose_file_70['err_sf_0b2c_effloosetide_50'][:]
err_sf_1b0c_effloosetide_70 = effloose_file_70['err_sf_1b0c_effloosetide_50'][:]
err_sf_2b0c_effloosetide_70 = effloose_file_70['err_sf_2b0c_effloosetide_50'][:]
err_sf_1b1c_effloosetide_70 = effloose_file_70['err_sf_1b1c_effloosetide_50'][:]

#TRK_EFF_RATE_LOOSE_TIDE
sf_0b0c_effrateloosetide_70 = effrate_file_70['sf_0b0c_effrateloosetide_50'][:]
sf_0b1c_effrateloosetide_70 = effrate_file_70['sf_0b1c_effrateloosetide_50'][:]
sf_0b2c_effrateloosetide_70 = effrate_file_70['sf_0b2c_effrateloosetide_50'][:]
sf_1b0c_effrateloosetide_70 = effrate_file_70['sf_1b0c_effrateloosetide_50'][:]
sf_2b0c_effrateloosetide_70 = effrate_file_70['sf_2b0c_effrateloosetide_50'][:]
sf_1b1c_effrateloosetide_70 = effrate_file_70['sf_1b1c_effrateloosetide_50'][:]

err_sf_0b0c_effrateloosetide_70 = effrate_file_70['err_sf_0b0c_effrateloosetide_50'][:]
err_sf_0b1c_effrateloosetide_70 = effrate_file_70['err_sf_0b1c_effrateloosetide_50'][:]
err_sf_0b2c_effrateloosetide_70 = effrate_file_70['err_sf_0b2c_effrateloosetide_50'][:]
err_sf_1b0c_effrateloosetide_70 = effrate_file_70['err_sf_1b0c_effrateloosetide_50'][:]
err_sf_2b0c_effrateloosetide_70 = effrate_file_70['err_sf_2b0c_effrateloosetide_50'][:]
err_sf_1b1c_effrateloosetide_70 = effrate_file_70['err_sf_1b1c_effrateloosetide_50'][:]

#TRK_EFF_RATE_LOOSE_ROB
sf_0b0c_effratelooserob_70 = effrate_file_70['sf_0b0c_effratelooserob_50'][:]
sf_0b1c_effratelooserob_70 = effrate_file_70['sf_0b1c_effratelooserob_50'][:]
sf_0b2c_effratelooserob_70 = effrate_file_70['sf_0b2c_effratelooserob_50'][:]
sf_1b0c_effratelooserob_70 = effrate_file_70['sf_1b0c_effratelooserob_50'][:]
sf_2b0c_effratelooserob_70 = effrate_file_70['sf_2b0c_effratelooserob_50'][:]
sf_1b1c_effratelooserob_70 = effrate_file_70['sf_1b1c_effratelooserob_50'][:]

err_sf_0b0c_effratelooserob_70 = effrate_file_70['err_sf_0b0c_effratelooserob_50'][:]
err_sf_0b1c_effratelooserob_70 = effrate_file_70['err_sf_0b1c_effratelooserob_50'][:]
err_sf_0b2c_effratelooserob_70 = effrate_file_70['err_sf_0b2c_effratelooserob_50'][:]
err_sf_1b0c_effratelooserob_70 = effrate_file_70['err_sf_1b0c_effratelooserob_50'][:]
err_sf_2b0c_effratelooserob_70 = effrate_file_70['err_sf_2b0c_effratelooserob_50'][:]
err_sf_1b1c_effratelooserob_70 = effrate_file_70['err_sf_1b1c_effratelooserob_50'][:]

#FAKE_RATE_LOOSE
sf_0b0c_fakerateloose_70 = fakerate_file_70['sf_0b0c_fakerateloose_50'][:]
sf_0b1c_fakerateloose_70 = fakerate_file_70['sf_0b1c_fakerateloose_50'][:]
sf_0b2c_fakerateloose_70 = fakerate_file_70['sf_0b2c_fakerateloose_50'][:]
sf_1b0c_fakerateloose_70 = fakerate_file_70['sf_1b0c_fakerateloose_50'][:]
sf_2b0c_fakerateloose_70 = fakerate_file_70['sf_2b0c_fakerateloose_50'][:]
sf_1b1c_fakerateloose_70 = fakerate_file_70['sf_1b1c_fakerateloose_50'][:]
err_sf_0b0c_fakerateloose_70 = fakerate_file_70['err_sf_0b0c_fakerateloose_50'][:]
err_sf_0b1c_fakerateloose_70 = fakerate_file_70['err_sf_0b1c_fakerateloose_50'][:]
err_sf_0b2c_fakerateloose_70 = fakerate_file_70['err_sf_0b2c_fakerateloose_50'][:]
err_sf_1b0c_fakerateloose_70 = fakerate_file_70['err_sf_1b0c_fakerateloose_50'][:]
err_sf_2b0c_fakerateloose_70 = fakerate_file_70['err_sf_2b0c_fakerateloose_50'][:]
err_sf_1b1c_fakerateloose_70 = fakerate_file_70['err_sf_1b1c_fakerateloose_50'][:]

## compute final scale factors for each category 
# cental value is the multiplication of trkd0, trkz0 and fakerateloose sfs

sf_0b0c_70 = sf_0b0c_trkd0_70*sf_0b0c_trkz0_70*sf_0b0c_fakerateloose_70
sf_0b1c_70 = sf_0b1c_trkd0_70*sf_0b1c_trkz0_70*sf_0b1c_fakerateloose_70
sf_0b2c_70 = sf_0b2c_trkd0_70*sf_0b2c_trkz0_70*sf_0b2c_fakerateloose_70
sf_1b0c_70 = sf_1b0c_trkd0_70*sf_1b0c_trkz0_70*sf_1b0c_fakerateloose_70
sf_2b0c_70 = sf_2b0c_trkd0_70*sf_2b0c_trkz0_70*sf_2b0c_fakerateloose_70
sf_1b1c_70 = sf_1b1c_trkd0_70*sf_1b1c_trkz0_70*sf_1b1c_fakerateloose_70

# all the others comes in as systematic uncertainties 
delta_sf_0b0c_qoverp_70 = 1 - sf_0b0c_qoverp_70
delta_sf_0b1c_qoverp_70 = 1 - sf_0b1c_qoverp_70
delta_sf_0b2c_qoverp_70 = 1 - sf_0b2c_qoverp_70
delta_sf_1b0c_qoverp_70 = 1 - sf_1b0c_qoverp_70
delta_sf_2b0c_qoverp_70 = 1 - sf_2b0c_qoverp_70
delta_sf_1b1c_qoverp_70 = 1 - sf_1b1c_qoverp_70


delta_sf_0b0c_trkd0up_70 = 1 - sf_0b0c_trkd0up_70
delta_sf_0b1c_trkd0up_70 = 1 - sf_0b1c_trkd0up_70
delta_sf_0b2c_trkd0up_70 = 1 - sf_0b2c_trkd0up_70
delta_sf_1b0c_trkd0up_70 = 1 - sf_1b0c_trkd0up_70
delta_sf_2b0c_trkd0up_70 = 1 - sf_2b0c_trkd0up_70
delta_sf_1b1c_trkd0up_70 = 1 - sf_1b1c_trkd0up_70

delta_sf_0b0c_trkd0down_70 = 1 - sf_0b0c_trkd0down_70
delta_sf_0b1c_trkd0down_70 = 1 - sf_0b1c_trkd0down_70
delta_sf_0b2c_trkd0down_70 = 1 - sf_0b2c_trkd0down_70
delta_sf_1b0c_trkd0down_70 = 1 - sf_1b0c_trkd0down_70
delta_sf_2b0c_trkd0down_70 = 1 - sf_2b0c_trkd0down_70
delta_sf_1b1c_trkd0down_70 = 1 - sf_1b1c_trkd0down_70

delta_sf_0b0c_trkd0dead_70 = 1 - sf_0b0c_trkd0dead_70
delta_sf_0b1c_trkd0dead_70 = 1 - sf_0b1c_trkd0dead_70
delta_sf_0b2c_trkd0dead_70 = 1 - sf_0b2c_trkd0dead_70
delta_sf_1b0c_trkd0dead_70 = 1 - sf_1b0c_trkd0dead_70
delta_sf_2b0c_trkd0dead_70 = 1 - sf_2b0c_trkd0dead_70
delta_sf_1b1c_trkd0dead_70 = 1 - sf_1b1c_trkd0dead_70

delta_sf_0b0c_trkz0up_70 = 1 - sf_0b0c_trkz0up_70
delta_sf_0b1c_trkz0up_70 = 1 - sf_0b1c_trkz0up_70
delta_sf_0b2c_trkz0up_70 = 1 - sf_0b2c_trkz0up_70
delta_sf_1b0c_trkz0up_70 = 1 - sf_1b0c_trkz0up_70
delta_sf_2b0c_trkz0up_70 = 1 - sf_2b0c_trkz0up_70
delta_sf_1b1c_trkz0up_70 = 1 - sf_1b1c_trkz0up_70

delta_sf_0b0c_trkz0down_70 = 1 - sf_0b0c_trkz0down_70
delta_sf_0b1c_trkz0down_70 = 1 - sf_0b1c_trkz0down_70
delta_sf_0b2c_trkz0down_70 = 1 - sf_0b2c_trkz0down_70
delta_sf_1b0c_trkz0down_70 = 1 - sf_1b0c_trkz0down_70
delta_sf_2b0c_trkz0down_70 = 1 - sf_2b0c_trkz0down_70
delta_sf_1b1c_trkz0down_70 = 1 - sf_1b1c_trkz0down_70

delta_sf_0b0c_trkz0dead_70 = 1 - sf_0b0c_trkz0dead_70
delta_sf_0b1c_trkz0dead_70 = 1 - sf_0b1c_trkz0dead_70
delta_sf_0b2c_trkz0dead_70 = 1 - sf_0b2c_trkz0dead_70
delta_sf_1b0c_trkz0dead_70 = 1 - sf_1b0c_trkz0dead_70
delta_sf_2b0c_trkz0dead_70 = 1 - sf_2b0c_trkz0dead_70
delta_sf_1b1c_trkz0dead_70 = 1 - sf_1b1c_trkz0dead_70

delta_sf_0b0c_efflooseglob_70 = 1 - sf_0b0c_efflooseglob_70
delta_sf_0b1c_efflooseglob_70 = 1 - sf_0b1c_efflooseglob_70
delta_sf_0b2c_efflooseglob_70 = 1 - sf_0b2c_efflooseglob_70
delta_sf_1b0c_efflooseglob_70 = 1 - sf_1b0c_efflooseglob_70
delta_sf_2b0c_efflooseglob_70 = 1 - sf_2b0c_efflooseglob_70
delta_sf_1b1c_efflooseglob_70 = 1 - sf_1b1c_efflooseglob_70

delta_sf_0b0c_efflooseibl_70 = 1 - sf_0b0c_efflooseibl_70
delta_sf_0b1c_efflooseibl_70 = 1 - sf_0b1c_efflooseibl_70
delta_sf_0b2c_efflooseibl_70 = 1 - sf_0b2c_efflooseibl_70
delta_sf_1b0c_efflooseibl_70 = 1 - sf_1b0c_efflooseibl_70
delta_sf_2b0c_efflooseibl_70 = 1 - sf_2b0c_efflooseibl_70
delta_sf_1b1c_efflooseibl_70 = 1 - sf_1b1c_efflooseibl_70

delta_sf_0b0c_effloosetide_70 = 1 - sf_0b0c_effloosetide_70
delta_sf_0b1c_effloosetide_70 = 1 - sf_0b1c_effloosetide_70
delta_sf_0b2c_effloosetide_70 = 1 - sf_0b2c_effloosetide_70
delta_sf_1b0c_effloosetide_70 = 1 - sf_1b0c_effloosetide_70
delta_sf_2b0c_effloosetide_70 = 1 - sf_2b0c_effloosetide_70
delta_sf_1b1c_effloosetide_70 = 1 - sf_1b1c_effloosetide_70

delta_sf_0b0c_effrateloosetide_70 = 1 - sf_0b0c_effrateloosetide_70
delta_sf_0b1c_effrateloosetide_70 = 1 - sf_0b1c_effrateloosetide_70
delta_sf_0b2c_effrateloosetide_70 = 1 - sf_0b2c_effrateloosetide_70
delta_sf_1b0c_effrateloosetide_70 = 1 - sf_1b0c_effrateloosetide_70
delta_sf_2b0c_effrateloosetide_70 = 1 - sf_2b0c_effrateloosetide_70
delta_sf_1b1c_effrateloosetide_70 = 1 - sf_1b1c_effrateloosetide_70

delta_sf_0b0c_effratelooserob_70 = 1 - sf_0b0c_effratelooserob_70
delta_sf_0b1c_effratelooserob_70 = 1 - sf_0b1c_effratelooserob_70
delta_sf_0b2c_effratelooserob_70 = 1 - sf_0b2c_effratelooserob_70
delta_sf_1b0c_effratelooserob_70 = 1 - sf_1b0c_effratelooserob_70
delta_sf_2b0c_effratelooserob_70 = 1 - sf_2b0c_effratelooserob_70
delta_sf_1b1c_effratelooserob_70 = 1 - sf_1b1c_effratelooserob_70

delta_sf_0b0c_fakerateloose_70 = 1 - sf_0b0c_fakerateloose_70
delta_sf_0b1c_fakerateloose_70 = 1 - sf_0b1c_fakerateloose_70
delta_sf_0b2c_fakerateloose_70 = 1 - sf_0b2c_fakerateloose_70
delta_sf_1b0c_fakerateloose_70 = 1 - sf_1b0c_fakerateloose_70
delta_sf_2b0c_fakerateloose_70 = 1 - sf_2b0c_fakerateloose_70
delta_sf_1b1c_fakerateloose_70 = 1 - sf_1b1c_fakerateloose_70

# compute the total systematic uncertainty for each category

err_sf_0b0c_70 = np.sqrt(delta_sf_0b0c_qoverp_70**2 + delta_sf_0b0c_trkd0up_70**2 + delta_sf_0b0c_trkd0down_70**2 + delta_sf_0b0c_trkd0dead_70**2 + delta_sf_0b0c_trkz0up_70**2 + delta_sf_0b0c_trkz0down_70**2 + delta_sf_0b0c_trkz0dead_70**2 + delta_sf_0b0c_efflooseglob_70**2 + delta_sf_0b0c_efflooseibl_70**2 + delta_sf_0b0c_effloosetide_70**2 + delta_sf_0b0c_effrateloosetide_70**2 + delta_sf_0b0c_effratelooserob_70**2 + delta_sf_0b0c_fakerateloose_70**2)
err_sf_0b1c_70 = np.sqrt(delta_sf_0b1c_qoverp_70**2 +delta_sf_0b1c_trkd0up_70**2 + delta_sf_0b1c_trkd0down_70**2 + delta_sf_0b1c_trkd0dead_70**2 + delta_sf_0b1c_trkz0up_70**2 + delta_sf_0b1c_trkz0down_70**2 + delta_sf_0b1c_trkz0dead_70**2 + delta_sf_0b1c_efflooseglob_70**2 + delta_sf_0b1c_efflooseibl_70**2 + delta_sf_0b1c_effloosetide_70**2 + delta_sf_0b1c_effrateloosetide_70**2 + delta_sf_0b1c_effratelooserob_70**2 + delta_sf_0b1c_fakerateloose_70**2)
err_sf_0b2c_70 = np.sqrt(delta_sf_0b2c_qoverp_70**2 +delta_sf_0b2c_trkd0up_70**2 + delta_sf_0b2c_trkd0down_70**2 + delta_sf_0b2c_trkd0dead_70**2 + delta_sf_0b2c_trkz0up_70**2 + delta_sf_0b2c_trkz0down_70**2 + delta_sf_0b2c_trkz0dead_70**2 + delta_sf_0b2c_efflooseglob_70**2 + delta_sf_0b2c_efflooseibl_70**2 + delta_sf_0b2c_effloosetide_70**2 + delta_sf_0b2c_effrateloosetide_70**2 + delta_sf_0b2c_effratelooserob_70**2 + delta_sf_0b2c_fakerateloose_70**2)
err_sf_1b0c_70 = np.sqrt(delta_sf_1b0c_qoverp_70**2 +delta_sf_1b0c_trkd0up_70**2 + delta_sf_1b0c_trkd0down_70**2 + delta_sf_1b0c_trkd0dead_70**2 + delta_sf_1b0c_trkz0up_70**2 + delta_sf_1b0c_trkz0down_70**2 + delta_sf_1b0c_trkz0dead_70**2 + delta_sf_1b0c_efflooseglob_70**2 + delta_sf_1b0c_efflooseibl_70**2 + delta_sf_1b0c_effloosetide_70**2 + delta_sf_1b0c_effrateloosetide_70**2 + delta_sf_1b0c_effratelooserob_70**2 + delta_sf_1b0c_fakerateloose_70**2)
err_sf_2b0c_70 = np.sqrt(delta_sf_2b0c_qoverp_70**2 +delta_sf_2b0c_trkd0up_70**2 + delta_sf_2b0c_trkd0down_70**2 + delta_sf_2b0c_trkd0dead_70**2 + delta_sf_2b0c_trkz0up_70**2 + delta_sf_2b0c_trkz0down_70**2 + delta_sf_2b0c_trkz0dead_70**2 + delta_sf_2b0c_efflooseglob_70**2 + delta_sf_2b0c_efflooseibl_70**2 + delta_sf_2b0c_effloosetide_70**2 + delta_sf_2b0c_effrateloosetide_70**2 + delta_sf_2b0c_effratelooserob_70**2 + delta_sf_2b0c_fakerateloose_70**2)
err_sf_1b1c_70 = np.sqrt(delta_sf_1b1c_qoverp_70**2 +delta_sf_1b1c_trkd0up_70**2 + delta_sf_1b1c_trkd0down_70**2 + delta_sf_1b1c_trkd0dead_70**2 + delta_sf_1b1c_trkz0up_70**2 + delta_sf_1b1c_trkz0down_70**2 + delta_sf_1b1c_trkz0dead_70**2 + delta_sf_1b1c_efflooseglob_70**2 + delta_sf_1b1c_efflooseibl_70**2 + delta_sf_1b1c_effloosetide_70**2 + delta_sf_1b1c_effrateloosetide_70**2 + delta_sf_1b1c_effratelooserob_70**2 + delta_sf_1b1c_fakerateloose_70**2)

# plot

fig = plt.figure(figsize=(20, 18))

plt.errorbar(pt_bins, sf_0b0c_70, xerr = pt_bin_width/2, yerr=err_sf_0b0c_70, fmt='o', label='0b + 0c')
plt.errorbar(pt_bins, sf_0b1c_70, xerr = pt_bin_width/2, yerr=err_sf_0b1c_70, fmt='o', label='0b + >=1c')
plt.errorbar(pt_bins, sf_0b2c_70, xerr = pt_bin_width/2, yerr=err_sf_0b2c_70, fmt='o', label='0b + >=2c')
plt.errorbar(pt_bins, sf_1b0c_70, xerr = pt_bin_width/2, yerr=err_sf_1b0c_70, fmt='o', label='1b + 0c')
plt.errorbar(pt_bins, sf_2b0c_70, xerr = pt_bin_width/2, yerr=err_sf_2b0c_70, fmt='o', label='>=2b + 0c')
plt.errorbar(pt_bins, sf_1b1c_70, xerr = pt_bin_width/2, yerr=err_sf_1b1c_70, fmt='o', label='1b + >=1c')

plt.text(0.02, 0.97, 'ATLAS', fontsize=20, fontweight='bold', fontstyle='italic', color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.09, 0.97, 'Simulation Preliminary', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.95, r'$\sqrt{s}$ = 13 TeV, Anti-$k_t$ R = 1.0 UFO jets', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.92, r'$\Delta$R = 1.0 track-jet association', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.90, r'GN2X Scores, QCD Samples, 70% H($b\bar{b}$) WP', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)


plt.xlabel(r'$p_T$ [GeV]',  ha = 'right', fontsize=18)
plt.ylabel('Scale Factor',  ha = 'right', fontsize=18)
plt.legend(fontsize=18)
plt.grid(True)


plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.ylim(0.4, 1.9)


#add text on the plot with central value and error value
for i in range(len(pt_bins)):
 plt.text(pt_bins[i], sf_0b0c_70[i], str(round(sf_0b0c_70[i],2)) + '+/-' + str(round(err_sf_0b0c_70[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_0b1c_70[i], str(round(sf_0b1c_70[i],2)) + '+/-' + str(round(err_sf_0b1c_70[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_0b2c_70[i], str(round(sf_0b2c_70[i],2)) + '+/-' + str(round(err_sf_0b2c_70[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_1b0c_70[i], str(round(sf_1b0c_70[i],2)) + '+/-' + str(round(err_sf_1b0c_70[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_2b0c_70[i], str(round(sf_2b0c_70[i],2)) + '+/-' + str(round(err_sf_2b0c_70[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_1b1c_70[i], str(round(sf_1b1c_70[i],2)) + '+/-' + str(round(err_sf_1b1c_70[i],2)), fontsize=14)
   

plt.savefig('final_sf_70.png', dpi=1000)

bias_file_85 = h5py.File('sf_bias85.h5', 'r')
effloose_file_85 = h5py.File('sf_effloose85.h5', 'r')
effrate_file_85 = h5py.File('sf_effrate85.h5', 'r')
fakerate_file_85 = h5py.File('sf_fakerate85.h5', 'r')
trkd0_file_85 = h5py.File('sf_trkd085.h5', 'r')
trkz0_file_85 = h5py.File('sf_trkz085.h5', 'r')
dead_file_85 = h5py.File('sf_dead85.h5', 'r')
#TRK_D0
sf_0b0c_trkd0_85 = trkd0_file_85['sf_0b0c_trkd0_50'][:]
sf_0b1c_trkd0_85 = trkd0_file_85['sf_0b1c_trkd0_50'][:]
sf_0b2c_trkd0_85 = trkd0_file_85['sf_0b2c_trkd0_50'][:]
sf_1b0c_trkd0_85 = trkd0_file_85['sf_1b0c_trkd0_50'][:]
sf_2b0c_trkd0_85 = trkd0_file_85['sf_2b0c_trkd0_50'][:]
sf_1b1c_trkd0_85 = trkd0_file_85['sf_1b1c_trkd0_50'][:]

err_sf_0b0c_trkd0_85 = trkd0_file_85['err_sf_0b0c_trkd0_50'][:]
err_sf_0b1c_trkd0_85 = trkd0_file_85['err_sf_0b1c_trkd0_50'][:]
err_sf_0b2c_trkd0_85 = trkd0_file_85['err_sf_0b2c_trkd0_50'][:]
err_sf_1b0c_trkd0_85 = trkd0_file_85['err_sf_1b0c_trkd0_50'][:]
err_sf_2b0c_trkd0_85 = trkd0_file_85['err_sf_2b0c_trkd0_50'][:]
err_sf_1b1c_trkd0_85 = trkd0_file_85['err_sf_1b1c_trkd0_50'][:]

#TRK_D0_UP
sf_0b0c_trkd0up_85 = trkd0_file_85['sf_0b0c_trkd0up_50'][:]
sf_0b1c_trkd0up_85 = trkd0_file_85['sf_0b1c_trkd0up_50'][:]
sf_0b2c_trkd0up_85 = trkd0_file_85['sf_0b2c_trkd0up_50'][:]
sf_1b0c_trkd0up_85 = trkd0_file_85['sf_1b0c_trkd0up_50'][:]
sf_2b0c_trkd0up_85 = trkd0_file_85['sf_2b0c_trkd0up_50'][:]
sf_1b1c_trkd0up_85 = trkd0_file_85['sf_1b1c_trkd0up_50'][:]

err_sf_0b0c_trkd0up_85 = trkd0_file_85['err_sf_0b0c_trkd0up_50'][:]
err_sf_0b1c_trkd0up_85 = trkd0_file_85['err_sf_0b1c_trkd0up_50'][:]
err_sf_0b2c_trkd0up_85 = trkd0_file_85['err_sf_0b2c_trkd0up_50'][:]
err_sf_1b0c_trkd0up_85 = trkd0_file_85['err_sf_1b0c_trkd0up_50'][:]
err_sf_2b0c_trkd0up_85 = trkd0_file_85['err_sf_2b0c_trkd0up_50'][:]
err_sf_1b1c_trkd0up_85 = trkd0_file_85['err_sf_1b1c_trkd0up_50'][:]

#TRK_D0_DOWN
sf_0b0c_trkd0down_85 = trkd0_file_85['sf_0b0c_trkd0down_50'][:]
sf_0b1c_trkd0down_85 = trkd0_file_85['sf_0b1c_trkd0down_50'][:]
sf_0b2c_trkd0down_85 = trkd0_file_85['sf_0b2c_trkd0down_50'][:]
sf_1b0c_trkd0down_85 = trkd0_file_85['sf_1b0c_trkd0down_50'][:]
sf_2b0c_trkd0down_85 = trkd0_file_85['sf_2b0c_trkd0down_50'][:]
sf_1b1c_trkd0down_85 = trkd0_file_85['sf_1b1c_trkd0down_50'][:]

err_sf_0b0c_trkd0down_85 = trkd0_file_85['err_sf_0b0c_trkd0down_50'][:]
err_sf_0b1c_trkd0down_85 = trkd0_file_85['err_sf_0b1c_trkd0down_50'][:]
err_sf_0b2c_trkd0down_85 = trkd0_file_85['err_sf_0b2c_trkd0down_50'][:]
err_sf_1b0c_trkd0down_85 = trkd0_file_85['err_sf_1b0c_trkd0down_50'][:]
err_sf_2b0c_trkd0down_85 = trkd0_file_85['err_sf_2b0c_trkd0down_50'][:]
err_sf_1b1c_trkd0down_85 = trkd0_file_85['err_sf_1b1c_trkd0down_50'][:]

#TRK_D0_BIAS
sf_0b0c_trkd0bias_85 = bias_file_85['sf_0b0c_trkd0bias_50'][:]
sf_0b1c_trkd0bias_85 = bias_file_85['sf_0b1c_trkd0bias_50'][:]
sf_0b2c_trkd0bias_85 = bias_file_85['sf_0b2c_trkd0bias_50'][:]
sf_1b0c_trkd0bias_85 = bias_file_85['sf_1b0c_trkd0bias_50'][:]
sf_2b0c_trkd0bias_85 = bias_file_85['sf_2b0c_trkd0bias_50'][:]
sf_1b1c_trkd0bias_85 = bias_file_85['sf_1b1c_trkd0bias_50'][:]

err_sf_0b0c_trkd0bias_85 = bias_file_85['err_sf_0b0c_trkd0bias_50'][:]
err_sf_0b1c_trkd0bias_85 = bias_file_85['err_sf_0b1c_trkd0bias_50'][:]
err_sf_0b2c_trkd0bias_85 = bias_file_85['err_sf_0b2c_trkd0bias_50'][:]
err_sf_1b0c_trkd0bias_85 = bias_file_85['err_sf_1b0c_trkd0bias_50'][:]
err_sf_2b0c_trkd0bias_85 = bias_file_85['err_sf_2b0c_trkd0bias_50'][:]
err_sf_1b1c_trkd0bias_85 = bias_file_85['err_sf_1b1c_trkd0bias_50'][:]

#QOVERP
sf_0b0c_qoverp_85 = bias_file_85['sf_0b0c_qoverp_50'][:]
sf_0b1c_qoverp_85 = bias_file_85['sf_0b1c_qoverp_50'][:]
sf_0b2c_qoverp_85 = bias_file_85['sf_0b2c_qoverp_50'][:]
sf_1b0c_qoverp_85 = bias_file_85['sf_1b0c_qoverp_50'][:]
sf_2b0c_qoverp_85 = bias_file_85['sf_2b0c_qoverp_50'][:]
sf_1b1c_qoverp_85 = bias_file_85['sf_1b1c_qoverp_50'][:]

err_sf_0b0c_qoverp_85 = bias_file_85['err_sf_0b0c_qoverp_50'][:]
err_sf_0b1c_qoverp_85 = bias_file_85['err_sf_0b1c_qoverp_50'][:]
err_sf_0b2c_qoverp_85 = bias_file_85['err_sf_0b2c_qoverp_50'][:]
err_sf_1b0c_qoverp_85 = bias_file_85['err_sf_1b0c_qoverp_50'][:]
err_sf_2b0c_qoverp_85 = bias_file_85['err_sf_2b0c_qoverp_50'][:]
err_sf_1b1c_qoverp_85 = bias_file_85['err_sf_1b1c_qoverp_50'][:]

#TRK_Z0
sf_0b0c_trkz0_85 = trkz0_file_85['sf_0b0c_trkz0_50'][:]
sf_0b1c_trkz0_85 = trkz0_file_85['sf_0b1c_trkz0_50'][:]
sf_0b2c_trkz0_85 = trkz0_file_85['sf_0b2c_trkz0_50'][:]
sf_1b0c_trkz0_85 = trkz0_file_85['sf_1b0c_trkz0_50'][:]
sf_2b0c_trkz0_85 = trkz0_file_85['sf_2b0c_trkz0_50'][:]
sf_1b1c_trkz0_85 = trkz0_file_85['sf_1b1c_trkz0_50'][:]

err_sf_0b0c_trkz0_85 = trkz0_file_85['err_sf_0b0c_trkz0_50'][:]
err_sf_0b1c_trkz0_85 = trkz0_file_85['err_sf_0b1c_trkz0_50'][:]
err_sf_0b2c_trkz0_85 = trkz0_file_85['err_sf_0b2c_trkz0_50'][:]
err_sf_1b0c_trkz0_85 = trkz0_file_85['err_sf_1b0c_trkz0_50'][:]
err_sf_2b0c_trkz0_85 = trkz0_file_85['err_sf_2b0c_trkz0_50'][:]
err_sf_1b1c_trkz0_85 = trkz0_file_85['err_sf_1b1c_trkz0_50'][:]

#TRK_Z0_UP
sf_0b0c_trkz0up_85 = trkz0_file_85['sf_0b0c_trkz0up_50'][:]
sf_0b1c_trkz0up_85 = trkz0_file_85['sf_0b1c_trkz0up_50'][:]
sf_0b2c_trkz0up_85 = trkz0_file_85['sf_0b2c_trkz0up_50'][:]
sf_1b0c_trkz0up_85 = trkz0_file_85['sf_1b0c_trkz0up_50'][:]
sf_2b0c_trkz0up_85 = trkz0_file_85['sf_2b0c_trkz0up_50'][:]
sf_1b1c_trkz0up_85 = trkz0_file_85['sf_1b1c_trkz0up_50'][:]

err_sf_0b0c_trkz0up_85 = trkz0_file_85['err_sf_0b0c_trkz0up_50'][:]
err_sf_0b1c_trkz0up_85 = trkz0_file_85['err_sf_0b1c_trkz0up_50'][:]
err_sf_0b2c_trkz0up_85 = trkz0_file_85['err_sf_0b2c_trkz0up_50'][:]
err_sf_1b0c_trkz0up_85 = trkz0_file_85['err_sf_1b0c_trkz0up_50'][:]
err_sf_2b0c_trkz0up_85 = trkz0_file_85['err_sf_2b0c_trkz0up_50'][:]
err_sf_1b1c_trkz0up_85 = trkz0_file_85['err_sf_1b1c_trkz0up_50'][:]

#TRK_Z0_DOWN
sf_0b0c_trkz0down_85 = trkz0_file_85['sf_0b0c_trkz0down_50'][:]
sf_0b1c_trkz0down_85 = trkz0_file_85['sf_0b1c_trkz0down_50'][:]
sf_0b2c_trkz0down_85 = trkz0_file_85['sf_0b2c_trkz0down_50'][:]
sf_1b0c_trkz0down_85 = trkz0_file_85['sf_1b0c_trkz0down_50'][:]
sf_2b0c_trkz0down_85 = trkz0_file_85['sf_2b0c_trkz0down_50'][:]
sf_1b1c_trkz0down_85 = trkz0_file_85['sf_1b1c_trkz0down_50'][:]

err_sf_0b0c_trkz0down_85 = trkz0_file_85['err_sf_0b0c_trkz0down_50'][:]
err_sf_0b1c_trkz0down_85 = trkz0_file_85['err_sf_0b1c_trkz0down_50'][:]
err_sf_0b2c_trkz0down_85 = trkz0_file_85['err_sf_0b2c_trkz0down_50'][:]
err_sf_1b0c_trkz0down_85 = trkz0_file_85['err_sf_1b0c_trkz0down_50'][:]
err_sf_2b0c_trkz0down_85 = trkz0_file_85['err_sf_2b0c_trkz0down_50'][:]
err_sf_1b1c_trkz0down_85 = trkz0_file_85['err_sf_1b1c_trkz0down_50'][:]



#TRK_D0_DEAD
sf_0b0c_trkd0dead_85 = dead_file_85['sf_0b0c_trkd0dead_50'][:]
sf_0b1c_trkd0dead_85 = dead_file_85['sf_0b1c_trkd0dead_50'][:]
sf_0b2c_trkd0dead_85 = dead_file_85['sf_0b2c_trkd0dead_50'][:]
sf_1b0c_trkd0dead_85 = dead_file_85['sf_1b0c_trkd0dead_50'][:]
sf_2b0c_trkd0dead_85 = dead_file_85['sf_2b0c_trkd0dead_50'][:]
sf_1b1c_trkd0dead_85 = dead_file_85['sf_1b1c_trkd0dead_50'][:]

err_sf_0b0c_trkd0dead_85 = dead_file_85['err_sf_0b0c_trkd0dead_50'][:]
err_sf_0b1c_trkd0dead_85 = dead_file_85['err_sf_0b1c_trkd0dead_50'][:]
err_sf_0b2c_trkd0dead_85 = dead_file_85['err_sf_0b2c_trkd0dead_50'][:]
err_sf_1b0c_trkd0dead_85 = dead_file_85['err_sf_1b0c_trkd0dead_50'][:]
err_sf_2b0c_trkd0dead_85 = dead_file_85['err_sf_2b0c_trkd0dead_50'][:]
err_sf_1b1c_trkd0dead_85 = dead_file_85['err_sf_1b1c_trkd0dead_50'][:]

#TRK_Z0_DEAD
sf_0b0c_trkz0dead_85 = dead_file_85['sf_0b0c_trkz0dead_50'][:]
sf_0b1c_trkz0dead_85 = dead_file_85['sf_0b1c_trkz0dead_50'][:]
sf_0b2c_trkz0dead_85 = dead_file_85['sf_0b2c_trkz0dead_50'][:]
sf_1b0c_trkz0dead_85 = dead_file_85['sf_1b0c_trkz0dead_50'][:]
sf_2b0c_trkz0dead_85 = dead_file_85['sf_2b0c_trkz0dead_50'][:]
sf_1b1c_trkz0dead_85 = dead_file_85['sf_1b1c_trkz0dead_50'][:]

err_sf_0b0c_trkz0dead_85 = dead_file_85['err_sf_0b0c_trkz0dead_50'][:]
err_sf_0b1c_trkz0dead_85 = dead_file_85['err_sf_0b1c_trkz0dead_50'][:]
err_sf_0b2c_trkz0dead_85 = dead_file_85['err_sf_0b2c_trkz0dead_50'][:]
err_sf_1b0c_trkz0dead_85 = dead_file_85['err_sf_1b0c_trkz0dead_50'][:]
err_sf_2b0c_trkz0dead_85 = dead_file_85['err_sf_2b0c_trkz0dead_50'][:]
err_sf_1b1c_trkz0dead_85 = dead_file_85['err_sf_1b1c_trkz0dead_50'][:]

#TRK_EFF_LOOSE_GLOB
sf_0b0c_efflooseglob_85 = effloose_file_85['sf_0b0c_efflooseglob_50'][:]
sf_0b1c_efflooseglob_85 = effloose_file_85['sf_0b1c_efflooseglob_50'][:]
sf_0b2c_efflooseglob_85 = effloose_file_85['sf_0b2c_efflooseglob_50'][:]
sf_1b0c_efflooseglob_85 = effloose_file_85['sf_1b0c_efflooseglob_50'][:]
sf_2b0c_efflooseglob_85 = effloose_file_85['sf_2b0c_efflooseglob_50'][:]
sf_1b1c_efflooseglob_85 = effloose_file_85['sf_1b1c_efflooseglob_50'][:]

err_sf_0b0c_efflooseglob_85 = effloose_file_85['err_sf_0b0c_efflooseglob_50'][:]
err_sf_0b1c_efflooseglob_85 = effloose_file_85['err_sf_0b1c_efflooseglob_50'][:]
err_sf_0b2c_efflooseglob_85 = effloose_file_85['err_sf_0b2c_efflooseglob_50'][:]
err_sf_1b0c_efflooseglob_85 = effloose_file_85['err_sf_1b0c_efflooseglob_50'][:]
err_sf_2b0c_efflooseglob_85 = effloose_file_85['err_sf_2b0c_efflooseglob_50'][:]
err_sf_1b1c_efflooseglob_85 = effloose_file_85['err_sf_1b1c_efflooseglob_50'][:]

#TRK_EFF_LOOSE_IBL
sf_0b0c_efflooseibl_85 = effloose_file_85['sf_0b0c_efflooseibl_50'][:]
sf_0b1c_efflooseibl_85 = effloose_file_85['sf_0b1c_efflooseibl_50'][:]
sf_0b2c_efflooseibl_85 = effloose_file_85['sf_0b2c_efflooseibl_50'][:]
sf_1b0c_efflooseibl_85 = effloose_file_85['sf_1b0c_efflooseibl_50'][:]
sf_2b0c_efflooseibl_85 = effloose_file_85['sf_2b0c_efflooseibl_50'][:]
sf_1b1c_efflooseibl_85 = effloose_file_85['sf_1b1c_efflooseibl_50'][:]

err_sf_0b0c_efflooseibl_85 = effloose_file_85['err_sf_0b0c_efflooseibl_50'][:]
err_sf_0b1c_efflooseibl_85 = effloose_file_85['err_sf_0b1c_efflooseibl_50'][:]
err_sf_0b2c_efflooseibl_85 = effloose_file_85['err_sf_0b2c_efflooseibl_50'][:]
err_sf_1b0c_efflooseibl_85 = effloose_file_85['err_sf_1b0c_efflooseibl_50'][:]
err_sf_2b0c_efflooseibl_85 = effloose_file_85['err_sf_2b0c_efflooseibl_50'][:]
err_sf_1b1c_efflooseibl_85 = effloose_file_85['err_sf_1b1c_efflooseibl_50'][:]

#TRK_EFF_LOOSE_TIDE
sf_0b0c_effloosetide_85 = effloose_file_85['sf_0b0c_effloosetide_50'][:]
sf_0b1c_effloosetide_85 = effloose_file_85['sf_0b1c_effloosetide_50'][:]
sf_0b2c_effloosetide_85 = effloose_file_85['sf_0b2c_effloosetide_50'][:]
sf_1b0c_effloosetide_85 = effloose_file_85['sf_1b0c_effloosetide_50'][:]
sf_2b0c_effloosetide_85 = effloose_file_85['sf_2b0c_effloosetide_50'][:]
sf_1b1c_effloosetide_85 = effloose_file_85['sf_1b1c_effloosetide_50'][:]

err_sf_0b0c_effloosetide_85 = effloose_file_85['err_sf_0b0c_effloosetide_50'][:]
err_sf_0b1c_effloosetide_85 = effloose_file_85['err_sf_0b1c_effloosetide_50'][:]
err_sf_0b2c_effloosetide_85 = effloose_file_85['err_sf_0b2c_effloosetide_50'][:]
err_sf_1b0c_effloosetide_85 = effloose_file_85['err_sf_1b0c_effloosetide_50'][:]
err_sf_2b0c_effloosetide_85 = effloose_file_85['err_sf_2b0c_effloosetide_50'][:]
err_sf_1b1c_effloosetide_85 = effloose_file_85['err_sf_1b1c_effloosetide_50'][:]

#TRK_EFF_RATE_LOOSE_TIDE
sf_0b0c_effrateloosetide_85 = effrate_file_85['sf_0b0c_effrateloosetide_50'][:]
sf_0b1c_effrateloosetide_85 = effrate_file_85['sf_0b1c_effrateloosetide_50'][:]
sf_0b2c_effrateloosetide_85 = effrate_file_85['sf_0b2c_effrateloosetide_50'][:]
sf_1b0c_effrateloosetide_85 = effrate_file_85['sf_1b0c_effrateloosetide_50'][:]
sf_2b0c_effrateloosetide_85 = effrate_file_85['sf_2b0c_effrateloosetide_50'][:]
sf_1b1c_effrateloosetide_85 = effrate_file_85['sf_1b1c_effrateloosetide_50'][:]

err_sf_0b0c_effrateloosetide_85 = effrate_file_85['err_sf_0b0c_effrateloosetide_50'][:]
err_sf_0b1c_effrateloosetide_85 = effrate_file_85['err_sf_0b1c_effrateloosetide_50'][:]
err_sf_0b2c_effrateloosetide_85 = effrate_file_85['err_sf_0b2c_effrateloosetide_50'][:]
err_sf_1b0c_effrateloosetide_85 = effrate_file_85['err_sf_1b0c_effrateloosetide_50'][:]
err_sf_2b0c_effrateloosetide_85 = effrate_file_85['err_sf_2b0c_effrateloosetide_50'][:]
err_sf_1b1c_effrateloosetide_85 = effrate_file_85['err_sf_1b1c_effrateloosetide_50'][:]

#TRK_EFF_RATE_LOOSE_ROB
sf_0b0c_effratelooserob_85 = effrate_file_85['sf_0b0c_effratelooserob_50'][:]
sf_0b1c_effratelooserob_85 = effrate_file_85['sf_0b1c_effratelooserob_50'][:]
sf_0b2c_effratelooserob_85 = effrate_file_85['sf_0b2c_effratelooserob_50'][:]
sf_1b0c_effratelooserob_85 = effrate_file_85['sf_1b0c_effratelooserob_50'][:]
sf_2b0c_effratelooserob_85 = effrate_file_85['sf_2b0c_effratelooserob_50'][:]
sf_1b1c_effratelooserob_85 = effrate_file_85['sf_1b1c_effratelooserob_50'][:]

err_sf_0b0c_effratelooserob_85 = effrate_file_85['err_sf_0b0c_effratelooserob_50'][:]
err_sf_0b1c_effratelooserob_85 = effrate_file_85['err_sf_0b1c_effratelooserob_50'][:]
err_sf_0b2c_effratelooserob_85 = effrate_file_85['err_sf_0b2c_effratelooserob_50'][:]
err_sf_1b0c_effratelooserob_85 = effrate_file_85['err_sf_1b0c_effratelooserob_50'][:]
err_sf_2b0c_effratelooserob_85 = effrate_file_85['err_sf_2b0c_effratelooserob_50'][:]
err_sf_1b1c_effratelooserob_85 = effrate_file_85['err_sf_1b1c_effratelooserob_50'][:]

#FAKE_RATE_LOOSE
sf_0b0c_fakerateloose_85 = fakerate_file_85['sf_0b0c_fakerateloose_50'][:]
sf_0b1c_fakerateloose_85 = fakerate_file_85['sf_0b1c_fakerateloose_50'][:]
sf_0b2c_fakerateloose_85 = fakerate_file_85['sf_0b2c_fakerateloose_50'][:]
sf_1b0c_fakerateloose_85 = fakerate_file_85['sf_1b0c_fakerateloose_50'][:]
sf_2b0c_fakerateloose_85 = fakerate_file_85['sf_2b0c_fakerateloose_50'][:]
sf_1b1c_fakerateloose_85 = fakerate_file_85['sf_1b1c_fakerateloose_50'][:]
err_sf_0b0c_fakerateloose_85 = fakerate_file_85['err_sf_0b0c_fakerateloose_50'][:]
err_sf_0b1c_fakerateloose_85 = fakerate_file_85['err_sf_0b1c_fakerateloose_50'][:]
err_sf_0b2c_fakerateloose_85 = fakerate_file_85['err_sf_0b2c_fakerateloose_50'][:]
err_sf_1b0c_fakerateloose_85 = fakerate_file_85['err_sf_1b0c_fakerateloose_50'][:]
err_sf_2b0c_fakerateloose_85 = fakerate_file_85['err_sf_2b0c_fakerateloose_50'][:]
err_sf_1b1c_fakerateloose_85 = fakerate_file_85['err_sf_1b1c_fakerateloose_50'][:]

## compute final scale factors for each category 
# cental value is the multiplication of trkd0, trkz0 and fakerateloose sfs

sf_0b0c_85 = sf_0b0c_trkd0_85*sf_0b0c_trkz0_85*sf_0b0c_fakerateloose_85
sf_0b1c_85 = sf_0b1c_trkd0_85*sf_0b1c_trkz0_85*sf_0b1c_fakerateloose_85
sf_0b2c_85 = sf_0b2c_trkd0_85*sf_0b2c_trkz0_85*sf_0b2c_fakerateloose_85
sf_1b0c_85 = sf_1b0c_trkd0_85*sf_1b0c_trkz0_85*sf_1b0c_fakerateloose_85
sf_2b0c_85 = sf_2b0c_trkd0_85*sf_2b0c_trkz0_85*sf_2b0c_fakerateloose_85
sf_1b1c_85 = sf_1b1c_trkd0_85*sf_1b1c_trkz0_85*sf_1b1c_fakerateloose_85

# all the others comes in as systematic uncertainties 
delta_sf_0b0c_qoverp_85 = 1 - sf_0b0c_qoverp_85
delta_sf_0b1c_qoverp_85 = 1 - sf_0b1c_qoverp_85
delta_sf_0b2c_qoverp_85 = 1 - sf_0b2c_qoverp_85
delta_sf_1b0c_qoverp_85 = 1 - sf_1b0c_qoverp_85
delta_sf_2b0c_qoverp_85 = 1 - sf_2b0c_qoverp_85
delta_sf_1b1c_qoverp_85 = 1 - sf_1b1c_qoverp_85

delta_sf_0b0c_trkd0up_85 = 1 - sf_0b0c_trkd0up_85
delta_sf_0b1c_trkd0up_85 = 1 - sf_0b1c_trkd0up_85
delta_sf_0b2c_trkd0up_85 = 1 - sf_0b2c_trkd0up_85
delta_sf_1b0c_trkd0up_85 = 1 - sf_1b0c_trkd0up_85
delta_sf_2b0c_trkd0up_85 = 1 - sf_2b0c_trkd0up_85
delta_sf_1b1c_trkd0up_85 = 1 - sf_1b1c_trkd0up_85

delta_sf_0b0c_trkd0down_85 = 1 - sf_0b0c_trkd0down_85
delta_sf_0b1c_trkd0down_85 = 1 - sf_0b1c_trkd0down_85
delta_sf_0b2c_trkd0down_85 = 1 - sf_0b2c_trkd0down_85
delta_sf_1b0c_trkd0down_85 = 1 - sf_1b0c_trkd0down_85
delta_sf_2b0c_trkd0down_85 = 1 - sf_2b0c_trkd0down_85
delta_sf_1b1c_trkd0down_85 = 1 - sf_1b1c_trkd0down_85

delta_sf_0b0c_trkd0dead_85 = 1 - sf_0b0c_trkd0dead_85
delta_sf_0b1c_trkd0dead_85 = 1 - sf_0b1c_trkd0dead_85
delta_sf_0b2c_trkd0dead_85 = 1 - sf_0b2c_trkd0dead_85
delta_sf_1b0c_trkd0dead_85 = 1 - sf_1b0c_trkd0dead_85
delta_sf_2b0c_trkd0dead_85 = 1 - sf_2b0c_trkd0dead_85
delta_sf_1b1c_trkd0dead_85 = 1 - sf_1b1c_trkd0dead_85

delta_sf_0b0c_trkz0up_85 = 1 - sf_0b0c_trkz0up_85
delta_sf_0b1c_trkz0up_85 = 1 - sf_0b1c_trkz0up_85
delta_sf_0b2c_trkz0up_85 = 1 - sf_0b2c_trkz0up_85
delta_sf_1b0c_trkz0up_85 = 1 - sf_1b0c_trkz0up_85
delta_sf_2b0c_trkz0up_85 = 1 - sf_2b0c_trkz0up_85
delta_sf_1b1c_trkz0up_85 = 1 - sf_1b1c_trkz0up_85

delta_sf_0b0c_trkz0down_85 = 1 - sf_0b0c_trkz0down_85
delta_sf_0b1c_trkz0down_85 = 1 - sf_0b1c_trkz0down_85
delta_sf_0b2c_trkz0down_85 = 1 - sf_0b2c_trkz0down_85
delta_sf_1b0c_trkz0down_85 = 1 - sf_1b0c_trkz0down_85
delta_sf_2b0c_trkz0down_85 = 1 - sf_2b0c_trkz0down_85
delta_sf_1b1c_trkz0down_85 = 1 - sf_1b1c_trkz0down_85

delta_sf_0b0c_trkz0dead_85 = 1 - sf_0b0c_trkz0dead_85
delta_sf_0b1c_trkz0dead_85 = 1 - sf_0b1c_trkz0dead_85
delta_sf_0b2c_trkz0dead_85 = 1 - sf_0b2c_trkz0dead_85
delta_sf_1b0c_trkz0dead_85 = 1 - sf_1b0c_trkz0dead_85
delta_sf_2b0c_trkz0dead_85 = 1 - sf_2b0c_trkz0dead_85
delta_sf_1b1c_trkz0dead_85 = 1 - sf_1b1c_trkz0dead_85

delta_sf_0b0c_efflooseglob_85 = 1 - sf_0b0c_efflooseglob_85
delta_sf_0b1c_efflooseglob_85 = 1 - sf_0b1c_efflooseglob_85
delta_sf_0b2c_efflooseglob_85 = 1 - sf_0b2c_efflooseglob_85
delta_sf_1b0c_efflooseglob_85 = 1 - sf_1b0c_efflooseglob_85
delta_sf_2b0c_efflooseglob_85 = 1 - sf_2b0c_efflooseglob_85
delta_sf_1b1c_efflooseglob_85 = 1 - sf_1b1c_efflooseglob_85

delta_sf_0b0c_efflooseibl_85 = 1 - sf_0b0c_efflooseibl_85
delta_sf_0b1c_efflooseibl_85 = 1 - sf_0b1c_efflooseibl_85
delta_sf_0b2c_efflooseibl_85 = 1 - sf_0b2c_efflooseibl_85
delta_sf_1b0c_efflooseibl_85 = 1 - sf_1b0c_efflooseibl_85
delta_sf_2b0c_efflooseibl_85 = 1 - sf_2b0c_efflooseibl_85
delta_sf_1b1c_efflooseibl_85 = 1 - sf_1b1c_efflooseibl_85

delta_sf_0b0c_effloosetide_85 = 1 - sf_0b0c_effloosetide_85
delta_sf_0b1c_effloosetide_85 = 1 - sf_0b1c_effloosetide_85
delta_sf_0b2c_effloosetide_85 = 1 - sf_0b2c_effloosetide_85
delta_sf_1b0c_effloosetide_85 = 1 - sf_1b0c_effloosetide_85
delta_sf_2b0c_effloosetide_85 = 1 - sf_2b0c_effloosetide_85
delta_sf_1b1c_effloosetide_85 = 1 - sf_1b1c_effloosetide_85

delta_sf_0b0c_effrateloosetide_85 = 1 - sf_0b0c_effrateloosetide_85
delta_sf_0b1c_effrateloosetide_85 = 1 - sf_0b1c_effrateloosetide_85
delta_sf_0b2c_effrateloosetide_85 = 1 - sf_0b2c_effrateloosetide_85
delta_sf_1b0c_effrateloosetide_85 = 1 - sf_1b0c_effrateloosetide_85
delta_sf_2b0c_effrateloosetide_85 = 1 - sf_2b0c_effrateloosetide_85
delta_sf_1b1c_effrateloosetide_85 = 1 - sf_1b1c_effrateloosetide_85

delta_sf_0b0c_effratelooserob_85 = 1 - sf_0b0c_effratelooserob_85
delta_sf_0b1c_effratelooserob_85 = 1 - sf_0b1c_effratelooserob_85
delta_sf_0b2c_effratelooserob_85 = 1 - sf_0b2c_effratelooserob_85
delta_sf_1b0c_effratelooserob_85 = 1 - sf_1b0c_effratelooserob_85
delta_sf_2b0c_effratelooserob_85 = 1 - sf_2b0c_effratelooserob_85
delta_sf_1b1c_effratelooserob_85 = 1 - sf_1b1c_effratelooserob_85

delta_sf_0b0c_fakerateloose_85 = 1 - sf_0b0c_fakerateloose_85
delta_sf_0b1c_fakerateloose_85 = 1 - sf_0b1c_fakerateloose_85
delta_sf_0b2c_fakerateloose_85 = 1 - sf_0b2c_fakerateloose_85
delta_sf_1b0c_fakerateloose_85 = 1 - sf_1b0c_fakerateloose_85
delta_sf_2b0c_fakerateloose_85 = 1 - sf_2b0c_fakerateloose_85
delta_sf_1b1c_fakerateloose_85 = 1 - sf_1b1c_fakerateloose_85

# compute the total systematic uncertainty for each category

err_sf_0b0c_85 = np.sqrt(delta_sf_0b0c_qoverp_85**2 + delta_sf_0b0c_trkd0up_85**2 + delta_sf_0b0c_trkd0down_85**2 + delta_sf_0b0c_trkd0dead_85**2 + delta_sf_0b0c_trkz0up_85**2 + delta_sf_0b0c_trkz0down_85**2 + delta_sf_0b0c_trkz0dead_85**2 + delta_sf_0b0c_efflooseglob_85**2 + delta_sf_0b0c_efflooseibl_85**2 + delta_sf_0b0c_effloosetide_85**2 + delta_sf_0b0c_effrateloosetide_85**2 + delta_sf_0b0c_effratelooserob_85**2 + delta_sf_0b0c_fakerateloose_85**2)
err_sf_0b1c_85 = np.sqrt(delta_sf_0b1c_qoverp_85**2 +delta_sf_0b1c_trkd0up_85**2 + delta_sf_0b1c_trkd0down_85**2 + delta_sf_0b1c_trkd0dead_85**2 + delta_sf_0b1c_trkz0up_85**2 + delta_sf_0b1c_trkz0down_85**2 + delta_sf_0b1c_trkz0dead_85**2 + delta_sf_0b1c_efflooseglob_85**2 + delta_sf_0b1c_efflooseibl_85**2 + delta_sf_0b1c_effloosetide_85**2 + delta_sf_0b1c_effrateloosetide_85**2 + delta_sf_0b1c_effratelooserob_85**2 + delta_sf_0b1c_fakerateloose_85**2)
err_sf_0b2c_85 = np.sqrt(delta_sf_0b2c_qoverp_85**2 +delta_sf_0b2c_trkd0up_85**2 + delta_sf_0b2c_trkd0down_85**2 + delta_sf_0b2c_trkd0dead_85**2 + delta_sf_0b2c_trkz0up_85**2 + delta_sf_0b2c_trkz0down_85**2 + delta_sf_0b2c_trkz0dead_85**2 + delta_sf_0b2c_efflooseglob_85**2 + delta_sf_0b2c_efflooseibl_85**2 + delta_sf_0b2c_effloosetide_85**2 + delta_sf_0b2c_effrateloosetide_85**2 + delta_sf_0b2c_effratelooserob_85**2 + delta_sf_0b2c_fakerateloose_85**2)
err_sf_1b0c_85 = np.sqrt(delta_sf_1b0c_qoverp_85**2 +delta_sf_1b0c_trkd0up_85**2 + delta_sf_1b0c_trkd0down_85**2 + delta_sf_1b0c_trkd0dead_85**2 + delta_sf_1b0c_trkz0up_85**2 + delta_sf_1b0c_trkz0down_85**2 + delta_sf_1b0c_trkz0dead_85**2 + delta_sf_1b0c_efflooseglob_85**2 + delta_sf_1b0c_efflooseibl_85**2 + delta_sf_1b0c_effloosetide_85**2 + delta_sf_1b0c_effrateloosetide_85**2 + delta_sf_1b0c_effratelooserob_85**2 + delta_sf_1b0c_fakerateloose_85**2)
err_sf_2b0c_85 = np.sqrt(delta_sf_2b0c_qoverp_85**2 +delta_sf_2b0c_trkd0up_85**2 + delta_sf_2b0c_trkd0down_85**2 + delta_sf_2b0c_trkd0dead_85**2 + delta_sf_2b0c_trkz0up_85**2 + delta_sf_2b0c_trkz0down_85**2 + delta_sf_2b0c_trkz0dead_85**2 + delta_sf_2b0c_efflooseglob_85**2 + delta_sf_2b0c_efflooseibl_85**2 + delta_sf_2b0c_effloosetide_85**2 + delta_sf_2b0c_effrateloosetide_85**2 + delta_sf_2b0c_effratelooserob_85**2 + delta_sf_2b0c_fakerateloose_85**2)
err_sf_1b1c_85 = np.sqrt(delta_sf_1b1c_qoverp_85**2 +delta_sf_1b1c_trkd0up_85**2 + delta_sf_1b1c_trkd0down_85**2 + delta_sf_1b1c_trkd0dead_85**2 + delta_sf_1b1c_trkz0up_85**2 + delta_sf_1b1c_trkz0down_85**2 + delta_sf_1b1c_trkz0dead_85**2 + delta_sf_1b1c_efflooseglob_85**2 + delta_sf_1b1c_efflooseibl_85**2 + delta_sf_1b1c_effloosetide_85**2 + delta_sf_1b1c_effrateloosetide_85**2 + delta_sf_1b1c_effratelooserob_85**2 + delta_sf_1b1c_fakerateloose_85**2)

# plot

fig = plt.figure(figsize=(20, 18))

plt.errorbar(pt_bins, sf_0b0c_85, xerr = pt_bin_width/2, yerr=err_sf_0b0c_85, fmt='o', label='0b + 0c')
plt.errorbar(pt_bins, sf_0b1c_85, xerr = pt_bin_width/2, yerr=err_sf_0b1c_85, fmt='o', label='0b + >=1c')
plt.errorbar(pt_bins, sf_0b2c_85, xerr = pt_bin_width/2, yerr=err_sf_0b2c_85, fmt='o', label='0b + >=2c')
plt.errorbar(pt_bins, sf_1b0c_85, xerr = pt_bin_width/2, yerr=err_sf_1b0c_85, fmt='o', label='1b + 0c')
plt.errorbar(pt_bins, sf_2b0c_85, xerr = pt_bin_width/2, yerr=err_sf_2b0c_85, fmt='o', label='>=2b + 0c')
plt.errorbar(pt_bins, sf_1b1c_85, xerr = pt_bin_width/2, yerr=err_sf_1b1c_85, fmt='o', label='1b + >=1c')

plt.text(0.02, 0.97, 'ATLAS', fontsize=20, fontweight='bold', fontstyle='italic', color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.09, 0.97, 'Simulation Preliminary', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.95, r'$\sqrt{s}$ = 13 TeV, Anti-$k_t$ R = 1.0 UFO jets', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.92, r'$\Delta$R = 1.0 track-jet association', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.90, r'GN2X Scores, QCD Samples, 85% H($b\bar{b}$) WP', fontsize=20, color='black', ha='left', va='top', transform=plt.gca().transAxes)


plt.xlabel(r'$p_T$ [GeV]',  ha = 'right', fontsize=18)
plt.ylabel('Scale Factor',  ha = 'right', fontsize=18)
plt.legend(fontsize=18)
plt.grid(True)


plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.ylim(0.4, 1.9)


#add text on the plot with central value and error value
for i in range(len(pt_bins)):
 plt.text(pt_bins[i], sf_0b0c_85[i], str(round(sf_0b0c_85[i],2)) + '+/-' + str(round(err_sf_0b0c_85[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_0b1c_85[i], str(round(sf_0b1c_85[i],2)) + '+/-' + str(round(err_sf_0b1c_85[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_0b2c_85[i], str(round(sf_0b2c_85[i],2)) + '+/-' + str(round(err_sf_0b2c_85[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_1b0c_85[i], str(round(sf_1b0c_85[i],2)) + '+/-' + str(round(err_sf_1b0c_85[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_2b0c_85[i], str(round(sf_2b0c_85[i],2)) + '+/-' + str(round(err_sf_2b0c_85[i],2)), fontsize=14)
 plt.text(pt_bins[i], sf_1b1c_85[i], str(round(sf_1b1c_85[i],2)) + '+/-' + str(round(err_sf_1b1c_85[i],2)), fontsize=14)
   

plt.savefig('final_sf_85.png', dpi=1000)

# produce for each category in a separate subplot the SF as a function of pt with the error bars
# adding in the same plot for each category all the cases (50, 60, 70, 85)

fig, axs = plt.subplots(3, 2, figsize=(24, 24))
axs[0, 0].errorbar(pt_bins, sf_0b0c_50, xerr = pt_bin_width/2, yerr=err_sf_0b0c_50, fmt='o', label=r'50% H($b\bar{b}$) WP')
axs[0, 0].errorbar(pt_bins, sf_0b0c_60, xerr = pt_bin_width/2, yerr=err_sf_0b0c_60, fmt='o', label=r'60% H($b\bar{b}$) WP')
axs[0, 0].errorbar(pt_bins, sf_0b0c_70, xerr = pt_bin_width/2, yerr=err_sf_0b0c_70, fmt='o', label=r'70% H($b\bar{b}$) WP')
axs[0, 0].errorbar(pt_bins, sf_0b0c_85, xerr = pt_bin_width/2, yerr=err_sf_0b0c_85, fmt='o', label=r'85% H($b\bar{b}$) WP')

axs[0, 0].grid(True)
axs[0, 0].tick_params(axis='both', which='major', labelsize=25)
axs[0, 0].set_xlabel(r'$p_T$ [GeV]',  ha = 'right', fontsize=25)
axs[0, 0].set_ylabel('Scale Factor',  ha = 'right', fontsize=25)

axs[0, 1].errorbar(pt_bins, sf_0b1c_50, xerr = pt_bin_width/2, yerr=err_sf_0b1c_50, fmt='o', label=r'50% H($b\bar{b}$) WP')
axs[0, 1].errorbar(pt_bins, sf_0b1c_60, xerr = pt_bin_width/2, yerr=err_sf_0b1c_60, fmt='o', label=r'60% H($b\bar{b}$) WP')
axs[0, 1].errorbar(pt_bins, sf_0b1c_70, xerr = pt_bin_width/2, yerr=err_sf_0b1c_70, fmt='o', label=r'70% H($b\bar{b}$) WP')
axs[0, 1].errorbar(pt_bins, sf_0b1c_85, xerr = pt_bin_width/2, yerr=err_sf_0b1c_85, fmt='o', label=r'85% H($b\bar{b}$) WP')

axs[0, 1].grid(True)
axs[0, 1].tick_params(axis='both', which='major', labelsize=25)
axs[0, 1].set_xlabel(r'$p_T$ [GeV]',  ha = 'right', fontsize=25)
axs[0, 1].set_ylabel('Scale Factor',  ha = 'right', fontsize=25)


axs[1, 0].errorbar(pt_bins, sf_0b2c_50, xerr = pt_bin_width/2, yerr=err_sf_0b2c_50, fmt='o', label=r'50% H($b\bar{b}$) WP')
axs[1, 0].errorbar(pt_bins, sf_0b2c_60, xerr = pt_bin_width/2, yerr=err_sf_0b2c_60, fmt='o', label=r'60% H($b\bar{b}$) WP')
axs[1, 0].errorbar(pt_bins, sf_0b2c_70, xerr = pt_bin_width/2, yerr=err_sf_0b2c_70, fmt='o', label=r'70% H($b\bar{b}$) WP')
axs[1, 0].errorbar(pt_bins, sf_0b2c_85, xerr = pt_bin_width/2, yerr=err_sf_0b2c_85, fmt='o', label=r'85% H($b\bar{b}$) WP')

axs[1, 0].grid(True)
axs[1, 0].tick_params(axis='both', which='major', labelsize=25)
axs[1, 0].set_xlabel(r'$p_T$ [GeV]',  ha = 'right', fontsize=25)
axs[1, 0].set_ylabel('Scale Factor',  ha = 'right', fontsize=25)


axs[1, 1].errorbar(pt_bins, sf_1b0c_50, xerr = pt_bin_width/2, yerr=err_sf_1b0c_50, fmt='o', label=r'50% H($b\bar{b}$) WP')
axs[1, 1].errorbar(pt_bins, sf_1b0c_60, xerr = pt_bin_width/2, yerr=err_sf_1b0c_60, fmt='o', label=r'60% H($b\bar{b}$) WP')
axs[1, 1].errorbar(pt_bins, sf_1b0c_70, xerr = pt_bin_width/2, yerr=err_sf_1b0c_70, fmt='o', label=r'70% H($b\bar{b}$) WP')
axs[1, 1].errorbar(pt_bins, sf_1b0c_85, xerr = pt_bin_width/2, yerr=err_sf_1b0c_85, fmt='o', label=r'85% H($b\bar{b}$) WP')

axs[1, 1].grid(True)
axs[1, 1].tick_params(axis='both', which='major', labelsize=25)
axs[1, 1].set_xlabel(r'$p_T$ [GeV]',  ha = 'right', fontsize=25)
axs[1, 1].set_ylabel('Scale Factor',  ha = 'right', fontsize=25)


axs[2, 0].errorbar(pt_bins, sf_2b0c_50, xerr = pt_bin_width/2, yerr=err_sf_2b0c_50, fmt='o', label=r'50% H($b\bar{b}$) WP')
axs[2, 0].errorbar(pt_bins, sf_2b0c_60, xerr = pt_bin_width/2, yerr=err_sf_2b0c_60, fmt='o', label=r'60% H($b\bar{b}$) WP')
axs[2, 0].errorbar(pt_bins, sf_2b0c_70, xerr = pt_bin_width/2, yerr=err_sf_2b0c_70, fmt='o', label=r'70% H($b\bar{b}$) WP')
axs[2, 0].errorbar(pt_bins, sf_2b0c_85, xerr = pt_bin_width/2, yerr=err_sf_2b0c_85, fmt='o', label=r'85% H($b\bar{b}$) WP')

axs[2, 0].grid(True)
axs[2, 0].tick_params(axis='both', which='major', labelsize=25)
axs[2, 0].set_xlabel(r'$p_T$ [GeV]',  ha = 'right', fontsize=25)
axs[2, 0].set_ylabel('Scale Factor',  ha = 'right', fontsize=25)


axs[2, 1].errorbar(pt_bins, sf_1b1c_50, xerr = pt_bin_width/2, yerr=err_sf_1b1c_50, fmt='o', label=r'50% H($b\bar{b}$) WP')
axs[2, 1].errorbar(pt_bins, sf_1b1c_60, xerr = pt_bin_width/2, yerr=err_sf_1b1c_60, fmt='o', label=r'60% H($b\bar{b}$) WP')
axs[2, 1].errorbar(pt_bins, sf_1b1c_70, xerr = pt_bin_width/2, yerr=err_sf_1b1c_70, fmt='o', label=r'70% H($b\bar{b}$) WP')
axs[2, 1].errorbar(pt_bins, sf_1b1c_85, xerr = pt_bin_width/2, yerr=err_sf_1b1c_85, fmt='o', label=r'85% H($b\bar{b}$) WP')

axs[2, 1].grid(True)
axs[2, 1].tick_params(axis='both', which='major', labelsize=25)
axs[2, 1].set_xlabel(r'$p_T$ [GeV]',  ha = 'right', fontsize=25)
axs[2, 1].set_ylabel('Scale Factor',  ha = 'right', fontsize=25)

title_fontsize = 30

# Adjust the space between subplots
fig.subplots_adjust(hspace=0.5)  # You can adjust the value as needed
fig.subplots_adjust(wspace=0.5)  # You can adjust the value as needed
# Rest of your code remains unchanged

# Modify the title font size
axs[0, 0].set_title('0b + 0c', fontsize=title_fontsize)
axs[0, 1].set_title('0b + >=1c', fontsize=title_fontsize)
axs[1, 0].set_title('0b + >=2c', fontsize=title_fontsize)
axs[1, 1].set_title('1b + 0c', fontsize=title_fontsize)
axs[2, 0].set_title('>=2b + 0c', fontsize=title_fontsize)
axs[2, 1].set_title('1b + >=1c', fontsize=title_fontsize)
fig.tight_layout()
plt.savefig('final_sf_all_wp.png', dpi=1000)


with open('scale_factor_values.txt', 'w') as file:
    for wp in [50, 60, 70, 85]:
        file.write(str(wp) + '\n')
        file.write('0b0c: sf_0b0c_' + str(wp) + ', err_sf_0b0c_' + str(wp) + ', values: ' + str(sf_0b0c_85) + ', ' + str(err_sf_0b0c_85) + '\n')
        file.write('0b1c: sf_0b1c_' + str(wp) + ', err_sf_0b1c_' + str(wp) + ', values: ' + str(sf_0b1c_85) + ', ' + str(err_sf_0b1c_85) + '\n')
        file.write('0b2c: sf_0b2c_' + str(wp) + ', err_sf_0b2c_' + str(wp) + ', values: ' + str(sf_0b2c_85) + ', ' + str(err_sf_0b2c_85) + '\n')
        file.write('1b0c: sf_1b0c_' + str(wp) + ', err_sf_1b0c_' + str(wp) + ', values: ' + str(sf_1b0c_85) + ', ' + str(err_sf_1b0c_85) + '\n')
        file.write('2b0c: sf_2b0c_' + str(wp) + ', err_sf_2b0c_' + str(wp) + ', values: ' + str(sf_2b0c_85) + ', ' + str(err_sf_2b0c_85) + '\n')
        file.write('1b1c: sf_1b1c_' + str(wp) + ', err_sf_1b1c_' + str(wp) + ', values: ' + str(sf_1b1c_85) + ', ' + str(err_sf_1b1c_85) + '\n')
        file.write('\n')
        
