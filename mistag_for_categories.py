import numpy as np
import matplotlib.pyplot as plt
import h5py

weights_slice3 = 26450000 * 1.165833e-2
weights_slice4 = 254610 * 1.336553e-2
weights_slice5 = 4553.2 * 1.452648e-2

discr_cut_50 = 4.335
discr_cut_55 = 4.087
discr_cut_60 = 3.818
discr_cut_65 = 3.518
discr_cut_70 = 3.166
discr_cut_75 = 2.735
discr_cut_80 = 2.211
discr_cut_85 = 1.560

discr_cut = discr_cut_70
wp = 70

mistag_eff_nominal_filename = f'last_eff_nominal_{wp}.png'
mistag_eff_trkd0_filename = f'last_eff_trkd0_{wp}.png'

pt_values = np.asarray([250,300, 350, 450 ,600,800,1000,1500])
pt_bins = (pt_values[1:] + pt_values[:-1])/2
pt_bin_width = pt_values[1:] - pt_values[:-1]

def compute_final_weights(w, w_lumi, total_w):        
        return w * w_lumi / total_w


# Load the data

#slice3 nominal
with h5py.File('../files/discriminant_values_nominal3_2.h5', 'r') as f:
    discriminant_1b0c_3n = f['discriminant_1b0c'][:]
    discriminant_2b0c_3n = f['discriminant_2b0c'][:]
    discriminant_1b1c_3n = f['discriminant_1b1c'][:]
    discriminant_0b1c_3n = f['discriminant_0b1c'][:]
    discriminant_0b2c_3n = f['discriminant_0b2c'][:]
    discriminant_0b0c_3n = f['discriminant_0b0c'][:]
    mc_weights_1b0c_3n = f['mc_weights_1b0c'][:] 
    mc_weights_2b0c_3n = f['mc_weights_2b0c'][:]
    mc_weights_1b1c_3n = f['mc_weights_1b1c'][:]
    mc_weights_0b1c_3n = f['mc_weights_0b1c'][:] 
    mc_weights_0b2c_3n = f['mc_weights_0b2c'][:] 
    mc_weights_0b0c_3n = f['mc_weights_0b0c'][:] 
    pt_values_1b0c_3n = f['pt_values_1b0c'][:] 
    pt_values_2b0c_3n = f['pt_values_2b0c'][:]
    pt_values_1b1c_3n = f['pt_values_1b1c'][:]
    pt_values_0b1c_3n = f['pt_values_0b1c'][:]
    pt_values_0b2c_3n = f['pt_values_0b2c'][:]
    pt_values_0b0c_3n = f['pt_values_0b0c'][:]
    total_w_nominal3 = f['total_weights'][:]
    

mc_weights_1b0c_3n = compute_final_weights(mc_weights_1b0c_3n, weights_slice3, total_w_nominal3)
mc_weights_2b0c_3n = compute_final_weights(mc_weights_2b0c_3n, weights_slice3, total_w_nominal3)
mc_weights_1b1c_3n = compute_final_weights(mc_weights_1b1c_3n, weights_slice3, total_w_nominal3)
mc_weights_0b1c_3n = compute_final_weights(mc_weights_0b1c_3n, weights_slice3, total_w_nominal3)
mc_weights_0b2c_3n = compute_final_weights(mc_weights_0b2c_3n, weights_slice3, total_w_nominal3)
mc_weights_0b0c_3n = compute_final_weights(mc_weights_0b0c_3n, weights_slice3, total_w_nominal3)

#slice4 nominal
with h5py.File('../files/discriminant_values_nominal4.h5', 'r') as f:
    discriminant_1b0c_4n = f['discriminant_1b0c'][:]
    discriminant_2b0c_4n = f['discriminant_2b0c'][:]
    discriminant_1b1c_4n = f['discriminant_1b1c'][:]
    discriminant_0b1c_4n = f['discriminant_0b1c'][:]
    discriminant_0b2c_4n = f['discriminant_0b2c'][:]
    discriminant_0b0c_4n = f['discriminant_0b0c'][:]
    mc_weights_1b0c_4n = f['mc_weights_1b0c'][:]
    mc_weights_2b0c_4n = f['mc_weights_2b0c'][:]
    mc_weights_1b1c_4n = f['mc_weights_1b1c'][:]
    mc_weights_0b1c_4n = f['mc_weights_0b1c'][:]
    mc_weights_0b2c_4n = f['mc_weights_0b2c'][:]
    mc_weights_0b0c_4n = f['mc_weights_0b0c'][:]
    pt_values_1b0c_4n = f['pt_values_1b0c'][:]
    pt_values_2b0c_4n = f['pt_values_2b0c'][:]
    pt_values_1b1c_4n = f['pt_values_1b1c'][:]
    pt_values_0b1c_4n = f['pt_values_0b1c'][:]
    pt_values_0b2c_4n = f['pt_values_0b2c'][:]
    pt_values_0b0c_4n = f['pt_values_0b0c'][:]
    total_w_nominal4 = f['total_weights'][:]


mc_weights_1b0c_4n = compute_final_weights(mc_weights_1b0c_4n, weights_slice4, total_w_nominal4)
mc_weights_2b0c_4n = compute_final_weights(mc_weights_2b0c_4n, weights_slice4, total_w_nominal4)
mc_weights_1b1c_4n = compute_final_weights(mc_weights_1b1c_4n, weights_slice4, total_w_nominal4)
mc_weights_0b1c_4n = compute_final_weights(mc_weights_0b1c_4n, weights_slice4, total_w_nominal4)
mc_weights_0b2c_4n = compute_final_weights(mc_weights_0b2c_4n, weights_slice4, total_w_nominal4)
mc_weights_0b0c_4n = compute_final_weights(mc_weights_0b0c_4n, weights_slice4, total_w_nominal4)

#slice5 nominal
with h5py.File('../files/discriminant_values_nominal5_2.h5', 'r') as f:
    discriminant_1b0c_5n = f['discriminant_1b0c'][:]
    discriminant_2b0c_5n = f['discriminant_2b0c'][:]
    discriminant_1b1c_5n = f['discriminant_1b1c'][:]
    discriminant_0b1c_5n = f['discriminant_0b1c'][:]
    discriminant_0b2c_5n = f['discriminant_0b2c'][:]
    discriminant_0b0c_5n = f['discriminant_0b0c'][:]
    mc_weights_1b0c_5n = f['mc_weights_1b0c'][:]
    mc_weights_2b0c_5n = f['mc_weights_2b0c'][:]
    mc_weights_1b1c_5n = f['mc_weights_1b1c'][:]
    mc_weights_0b1c_5n = f['mc_weights_0b1c'][:]
    mc_weights_0b2c_5n = f['mc_weights_0b2c'][:]
    mc_weights_0b0c_5n = f['mc_weights_0b0c'][:]
    pt_values_1b0c_5n = f['pt_values_1b0c'][:]
    pt_values_2b0c_5n = f['pt_values_2b0c'][:]
    pt_values_1b1c_5n = f['pt_values_1b1c'][:]
    pt_values_0b1c_5n = f['pt_values_0b1c'][:]
    pt_values_0b2c_5n = f['pt_values_0b2c'][:]
    pt_values_0b0c_5n = f['pt_values_0b0c'][:]
    total_w_nominal5 = f['total_weights'][:]



mc_weights_1b0c_5n = compute_final_weights(mc_weights_1b0c_5n, weights_slice5, total_w_nominal5)
mc_weights_2b0c_5n = compute_final_weights(mc_weights_2b0c_5n, weights_slice5, total_w_nominal5)
mc_weights_1b1c_5n = compute_final_weights(mc_weights_1b1c_5n, weights_slice5, total_w_nominal5)
mc_weights_0b1c_5n = compute_final_weights(mc_weights_0b1c_5n, weights_slice5, total_w_nominal5)
mc_weights_0b2c_5n = compute_final_weights(mc_weights_0b2c_5n, weights_slice5, total_w_nominal5)
mc_weights_0b0c_5n = compute_final_weights(mc_weights_0b0c_5n, weights_slice5, total_w_nominal5)


total_mc_weights_1b0c = np.concatenate((mc_weights_1b0c_3n, mc_weights_1b0c_4n, mc_weights_1b0c_5n))
total_mc_weights_2b0c = np.concatenate((mc_weights_2b0c_3n, mc_weights_2b0c_4n, mc_weights_2b0c_5n))
total_mc_weights_1b1c = np.concatenate((mc_weights_1b1c_3n, mc_weights_1b1c_4n, mc_weights_1b1c_5n))
total_mc_weights_0b1c = np.concatenate((mc_weights_0b1c_3n, mc_weights_0b1c_4n, mc_weights_0b1c_5n))
total_mc_weights_0b2c = np.concatenate((mc_weights_0b2c_3n, mc_weights_0b2c_4n, mc_weights_0b2c_5n))
total_mc_weights_0b0c = np.concatenate((mc_weights_0b0c_3n, mc_weights_0b0c_4n, mc_weights_0b0c_5n))

total_pt_values_1b0c = np.concatenate((pt_values_1b0c_3n, pt_values_1b0c_4n, pt_values_1b0c_5n))
total_pt_values_2b0c = np.concatenate((pt_values_2b0c_3n, pt_values_2b0c_4n, pt_values_2b0c_5n))
total_pt_values_1b1c = np.concatenate((pt_values_1b1c_3n, pt_values_1b1c_4n, pt_values_1b1c_5n))
total_pt_values_0b1c = np.concatenate((pt_values_0b1c_3n, pt_values_0b1c_4n, pt_values_0b1c_5n))
total_pt_values_0b2c = np.concatenate((pt_values_0b2c_3n, pt_values_0b2c_4n, pt_values_0b2c_5n))
total_pt_values_0b0c = np.concatenate((pt_values_0b0c_3n, pt_values_0b0c_4n, pt_values_0b0c_5n))

total_discriminant_1b0c = np.concatenate((discriminant_1b0c_3n, discriminant_1b0c_4n, discriminant_1b0c_5n))
total_discriminant_2b0c = np.concatenate((discriminant_2b0c_3n, discriminant_2b0c_4n, discriminant_2b0c_5n))
total_discriminant_1b1c = np.concatenate((discriminant_1b1c_3n, discriminant_1b1c_4n, discriminant_1b1c_5n))
total_discriminant_0b1c = np.concatenate((discriminant_0b1c_3n, discriminant_0b1c_4n, discriminant_0b1c_5n))
total_discriminant_0b2c = np.concatenate((discriminant_0b2c_3n, discriminant_0b2c_4n, discriminant_0b2c_5n))
total_discriminant_0b0c = np.concatenate((discriminant_0b0c_3n, discriminant_0b0c_4n, discriminant_0b0c_5n))




# counting selected and non selected weighted events for each category
# as mistag = selected / (selected + non_selected)
# mistag error for each bin computed as the square root of the sum of the squared weights 

def compute_weightedcount_error(weights_array):
    squared_weights = weights_array**2
    err_weighted_square = np.sum(squared_weights)
    return np.sqrt(err_weighted_square)

selected_pt_1b0c_3n = pt_values_1b0c_3n[discriminant_1b0c_3n > discr_cut]
selected_pt_2b0c_3n = pt_values_2b0c_3n[discriminant_2b0c_3n > discr_cut]
selected_pt_1b1c_3n = pt_values_1b1c_3n[discriminant_1b1c_3n > discr_cut]
selected_pt_0b1c_3n = pt_values_0b1c_3n[discriminant_0b1c_3n > discr_cut]
selected_pt_0b2c_3n = pt_values_0b2c_3n[discriminant_0b2c_3n > discr_cut]
selected_pt_0b0c_3n = pt_values_0b0c_3n[discriminant_0b0c_3n > discr_cut]

selected_weights_1b0c_3n = mc_weights_1b0c_3n[discriminant_1b0c_3n > discr_cut]
selected_weights_2b0c_3n = mc_weights_2b0c_3n[discriminant_2b0c_3n > discr_cut]
selected_weights_1b1c_3n = mc_weights_1b1c_3n[discriminant_1b1c_3n > discr_cut]
selected_weights_0b1c_3n = mc_weights_0b1c_3n[discriminant_0b1c_3n > discr_cut]
selected_weights_0b2c_3n = mc_weights_0b2c_3n[discriminant_0b2c_3n > discr_cut]
selected_weights_0b0c_3n = mc_weights_0b0c_3n[discriminant_0b0c_3n > discr_cut]

selected_pt_1b1c_4n = pt_values_1b1c_4n[discriminant_1b1c_4n > discr_cut]
selected_pt_0b1c_4n = pt_values_0b1c_4n[discriminant_0b1c_4n > discr_cut]
selected_pt_0b2c_4n = pt_values_0b2c_4n[discriminant_0b2c_4n > discr_cut]
selected_pt_0b0c_4n = pt_values_0b0c_4n[discriminant_0b0c_4n > discr_cut]
selected_pt_2b0c_4n = pt_values_2b0c_4n[discriminant_2b0c_4n > discr_cut]
selected_pt_1b0c_4n = pt_values_1b0c_4n[discriminant_1b0c_4n > discr_cut]

selected_weights_1b1c_4n = mc_weights_1b1c_4n[discriminant_1b1c_4n > discr_cut]
selected_weights_0b1c_4n = mc_weights_0b1c_4n[discriminant_0b1c_4n > discr_cut]
selected_weights_0b2c_4n = mc_weights_0b2c_4n[discriminant_0b2c_4n > discr_cut]
selected_weights_0b0c_4n = mc_weights_0b0c_4n[discriminant_0b0c_4n > discr_cut]
selected_weights_2b0c_4n = mc_weights_2b0c_4n[discriminant_2b0c_4n > discr_cut]
selected_weights_1b0c_4n = mc_weights_1b0c_4n[discriminant_1b0c_4n > discr_cut]

selected_pt_1b1c_5n = pt_values_1b1c_5n[discriminant_1b1c_5n > discr_cut]
selected_pt_0b1c_5n = pt_values_0b1c_5n[discriminant_0b1c_5n > discr_cut]
selected_pt_0b2c_5n = pt_values_0b2c_5n[discriminant_0b2c_5n > discr_cut]
selected_pt_0b0c_5n = pt_values_0b0c_5n[discriminant_0b0c_5n > discr_cut]
selected_pt_2b0c_5n = pt_values_2b0c_5n[discriminant_2b0c_5n > discr_cut]
selected_pt_1b0c_5n = pt_values_1b0c_5n[discriminant_1b0c_5n > discr_cut]

selected_weights_1b1c_5n = mc_weights_1b1c_5n[discriminant_1b1c_5n > discr_cut]
selected_weights_0b1c_5n = mc_weights_0b1c_5n[discriminant_0b1c_5n > discr_cut]
selected_weights_0b2c_5n = mc_weights_0b2c_5n[discriminant_0b2c_5n > discr_cut]
selected_weights_0b0c_5n = mc_weights_0b0c_5n[discriminant_0b0c_5n > discr_cut]
selected_weights_2b0c_5n = mc_weights_2b0c_5n[discriminant_2b0c_5n > discr_cut]
selected_weights_1b0c_5n = mc_weights_1b0c_5n[discriminant_1b0c_5n > discr_cut]

nonselected_pt_1b0c_3n = pt_values_1b0c_3n[discriminant_1b0c_3n <= discr_cut]   
nonselected_pt_2b0c_3n = pt_values_2b0c_3n[discriminant_2b0c_3n <= discr_cut]
nonselected_pt_1b1c_3n = pt_values_1b1c_3n[discriminant_1b1c_3n <= discr_cut]
nonselected_pt_0b1c_3n = pt_values_0b1c_3n[discriminant_0b1c_3n <= discr_cut]
nonselected_pt_0b2c_3n = pt_values_0b2c_3n[discriminant_0b2c_3n <= discr_cut]
nonselected_pt_0b0c_3n = pt_values_0b0c_3n[discriminant_0b0c_3n <= discr_cut]

nonselected_weights_1b0c_3n = mc_weights_1b0c_3n[discriminant_1b0c_3n <= discr_cut]
nonselected_weights_2b0c_3n = mc_weights_2b0c_3n[discriminant_2b0c_3n <= discr_cut]
nonselected_weights_1b1c_3n = mc_weights_1b1c_3n[discriminant_1b1c_3n <= discr_cut]
nonselected_weights_0b1c_3n = mc_weights_0b1c_3n[discriminant_0b1c_3n <= discr_cut]
nonselected_weights_0b2c_3n = mc_weights_0b2c_3n[discriminant_0b2c_3n <= discr_cut]
nonselected_weights_0b0c_3n = mc_weights_0b0c_3n[discriminant_0b0c_3n <= discr_cut]

nonselected_pt_1b1c_4n = pt_values_1b1c_4n[discriminant_1b1c_4n <= discr_cut]
nonselected_pt_0b1c_4n = pt_values_0b1c_4n[discriminant_0b1c_4n <= discr_cut]
nonselected_pt_0b2c_4n = pt_values_0b2c_4n[discriminant_0b2c_4n <= discr_cut]
nonselected_pt_0b0c_4n = pt_values_0b0c_4n[discriminant_0b0c_4n <= discr_cut]
nonselected_pt_2b0c_4n = pt_values_2b0c_4n[discriminant_2b0c_4n <= discr_cut]
nonselected_pt_1b0c_4n = pt_values_1b0c_4n[discriminant_1b0c_4n <= discr_cut]

nonselected_weights_1b1c_4n = mc_weights_1b1c_4n[discriminant_1b1c_4n <= discr_cut]
nonselected_weights_0b1c_4n = mc_weights_0b1c_4n[discriminant_0b1c_4n <= discr_cut]
nonselected_weights_0b2c_4n = mc_weights_0b2c_4n[discriminant_0b2c_4n <= discr_cut]
nonselected_weights_0b0c_4n = mc_weights_0b0c_4n[discriminant_0b0c_4n <= discr_cut]
nonselected_weights_2b0c_4n = mc_weights_2b0c_4n[discriminant_2b0c_4n <= discr_cut]
nonselected_weights_1b0c_4n = mc_weights_1b0c_4n[discriminant_1b0c_4n <= discr_cut]

nonselected_pt_1b1c_5n = pt_values_1b1c_5n[discriminant_1b1c_5n <= discr_cut]
nonselected_pt_0b1c_5n = pt_values_0b1c_5n[discriminant_0b1c_5n <= discr_cut]
nonselected_pt_0b2c_5n = pt_values_0b2c_5n[discriminant_0b2c_5n <= discr_cut]
nonselected_pt_0b0c_5n = pt_values_0b0c_5n[discriminant_0b0c_5n <= discr_cut]
nonselected_pt_2b0c_5n = pt_values_2b0c_5n[discriminant_2b0c_5n <= discr_cut]
nonselected_pt_1b0c_5n = pt_values_1b0c_5n[discriminant_1b0c_5n <= discr_cut]

nonselected_weights_1b1c_5n = mc_weights_1b1c_5n[discriminant_1b1c_5n <= discr_cut]
nonselected_weights_0b1c_5n = mc_weights_0b1c_5n[discriminant_0b1c_5n <= discr_cut]
nonselected_weights_0b2c_5n = mc_weights_0b2c_5n[discriminant_0b2c_5n <= discr_cut]
nonselected_weights_0b0c_5n = mc_weights_0b0c_5n[discriminant_0b0c_5n <= discr_cut]
nonselected_weights_2b0c_5n = mc_weights_2b0c_5n[discriminant_2b0c_5n <= discr_cut]
nonselected_weights_1b0c_5n = mc_weights_1b0c_5n[discriminant_1b0c_5n <= discr_cut]

# digitize pt values to get the bin indices
pt_bin_indices_selected_1b0c_3n = np.digitize(selected_pt_1b0c_3n, pt_values)
pt_bin_indices_selected_2b0c_3n = np.digitize(selected_pt_2b0c_3n, pt_values)
pt_bin_indices_selected_1b1c_3n = np.digitize(selected_pt_1b1c_3n, pt_values)
pt_bin_indices_selected_0b1c_3n = np.digitize(selected_pt_0b1c_3n, pt_values)
pt_bin_indices_selected_0b2c_3n = np.digitize(selected_pt_0b2c_3n, pt_values)
pt_bin_indices_selected_0b0c_3n = np.digitize(selected_pt_0b0c_3n, pt_values)

pt_bin_indices_selected_1b1c_4n = np.digitize(selected_pt_1b1c_4n, pt_values)   
pt_bin_indices_selected_0b1c_4n = np.digitize(selected_pt_0b1c_4n, pt_values)
pt_bin_indices_selected_0b2c_4n = np.digitize(selected_pt_0b2c_4n, pt_values)
pt_bin_indices_selected_0b0c_4n = np.digitize(selected_pt_0b0c_4n, pt_values)
pt_bin_indices_selected_2b0c_4n = np.digitize(selected_pt_2b0c_4n, pt_values)
pt_bin_indices_selected_1b0c_4n = np.digitize(selected_pt_1b0c_4n, pt_values)

pt_bin_indices_selected_1b1c_5n = np.digitize(selected_pt_1b1c_5n, pt_values)
pt_bin_indices_selected_0b1c_5n = np.digitize(selected_pt_0b1c_5n, pt_values)
pt_bin_indices_selected_0b2c_5n = np.digitize(selected_pt_0b2c_5n, pt_values)
pt_bin_indices_selected_0b0c_5n = np.digitize(selected_pt_0b0c_5n, pt_values)
pt_bin_indices_selected_2b0c_5n = np.digitize(selected_pt_2b0c_5n, pt_values)
pt_bin_indices_selected_1b0c_5n = np.digitize(selected_pt_1b0c_5n, pt_values)

pt_bin_indices_nonselected_1b0c_3n = np.digitize(nonselected_pt_1b0c_3n, pt_values)
pt_bin_indices_nonselected_2b0c_3n = np.digitize(nonselected_pt_2b0c_3n, pt_values)
pt_bin_indices_nonselected_1b1c_3n = np.digitize(nonselected_pt_1b1c_3n, pt_values)
pt_bin_indices_nonselected_0b1c_3n = np.digitize(nonselected_pt_0b1c_3n, pt_values)
pt_bin_indices_nonselected_0b2c_3n = np.digitize(nonselected_pt_0b2c_3n, pt_values)
pt_bin_indices_nonselected_0b0c_3n = np.digitize(nonselected_pt_0b0c_3n, pt_values)

pt_bin_indices_nonselected_1b1c_4n = np.digitize(nonselected_pt_1b1c_4n, pt_values)
pt_bin_indices_nonselected_0b1c_4n = np.digitize(nonselected_pt_0b1c_4n, pt_values)
pt_bin_indices_nonselected_0b2c_4n = np.digitize(nonselected_pt_0b2c_4n, pt_values)
pt_bin_indices_nonselected_0b0c_4n = np.digitize(nonselected_pt_0b0c_4n, pt_values)
pt_bin_indices_nonselected_2b0c_4n = np.digitize(nonselected_pt_2b0c_4n, pt_values)
pt_bin_indices_nonselected_1b0c_4n = np.digitize(nonselected_pt_1b0c_4n, pt_values)

pt_bin_indices_nonselected_1b1c_5n = np.digitize(nonselected_pt_1b1c_5n, pt_values)
pt_bin_indices_nonselected_0b1c_5n = np.digitize(nonselected_pt_0b1c_5n, pt_values)
pt_bin_indices_nonselected_0b2c_5n = np.digitize(nonselected_pt_0b2c_5n, pt_values)
pt_bin_indices_nonselected_0b0c_5n = np.digitize(nonselected_pt_0b0c_5n, pt_values)
pt_bin_indices_nonselected_2b0c_5n = np.digitize(nonselected_pt_2b0c_5n, pt_values)
pt_bin_indices_nonselected_1b0c_5n = np.digitize(nonselected_pt_1b0c_5n, pt_values)

# count the number of selected and non selected events in each bin
nsel_1b1c_n3 = []
nsel_0b1c_n3 = []
nsel_0b2c_n3 = []
nsel_0b0c_n3 = []
nsel_2b0c_n3 = []
nsel_1b0c_n3 = []

nsel_1b1c_n4 = []
nsel_0b1c_n4 = []
nsel_0b2c_n4 = []
nsel_0b0c_n4 = []
nsel_2b0c_n4 = []
nsel_1b0c_n4 = []

nsel_1b1c_n5 = []
nsel_0b1c_n5 = []
nsel_0b2c_n5 = []
nsel_0b0c_n5 = []
nsel_2b0c_n5 = []
nsel_1b0c_n5 = []

err_nsel_1b1c_n3 = []
err_nsel_0b1c_n3 = []
err_nsel_0b2c_n3 = []
err_nsel_0b0c_n3 = []
err_nsel_2b0c_n3 = []
err_nsel_1b0c_n3 = []

err_nsel_1b1c_n4 = []
err_nsel_0b1c_n4 = []
err_nsel_0b2c_n4 = []
err_nsel_0b0c_n4 = []
err_nsel_2b0c_n4 = []
err_nsel_1b0c_n4 = []

err_nsel_1b1c_n5 = []
err_nsel_0b1c_n5 = []
err_nsel_0b2c_n5 = []
err_nsel_0b0c_n5 = []
err_nsel_2b0c_n5 = []
err_nsel_1b0c_n5 = []

nunsel_1b1c_n3 = []
nunsel_0b1c_n3 = []
nunsel_0b2c_n3 = []
nunsel_0b0c_n3 = []
nunsel_2b0c_n3 = []
nunsel_1b0c_n3 = []

nunsel_1b1c_n4 = []
nunsel_0b1c_n4 = []
nunsel_0b2c_n4 = []
nunsel_0b0c_n4 = []
nunsel_2b0c_n4 = []
nunsel_1b0c_n4 = []

nunsel_1b1c_n5 = []
nunsel_0b1c_n5 = []
nunsel_0b2c_n5 = []
nunsel_0b0c_n5 = []
nunsel_2b0c_n5 = []
nunsel_1b0c_n5 = []

err_nunsel_1b1c_n3 = []
err_nunsel_0b1c_n3 = []
err_nunsel_0b2c_n3 = []
err_nunsel_0b0c_n3 = []
err_nunsel_2b0c_n3 = []
err_nunsel_1b0c_n3 = []

err_nunsel_1b1c_n4 = []
err_nunsel_0b1c_n4 = []
err_nunsel_0b2c_n4 = []
err_nunsel_0b0c_n4 = []
err_nunsel_2b0c_n4 = []
err_nunsel_1b0c_n4 = []

err_nunsel_1b1c_n5 = []
err_nunsel_0b1c_n5 = []
err_nunsel_0b2c_n5 = []
err_nunsel_0b0c_n5 = []
err_nunsel_2b0c_n5 = []
err_nunsel_1b0c_n5 = []


for i in range(0, len(pt_bins)):
        events_in_bin_1b1c_n3_sel = selected_weights_1b1c_3n[pt_bin_indices_selected_1b1c_3n == i]
        events_in_bin_0b1c_n3_sel = selected_weights_0b1c_3n[pt_bin_indices_selected_0b1c_3n == i]
        events_in_bin_0b2c_n3_sel = selected_weights_0b2c_3n[pt_bin_indices_selected_0b2c_3n == i]
        events_in_bin_0b0c_n3_sel = selected_weights_0b0c_3n[pt_bin_indices_selected_0b0c_3n == i]
        events_in_bin_2b0c_n3_sel = selected_weights_2b0c_3n[pt_bin_indices_selected_2b0c_3n == i]
        events_in_bin_1b0c_n3_sel = selected_weights_1b0c_3n[pt_bin_indices_selected_1b0c_3n == i]

        events_in_bin_1b1c_n3_unsel = nonselected_weights_1b1c_3n[pt_bin_indices_nonselected_1b1c_3n == i]
        events_in_bin_0b1c_n3_unsel = nonselected_weights_0b1c_3n[pt_bin_indices_nonselected_0b1c_3n == i]
        events_in_bin_0b2c_n3_unsel = nonselected_weights_0b2c_3n[pt_bin_indices_nonselected_0b2c_3n == i]
        events_in_bin_0b0c_n3_unsel = nonselected_weights_0b0c_3n[pt_bin_indices_nonselected_0b0c_3n == i]
        events_in_bin_2b0c_n3_unsel = nonselected_weights_2b0c_3n[pt_bin_indices_nonselected_2b0c_3n == i]
        events_in_bin_1b0c_n3_unsel = nonselected_weights_1b0c_3n[pt_bin_indices_nonselected_1b0c_3n == i]

        events_in_bin_1b1c_n4_sel = selected_weights_1b1c_4n[pt_bin_indices_selected_1b1c_4n == i]
        events_in_bin_0b1c_n4_sel = selected_weights_0b1c_4n[pt_bin_indices_selected_0b1c_4n == i]
        events_in_bin_0b2c_n4_sel = selected_weights_0b2c_4n[pt_bin_indices_selected_0b2c_4n == i]
        events_in_bin_0b0c_n4_sel = selected_weights_0b0c_4n[pt_bin_indices_selected_0b0c_4n == i]
        events_in_bin_2b0c_n4_sel = selected_weights_2b0c_4n[pt_bin_indices_selected_2b0c_4n == i]
        events_in_bin_1b0c_n4_sel = selected_weights_1b0c_4n[pt_bin_indices_selected_1b0c_4n == i]

        events_in_bin_1b1c_n4_unsel = nonselected_weights_1b1c_4n[pt_bin_indices_nonselected_1b1c_4n == i]
        events_in_bin_0b1c_n4_unsel = nonselected_weights_0b1c_4n[pt_bin_indices_nonselected_0b1c_4n == i]
        events_in_bin_0b2c_n4_unsel = nonselected_weights_0b2c_4n[pt_bin_indices_nonselected_0b2c_4n == i]
        events_in_bin_0b0c_n4_unsel = nonselected_weights_0b0c_4n[pt_bin_indices_nonselected_0b0c_4n == i]
        events_in_bin_2b0c_n4_unsel = nonselected_weights_2b0c_4n[pt_bin_indices_nonselected_2b0c_4n == i]
        events_in_bin_1b0c_n4_unsel = nonselected_weights_1b0c_4n[pt_bin_indices_nonselected_1b0c_4n == i]

        events_in_bin_1b1c_n5_sel = selected_weights_1b1c_5n[pt_bin_indices_selected_1b1c_5n == i]
        events_in_bin_0b1c_n5_sel = selected_weights_0b1c_5n[pt_bin_indices_selected_0b1c_5n == i]
        events_in_bin_0b2c_n5_sel = selected_weights_0b2c_5n[pt_bin_indices_selected_0b2c_5n == i]
        events_in_bin_0b0c_n5_sel = selected_weights_0b0c_5n[pt_bin_indices_selected_0b0c_5n == i]
        events_in_bin_2b0c_n5_sel = selected_weights_2b0c_5n[pt_bin_indices_selected_2b0c_5n == i]
        events_in_bin_1b0c_n5_sel = selected_weights_1b0c_5n[pt_bin_indices_selected_1b0c_5n == i]

        events_in_bin_1b1c_n5_unsel = nonselected_weights_1b1c_5n[pt_bin_indices_nonselected_1b1c_5n == i]
        events_in_bin_0b1c_n5_unsel = nonselected_weights_0b1c_5n[pt_bin_indices_nonselected_0b1c_5n == i]
        events_in_bin_0b2c_n5_unsel = nonselected_weights_0b2c_5n[pt_bin_indices_nonselected_0b2c_5n == i]
        events_in_bin_0b0c_n5_unsel = nonselected_weights_0b0c_5n[pt_bin_indices_nonselected_0b0c_5n == i]
        events_in_bin_2b0c_n5_unsel = nonselected_weights_2b0c_5n[pt_bin_indices_nonselected_2b0c_5n == i]
        events_in_bin_1b0c_n5_unsel = nonselected_weights_1b0c_5n[pt_bin_indices_nonselected_1b0c_5n == i]

        nsel_1b1c_n3.append(np.sum(events_in_bin_1b1c_n3_sel))
        nsel_0b1c_n3.append(np.sum(events_in_bin_0b1c_n3_sel))
        nsel_0b2c_n3.append(np.sum(events_in_bin_0b2c_n3_sel))
        nsel_0b0c_n3.append(np.sum(events_in_bin_0b0c_n3_sel))
        nsel_2b0c_n3.append(np.sum(events_in_bin_2b0c_n3_sel))
        nsel_1b0c_n3.append(np.sum(events_in_bin_1b0c_n3_sel))

        nsel_1b1c_n4.append(np.sum(events_in_bin_1b1c_n4_sel))
        nsel_0b1c_n4.append(np.sum(events_in_bin_0b1c_n4_sel))
        nsel_0b2c_n4.append(np.sum(events_in_bin_0b2c_n4_sel))
        nsel_0b0c_n4.append(np.sum(events_in_bin_0b0c_n4_sel))
        nsel_2b0c_n4.append(np.sum(events_in_bin_2b0c_n4_sel))
        nsel_1b0c_n4.append(np.sum(events_in_bin_1b0c_n4_sel))

        nsel_1b1c_n5.append(np.sum(events_in_bin_1b1c_n5_sel))
        nsel_0b1c_n5.append(np.sum(events_in_bin_0b1c_n5_sel))
        nsel_0b2c_n5.append(np.sum(events_in_bin_0b2c_n5_sel))
        nsel_0b0c_n5.append(np.sum(events_in_bin_0b0c_n5_sel))
        nsel_2b0c_n5.append(np.sum(events_in_bin_2b0c_n5_sel))
        nsel_1b0c_n5.append(np.sum(events_in_bin_1b0c_n5_sel))

        err_nsel_1b1c_n3.append(compute_weightedcount_error(events_in_bin_1b1c_n3_sel))
        err_nsel_0b1c_n3.append(compute_weightedcount_error(events_in_bin_0b1c_n3_sel))
        err_nsel_0b2c_n3.append(compute_weightedcount_error(events_in_bin_0b2c_n3_sel))
        err_nsel_0b0c_n3.append(compute_weightedcount_error(events_in_bin_0b0c_n3_sel))
        err_nsel_2b0c_n3.append(compute_weightedcount_error(events_in_bin_2b0c_n3_sel))
        err_nsel_1b0c_n3.append(compute_weightedcount_error(events_in_bin_1b0c_n3_sel))

        err_nsel_1b1c_n4.append(compute_weightedcount_error(events_in_bin_1b1c_n4_sel))
        err_nsel_0b1c_n4.append(compute_weightedcount_error(events_in_bin_0b1c_n4_sel))
        err_nsel_0b2c_n4.append(compute_weightedcount_error(events_in_bin_0b2c_n4_sel))
        err_nsel_0b0c_n4.append(compute_weightedcount_error(events_in_bin_0b0c_n4_sel))
        err_nsel_2b0c_n4.append(compute_weightedcount_error(events_in_bin_2b0c_n4_sel))
        err_nsel_1b0c_n4.append(compute_weightedcount_error(events_in_bin_1b0c_n4_sel))

        err_nsel_1b1c_n5.append(compute_weightedcount_error(events_in_bin_1b1c_n5_sel))
        err_nsel_0b1c_n5.append(compute_weightedcount_error(events_in_bin_0b1c_n5_sel))
        err_nsel_0b2c_n5.append(compute_weightedcount_error(events_in_bin_0b2c_n5_sel))
        err_nsel_0b0c_n5.append(compute_weightedcount_error(events_in_bin_0b0c_n5_sel))
        err_nsel_2b0c_n5.append(compute_weightedcount_error(events_in_bin_2b0c_n5_sel))
        err_nsel_1b0c_n5.append(compute_weightedcount_error(events_in_bin_1b0c_n5_sel))

        nunsel_1b1c_n3.append(np.sum(events_in_bin_1b1c_n3_unsel))
        nunsel_0b1c_n3.append(np.sum(events_in_bin_0b1c_n3_unsel))
        nunsel_0b2c_n3.append(np.sum(events_in_bin_0b2c_n3_unsel))
        nunsel_0b0c_n3.append(np.sum(events_in_bin_0b0c_n3_unsel))
        nunsel_2b0c_n3.append(np.sum(events_in_bin_2b0c_n3_unsel))
        nunsel_1b0c_n3.append(np.sum(events_in_bin_1b0c_n3_unsel))

        nunsel_1b1c_n4.append(np.sum(events_in_bin_1b1c_n4_unsel))
        nunsel_0b1c_n4.append(np.sum(events_in_bin_0b1c_n4_unsel))
        nunsel_0b2c_n4.append(np.sum(events_in_bin_0b2c_n4_unsel))
        nunsel_0b0c_n4.append(np.sum(events_in_bin_0b0c_n4_unsel))
        nunsel_2b0c_n4.append(np.sum(events_in_bin_2b0c_n4_unsel))
        nunsel_1b0c_n4.append(np.sum(events_in_bin_1b0c_n4_unsel))

        nunsel_1b1c_n5.append(np.sum(events_in_bin_1b1c_n5_unsel))
        nunsel_0b1c_n5.append(np.sum(events_in_bin_0b1c_n5_unsel))
        nunsel_0b2c_n5.append(np.sum(events_in_bin_0b2c_n5_unsel))
        nunsel_0b0c_n5.append(np.sum(events_in_bin_0b0c_n5_unsel))
        nunsel_2b0c_n5.append(np.sum(events_in_bin_2b0c_n5_unsel))
        nunsel_1b0c_n5.append(np.sum(events_in_bin_1b0c_n5_unsel))

        err_nunsel_1b1c_n3.append(compute_weightedcount_error(events_in_bin_1b1c_n3_unsel))
        err_nunsel_0b1c_n3.append(compute_weightedcount_error(events_in_bin_0b1c_n3_unsel))
        err_nunsel_0b2c_n3.append(compute_weightedcount_error(events_in_bin_0b2c_n3_unsel))
        err_nunsel_0b0c_n3.append(compute_weightedcount_error(events_in_bin_0b0c_n3_unsel))
        err_nunsel_2b0c_n3.append(compute_weightedcount_error(events_in_bin_2b0c_n3_unsel))
        err_nunsel_1b0c_n3.append(compute_weightedcount_error(events_in_bin_1b0c_n3_unsel))

        err_nunsel_1b1c_n4.append(compute_weightedcount_error(events_in_bin_1b1c_n4_unsel))
        err_nunsel_0b1c_n4.append(compute_weightedcount_error(events_in_bin_0b1c_n4_unsel))
        err_nunsel_0b2c_n4.append(compute_weightedcount_error(events_in_bin_0b2c_n4_unsel))
        err_nunsel_0b0c_n4.append(compute_weightedcount_error(events_in_bin_0b0c_n4_unsel))
        err_nunsel_2b0c_n4.append(compute_weightedcount_error(events_in_bin_2b0c_n4_unsel))
        err_nunsel_1b0c_n4.append(compute_weightedcount_error(events_in_bin_1b0c_n4_unsel))

        err_nunsel_1b1c_n5.append(compute_weightedcount_error(events_in_bin_1b1c_n5_unsel))
        err_nunsel_0b1c_n5.append(compute_weightedcount_error(events_in_bin_0b1c_n5_unsel))
        err_nunsel_0b2c_n5.append(compute_weightedcount_error(events_in_bin_0b2c_n5_unsel))
        err_nunsel_0b0c_n5.append(compute_weightedcount_error(events_in_bin_0b0c_n5_unsel))
        err_nunsel_2b0c_n5.append(compute_weightedcount_error(events_in_bin_2b0c_n5_unsel))
        err_nunsel_1b0c_n5.append(compute_weightedcount_error(events_in_bin_1b0c_n5_unsel))



nsel_1b1c_n3 = np.array(nsel_1b1c_n3)
nsel_0b1c_n3 = np.array(nsel_0b1c_n3)
nsel_0b2c_n3 = np.array(nsel_0b2c_n3)
nsel_0b0c_n3 = np.array(nsel_0b0c_n3)
nsel_2b0c_n3 = np.array(nsel_2b0c_n3)
nsel_1b0c_n3 = np.array(nsel_1b0c_n3)

nsel_1b1c_n4 = np.array(nsel_1b1c_n4)
nsel_0b1c_n4 = np.array(nsel_0b1c_n4)
nsel_0b2c_n4 = np.array(nsel_0b2c_n4)
nsel_0b0c_n4 = np.array(nsel_0b0c_n4)
nsel_2b0c_n4 = np.array(nsel_2b0c_n4)
nsel_1b0c_n4 = np.array(nsel_1b0c_n4)

nsel_1b1c_n5 = np.array(nsel_1b1c_n5)
nsel_0b1c_n5 = np.array(nsel_0b1c_n5)
nsel_0b2c_n5 = np.array(nsel_0b2c_n5)
nsel_0b0c_n5 = np.array(nsel_0b0c_n5)
nsel_2b0c_n5 = np.array(nsel_2b0c_n5)
nsel_1b0c_n5 = np.array(nsel_1b0c_n5)

err_nsel_1b1c_n3 = np.array(err_nsel_1b1c_n3)
err_nsel_0b1c_n3 = np.array(err_nsel_0b1c_n3)
err_nsel_0b2c_n3 = np.array(err_nsel_0b2c_n3)
err_nsel_0b0c_n3 = np.array(err_nsel_0b0c_n3)
err_nsel_2b0c_n3 = np.array(err_nsel_2b0c_n3)
err_nsel_1b0c_n3 = np.array(err_nsel_1b0c_n3)

err_nsel_1b1c_n4 = np.array(err_nsel_1b1c_n4)
err_nsel_0b1c_n4 = np.array(err_nsel_0b1c_n4)
err_nsel_0b2c_n4 = np.array(err_nsel_0b2c_n4)
err_nsel_0b0c_n4 = np.array(err_nsel_0b0c_n4)
err_nsel_2b0c_n4 = np.array(err_nsel_2b0c_n4)
err_nsel_1b0c_n4 = np.array(err_nsel_1b0c_n4)

err_nsel_1b1c_n5 = np.array(err_nsel_1b1c_n5)
err_nsel_0b1c_n5 = np.array(err_nsel_0b1c_n5)
err_nsel_0b2c_n5 = np.array(err_nsel_0b2c_n5)
err_nsel_0b0c_n5 = np.array(err_nsel_0b0c_n5)
err_nsel_2b0c_n5 = np.array(err_nsel_2b0c_n5)
err_nsel_1b0c_n5 = np.array(err_nsel_1b0c_n5)

nunsel_1b1c_n3 = np.array(nunsel_1b1c_n3)
nunsel_0b1c_n3 = np.array(nunsel_0b1c_n3)
nunsel_0b2c_n3 = np.array(nunsel_0b2c_n3)
nunsel_0b0c_n3 = np.array(nunsel_0b0c_n3)
nunsel_2b0c_n3 = np.array(nunsel_2b0c_n3)
nunsel_1b0c_n3 = np.array(nunsel_1b0c_n3)

nunsel_1b1c_n4 = np.array(nunsel_1b1c_n4)
nunsel_0b1c_n4 = np.array(nunsel_0b1c_n4)
nunsel_0b2c_n4 = np.array(nunsel_0b2c_n4)
nunsel_0b0c_n4 = np.array(nunsel_0b0c_n4)
nunsel_2b0c_n4 = np.array(nunsel_2b0c_n4)
nunsel_1b0c_n4 = np.array(nunsel_1b0c_n4)

nunsel_1b1c_n5 = np.array(nunsel_1b1c_n5)
nunsel_0b1c_n5 = np.array(nunsel_0b1c_n5)
nunsel_0b2c_n5 = np.array(nunsel_0b2c_n5)
nunsel_0b0c_n5 = np.array(nunsel_0b0c_n5)
nunsel_2b0c_n5 = np.array(nunsel_2b0c_n5)
nunsel_1b0c_n5 = np.array(nunsel_1b0c_n5)

err_nunsel_1b1c_n3 = np.array(err_nunsel_1b1c_n3)
err_nunsel_0b1c_n3 = np.array(err_nunsel_0b1c_n3)
err_nunsel_0b2c_n3 = np.array(err_nunsel_0b2c_n3)
err_nunsel_0b0c_n3 = np.array(err_nunsel_0b0c_n3)
err_nunsel_2b0c_n3 = np.array(err_nunsel_2b0c_n3)
err_nunsel_1b0c_n3 = np.array(err_nunsel_1b0c_n3)

err_nunsel_1b1c_n4 = np.array(err_nunsel_1b1c_n4)
err_nunsel_0b1c_n4 = np.array(err_nunsel_0b1c_n4)
err_nunsel_0b2c_n4 = np.array(err_nunsel_0b2c_n4)
err_nunsel_0b0c_n4 = np.array(err_nunsel_0b0c_n4)
err_nunsel_2b0c_n4 = np.array(err_nunsel_2b0c_n4)
err_nunsel_1b0c_n4 = np.array(err_nunsel_1b0c_n4)

err_nunsel_1b1c_n5 = np.array(err_nunsel_1b1c_n5)
err_nunsel_0b1c_n5 = np.array(err_nunsel_0b1c_n5)
err_nunsel_0b2c_n5 = np.array(err_nunsel_0b2c_n5)
err_nunsel_0b0c_n5 = np.array(err_nunsel_0b0c_n5)
err_nunsel_2b0c_n5 = np.array(err_nunsel_2b0c_n5)
err_nunsel_1b0c_n5 = np.array(err_nunsel_1b0c_n5)

## compute mistag ans errors

eff_1b1c_n3 = nsel_1b1c_n3 / (nsel_1b1c_n3 + nunsel_1b1c_n3)
eff_0b1c_n3 = nsel_0b1c_n3 / (nsel_0b1c_n3 + nunsel_0b1c_n3)
eff_0b2c_n3 = nsel_0b2c_n3 / (nsel_0b2c_n3 + nunsel_0b2c_n3)
eff_0b0c_n3 = nsel_0b0c_n3 / (nsel_0b0c_n3 + nunsel_0b0c_n3)
eff_2b0c_n3 = nsel_2b0c_n3 / (nsel_2b0c_n3 + nunsel_2b0c_n3)
eff_1b0c_n3 = nsel_1b0c_n3 / (nsel_1b0c_n3 + nunsel_1b0c_n3)

eff_1b1c_n4 = nsel_1b1c_n4 / (nsel_1b1c_n4 + nunsel_1b1c_n4)
eff_0b1c_n4 = nsel_0b1c_n4 / (nsel_0b1c_n4 + nunsel_0b1c_n4)
eff_0b2c_n4 = nsel_0b2c_n4 / (nsel_0b2c_n4 + nunsel_0b2c_n4)
eff_0b0c_n4 = nsel_0b0c_n4 / (nsel_0b0c_n4 + nunsel_0b0c_n4)
eff_2b0c_n4 = nsel_2b0c_n4 / (nsel_2b0c_n4 + nunsel_2b0c_n4)
eff_1b0c_n4 = nsel_1b0c_n4 / (nsel_1b0c_n4 + nunsel_1b0c_n4)

eff_1b1c_n5 = nsel_1b1c_n5 / (nsel_1b1c_n5 + nunsel_1b1c_n5)
eff_0b1c_n5 = nsel_0b1c_n5 / (nsel_0b1c_n5 + nunsel_0b1c_n5)
eff_0b2c_n5 = nsel_0b2c_n5 / (nsel_0b2c_n5 + nunsel_0b2c_n5)
eff_0b0c_n5 = nsel_0b0c_n5 / (nsel_0b0c_n5 + nunsel_0b0c_n5)
eff_2b0c_n5 = nsel_2b0c_n5 / (nsel_2b0c_n5 + nunsel_2b0c_n5)
eff_1b0c_n5 = nsel_1b0c_n5 / (nsel_1b0c_n5 + nunsel_1b0c_n5)

err_eff_1b1c_n3 = np.sqrt(((err_nsel_1b1c_n3**2 * nunsel_1b1c_n3**2) + (err_nunsel_1b1c_n3**2 * nsel_1b1c_n3**2)) / (nsel_1b1c_n3 + nunsel_1b1c_n3)**4)
err_eff_0b1c_n3 = np.sqrt(((err_nsel_0b1c_n3**2 * nunsel_0b1c_n3**2) + (err_nunsel_0b1c_n3**2 * nsel_0b1c_n3**2)) / (nsel_0b1c_n3 + nunsel_0b1c_n3)**4)
err_eff_0b2c_n3 = np.sqrt(((err_nsel_0b2c_n3**2 * nunsel_0b2c_n3**2) + (err_nunsel_0b2c_n3**2 * nsel_0b2c_n3**2)) / (nsel_0b2c_n3 + nunsel_0b2c_n3)**4)
err_eff_0b0c_n3 = np.sqrt(((err_nsel_0b0c_n3**2 * nunsel_0b0c_n3**2) + (err_nunsel_0b0c_n3**2 * nsel_0b0c_n3**2)) / (nsel_0b0c_n3 + nunsel_0b0c_n3)**4)
err_eff_2b0c_n3 = np.sqrt(((err_nsel_2b0c_n3**2 * nunsel_2b0c_n3**2) + (err_nunsel_2b0c_n3**2 * nsel_2b0c_n3**2)) / (nsel_2b0c_n3 + nunsel_2b0c_n3)**4)
err_eff_1b0c_n3 = np.sqrt(((err_nsel_1b0c_n3**2 * nunsel_1b0c_n3**2) + (err_nunsel_1b0c_n3**2 * nsel_1b0c_n3**2)) / (nsel_1b0c_n3 + nunsel_1b0c_n3)**4)

err_eff_1b1c_n4 = np.sqrt(((err_nsel_1b1c_n4**2 * nunsel_1b1c_n4**2) + (err_nunsel_1b1c_n4**2 * nsel_1b1c_n4**2)) / (nsel_1b1c_n4 + nunsel_1b1c_n4)**4)
err_eff_0b1c_n4 = np.sqrt(((err_nsel_0b1c_n4**2 * nunsel_0b1c_n4**2) + (err_nunsel_0b1c_n4**2 * nsel_0b1c_n4**2)) / (nsel_0b1c_n4 + nunsel_0b1c_n4)**4)
err_eff_0b2c_n4 = np.sqrt(((err_nsel_0b2c_n4**2 * nunsel_0b2c_n4**2) + (err_nunsel_0b2c_n4**2 * nsel_0b2c_n4**2)) / (nsel_0b2c_n4 + nunsel_0b2c_n4)**4)
err_eff_0b0c_n4 = np.sqrt(((err_nsel_0b0c_n4**2 * nunsel_0b0c_n4**2) + (err_nunsel_0b0c_n4**2 * nsel_0b0c_n4**2)) / (nsel_0b0c_n4 + nunsel_0b0c_n4)**4)
err_eff_2b0c_n4 = np.sqrt(((err_nsel_2b0c_n4**2 * nunsel_2b0c_n4**2) + (err_nunsel_2b0c_n4**2 * nsel_2b0c_n4**2)) / (nsel_2b0c_n4 + nunsel_2b0c_n4)**4)
err_eff_1b0c_n4 = np.sqrt(((err_nsel_1b0c_n4**2 * nunsel_1b0c_n4**2) + (err_nunsel_1b0c_n4**2 * nsel_1b0c_n4**2)) / (nsel_1b0c_n4 + nunsel_1b0c_n4)**4)

err_eff_1b1c_n5 = np.sqrt(((err_nsel_1b1c_n5**2 * nunsel_1b1c_n5**2) + (err_nunsel_1b1c_n5**2 * nsel_1b1c_n5**2)) / (nsel_1b1c_n5 + nunsel_1b1c_n5)**4)
err_eff_0b1c_n5 = np.sqrt(((err_nsel_0b1c_n5**2 * nunsel_0b1c_n5**2) + (err_nunsel_0b1c_n5**2 * nsel_0b1c_n5**2)) / (nsel_0b1c_n5 + nunsel_0b1c_n5)**4)
err_eff_0b2c_n5 = np.sqrt(((err_nsel_0b2c_n5**2 * nunsel_0b2c_n5**2) + (err_nunsel_0b2c_n5**2 * nsel_0b2c_n5**2)) / (nsel_0b2c_n5 + nunsel_0b2c_n5)**4)
err_eff_0b0c_n5 = np.sqrt(((err_nsel_0b0c_n5**2 * nunsel_0b0c_n5**2) + (err_nunsel_0b0c_n5**2 * nsel_0b0c_n5**2)) / (nsel_0b0c_n5 + nunsel_0b0c_n5)**4)
err_eff_2b0c_n5 = np.sqrt(((err_nsel_2b0c_n5**2 * nunsel_2b0c_n5**2) + (err_nunsel_2b0c_n5**2 * nsel_2b0c_n5**2)) / (nsel_2b0c_n5 + nunsel_2b0c_n5)**4)
err_eff_1b0c_n5 = np.sqrt(((err_nsel_1b0c_n5**2 * nunsel_1b0c_n5**2) + (err_nunsel_1b0c_n5**2 * nsel_1b0c_n5**2)) / (nsel_1b0c_n5 + nunsel_1b0c_n5)**4)

## combine the efficiencies
element_0b0c_n3 = np.nan_to_num(1 / err_eff_0b0c_n3**2)
element_0b0c_n4 = np.nan_to_num(1 / err_eff_0b0c_n4**2)
element_0b0c_n5 = np.nan_to_num(1 / err_eff_0b0c_n5**2)

element_0b0c_n3[err_eff_0b0c_n3 == 0] = 0
element_0b0c_n4[err_eff_0b0c_n4 == 0] = 0
element_0b0c_n5[err_eff_0b0c_n5 == 0] = 0
eff_0b0c_combined = (eff_0b0c_n3 * element_0b0c_n3 + eff_0b0c_n4 * element_0b0c_n4 + eff_0b0c_n5 * element_0b0c_n5) / (element_0b0c_n3 + element_0b0c_n4 + element_0b0c_n5)
err_eff_0b0c_combined = 1 / (element_0b0c_n3 + element_0b0c_n4 + element_0b0c_n5)

element_1b0c_n3 = np.nan_to_num(1 / err_eff_1b0c_n3**2)
element_1b0c_n4 = np.nan_to_num(1 / err_eff_1b0c_n4**2)
element_1b0c_n5 = np.nan_to_num(1 / err_eff_1b0c_n5**2)

element_1b0c_n3[err_eff_1b0c_n3 == 0] = 0
element_1b0c_n4[err_eff_1b0c_n4 == 0] = 0
element_1b0c_n5[err_eff_1b0c_n5 == 0] = 0
eff_1b0c_combined = (eff_1b0c_n3 * element_1b0c_n3 + eff_1b0c_n4 * element_1b0c_n4 + eff_1b0c_n5 * element_1b0c_n5) / (element_1b0c_n3 + element_1b0c_n4 + element_1b0c_n5)
err_eff_1b0c_combined = 1 / (element_1b0c_n3 + element_1b0c_n4 + element_1b0c_n5)

element_2b0c_n3 = np.nan_to_num(1 / err_eff_2b0c_n3**2)
element_2b0c_n4 = np.nan_to_num(1 / err_eff_2b0c_n4**2)
element_2b0c_n5 = np.nan_to_num(1 / err_eff_2b0c_n5**2)
eff_2b0c_combined = (eff_2b0c_n3 * element_2b0c_n3 + eff_2b0c_n4 * element_2b0c_n4 + eff_2b0c_n5 * element_2b0c_n5) / (element_2b0c_n3 + element_2b0c_n4 + element_2b0c_n5)
err_eff_2b0c_combined = 1 / (element_2b0c_n3 + element_2b0c_n4 + element_2b0c_n5)

element_0b2c_n3 = np.nan_to_num(1 / err_eff_0b2c_n3**2)
element_0b2c_n4 = np.nan_to_num(1 / err_eff_0b2c_n4**2)
element_0b2c_n5 = np.nan_to_num(1 / err_eff_0b2c_n5**2)

element_0b2c_n3[err_eff_0b2c_n3 == 0] = 0
element_0b2c_n4[err_eff_0b2c_n4 == 0] = 0
element_0b2c_n5[err_eff_0b2c_n5 == 0] = 0
eff_0b2c_combined = (eff_0b2c_n3 * element_0b2c_n3 + eff_0b2c_n4 * element_0b2c_n4 + eff_0b2c_n5 * element_0b2c_n5) / (element_0b2c_n3 + element_0b2c_n4 + element_0b2c_n5)
err_eff_0b2c_combined = 1 / (element_0b2c_n3 + element_0b2c_n4 + element_0b2c_n5)

element_0b1c_n3 = np.nan_to_num(1 / err_eff_0b1c_n3**2)
element_0b1c_n4 = np.nan_to_num(1 / err_eff_0b1c_n4**2)
element_0b1c_n5 = np.nan_to_num(1 / err_eff_0b1c_n5**2)

element_0b1c_n3[err_eff_0b1c_n3 == 0] = 0
element_0b1c_n4[err_eff_0b1c_n4 == 0] = 0
element_0b1c_n5[err_eff_0b1c_n5 == 0] = 0
eff_0b1c_combined = (eff_0b1c_n3 * element_0b1c_n3 + eff_0b1c_n4 * element_0b1c_n4 + eff_0b1c_n5 * element_0b1c_n5) / (element_0b1c_n3 + element_0b1c_n4 + element_0b1c_n5)
err_eff_0b1c_combined = 1 / (element_0b1c_n3 + element_0b1c_n4 + element_0b1c_n5)

element_1b1c_n3 = np.nan_to_num(1 / err_eff_1b1c_n3**2)
element_1b1c_n4 = np.nan_to_num(1 / err_eff_1b1c_n4**2)
element_1b1c_n5 = np.nan_to_num(1 / err_eff_1b1c_n5**2)

element_1b1c_n3[err_eff_1b1c_n3 == 0] = 0
element_1b1c_n4[err_eff_1b1c_n4 == 0] = 0
element_1b1c_n5[err_eff_1b1c_n5 == 0] = 0
eff_1b1c_combined = (eff_1b1c_n3 * element_1b1c_n3 + eff_1b1c_n4 * element_1b1c_n4 + eff_1b1c_n5 * element_1b1c_n5) / (element_1b1c_n3 + element_1b1c_n4 + element_1b1c_n5)
err_eff_1b1c_combined = 1 / (element_1b1c_n3 + element_1b1c_n4 + element_1b1c_n5)


# Load the data for TRK_RES_D0_MEAS track syst
with h5py.File('../files/discriminant_values_trkd03.h5', 'r') as f:
    discriminant_1b0c_trkd03 = f['discriminant_1b0c'][:]
    discriminant_2b0c_trkd03 = f['discriminant_2b0c'][:]
    discriminant_1b1c_trkd03 = f['discriminant_1b1c'][:]
    discriminant_0b1c_trkd03 = f['discriminant_0b1c'][:]
    discriminant_0b2c_trkd03 = f['discriminant_0b2c'][:]
    discriminant_0b0c_trkd03 = f['discriminant_0b0c'][:]
    mc_weights_1b0c_trkd03 = f['mc_weights_1b0c'][:]
    mc_weights_2b0c_trkd03 = f['mc_weights_2b0c'][:]
    mc_weights_1b1c_trkd03 = f['mc_weights_1b1c'][:]
    mc_weights_0b1c_trkd03 = f['mc_weights_0b1c'][:]
    mc_weights_0b2c_trkd03 = f['mc_weights_0b2c'][:]
    mc_weights_0b0c_trkd03 = f['mc_weights_0b0c'][:]
    pt_values_1b0c_trkd03 = f['pt_values_1b0c'][:]
    pt_values_2b0c_trkd03 = f['pt_values_2b0c'][:]
    pt_values_1b1c_trkd03 = f['pt_values_1b1c'][:]
    pt_values_0b1c_trkd03 = f['pt_values_0b1c'][:]
    pt_values_0b2c_trkd03 = f['pt_values_0b2c'][:]
    pt_values_0b0c_trkd03 = f['pt_values_0b0c'][:]
    total_w_trkd03 = f['total_w'][:]

    
mc_weights_0b0c_trkd03 = compute_final_weights(mc_weights_0b0c_trkd03, weights_slice3, total_w_trkd03)
mc_weights_1b0c_trkd03 = compute_final_weights(mc_weights_1b0c_trkd03, weights_slice3, total_w_trkd03)
mc_weights_2b0c_trkd03 = compute_final_weights(mc_weights_2b0c_trkd03, weights_slice3, total_w_trkd03)
mc_weights_1b1c_trkd03 = compute_final_weights(mc_weights_1b1c_trkd03, weights_slice3, total_w_trkd03)
mc_weights_0b1c_trkd03 = compute_final_weights(mc_weights_0b1c_trkd03, weights_slice3, total_w_trkd03)
mc_weights_0b2c_trkd03 = compute_final_weights(mc_weights_0b2c_trkd03, weights_slice3, total_w_trkd03)

with h5py.File('../files/discriminant_values_trkd04.h5', 'r') as f:
    discriminant_1b0c_trkd04 = f['discriminant_1b0c'][:]
    discriminant_2b0c_trkd04 = f['discriminant_2b0c'][:]
    discriminant_1b1c_trkd04 = f['discriminant_1b1c'][:]
    discriminant_0b1c_trkd04 = f['discriminant_0b1c'][:]
    discriminant_0b2c_trkd04 = f['discriminant_0b2c'][:]
    discriminant_0b0c_trkd04 = f['discriminant_0b0c'][:]
    mc_weights_1b0c_trkd04 = f['mc_weights_1b0c'][:]
    mc_weights_2b0c_trkd04 = f['mc_weights_2b0c'][:]
    mc_weights_1b1c_trkd04 = f['mc_weights_1b1c'][:]
    mc_weights_0b1c_trkd04 = f['mc_weights_0b1c'][:]
    mc_weights_0b2c_trkd04 = f['mc_weights_0b2c'][:]
    mc_weights_0b0c_trkd04 = f['mc_weights_0b0c'][:]
    pt_values_1b0c_trkd04 = f['pt_values_1b0c'][:]
    pt_values_2b0c_trkd04 = f['pt_values_2b0c'][:]
    pt_values_1b1c_trkd04 = f['pt_values_1b1c'][:]
    pt_values_0b1c_trkd04 = f['pt_values_0b1c'][:]
    pt_values_0b2c_trkd04 = f['pt_values_0b2c'][:]
    pt_values_0b0c_trkd04 = f['pt_values_0b0c'][:]
    total_w_trkd04 = f['total_w'][:]

    
mc_weights_0b0c_trkd04 = compute_final_weights(mc_weights_0b0c_trkd04, weights_slice3, total_w_trkd04)
mc_weights_1b0c_trkd04 = compute_final_weights(mc_weights_1b0c_trkd04, weights_slice3, total_w_trkd04)
mc_weights_2b0c_trkd04 = compute_final_weights(mc_weights_2b0c_trkd04, weights_slice3, total_w_trkd04)
mc_weights_1b1c_trkd04 = compute_final_weights(mc_weights_1b1c_trkd04, weights_slice3, total_w_trkd04)
mc_weights_0b1c_trkd04 = compute_final_weights(mc_weights_0b1c_trkd04, weights_slice3, total_w_trkd04)
mc_weights_0b2c_trkd04 = compute_final_weights(mc_weights_0b2c_trkd04, weights_slice3, total_w_trkd04)

with h5py.File('../files/discriminant_values_trkd05.h5', 'r') as f:
    discriminant_1b0c_trkd05 = f['discriminant_1b0c'][:]
    discriminant_2b0c_trkd05 = f['discriminant_2b0c'][:]
    discriminant_1b1c_trkd05 = f['discriminant_1b1c'][:]
    discriminant_0b1c_trkd05 = f['discriminant_0b1c'][:]
    discriminant_0b2c_trkd05 = f['discriminant_0b2c'][:]
    discriminant_0b0c_trkd05 = f['discriminant_0b0c'][:]
    mc_weights_1b0c_trkd05 = f['mc_weights_1b0c'][:]
    mc_weights_2b0c_trkd05 = f['mc_weights_2b0c'][:]
    mc_weights_1b1c_trkd05 = f['mc_weights_1b1c'][:]
    mc_weights_0b1c_trkd05 = f['mc_weights_0b1c'][:]
    mc_weights_0b2c_trkd05 = f['mc_weights_0b2c'][:]
    mc_weights_0b0c_trkd05 = f['mc_weights_0b0c'][:]
    pt_values_1b0c_trkd05 = f['pt_values_1b0c'][:]
    pt_values_2b0c_trkd05 = f['pt_values_2b0c'][:]
    pt_values_1b1c_trkd05 = f['pt_values_1b1c'][:]
    pt_values_0b1c_trkd05 = f['pt_values_0b1c'][:]
    pt_values_0b2c_trkd05 = f['pt_values_0b2c'][:]
    pt_values_0b0c_trkd05 = f['pt_values_0b0c'][:]
    total_w_trkd05 = f['total_w'][:]

    
mc_weights_0b0c_trkd05 = compute_final_weights(mc_weights_0b0c_trkd05, weights_slice3, total_w_trkd05)
mc_weights_1b0c_trkd05 = compute_final_weights(mc_weights_1b0c_trkd05, weights_slice3, total_w_trkd05)
mc_weights_2b0c_trkd05 = compute_final_weights(mc_weights_2b0c_trkd05, weights_slice3, total_w_trkd05)
mc_weights_1b1c_trkd05 = compute_final_weights(mc_weights_1b1c_trkd05, weights_slice3, total_w_trkd05)
mc_weights_0b1c_trkd05 = compute_final_weights(mc_weights_0b1c_trkd05, weights_slice3, total_w_trkd05)
mc_weights_0b2c_trkd05 = compute_final_weights(mc_weights_0b2c_trkd05, weights_slice3, total_w_trkd05)



selected_pt_1b0c_trkd03 = pt_values_1b0c_trkd03[discriminant_1b0c_trkd03 > discr_cut]
selected_pt_2b0c_trkd03 = pt_values_2b0c_trkd03[discriminant_2b0c_trkd03 > discr_cut]
selected_pt_1b1c_trkd03 = pt_values_1b1c_trkd03[discriminant_1b1c_trkd03 > discr_cut]
selected_pt_0b1c_trkd03 = pt_values_0b1c_trkd03[discriminant_0b1c_trkd03 > discr_cut]
selected_pt_0b2c_trkd03 = pt_values_0b2c_trkd03[discriminant_0b2c_trkd03 > discr_cut]
selected_pt_0b0c_trkd03 = pt_values_0b0c_trkd03[discriminant_0b0c_trkd03 > discr_cut]

selected_weights_1b0c_trkd03 = mc_weights_1b0c_trkd03[discriminant_1b0c_trkd03 > discr_cut]
selected_weights_2b0c_trkd03 = mc_weights_2b0c_trkd03[discriminant_2b0c_trkd03 > discr_cut]
selected_weights_1b1c_trkd03 = mc_weights_1b1c_trkd03[discriminant_1b1c_trkd03 > discr_cut]
selected_weights_0b1c_trkd03 = mc_weights_0b1c_trkd03[discriminant_0b1c_trkd03 > discr_cut]
selected_weights_0b2c_trkd03 = mc_weights_0b2c_trkd03[discriminant_0b2c_trkd03 > discr_cut]
selected_weights_0b0c_trkd03 = mc_weights_0b0c_trkd03[discriminant_0b0c_trkd03 > discr_cut]

selected_pt_1b1c_trkd04 = pt_values_1b1c_trkd04[discriminant_1b1c_trkd04 > discr_cut]
selected_pt_0b1c_trkd04 = pt_values_0b1c_trkd04[discriminant_0b1c_trkd04 > discr_cut]
selected_pt_0b2c_trkd04 = pt_values_0b2c_trkd04[discriminant_0b2c_trkd04 > discr_cut]
selected_pt_0b0c_trkd04 = pt_values_0b0c_trkd04[discriminant_0b0c_trkd04 > discr_cut]
selected_pt_2b0c_trkd04 = pt_values_2b0c_trkd04[discriminant_2b0c_trkd04 > discr_cut]
selected_pt_1b0c_trkd04 = pt_values_1b0c_trkd04[discriminant_1b0c_trkd04 > discr_cut]

selected_weights_1b1c_trkd04 = mc_weights_1b1c_trkd04[discriminant_1b1c_trkd04 > discr_cut]
selected_weights_0b1c_trkd04 = mc_weights_0b1c_trkd04[discriminant_0b1c_trkd04 > discr_cut]
selected_weights_0b2c_trkd04 = mc_weights_0b2c_trkd04[discriminant_0b2c_trkd04 > discr_cut]
selected_weights_0b0c_trkd04 = mc_weights_0b0c_trkd04[discriminant_0b0c_trkd04 > discr_cut]
selected_weights_2b0c_trkd04 = mc_weights_2b0c_trkd04[discriminant_2b0c_trkd04 > discr_cut]
selected_weights_1b0c_trkd04 = mc_weights_1b0c_trkd04[discriminant_1b0c_trkd04 > discr_cut]

selected_pt_1b1c_trkd05 = pt_values_1b1c_trkd05[discriminant_1b1c_trkd05 > discr_cut]
selected_pt_0b1c_trkd05 = pt_values_0b1c_trkd05[discriminant_0b1c_trkd05 > discr_cut]
selected_pt_0b2c_trkd05 = pt_values_0b2c_trkd05[discriminant_0b2c_trkd05 > discr_cut]
selected_pt_0b0c_trkd05 = pt_values_0b0c_trkd05[discriminant_0b0c_trkd05 > discr_cut]
selected_pt_2b0c_trkd05 = pt_values_2b0c_trkd05[discriminant_2b0c_trkd05 > discr_cut]
selected_pt_1b0c_trkd05 = pt_values_1b0c_trkd05[discriminant_1b0c_trkd05 > discr_cut]

selected_weights_1b1c_trkd05 = mc_weights_1b1c_trkd05[discriminant_1b1c_trkd05 > discr_cut]
selected_weights_0b1c_trkd05 = mc_weights_0b1c_trkd05[discriminant_0b1c_trkd05 > discr_cut]
selected_weights_0b2c_trkd05 = mc_weights_0b2c_trkd05[discriminant_0b2c_trkd05 > discr_cut]
selected_weights_0b0c_trkd05 = mc_weights_0b0c_trkd05[discriminant_0b0c_trkd05 > discr_cut]
selected_weights_2b0c_trkd05 = mc_weights_2b0c_trkd05[discriminant_2b0c_trkd05 > discr_cut]
selected_weights_1b0c_trkd05 = mc_weights_1b0c_trkd05[discriminant_1b0c_trkd05 > discr_cut]

nonselected_pt_1b0c_trkd03 = pt_values_1b0c_trkd03[discriminant_1b0c_trkd03 <= discr_cut]   
nonselected_pt_2b0c_trkd03 = pt_values_2b0c_trkd03[discriminant_2b0c_trkd03 <= discr_cut]
nonselected_pt_1b1c_trkd03 = pt_values_1b1c_trkd03[discriminant_1b1c_trkd03 <= discr_cut]
nonselected_pt_0b1c_trkd03 = pt_values_0b1c_trkd03[discriminant_0b1c_trkd03 <= discr_cut]
nonselected_pt_0b2c_trkd03 = pt_values_0b2c_trkd03[discriminant_0b2c_trkd03 <= discr_cut]
nonselected_pt_0b0c_trkd03 = pt_values_0b0c_trkd03[discriminant_0b0c_trkd03 <= discr_cut]

nonselected_weights_1b0c_trkd03 = mc_weights_1b0c_trkd03[discriminant_1b0c_trkd03 <= discr_cut]
nonselected_weights_2b0c_trkd03 = mc_weights_2b0c_trkd03[discriminant_2b0c_trkd03 <= discr_cut]
nonselected_weights_1b1c_trkd03 = mc_weights_1b1c_trkd03[discriminant_1b1c_trkd03 <= discr_cut]
nonselected_weights_0b1c_trkd03 = mc_weights_0b1c_trkd03[discriminant_0b1c_trkd03 <= discr_cut]
nonselected_weights_0b2c_trkd03 = mc_weights_0b2c_trkd03[discriminant_0b2c_trkd03 <= discr_cut]
nonselected_weights_0b0c_trkd03 = mc_weights_0b0c_trkd03[discriminant_0b0c_trkd03 <= discr_cut]

nonselected_pt_1b1c_trkd04 = pt_values_1b1c_trkd04[discriminant_1b1c_trkd04 <= discr_cut]
nonselected_pt_0b1c_trkd04 = pt_values_0b1c_trkd04[discriminant_0b1c_trkd04 <= discr_cut]
nonselected_pt_0b2c_trkd04 = pt_values_0b2c_trkd04[discriminant_0b2c_trkd04 <= discr_cut]
nonselected_pt_0b0c_trkd04 = pt_values_0b0c_trkd04[discriminant_0b0c_trkd04 <= discr_cut]
nonselected_pt_2b0c_trkd04 = pt_values_2b0c_trkd04[discriminant_2b0c_trkd04 <= discr_cut]
nonselected_pt_1b0c_trkd04 = pt_values_1b0c_trkd04[discriminant_1b0c_trkd04 <= discr_cut]

nonselected_weights_1b1c_trkd04 = mc_weights_1b1c_trkd04[discriminant_1b1c_trkd04 <= discr_cut]
nonselected_weights_0b1c_trkd04 = mc_weights_0b1c_trkd04[discriminant_0b1c_trkd04 <= discr_cut]
nonselected_weights_0b2c_trkd04 = mc_weights_0b2c_trkd04[discriminant_0b2c_trkd04 <= discr_cut]
nonselected_weights_0b0c_trkd04 = mc_weights_0b0c_trkd04[discriminant_0b0c_trkd04 <= discr_cut]
nonselected_weights_2b0c_trkd04 = mc_weights_2b0c_trkd04[discriminant_2b0c_trkd04 <= discr_cut]
nonselected_weights_1b0c_trkd04 = mc_weights_1b0c_trkd04[discriminant_1b0c_trkd04 <= discr_cut]

nonselected_pt_1b1c_trkd05 = pt_values_1b1c_trkd05[discriminant_1b1c_trkd05 <= discr_cut]
nonselected_pt_0b1c_trkd05 = pt_values_0b1c_trkd05[discriminant_0b1c_trkd05 <= discr_cut]
nonselected_pt_0b2c_trkd05 = pt_values_0b2c_trkd05[discriminant_0b2c_trkd05 <= discr_cut]
nonselected_pt_0b0c_trkd05 = pt_values_0b0c_trkd05[discriminant_0b0c_trkd05 <= discr_cut]
nonselected_pt_2b0c_trkd05 = pt_values_2b0c_trkd05[discriminant_2b0c_trkd05 <= discr_cut]
nonselected_pt_1b0c_trkd05 = pt_values_1b0c_trkd05[discriminant_1b0c_trkd05 <= discr_cut]

nonselected_weights_1b1c_trkd05 = mc_weights_1b1c_trkd05[discriminant_1b1c_trkd05 <= discr_cut]
nonselected_weights_0b1c_trkd05 = mc_weights_0b1c_trkd05[discriminant_0b1c_trkd05 <= discr_cut]
nonselected_weights_0b2c_trkd05 = mc_weights_0b2c_trkd05[discriminant_0b2c_trkd05 <= discr_cut]
nonselected_weights_0b0c_trkd05 = mc_weights_0b0c_trkd05[discriminant_0b0c_trkd05 <= discr_cut]
nonselected_weights_2b0c_trkd05 = mc_weights_2b0c_trkd05[discriminant_2b0c_trkd05 <= discr_cut]
nonselected_weights_1b0c_trkd05 = mc_weights_1b0c_trkd05[discriminant_1b0c_trkd05 <= discr_cut]

pt_bin_indices_selected_1b0c_trkd03 = np.digitize(selected_pt_1b0c_trkd03, pt_values)
pt_bin_indices_selected_2b0c_trkd03 = np.digitize(selected_pt_2b0c_trkd03, pt_values)
pt_bin_indices_selected_1b1c_trkd03 = np.digitize(selected_pt_1b1c_trkd03, pt_values)
pt_bin_indices_selected_0b1c_trkd03 = np.digitize(selected_pt_0b1c_trkd03, pt_values)
pt_bin_indices_selected_0b2c_trkd03 = np.digitize(selected_pt_0b2c_trkd03, pt_values)
pt_bin_indices_selected_0b0c_trkd03 = np.digitize(selected_pt_0b0c_trkd03, pt_values)

pt_bin_indices_selected_1b1c_trkd04 = np.digitize(selected_pt_1b1c_trkd04, pt_values)   
pt_bin_indices_selected_0b1c_trkd04 = np.digitize(selected_pt_0b1c_trkd04, pt_values)
pt_bin_indices_selected_0b2c_trkd04 = np.digitize(selected_pt_0b2c_trkd04, pt_values)
pt_bin_indices_selected_0b0c_trkd04 = np.digitize(selected_pt_0b0c_trkd04, pt_values)
pt_bin_indices_selected_2b0c_trkd04 = np.digitize(selected_pt_2b0c_trkd04, pt_values)
pt_bin_indices_selected_1b0c_trkd04 = np.digitize(selected_pt_1b0c_trkd04, pt_values)

pt_bin_indices_selected_1b1c_trkd05 = np.digitize(selected_pt_1b1c_trkd05, pt_values)
pt_bin_indices_selected_0b1c_trkd05 = np.digitize(selected_pt_0b1c_trkd05, pt_values)
pt_bin_indices_selected_0b2c_trkd05 = np.digitize(selected_pt_0b2c_trkd05, pt_values)
pt_bin_indices_selected_0b0c_trkd05 = np.digitize(selected_pt_0b0c_trkd05, pt_values)
pt_bin_indices_selected_2b0c_trkd05 = np.digitize(selected_pt_2b0c_trkd05, pt_values)
pt_bin_indices_selected_1b0c_trkd05 = np.digitize(selected_pt_1b0c_trkd05, pt_values)

pt_bin_indices_nonselected_1b0c_trkd03 = np.digitize(nonselected_pt_1b0c_trkd03, pt_values)
pt_bin_indices_nonselected_2b0c_trkd03 = np.digitize(nonselected_pt_2b0c_trkd03, pt_values)
pt_bin_indices_nonselected_1b1c_trkd03 = np.digitize(nonselected_pt_1b1c_trkd03, pt_values)
pt_bin_indices_nonselected_0b1c_trkd03 = np.digitize(nonselected_pt_0b1c_trkd03, pt_values)
pt_bin_indices_nonselected_0b2c_trkd03 = np.digitize(nonselected_pt_0b2c_trkd03, pt_values)
pt_bin_indices_nonselected_0b0c_trkd03 = np.digitize(nonselected_pt_0b0c_trkd03, pt_values)

pt_bin_indices_nonselected_1b1c_trkd04 = np.digitize(nonselected_pt_1b1c_trkd04, pt_values)
pt_bin_indices_nonselected_0b1c_trkd04 = np.digitize(nonselected_pt_0b1c_trkd04, pt_values)
pt_bin_indices_nonselected_0b2c_trkd04 = np.digitize(nonselected_pt_0b2c_trkd04, pt_values)
pt_bin_indices_nonselected_0b0c_trkd04 = np.digitize(nonselected_pt_0b0c_trkd04, pt_values)
pt_bin_indices_nonselected_2b0c_trkd04 = np.digitize(nonselected_pt_2b0c_trkd04, pt_values)
pt_bin_indices_nonselected_1b0c_trkd04 = np.digitize(nonselected_pt_1b0c_trkd04, pt_values)

pt_bin_indices_nonselected_1b1c_trkd05 = np.digitize(nonselected_pt_1b1c_trkd05, pt_values)
pt_bin_indices_nonselected_0b1c_trkd05 = np.digitize(nonselected_pt_0b1c_trkd05, pt_values)
pt_bin_indices_nonselected_0b2c_trkd05 = np.digitize(nonselected_pt_0b2c_trkd05, pt_values)
pt_bin_indices_nonselected_0b0c_trkd05 = np.digitize(nonselected_pt_0b0c_trkd05, pt_values)
pt_bin_indices_nonselected_2b0c_trkd05 = np.digitize(nonselected_pt_2b0c_trkd05, pt_values)
pt_bin_indices_nonselected_1b0c_trkd05 = np.digitize(nonselected_pt_1b0c_trkd05, pt_values)


nsel_1b1c_trkd03 = []
nsel_0b1c_trkd03 = []
nsel_0b2c_trkd03 = []
nsel_0b0c_trkd03 = []
nsel_2b0c_trkd03 = []
nsel_1b0c_trkd03 = []

nsel_1b1c_trkd04 = []
nsel_0b1c_trkd04 = []
nsel_0b2c_trkd04 = []
nsel_0b0c_trkd04 = []
nsel_2b0c_trkd04 = []
nsel_1b0c_trkd04 = []

nsel_1b1c_trkd05 = []
nsel_0b1c_trkd05 = []
nsel_0b2c_trkd05 = []
nsel_0b0c_trkd05 = []
nsel_2b0c_trkd05 = []
nsel_1b0c_trkd05 = []

err_nsel_1b1c_trkd03 = []
err_nsel_0b1c_trkd03 = []
err_nsel_0b2c_trkd03 = []
err_nsel_0b0c_trkd03 = []
err_nsel_2b0c_trkd03 = []
err_nsel_1b0c_trkd03 = []

err_nsel_1b1c_trkd04 = []
err_nsel_0b1c_trkd04 = []
err_nsel_0b2c_trkd04 = []
err_nsel_0b0c_trkd04 = []
err_nsel_2b0c_trkd04 = []
err_nsel_1b0c_trkd04 = []

err_nsel_1b1c_trkd05 = []
err_nsel_0b1c_trkd05 = []
err_nsel_0b2c_trkd05 = []
err_nsel_0b0c_trkd05 = []
err_nsel_2b0c_trkd05 = []
err_nsel_1b0c_trkd05 = []

nunsel_1b1c_trkd03 = []
nunsel_0b1c_trkd03 = []
nunsel_0b2c_trkd03 = []
nunsel_0b0c_trkd03 = []
nunsel_2b0c_trkd03 = []
nunsel_1b0c_trkd03 = []

nunsel_1b1c_trkd04 = []
nunsel_0b1c_trkd04 = []
nunsel_0b2c_trkd04 = []
nunsel_0b0c_trkd04 = []
nunsel_2b0c_trkd04 = []
nunsel_1b0c_trkd04 = []

nunsel_1b1c_trkd05 = []
nunsel_0b1c_trkd05 = []
nunsel_0b2c_trkd05 = []
nunsel_0b0c_trkd05 = []
nunsel_2b0c_trkd05 = []
nunsel_1b0c_trkd05 = []

err_nunsel_1b1c_trkd03 = []
err_nunsel_0b1c_trkd03 = []
err_nunsel_0b2c_trkd03 = []
err_nunsel_0b0c_trkd03 = []
err_nunsel_2b0c_trkd03 = []
err_nunsel_1b0c_trkd03 = []

err_nunsel_1b1c_trkd04 = []
err_nunsel_0b1c_trkd04 = []
err_nunsel_0b2c_trkd04 = []
err_nunsel_0b0c_trkd04 = []
err_nunsel_2b0c_trkd04 = []
err_nunsel_1b0c_trkd04 = []

err_nunsel_1b1c_trkd05 = []
err_nunsel_0b1c_trkd05 = []
err_nunsel_0b2c_trkd05 = []
err_nunsel_0b0c_trkd05 = []
err_nunsel_2b0c_trkd05 = []
err_nunsel_1b0c_trkd05 = []

for i in range(0, len(pt_bins)):
 events_in_bin_1b1c_trkd03_sel = selected_weights_1b1c_trkd03[pt_bin_indices_selected_1b1c_trkd03 == i]
 events_in_bin_0b1c_trkd03_sel = selected_weights_0b1c_trkd03[pt_bin_indices_selected_0b1c_trkd03 == i]
 events_in_bin_0b2c_trkd03_sel = selected_weights_0b2c_trkd03[pt_bin_indices_selected_0b2c_trkd03 == i]
 events_in_bin_0b0c_trkd03_sel = selected_weights_0b0c_trkd03[pt_bin_indices_selected_0b0c_trkd03 == i]
 events_in_bin_2b0c_trkd03_sel = selected_weights_2b0c_trkd03[pt_bin_indices_selected_2b0c_trkd03 == i]
 events_in_bin_1b0c_trkd03_sel = selected_weights_1b0c_trkd03[pt_bin_indices_selected_1b0c_trkd03 == i]

 events_in_bin_1b1c_trkd03_unsel = nonselected_weights_1b1c_trkd03[pt_bin_indices_nonselected_1b1c_trkd03 == i]
 events_in_bin_0b1c_trkd03_unsel = nonselected_weights_0b1c_trkd03[pt_bin_indices_nonselected_0b1c_trkd03 == i]
 events_in_bin_0b2c_trkd03_unsel = nonselected_weights_0b2c_trkd03[pt_bin_indices_nonselected_0b2c_trkd03 == i]
 events_in_bin_0b0c_trkd03_unsel = nonselected_weights_0b0c_trkd03[pt_bin_indices_nonselected_0b0c_trkd03 == i]
 events_in_bin_2b0c_trkd03_unsel = nonselected_weights_2b0c_trkd03[pt_bin_indices_nonselected_2b0c_trkd03 == i]
 events_in_bin_1b0c_trkd03_unsel = nonselected_weights_1b0c_trkd03[pt_bin_indices_nonselected_1b0c_trkd03 == i]

 events_in_bin_1b1c_trkd04_sel = selected_weights_1b1c_trkd04[pt_bin_indices_selected_1b1c_trkd04 == i]
 events_in_bin_0b1c_trkd04_sel = selected_weights_0b1c_trkd04[pt_bin_indices_selected_0b1c_trkd04 == i]
 events_in_bin_0b2c_trkd04_sel = selected_weights_0b2c_trkd04[pt_bin_indices_selected_0b2c_trkd04 == i]
 events_in_bin_0b0c_trkd04_sel = selected_weights_0b0c_trkd04[pt_bin_indices_selected_0b0c_trkd04 == i]
 events_in_bin_2b0c_trkd04_sel = selected_weights_2b0c_trkd04[pt_bin_indices_selected_2b0c_trkd04 == i]
 events_in_bin_1b0c_trkd04_sel = selected_weights_1b0c_trkd04[pt_bin_indices_selected_1b0c_trkd04 == i]

 events_in_bin_1b1c_trkd04_unsel = nonselected_weights_1b1c_trkd04[pt_bin_indices_nonselected_1b1c_trkd04 == i]
 events_in_bin_0b1c_trkd04_unsel = nonselected_weights_0b1c_trkd04[pt_bin_indices_nonselected_0b1c_trkd04 == i]
 events_in_bin_0b2c_trkd04_unsel = nonselected_weights_0b2c_trkd04[pt_bin_indices_nonselected_0b2c_trkd04 == i]
 events_in_bin_0b0c_trkd04_unsel = nonselected_weights_0b0c_trkd04[pt_bin_indices_nonselected_0b0c_trkd04 == i]
 events_in_bin_2b0c_trkd04_unsel = nonselected_weights_2b0c_trkd04[pt_bin_indices_nonselected_2b0c_trkd04 == i]
 events_in_bin_1b0c_trkd04_unsel = nonselected_weights_1b0c_trkd04[pt_bin_indices_nonselected_1b0c_trkd04 == i]

 events_in_bin_1b1c_trkd05_sel = selected_weights_1b1c_trkd05[pt_bin_indices_selected_1b1c_trkd05 == i]
 events_in_bin_0b1c_trkd05_sel = selected_weights_0b1c_trkd05[pt_bin_indices_selected_0b1c_trkd05 == i]
 events_in_bin_0b2c_trkd05_sel = selected_weights_0b2c_trkd05[pt_bin_indices_selected_0b2c_trkd05 == i]
 events_in_bin_0b0c_trkd05_sel = selected_weights_0b0c_trkd05[pt_bin_indices_selected_0b0c_trkd05 == i]
 events_in_bin_2b0c_trkd05_sel = selected_weights_2b0c_trkd05[pt_bin_indices_selected_2b0c_trkd05 == i]
 events_in_bin_1b0c_trkd05_sel = selected_weights_1b0c_trkd05[pt_bin_indices_selected_1b0c_trkd05 == i]

 events_in_bin_1b1c_trkd05_unsel = nonselected_weights_1b1c_trkd05[pt_bin_indices_nonselected_1b1c_trkd05 == i]
 events_in_bin_0b1c_trkd05_unsel = nonselected_weights_0b1c_trkd05[pt_bin_indices_nonselected_0b1c_trkd05 == i]
 events_in_bin_0b2c_trkd05_unsel = nonselected_weights_0b2c_trkd05[pt_bin_indices_nonselected_0b2c_trkd05 == i]
 events_in_bin_0b0c_trkd05_unsel = nonselected_weights_0b0c_trkd05[pt_bin_indices_nonselected_0b0c_trkd05 == i]
 events_in_bin_2b0c_trkd05_unsel = nonselected_weights_2b0c_trkd05[pt_bin_indices_nonselected_2b0c_trkd05 == i]
 events_in_bin_1b0c_trkd05_unsel = nonselected_weights_1b0c_trkd05[pt_bin_indices_nonselected_1b0c_trkd05 == i]


 nsel_1b1c_trkd03.append(np.sum(events_in_bin_1b1c_trkd03_sel))
 nsel_0b1c_trkd03.append(np.sum(events_in_bin_0b1c_trkd03_sel))
 nsel_0b2c_trkd03.append(np.sum(events_in_bin_0b2c_trkd03_sel))
 nsel_0b0c_trkd03.append(np.sum(events_in_bin_0b0c_trkd03_sel))
 nsel_2b0c_trkd03.append(np.sum(events_in_bin_2b0c_trkd03_sel))
 nsel_1b0c_trkd03.append(np.sum(events_in_bin_1b0c_trkd03_sel))

 nsel_1b1c_trkd04.append(np.sum(events_in_bin_1b1c_trkd04_sel))
 nsel_0b1c_trkd04.append(np.sum(events_in_bin_0b1c_trkd04_sel))
 nsel_0b2c_trkd04.append(np.sum(events_in_bin_0b2c_trkd04_sel))
 nsel_0b0c_trkd04.append(np.sum(events_in_bin_0b0c_trkd04_sel))
 nsel_2b0c_trkd04.append(np.sum(events_in_bin_2b0c_trkd04_sel))
 nsel_1b0c_trkd04.append(np.sum(events_in_bin_1b0c_trkd04_sel))

 nsel_1b1c_trkd05.append(np.sum(events_in_bin_1b1c_trkd05_sel))
 nsel_0b1c_trkd05.append(np.sum(events_in_bin_0b1c_trkd05_sel))
 nsel_0b2c_trkd05.append(np.sum(events_in_bin_0b2c_trkd05_sel))
 nsel_0b0c_trkd05.append(np.sum(events_in_bin_0b0c_trkd05_sel))
 nsel_2b0c_trkd05.append(np.sum(events_in_bin_2b0c_trkd05_sel))
 nsel_1b0c_trkd05.append(np.sum(events_in_bin_1b0c_trkd05_sel))

 err_nsel_1b1c_trkd03.append(compute_weightedcount_error(events_in_bin_1b1c_trkd03_sel))
 err_nsel_0b1c_trkd03.append(compute_weightedcount_error(events_in_bin_0b1c_trkd03_sel))
 err_nsel_0b2c_trkd03.append(compute_weightedcount_error(events_in_bin_0b2c_trkd03_sel))
 err_nsel_0b0c_trkd03.append(compute_weightedcount_error(events_in_bin_0b0c_trkd03_sel))
 err_nsel_2b0c_trkd03.append(compute_weightedcount_error(events_in_bin_2b0c_trkd03_sel))
 err_nsel_1b0c_trkd03.append(compute_weightedcount_error(events_in_bin_1b0c_trkd03_sel))

 err_nsel_1b1c_trkd04.append(compute_weightedcount_error(events_in_bin_1b1c_trkd04_sel))
 err_nsel_0b1c_trkd04.append(compute_weightedcount_error(events_in_bin_0b1c_trkd04_sel))
 err_nsel_0b2c_trkd04.append(compute_weightedcount_error(events_in_bin_0b2c_trkd04_sel))
 err_nsel_0b0c_trkd04.append(compute_weightedcount_error(events_in_bin_0b0c_trkd04_sel))
 err_nsel_2b0c_trkd04.append(compute_weightedcount_error(events_in_bin_2b0c_trkd04_sel))
 err_nsel_1b0c_trkd04.append(compute_weightedcount_error(events_in_bin_1b0c_trkd04_sel))

 err_nsel_1b1c_trkd05.append(compute_weightedcount_error(events_in_bin_1b1c_trkd05_sel))
 err_nsel_0b1c_trkd05.append(compute_weightedcount_error(events_in_bin_0b1c_trkd05_sel))
 err_nsel_0b2c_trkd05.append(compute_weightedcount_error(events_in_bin_0b2c_trkd05_sel))
 err_nsel_0b0c_trkd05.append(compute_weightedcount_error(events_in_bin_0b0c_trkd05_sel))
 err_nsel_2b0c_trkd05.append(compute_weightedcount_error(events_in_bin_2b0c_trkd05_sel))
 err_nsel_1b0c_trkd05.append(compute_weightedcount_error(events_in_bin_1b0c_trkd05_sel))

 nunsel_1b1c_trkd03.append(np.sum(events_in_bin_1b1c_trkd03_unsel))
 nunsel_0b1c_trkd03.append(np.sum(events_in_bin_0b1c_trkd03_unsel))
 nunsel_0b2c_trkd03.append(np.sum(events_in_bin_0b2c_trkd03_unsel))
 nunsel_0b0c_trkd03.append(np.sum(events_in_bin_0b0c_trkd03_unsel))
 nunsel_2b0c_trkd03.append(np.sum(events_in_bin_2b0c_trkd03_unsel))
 nunsel_1b0c_trkd03.append(np.sum(events_in_bin_1b0c_trkd03_unsel))

 nunsel_1b1c_trkd04.append(np.sum(events_in_bin_1b1c_trkd04_unsel))
 nunsel_0b1c_trkd04.append(np.sum(events_in_bin_0b1c_trkd04_unsel))
 nunsel_0b2c_trkd04.append(np.sum(events_in_bin_0b2c_trkd04_unsel))
 nunsel_0b0c_trkd04.append(np.sum(events_in_bin_0b0c_trkd04_unsel))
 nunsel_2b0c_trkd04.append(np.sum(events_in_bin_2b0c_trkd04_unsel))
 nunsel_1b0c_trkd04.append(np.sum(events_in_bin_1b0c_trkd04_unsel))

 nunsel_1b1c_trkd05.append(np.sum(events_in_bin_1b1c_trkd05_unsel))
 nunsel_0b1c_trkd05.append(np.sum(events_in_bin_0b1c_trkd05_unsel))
 nunsel_0b2c_trkd05.append(np.sum(events_in_bin_0b2c_trkd05_unsel))
 nunsel_0b0c_trkd05.append(np.sum(events_in_bin_0b0c_trkd05_unsel))
 nunsel_2b0c_trkd05.append(np.sum(events_in_bin_2b0c_trkd05_unsel))
 nunsel_1b0c_trkd05.append(np.sum(events_in_bin_1b0c_trkd05_unsel))

 err_nunsel_1b1c_trkd03.append(compute_weightedcount_error(events_in_bin_1b1c_trkd03_unsel))
 err_nunsel_0b1c_trkd03.append(compute_weightedcount_error(events_in_bin_0b1c_trkd03_unsel))
 err_nunsel_0b2c_trkd03.append(compute_weightedcount_error(events_in_bin_0b2c_trkd03_unsel))
 err_nunsel_0b0c_trkd03.append(compute_weightedcount_error(events_in_bin_0b0c_trkd03_unsel))
 err_nunsel_2b0c_trkd03.append(compute_weightedcount_error(events_in_bin_2b0c_trkd03_unsel))
 err_nunsel_1b0c_trkd03.append(compute_weightedcount_error(events_in_bin_1b0c_trkd03_unsel))

 err_nunsel_1b1c_trkd04.append(compute_weightedcount_error(events_in_bin_1b1c_trkd04_unsel))
 err_nunsel_0b1c_trkd04.append(compute_weightedcount_error(events_in_bin_0b1c_trkd04_unsel))
 err_nunsel_0b2c_trkd04.append(compute_weightedcount_error(events_in_bin_0b2c_trkd04_unsel))
 err_nunsel_0b0c_trkd04.append(compute_weightedcount_error(events_in_bin_0b0c_trkd04_unsel))
 err_nunsel_2b0c_trkd04.append(compute_weightedcount_error(events_in_bin_2b0c_trkd04_unsel))
 err_nunsel_1b0c_trkd04.append(compute_weightedcount_error(events_in_bin_1b0c_trkd04_unsel))

 err_nunsel_1b1c_trkd05.append(compute_weightedcount_error(events_in_bin_1b1c_trkd05_unsel))
 err_nunsel_0b1c_trkd05.append(compute_weightedcount_error(events_in_bin_0b1c_trkd05_unsel))
 err_nunsel_0b2c_trkd05.append(compute_weightedcount_error(events_in_bin_0b2c_trkd05_unsel))
 err_nunsel_0b0c_trkd05.append(compute_weightedcount_error(events_in_bin_0b0c_trkd05_unsel))
 err_nunsel_2b0c_trkd05.append(compute_weightedcount_error(events_in_bin_2b0c_trkd05_unsel))
 err_nunsel_1b0c_trkd05.append(compute_weightedcount_error(events_in_bin_1b0c_trkd05_unsel))

nsel_1b1c_trkd03 = np.array(nsel_1b1c_trkd03)
nsel_0b1c_trkd03 = np.array(nsel_0b1c_trkd03)
nsel_0b2c_trkd03 = np.array(nsel_0b2c_trkd03)
nsel_0b0c_trkd03 = np.array(nsel_0b0c_trkd03)
nsel_2b0c_trkd03 = np.array(nsel_2b0c_trkd03)
nsel_1b0c_trkd03 = np.array(nsel_1b0c_trkd03)

nsel_1b1c_trkd04 = np.array(nsel_1b1c_trkd04)
nsel_0b1c_trkd04 = np.array(nsel_0b1c_trkd04)
nsel_0b2c_trkd04 = np.array(nsel_0b2c_trkd04)
nsel_0b0c_trkd04 = np.array(nsel_0b0c_trkd04)
nsel_2b0c_trkd04 = np.array(nsel_2b0c_trkd04)
nsel_1b0c_trkd04 = np.array(nsel_1b0c_trkd04)

nsel_1b1c_trkd05 = np.array(nsel_1b1c_trkd05)
nsel_0b1c_trkd05 = np.array(nsel_0b1c_trkd05)
nsel_0b2c_trkd05 = np.array(nsel_0b2c_trkd05)
nsel_0b0c_trkd05 = np.array(nsel_0b0c_trkd05)
nsel_2b0c_trkd05 = np.array(nsel_2b0c_trkd05)
nsel_1b0c_trkd05 = np.array(nsel_1b0c_trkd05)

err_nsel_1b1c_trkd03 = np.array(err_nsel_1b1c_trkd03)
err_nsel_0b1c_trkd03 = np.array(err_nsel_0b1c_trkd03)
err_nsel_0b2c_trkd03 = np.array(err_nsel_0b2c_trkd03)
err_nsel_0b0c_trkd03 = np.array(err_nsel_0b0c_trkd03)
err_nsel_2b0c_trkd03 = np.array(err_nsel_2b0c_trkd03)
err_nsel_1b0c_trkd03 = np.array(err_nsel_1b0c_trkd03)

err_nsel_1b1c_trkd04 = np.array(err_nsel_1b1c_trkd04)
err_nsel_0b1c_trkd04 = np.array(err_nsel_0b1c_trkd04)
err_nsel_0b2c_trkd04 = np.array(err_nsel_0b2c_trkd04)
err_nsel_0b0c_trkd04 = np.array(err_nsel_0b0c_trkd04)
err_nsel_2b0c_trkd04 = np.array(err_nsel_2b0c_trkd04)
err_nsel_1b0c_trkd04 = np.array(err_nsel_1b0c_trkd04)

err_nsel_1b1c_trkd05 = np.array(err_nsel_1b1c_trkd05)
err_nsel_0b1c_trkd05 = np.array(err_nsel_0b1c_trkd05)
err_nsel_0b2c_trkd05 = np.array(err_nsel_0b2c_trkd05)
err_nsel_0b0c_trkd05 = np.array(err_nsel_0b0c_trkd05)
err_nsel_2b0c_trkd05 = np.array(err_nsel_2b0c_trkd05)
err_nsel_1b0c_trkd05 = np.array(err_nsel_1b0c_trkd05)

nunsel_1b1c_trkd03 = np.array(nunsel_1b1c_trkd03)
nunsel_0b1c_trkd03 = np.array(nunsel_0b1c_trkd03)
nunsel_0b2c_trkd03 = np.array(nunsel_0b2c_trkd03)
nunsel_0b0c_trkd03 = np.array(nunsel_0b0c_trkd03)
nunsel_2b0c_trkd03 = np.array(nunsel_2b0c_trkd03)
nunsel_1b0c_trkd03 = np.array(nunsel_1b0c_trkd03)

nunsel_1b1c_trkd04 = np.array(nunsel_1b1c_trkd04)
nunsel_0b1c_trkd04 = np.array(nunsel_0b1c_trkd04)
nunsel_0b2c_trkd04 = np.array(nunsel_0b2c_trkd04)
nunsel_0b0c_trkd04 = np.array(nunsel_0b0c_trkd04)
nunsel_2b0c_trkd04 = np.array(nunsel_2b0c_trkd04)
nunsel_1b0c_trkd04 = np.array(nunsel_1b0c_trkd04)

nunsel_1b1c_trkd05 = np.array(nunsel_1b1c_trkd05)
nunsel_0b1c_trkd05 = np.array(nunsel_0b1c_trkd05)
nunsel_0b2c_trkd05 = np.array(nunsel_0b2c_trkd05)
nunsel_0b0c_trkd05 = np.array(nunsel_0b0c_trkd05)
nunsel_2b0c_trkd05 = np.array(nunsel_2b0c_trkd05)
nunsel_1b0c_trkd05 = np.array(nunsel_1b0c_trkd05)

err_nunsel_1b1c_trkd03 = np.array(err_nunsel_1b1c_trkd03)
err_nunsel_0b1c_trkd03 = np.array(err_nunsel_0b1c_trkd03)
err_nunsel_0b2c_trkd03 = np.array(err_nunsel_0b2c_trkd03)
err_nunsel_0b0c_trkd03 = np.array(err_nunsel_0b0c_trkd03)
err_nunsel_2b0c_trkd03 = np.array(err_nunsel_2b0c_trkd03)
err_nunsel_1b0c_trkd03 = np.array(err_nunsel_1b0c_trkd03)

err_nunsel_1b1c_trkd04 = np.array(err_nunsel_1b1c_trkd04)
err_nunsel_0b1c_trkd04 = np.array(err_nunsel_0b1c_trkd04)
err_nunsel_0b2c_trkd04 = np.array(err_nunsel_0b2c_trkd04)
err_nunsel_0b0c_trkd04 = np.array(err_nunsel_0b0c_trkd04)
err_nunsel_2b0c_trkd04 = np.array(err_nunsel_2b0c_trkd04)
err_nunsel_1b0c_trkd04 = np.array(err_nunsel_1b0c_trkd04)

err_nunsel_1b1c_trkd05 = np.array(err_nunsel_1b1c_trkd05)
err_nunsel_0b1c_trkd05 = np.array(err_nunsel_0b1c_trkd05)
err_nunsel_0b2c_trkd05 = np.array(err_nunsel_0b2c_trkd05)
err_nunsel_0b0c_trkd05 = np.array(err_nunsel_0b0c_trkd05)
err_nunsel_2b0c_trkd05 = np.array(err_nunsel_2b0c_trkd05)
err_nunsel_1b0c_trkd05 = np.array(err_nunsel_1b0c_trkd05)

eff_1b1c_trkd03 = nsel_1b1c_trkd03 / (nsel_1b1c_trkd03 + nunsel_1b1c_trkd03)
eff_0b1c_trkd03 = nsel_0b1c_trkd03 / (nsel_0b1c_trkd03 + nunsel_0b1c_trkd03)
eff_0b2c_trkd03 = nsel_0b2c_trkd03 / (nsel_0b2c_trkd03 + nunsel_0b2c_trkd03)
eff_0b0c_trkd03 = nsel_0b0c_trkd03 / (nsel_0b0c_trkd03 + nunsel_0b0c_trkd03)
eff_2b0c_trkd03 = nsel_2b0c_trkd03 / (nsel_2b0c_trkd03 + nunsel_2b0c_trkd03)
eff_1b0c_trkd03 = nsel_1b0c_trkd03 / (nsel_1b0c_trkd03 + nunsel_1b0c_trkd03)

eff_1b1c_trkd04 = nsel_1b1c_trkd04 / (nsel_1b1c_trkd04 + nunsel_1b1c_trkd04)
eff_0b1c_trkd04 = nsel_0b1c_trkd04 / (nsel_0b1c_trkd04 + nunsel_0b1c_trkd04)
eff_0b2c_trkd04 = nsel_0b2c_trkd04 / (nsel_0b2c_trkd04 + nunsel_0b2c_trkd04)
eff_0b0c_trkd04 = nsel_0b0c_trkd04 / (nsel_0b0c_trkd04 + nunsel_0b0c_trkd04)
eff_2b0c_trkd04 = nsel_2b0c_trkd04 / (nsel_2b0c_trkd04 + nunsel_2b0c_trkd04)
eff_1b0c_trkd04 = nsel_1b0c_trkd04 / (nsel_1b0c_trkd04 + nunsel_1b0c_trkd04)

eff_1b1c_trkd05 = nsel_1b1c_trkd05 / (nsel_1b1c_trkd05 + nunsel_1b1c_trkd05)
eff_0b1c_trkd05 = nsel_0b1c_trkd05 / (nsel_0b1c_trkd05 + nunsel_0b1c_trkd05)
eff_0b2c_trkd05 = nsel_0b2c_trkd05 / (nsel_0b2c_trkd05 + nunsel_0b2c_trkd05)
eff_0b0c_trkd05 = nsel_0b0c_trkd05 / (nsel_0b0c_trkd05 + nunsel_0b0c_trkd05)
eff_2b0c_trkd05 = nsel_2b0c_trkd05 / (nsel_2b0c_trkd05 + nunsel_2b0c_trkd05)
eff_1b0c_trkd05 = nsel_1b0c_trkd05 / (nsel_1b0c_trkd05 + nunsel_1b0c_trkd05)

err_eff_1b1c_trkd03 = np.sqrt(((err_nsel_1b1c_trkd03**2 * nunsel_1b1c_trkd03**2) + (err_nunsel_1b1c_trkd03**2 * nsel_1b1c_trkd03**2)) / (nsel_1b1c_trkd03 + nunsel_1b1c_trkd03)**4)
err_eff_0b1c_trkd03 = np.sqrt(((err_nsel_0b1c_trkd03**2 * nunsel_0b1c_trkd03**2) + (err_nunsel_0b1c_trkd03**2 * nsel_0b1c_trkd03**2)) / (nsel_0b1c_trkd03 + nunsel_0b1c_trkd03)**4)
err_eff_0b2c_trkd03 = np.sqrt(((err_nsel_0b2c_trkd03**2 * nunsel_0b2c_trkd03**2) + (err_nunsel_0b2c_trkd03**2 * nsel_0b2c_trkd03**2)) / (nsel_0b2c_trkd03 + nunsel_0b2c_trkd03)**4)
err_eff_0b0c_trkd03 = np.sqrt(((err_nsel_0b0c_trkd03**2 * nunsel_0b0c_trkd03**2) + (err_nunsel_0b0c_trkd03**2 * nsel_0b0c_trkd03**2)) / (nsel_0b0c_trkd03 + nunsel_0b0c_trkd03)**4)
err_eff_2b0c_trkd03 = np.sqrt(((err_nsel_2b0c_trkd03**2 * nunsel_2b0c_trkd03**2) + (err_nunsel_2b0c_trkd03**2 * nsel_2b0c_trkd03**2)) / (nsel_2b0c_trkd03 + nunsel_2b0c_trkd03)**4)
err_eff_1b0c_trkd03 = np.sqrt(((err_nsel_1b0c_trkd03**2 * nunsel_1b0c_trkd03**2) + (err_nunsel_1b0c_trkd03**2 * nsel_1b0c_trkd03**2)) / (nsel_1b0c_trkd03 + nunsel_1b0c_trkd03)**4)

err_eff_1b1c_trkd04 = np.sqrt(((err_nsel_1b1c_trkd04**2 * nunsel_1b1c_trkd04**2) + (err_nunsel_1b1c_trkd04**2 * nsel_1b1c_trkd04**2)) / (nsel_1b1c_trkd04 + nunsel_1b1c_trkd04)**4)
err_eff_0b1c_trkd04 = np.sqrt(((err_nsel_0b1c_trkd04**2 * nunsel_0b1c_trkd04**2) + (err_nunsel_0b1c_trkd04**2 * nsel_0b1c_trkd04**2)) / (nsel_0b1c_trkd04 + nunsel_0b1c_trkd04)**4)
err_eff_0b2c_trkd04 = np.sqrt(((err_nsel_0b2c_trkd04**2 * nunsel_0b2c_trkd04**2) + (err_nunsel_0b2c_trkd04**2 * nsel_0b2c_trkd04**2)) / (nsel_0b2c_trkd04 + nunsel_0b2c_trkd04)**4)
err_eff_0b0c_trkd04 = np.sqrt(((err_nsel_0b0c_trkd04**2 * nunsel_0b0c_trkd04**2) + (err_nunsel_0b0c_trkd04**2 * nsel_0b0c_trkd04**2)) / (nsel_0b0c_trkd04 + nunsel_0b0c_trkd04)**4)
err_eff_2b0c_trkd04 = np.sqrt(((err_nsel_2b0c_trkd04**2 * nunsel_2b0c_trkd04**2) + (err_nunsel_2b0c_trkd04**2 * nsel_2b0c_trkd04**2)) / (nsel_2b0c_trkd04 + nunsel_2b0c_trkd04)**4)
err_eff_1b0c_trkd04 = np.sqrt(((err_nsel_1b0c_trkd04**2 * nunsel_1b0c_trkd04**2) + (err_nunsel_1b0c_trkd04**2 * nsel_1b0c_trkd04**2)) / (nsel_1b0c_trkd04 + nunsel_1b0c_trkd04)**4)

err_eff_1b1c_trkd05 = np.sqrt(((err_nsel_1b1c_trkd05**2 * nunsel_1b1c_trkd05**2) + (err_nunsel_1b1c_trkd05**2 * nsel_1b1c_trkd05**2)) / (nsel_1b1c_trkd05 + nunsel_1b1c_trkd05)**4)
err_eff_0b1c_trkd05 = np.sqrt(((err_nsel_0b1c_trkd05**2 * nunsel_0b1c_trkd05**2) + (err_nunsel_0b1c_trkd05**2 * nsel_0b1c_trkd05**2)) / (nsel_0b1c_trkd05 + nunsel_0b1c_trkd05)**4)
err_eff_0b2c_trkd05 = np.sqrt(((err_nsel_0b2c_trkd05**2 * nunsel_0b2c_trkd05**2) + (err_nunsel_0b2c_trkd05**2 * nsel_0b2c_trkd05**2)) / (nsel_0b2c_trkd05 + nunsel_0b2c_trkd05)**4)
err_eff_0b0c_trkd05 = np.sqrt(((err_nsel_0b0c_trkd05**2 * nunsel_0b0c_trkd05**2) + (err_nunsel_0b0c_trkd05**2 * nsel_0b0c_trkd05**2)) / (nsel_0b0c_trkd05 + nunsel_0b0c_trkd05)**4)
err_eff_2b0c_trkd05 = np.sqrt(((err_nsel_2b0c_trkd05**2 * nunsel_2b0c_trkd05**2) + (err_nunsel_2b0c_trkd05**2 * nsel_2b0c_trkd05**2)) / (nsel_2b0c_trkd05 + nunsel_2b0c_trkd05)**4)
err_eff_1b0c_trkd05 = np.sqrt(((err_nsel_1b0c_trkd05**2 * nunsel_1b0c_trkd05**2) + (err_nunsel_1b0c_trkd05**2 * nsel_1b0c_trkd05**2)) / (nsel_1b0c_trkd05 + nunsel_1b0c_trkd05)**4)

element_0b0c_trkd03 = np.nan_to_num(1 / err_eff_0b0c_trkd03**2)
element_0b0c_trkd04 = np.nan_to_num(1 / err_eff_0b0c_trkd04**2)
element_0b0c_trkd05 = np.nan_to_num(1 / err_eff_0b0c_trkd05**2)

element_0b0c_trkd03[err_eff_0b0c_trkd03 == 0] = 0
element_0b0c_trkd04[err_eff_0b0c_trkd04 == 0] = 0
element_0b0c_trkd05[err_eff_0b0c_trkd05 == 0] = 0
eff_0b0c_combined_trkd0 = (eff_0b0c_trkd03 * element_0b0c_trkd03 + eff_0b0c_trkd04 * element_0b0c_trkd04 + eff_0b0c_trkd05 * element_0b0c_trkd05) / (element_0b0c_trkd03 + element_0b0c_trkd04 + element_0b0c_trkd05)
err_eff_0b0c_combined_trkd0 = 1 / (element_0b0c_trkd03 + element_0b0c_trkd04 + element_0b0c_trkd05)

element_1b0c_trkd03 = np.nan_to_num(1 / err_eff_1b0c_trkd03**2)
element_1b0c_trkd04 = np.nan_to_num(1 / err_eff_1b0c_trkd04**2)
element_1b0c_trkd05 = np.nan_to_num(1 / err_eff_1b0c_trkd05**2)

element_1b0c_trkd03[err_eff_1b0c_trkd03 == 0] = 0
element_1b0c_trkd04[err_eff_1b0c_trkd04 == 0] = 0
element_1b0c_trkd05[err_eff_1b0c_trkd05 == 0] = 0
eff_1b0c_combined_trkd0 = (eff_1b0c_trkd03 * element_1b0c_trkd03 + eff_1b0c_trkd04 * element_1b0c_trkd04 + eff_1b0c_trkd05 * element_1b0c_trkd05) / (element_1b0c_trkd03 + element_1b0c_trkd04 + element_1b0c_trkd05)
err_eff_1b0c_combined_trkd0 = 1 / (element_1b0c_trkd03 + element_1b0c_trkd04 + element_1b0c_trkd05)

element_2b0c_trkd03 = np.nan_to_num(1 / err_eff_2b0c_trkd03**2)
element_2b0c_trkd04 = np.nan_to_num(1 / err_eff_2b0c_trkd04**2)
element_2b0c_trkd05 = np.nan_to_num(1 / err_eff_2b0c_trkd05**2)
eff_2b0c_combined_trkd0 = (eff_2b0c_trkd03 * element_2b0c_trkd03 + eff_2b0c_trkd04 * element_2b0c_trkd04 + eff_2b0c_trkd05 * element_2b0c_trkd05) / (element_2b0c_trkd03 + element_2b0c_trkd04 + element_2b0c_trkd05)
err_eff_2b0c_combined_trkd0 = 1 / (element_2b0c_trkd03 + element_2b0c_trkd04 + element_2b0c_trkd05)

element_0b2c_trkd03 = np.nan_to_num(1 / err_eff_0b2c_trkd03**2)
element_0b2c_trkd04 = np.nan_to_num(1 / err_eff_0b2c_trkd04**2)
element_0b2c_trkd05 = np.nan_to_num(1 / err_eff_0b2c_trkd05**2)

element_0b2c_trkd03[err_eff_0b2c_trkd03 == 0] = 0
element_0b2c_trkd04[err_eff_0b2c_trkd04 == 0] = 0
element_0b2c_trkd05[err_eff_0b2c_trkd05 == 0] = 0
eff_0b2c_combined_trkd0 = (eff_0b2c_trkd03 * element_0b2c_trkd03 + eff_0b2c_trkd04 * element_0b2c_trkd04 + eff_0b2c_trkd05 * element_0b2c_trkd05) / (element_0b2c_trkd03 + element_0b2c_trkd04 + element_0b2c_trkd05)
err_eff_0b2c_combined_trkd0 = 1 / (element_0b2c_trkd03 + element_0b2c_trkd04 + element_0b2c_trkd05)

element_0b1c_trkd03 = np.nan_to_num(1 / err_eff_0b1c_trkd03**2)
element_0b1c_trkd04 = np.nan_to_num(1 / err_eff_0b1c_trkd04**2)
element_0b1c_trkd05 = np.nan_to_num(1 / err_eff_0b1c_trkd05**2)

element_0b1c_trkd03[err_eff_0b1c_trkd03 == 0] = 0
element_0b1c_trkd04[err_eff_0b1c_trkd04 == 0] = 0
element_0b1c_trkd05[err_eff_0b1c_trkd05 == 0] = 0
eff_0b1c_combined_trkd0 = (eff_0b1c_trkd03 * element_0b1c_trkd03 + eff_0b1c_trkd04 * element_0b1c_trkd04 + eff_0b1c_trkd05 * element_0b1c_trkd05) / (element_0b1c_trkd03 + element_0b1c_trkd04 + element_0b1c_trkd05)
err_eff_0b1c_combined_trkd0 = 1 / (element_0b1c_trkd03 + element_0b1c_trkd04 + element_0b1c_trkd05)

element_1b1c_trkd03 = np.nan_to_num(1 / err_eff_1b1c_trkd03**2)
element_1b1c_trkd04 = np.nan_to_num(1 / err_eff_1b1c_trkd04**2)
element_1b1c_trkd05 = np.nan_to_num(1 / err_eff_1b1c_trkd05**2)

element_1b1c_trkd03[err_eff_1b1c_trkd03 == 0] = 0
element_1b1c_trkd04[err_eff_1b1c_trkd04 == 0] = 0
element_1b1c_trkd05[err_eff_1b1c_trkd05 == 0] = 0
eff_1b1c_combined_trkd0 = (eff_1b1c_trkd03 * element_1b1c_trkd03 + eff_1b1c_trkd04 * element_1b1c_trkd04 + eff_1b1c_trkd05 * element_1b1c_trkd05) / (element_1b1c_trkd03 + element_1b1c_trkd04 + element_1b1c_trkd05)
err_eff_1b1c_combined_trkd0 = 1 / (element_1b1c_trkd03 + element_1b1c_trkd04 + element_1b1c_trkd05)

# plot 0b + 0c case
plt.figure(figsize=(20,18))
plt.errorbar(pt_bins, eff_0b0c_combined, xerr = pt_bin_width/2, yerr = np.sqrt(err_eff_0b0c_combined), fmt='o', label='nominal', ms=15)
plt.errorbar(pt_bins, eff_0b0c_combined_trkd0, xerr = pt_bin_width/2, yerr = np.sqrt(err_eff_0b0c_combined_trkd0), fmt='o', label='TRK_RES_D0_MEAS', ms=15)

plt.xlabel(r'p$_T$ [GeV]', fontsize = 35)
plt.ylabel('Mistag Efficiency', fontsize = 35)
plt.text(0.02, 0.97, 'ATLAS', fontsize=45, fontweight='bold', fontstyle='italic', color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.18, 0.97, 'Simulation Internal', fontsize=45, color='black', ha='left', va='top', transform=plt.gca().transAxes)
plt.text(0.02, 0.925, 'GN2X_v01 Scores, 0b + 0c category', fontsize=36, color='black', ha='left', va='top', transform=plt.gca().transAxes)

plt.legend(loc = 'lower right', fontsize=32)
plt.grid(True)
plt.yscale('log')
plt.xticks(fontsize=35)
plt.yticks(fontsize=35)

plt.savefig('mistag_0b0c.png', dpi=1000)
