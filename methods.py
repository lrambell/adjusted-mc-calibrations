
import numpy as np

def SubCatSplit(subjets, cat):
    
    a, c = np.unique(np.where(subjets["valid"] == True)[0], return_counts=True)
    b, f = np.unique(np.where(subjets["valid"] == False)[0], return_counts=True)
    valid_2 = a[c == 2]
    valid_1 = a[c == 1]
    valid_3 = a[c == 3]
    invalid_3 = b[f == 3]
    valid_2_mask = subjets["valid"][valid_2]
    valid_1_mask = subjets["valid"][valid_1]
    valid_3_mask = subjets["valid"][valid_3]

    ### 0b + 0c category ###    
    if cat =="lll":
        a, c = np.unique(np.where(subjets["HadronConeExclTruthLabelID"] == 0)[0], return_counts=True)
        lll_jet = a[c == 3]
        intersection = np.intersect1d(valid_3, lll_jet)
        return intersection
    
    if cat == "ll":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:2]
              
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        ll_jet = a[c == 2]  

        intersection = np.intersect1d(valid_2, ll_jet)
        return intersection
    
    if cat == "l":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:1]
              
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        l_jet = a[c == 1]  
        intersection = np.intersect1d(valid_1, l_jet)
        return intersection
    
    if cat == "lt":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:2]
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        l_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        t_jet = a[c == 1]
        intersection = np.intersect1d(valid_2, l_jet)
        intersection = np.intersect1d(intersection, t_jet)
        return intersection
    
    if cat == "ltt":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]       

        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        l_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        tt_jet = a[c == 2]

        intersection = np.intersect1d(valid_3, l_jet)
        intersection = np.intersect1d(intersection, tt_jet)

        return intersection
    
    if cat == "t":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:1]
              
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        t_jet = a[c == 1]  
        intersection = np.intersect1d(valid_1, t_jet)
        return intersection

    if cat == "tt":
        a, c = np.unique(np.where(subjets["HadronConeExclTruthLabelID"] == 15)[0], return_counts=True)
        tt_jet = a[c == 2]
        intersection = np.intersect1d(valid_2, tt_jet)
        return intersection
    
    if cat == "ttt":
        a, c = np.unique(np.where(subjets["HadronConeExclTruthLabelID"] == 15)[0], return_counts=True)
        ttt_jet = a[c == 3]
        intersection = np.intersect1d(valid_3, ttt_jet)
        return intersection
    
    if cat == "tll":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        t_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        ll_jet = a[c == 2]
        intersection = np.intersect1d(valid_3, t_jet)
        intersection = np.intersect1d(intersection, ll_jet)
        return intersection

    

    ### 0b + 1c category ###

    if cat == "c":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:1]
              
        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        c_jet = a[c == 1]  
        intersection = np.intersect1d(valid_1, c_jet)
        return intersection

    if cat == "cl":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:2]
        

        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        l_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        c_jet = a[c == 1]

        intersection = np.intersect1d(valid_2, l_jet)
        intersection = np.intersect1d(intersection, c_jet)

        return intersection


    if cat == "cll":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]       

        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        c_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        ll_jet = a[c == 2]

        intersection = np.intersect1d(valid_3, c_jet)
        intersection = np.intersect1d(intersection, ll_jet)

        return intersection
    
    if cat == "ct":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:2]
        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        c_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        t_jet = a[c == 1]
        intersection = np.intersect1d(valid_2, c_jet)
        intersection = np.intersect1d(intersection, t_jet)
        return intersection
    
    if cat == "ctt":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]       

        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        c_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        tt_jet = a[c == 2]

        intersection = np.intersect1d(valid_3, c_jet)
        intersection = np.intersect1d(intersection, tt_jet)

        return intersection
    
    if cat == "ctl":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]
        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        c_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        t_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        l_jet = a[c == 1]
        intersection = np.intersect1d(valid_3, c_jet)
        intersection = np.intersect1d(intersection, t_jet)
        intersection = np.intersect1d(intersection, l_jet)

        return intersection
    
    ### 0b + 2c category ###

    if cat == "cc":
        a, c = np.unique(np.where(subjets["HadronConeExclTruthLabelID"] == 4)[0], return_counts=True)
        c_jet = a[c == 2]
        intersection = np.intersect1d(valid_2, c_jet)
        return intersection

    if cat == "ccl":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]       

        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        cc_jet = a[c == 2]
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        l_jet = a[c == 1]

        intersection = np.intersect1d(valid_1, cc_jet)
        intersection = np.intersect1d(intersection, l_jet)

        return intersection
    
    if cat == "ccc":
        a, c = np.unique(np.where(subjets["HadronConeExclTruthLabelID"] == 4)[0], return_counts=True)
        ccc_jet = a[c == 3]
        intersection = np.intersect1d(valid_3, ccc_jet)
        return intersection

    if cat == "cct":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]       

        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        cc_jet = a[c == 2]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        t_jet = a[c == 1]

        intersection = np.intersect1d(valid_3, cc_jet)
        intersection = np.intersect1d(intersection, t_jet)

        return intersection
    
    ### 1b + 0c category ###

    if cat == "b":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:1]
              
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 1]  
        intersection = np.intersect1d(valid_1, b_jet)
        return intersection    

    if cat == "bl":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:2]
              
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        l_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 1]

        intersection = np.intersect1d(valid_2, l_jet)
        intersection = np.intersect1d(intersection, b_jet)

        return intersection

    if cat == "bll":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]       

        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        ll_jet = a[c == 2]

        intersection = np.intersect1d(valid_3, b_jet)
        intersection = np.intersect1d(intersection, ll_jet)

        return intersection
    
    if cat == "btl":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        t_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        l_jet = a[c == 1]
        intersection = np.intersect1d(valid_3, b_jet)
        intersection = np.intersect1d(intersection, t_jet)
        intersection = np.intersect1d(intersection, l_jet)

        return intersection
    
    if cat == "btt":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        tt_jet = a[c == 2]
        intersection = np.intersect1d(valid_3, b_jet)
        intersection = np.intersect1d(intersection, tt_jet)

        return intersection
    
    if cat == "bt":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:2]
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        t_jet = a[c == 1]
        intersection = np.intersect1d(valid_2, b_jet)
        intersection = np.intersect1d(intersection, t_jet)
        return intersection

    ### 1b + 1c category ###

    if cat == "bc":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:2]
    
        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        c_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 1]

        intersection = np.intersect1d(valid_2, c_jet)
        intersection = np.intersect1d(intersection, b_jet)

        return intersection
    
    if cat == "bcc":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]       

        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        cc_jet = a[c == 2]

        intersection = np.intersect1d(valid_3, b_jet)
        intersection = np.intersect1d(intersection, cc_jet)

        return intersection
    
    if cat == "bct":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        t_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        c_jet = a[c == 1]
        intersection = np.intersect1d(valid_3, b_jet)
        intersection = np.intersect1d(intersection, t_jet)
        intersection = np.intersect1d(intersection, c_jet)

        return intersection
    
    if cat == "bcl":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:,0:2]
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 4)[0], return_counts=True)
        c_jet = a[c == 1]
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        l_jet = a[c == 1]
        intersection = np.intersect1d(valid_3, b_jet)
        intersection = np.intersect1d(intersection, c_jet)
        intersection = np.intersect1d(intersection, l_jet)

        return intersection
    
    ### 2b + 0c category ###

    if cat == "bb":
        a, c = np.unique(np.where(subjets["HadronConeExclTruthLabelID"] == 5)[0], return_counts=True)
        b_jet = a[c == 2]
        intersection = np.intersect1d(valid_2, b_jet)
        return intersection
    
    if cat == "bbb":
        a, c = np.unique(np.where(subjets["HadronConeExclTruthLabelID"] == 5)[0], return_counts=True)
        bbb_jet = a[c == 3]
        intersection = np.intersect1d(valid_3, bbb_jet)
        return intersection
    
    if cat == "bbt":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 2]
        a, c = np.unique(np.where(subjets_flav == 15)[0], return_counts=True)
        t_jet = a[c == 1]
        intersection = np.intersect1d(valid_3, b_jet)
        intersection = np.intersect1d(intersection, t_jet)
        return intersection
    
    if cat == "bbl":
        subjets_flav = np.array(subjets["HadronConeExclTruthLabelID"][:])[:]
        a, c = np.unique(np.where(subjets_flav == 5)[0], return_counts=True)
        b_jet = a[c == 2]
        a, c = np.unique(np.where(subjets_flav == 0)[0], return_counts=True)
        l_jet = a[c == 1]
        intersection = np.intersect1d(valid_3, b_jet)
        intersection = np.intersect1d(intersection, l_jet)
        return intersection
