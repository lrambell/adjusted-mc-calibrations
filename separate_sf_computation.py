## compute SFs for WPs ##
# 1. load data nominal and adjusted
# 2. compute mistag efficiency with errors vs pt for chosen wp for each category for each file
# 3. take for each pt value the ratio of the mistag efficiency for the adjusted over the nominal
# 4. compute the error on the ratio
# 5. save the ratio and the error on the ratio in a h5 file

import numpy as np
import matplotlib.pyplot as plt
import h5py


weights_slice3 = 26450000 * 1.165833e-2
weights_slice4 = 254610 * 1.336553e-2
weights_slice5 = 4553.2 * 1.452648e-2

discr_cut_50 = 4.335
discr_cut_55 = 4.087
discr_cut_60 = 3.818
discr_cut_65 = 3.518
discr_cut_70 = 3.166
discr_cut_75 = 2.735
discr_cut_80 = 2.211
discr_cut_85 = 1.560

discr_cut = discr_cut_70
wp = 70

efficiency_nominal_filename = f'efficiency_pt_{wp}_nominal.png'
efficiency_trkd0_filename = f'efficiency_pt_{wp}_trkd0.png'
efficiency_trkd0up_filename = f'efficiency_pt_{wp}_trkd0up.png'
efficiency_trkd0down_filename = f'efficiency_pt_{wp}_trkd0down.png'
mistag_eff_trkd0_filename = f'mistag_efficiencies_trkd0{wp}.png'
sf_filename = f'scale_factors_trkd0{wp}.png'
sf_h5file_filename = f'sf_trkd0{wp}.h5'


# preprocess nominal sample
with h5py.File('../files/discriminant_values_nominal3_2.h5', 'r') as f:
    discriminant_0b0c = f['discriminant_0b0c'][:]
    discriminant_1b0c = f['discriminant_1b0c'][:]
    discriminant_2b0c = f['discriminant_2b0c'][:]
    discriminant_1b1c = f['discriminant_1b1c'][:]
    discriminant_0b1c = f['discriminant_0b1c'][:]
    discriminant_0b2c = f['discriminant_0b2c'][:]
    mc_weights_0b0c = f['mc_weights_0b0c'][:]
    mc_weights_1b0c = f['mc_weights_1b0c'][:]
    mc_weights_2b0c = f['mc_weights_2b0c'][:]
    mc_weights_1b1c = f['mc_weights_1b1c'][:]
    mc_weights_0b1c = f['mc_weights_0b1c'][:]
    mc_weights_0b2c = f['mc_weights_0b2c'][:]
    pt_values_0b0c = f['pt_values_0b0c'][:]
    pt_values_1b0c = f['pt_values_1b0c'][:]
    pt_values_2b0c = f['pt_values_2b0c'][:]
    pt_values_1b1c = f['pt_values_1b1c'][:]
    pt_values_0b1c = f['pt_values_0b1c'][:]
    pt_values_0b2c = f['pt_values_0b2c'][:]
    total_w_nominal3 = f['total_weight'][:]
    
weights_nominal3_0b0c = mc_weights_0b0c*weights_slice3/total_w_nominal3
weights_nominal3_1b0c = mc_weights_1b0c*weights_slice3/total_w_nominal3
weights_nominal3_2b0c = mc_weights_2b0c*weights_slice3/total_w_nominal3
weights_nominal3_1b1c = mc_weights_1b1c*weights_slice3/total_w_nominal3
weights_nominal3_0b1c = mc_weights_0b1c*weights_slice3/total_w_nominal3
weights_nominal3_0b2c = mc_weights_0b2c*weights_slice3/total_w_nominal3

discr_w_nominal3_0b0c = discriminant_0b0c
discr_w_nominal3_1b0c = discriminant_1b0c
discr_w_nominal3_2b0c = discriminant_2b0c
discr_w_nominal3_1b1c = discriminant_1b1c
discr_w_nominal3_0b1c = discriminant_0b1c
discr_w_nominal3_0b2c = discriminant_0b2c

pt_values_nominal3_0b0c = pt_values_0b0c
pt_values_nominal3_1b0c = pt_values_1b0c
pt_values_nominal3_2b0c = pt_values_2b0c
pt_values_nominal3_1b1c = pt_values_1b1c
pt_values_nominal3_0b1c = pt_values_0b1c
pt_values_nominal3_0b2c = pt_values_0b2c

with h5py.File('../files/discriminant_values_nominal4.h5', 'r') as f:
    discriminant_0b0c = f['discriminant_0b0c'][:]
    discriminant_1b0c = f['discriminant_1b0c'][:]
    discriminant_2b0c = f['discriminant_2b0c'][:]
    discriminant_1b1c = f['discriminant_1b1c'][:]
    discriminant_0b1c = f['discriminant_0b1c'][:]
    discriminant_0b2c = f['discriminant_0b2c'][:]
    mc_weights_0b0c = f['mc_weights_0b0c'][:]
    mc_weights_1b0c = f['mc_weights_1b0c'][:]
    mc_weights_2b0c = f['mc_weights_2b0c'][:]
    mc_weights_1b1c = f['mc_weights_1b1c'][:]
    mc_weights_0b1c = f['mc_weights_0b1c'][:]
    mc_weights_0b2c = f['mc_weights_0b2c'][:]
    pt_values_0b0c = f['pt_values_0b0c'][:]
    pt_values_1b0c = f['pt_values_1b0c'][:]
    pt_values_2b0c = f['pt_values_2b0c'][:]
    pt_values_1b1c = f['pt_values_1b1c'][:]
    pt_values_0b1c = f['pt_values_0b1c'][:]
    pt_values_0b2c = f['pt_values_0b2c'][:]
    total_w_nominal4 = f['total_weight'][:]

weights_nominal4_0b0c = mc_weights_0b0c*weights_slice4/total_w_nominal4
weights_nominal4_1b0c = mc_weights_1b0c*weights_slice4/total_w_nominal4
weights_nominal4_2b0c = mc_weights_2b0c*weights_slice4/total_w_nominal4
weights_nominal4_1b1c = mc_weights_1b1c*weights_slice4/total_w_nominal4
weights_nominal4_0b1c = mc_weights_0b1c*weights_slice4/total_w_nominal4
weights_nominal4_0b2c = mc_weights_0b2c*weights_slice4/total_w_nominal4

discr_w_nominal4_0b0c = discriminant_0b0c
discr_w_nominal4_1b0c = discriminant_1b0c
discr_w_nominal4_2b0c = discriminant_2b0c
discr_w_nominal4_1b1c = discriminant_1b1c
discr_w_nominal4_0b1c = discriminant_0b1c
discr_w_nominal4_0b2c = discriminant_0b2c

pt_values_nominal4_0b0c = pt_values_0b0c
pt_values_nominal4_1b0c = pt_values_1b0c
pt_values_nominal4_2b0c = pt_values_2b0c
pt_values_nominal4_1b1c = pt_values_1b1c
pt_values_nominal4_0b1c = pt_values_0b1c
pt_values_nominal4_0b2c = pt_values_0b2c

with h5py.File('../files/discriminant_values_nominal5_2.h5', 'r') as f:
    discriminant_0b0c = f['discriminant_0b0c'][:]
    discriminant_1b0c = f['discriminant_1b0c'][:]
    discriminant_2b0c = f['discriminant_2b0c'][:]
    discriminant_1b1c = f['discriminant_1b1c'][:]
    discriminant_0b1c = f['discriminant_0b1c'][:]
    discriminant_0b2c = f['discriminant_0b2c'][:]
    mc_weights_0b0c = f['mc_weights_0b0c'][:]
    mc_weights_1b0c = f['mc_weights_1b0c'][:]
    mc_weights_2b0c = f['mc_weights_2b0c'][:]
    mc_weights_1b1c = f['mc_weights_1b1c'][:]
    mc_weights_0b1c = f['mc_weights_0b1c'][:]
    mc_weights_0b2c = f['mc_weights_0b2c'][:]
    pt_values_0b0c = f['pt_values_0b0c'][:]
    pt_values_1b0c = f['pt_values_1b0c'][:]
    pt_values_2b0c = f['pt_values_2b0c'][:]
    pt_values_1b1c = f['pt_values_1b1c'][:]
    pt_values_0b1c = f['pt_values_0b1c'][:]
    pt_values_0b2c = f['pt_values_0b2c'][:]
    total_w_nominal5 = f['total_weight'][:]

weights_nominal5_0b0c = mc_weights_0b0c*weights_slice5/total_w_nominal5
weights_nominal5_1b0c = mc_weights_1b0c*weights_slice5/total_w_nominal5
weights_nominal5_2b0c = mc_weights_2b0c*weights_slice5/total_w_nominal5
weights_nominal5_1b1c = mc_weights_1b1c*weights_slice5/total_w_nominal5
weights_nominal5_0b1c = mc_weights_0b1c*weights_slice5/total_w_nominal5
weights_nominal5_0b2c = mc_weights_0b2c*weights_slice5/total_w_nominal5

discr_w_nominal5_0b0c = discriminant_0b0c
discr_w_nominal5_1b0c = discriminant_1b0c
discr_w_nominal5_2b0c = discriminant_2b0c
discr_w_nominal5_1b1c = discriminant_1b1c
discr_w_nominal5_0b1c = discriminant_0b1c
discr_w_nominal5_0b2c = discriminant_0b2c

pt_values_nominal5_0b0c = pt_values_0b0c
pt_values_nominal5_1b0c = pt_values_1b0c
pt_values_nominal5_2b0c = pt_values_2b0c
pt_values_nominal5_1b1c = pt_values_1b1c
pt_values_nominal5_0b1c = pt_values_0b1c
pt_values_nominal5_0b2c = pt_values_0b2c

discriminant_0b0c_nominal = np.concatenate((discr_w_nominal3_0b0c, discr_w_nominal4_0b0c, discr_w_nominal5_0b0c))
discriminant_1b0c_nominal = np.concatenate((discr_w_nominal3_1b0c, discr_w_nominal4_1b0c, discr_w_nominal5_1b0c))
discriminant_2b0c_nominal = np.concatenate((discr_w_nominal3_2b0c, discr_w_nominal4_2b0c, discr_w_nominal5_2b0c))
discriminant_1b1c_nominal = np.concatenate((discr_w_nominal3_1b1c, discr_w_nominal4_1b1c, discr_w_nominal5_1b1c))
discriminant_0b1c_nominal = np.concatenate((discr_w_nominal3_0b1c, discr_w_nominal4_0b1c, discr_w_nominal5_0b1c))
discriminant_0b2c_nominal = np.concatenate((discr_w_nominal3_0b2c, discr_w_nominal4_0b2c, discr_w_nominal5_0b2c))

weights_0b0c_nominal = np.concatenate((weights_nominal3_0b0c, weights_nominal4_0b0c, weights_nominal5_0b0c))
weights_1b0c_nominal = np.concatenate((weights_nominal3_1b0c, weights_nominal4_1b0c, weights_nominal5_1b0c))
weights_2b0c_nominal = np.concatenate((weights_nominal3_2b0c, weights_nominal4_2b0c, weights_nominal5_2b0c))
weights_1b1c_nominal = np.concatenate((weights_nominal3_1b1c, weights_nominal4_1b1c, weights_nominal5_1b1c))
weights_0b1c_nominal = np.concatenate((weights_nominal3_0b1c, weights_nominal4_0b1c, weights_nominal5_0b1c))
weights_0b2c_nominal = np.concatenate((weights_nominal3_0b2c, weights_nominal4_0b2c, weights_nominal5_0b2c))

pt_values_0b0c_nominal = np.concatenate((pt_values_nominal3_0b0c, pt_values_nominal4_0b0c, pt_values_nominal5_0b0c))
pt_values_1b0c_nominal = np.concatenate((pt_values_nominal3_1b0c, pt_values_nominal4_1b0c, pt_values_nominal5_1b0c))
pt_values_2b0c_nominal = np.concatenate((pt_values_nominal3_2b0c, pt_values_nominal4_2b0c, pt_values_nominal5_2b0c))
pt_values_1b1c_nominal = np.concatenate((pt_values_nominal3_1b1c, pt_values_nominal4_1b1c, pt_values_nominal5_1b1c))
pt_values_0b1c_nominal = np.concatenate((pt_values_nominal3_0b1c, pt_values_nominal4_0b1c, pt_values_nominal5_0b1c))
pt_values_0b2c_nominal = np.concatenate((pt_values_nominal3_0b2c, pt_values_nominal4_0b2c, pt_values_nominal5_0b2c))


with h5py.File('../files/discriminant_values_trkd03.h5', 'r') as f:
    discriminant_1b0c = f['discriminant_1b0c'][:]
    discriminant_2b0c  = f['discriminant_2b0c'][:]
    discriminant_1b1c  = f['discriminant_1b1c'][:]
    discriminant_0b1c  = f['discriminant_0b1c'][:]
    discriminant_0b2c  = f['discriminant_0b2c'][:]
    discriminant_0b0c  = f['discriminant_0b0c'][:]
    mc_weights_1b0c  = f['mc_weights_1b0c'][:] 
    mc_weights_2b0c  = f['mc_weights_2b0c'][:]
    mc_weights_1b1c  = f['mc_weights_1b1c'][:]
    mc_weights_0b1c  = f['mc_weights_0b1c'][:] 
    mc_weights_0b2c  = f['mc_weights_0b2c'][:] 
    mc_weights_0b0c = f['mc_weights_0b0c'][:] 
    pt_values_1b0c  = f['pt_values_1b0c'][:] 
    pt_values_2b0c  = f['pt_values_2b0c'][:]
    pt_values_1b1c  = f['pt_values_1b1c'][:]
    pt_values_0b1c  = f['pt_values_0b1c'][:]
    pt_values_0b2c  = f['pt_values_0b2c'][:]
    pt_values_0b0c  = f['pt_values_0b0c'][:]
    total_w_trkd03 = f['total_weight'][:]

weights_trkd03_1b0c = mc_weights_1b0c*weights_slice3/total_w_trkd03
weights_trkd03_2b0c = mc_weights_2b0c*weights_slice3/total_w_trkd03
weights_trkd03_1b1c = mc_weights_1b1c*weights_slice3/total_w_trkd03
weights_trkd03_0b1c = mc_weights_0b1c*weights_slice3/total_w_trkd03
weights_trkd03_0b2c = mc_weights_0b2c*weights_slice3/total_w_trkd03
weights_trkd03_0b0c = mc_weights_0b0c*weights_slice3/total_w_trkd03

discr_w_trkd03_1b0c = discriminant_1b0c 
discr_w_trkd03_2b0c = discriminant_2b0c 
discr_w_trkd03_1b1c = discriminant_1b1c 
discr_w_trkd03_0b1c = discriminant_0b1c 
discr_w_trkd03_0b2c = discriminant_0b2c 
discr_w_trkd03_0b0c = discriminant_0b0c 


pt_values_trkd03_0b0c = pt_values_0b0c 
pt_values_trkd03_1b0c = pt_values_1b0c 
pt_values_trkd03_2b0c = pt_values_2b0c 
pt_values_trkd03_1b1c = pt_values_1b1c 
pt_values_trkd03_0b1c = pt_values_0b1c 
pt_values_trkd03_0b2c = pt_values_0b2c 

with h5py.File('../files/discriminant_values_trkd04.h5', 'r') as f:
    discriminant_1b0c  = f['discriminant_1b0c'][:]
    discriminant_2b0c  = f['discriminant_2b0c'][:]
    discriminant_1b1c  = f['discriminant_1b1c'][:]
    discriminant_0b1c  = f['discriminant_0b1c'][:]
    discriminant_0b2c  = f['discriminant_0b2c'][:]
    discriminant_0b0c  = f['discriminant_0b0c'][:]
    mc_weights_1b0c  = f['mc_weights_1b0c'][:] 
    mc_weights_2b0c  = f['mc_weights_2b0c'][:]
    mc_weights_1b1c  = f['mc_weights_1b1c'][:]
    mc_weights_0b1c  = f['mc_weights_0b1c'][:] 
    mc_weights_0b2c  = f['mc_weights_0b2c'][:] 
    mc_weights_0b0c  = f['mc_weights_0b0c'][:] 
    pt_values_1b0c  = f['pt_values_1b0c'][:] 
    pt_values_2b0c  = f['pt_values_2b0c'][:]
    pt_values_1b1c  = f['pt_values_1b1c'][:]
    pt_values_0b1c  = f['pt_values_0b1c'][:]
    pt_values_0b2c  = f['pt_values_0b2c'][:]
    pt_values_0b0c  = f['pt_values_0b0c'][:]
    total_w_trkd04 = f['total_weight'][:]


weights_trkd04_1b0c = mc_weights_1b0c*weights_slice4/total_w_trkd04
weights_trkd04_2b0c = mc_weights_2b0c*weights_slice4/total_w_trkd04
weights_trkd04_1b1c = mc_weights_1b1c*weights_slice4/total_w_trkd04
weights_trkd04_0b1c = mc_weights_0b1c*weights_slice4/total_w_trkd04
weights_trkd04_0b2c = mc_weights_0b2c*weights_slice4/total_w_trkd04
weights_trkd04_0b0c = mc_weights_0b0c*weights_slice4/total_w_trkd04

discr_w_trkd04_1b0c = discriminant_1b0c 
discr_w_trkd04_2b0c = discriminant_2b0c 
discr_w_trkd04_1b1c = discriminant_1b1c 
discr_w_trkd04_0b1c = discriminant_0b1c 
discr_w_trkd04_0b2c = discriminant_0b2c 
discr_w_trkd04_0b0c = discriminant_0b0c 


pt_values_trkd04_0b0c = pt_values_0b0c 
pt_values_trkd04_1b0c = pt_values_1b0c
pt_values_trkd04_2b0c = pt_values_2b0c 
pt_values_trkd04_1b1c = pt_values_1b1c 
pt_values_trkd04_0b1c = pt_values_0b1c
pt_values_trkd04_0b2c = pt_values_0b2c 


with h5py.File('../files/discriminant_values_trkd05.h5', 'r') as f:
    discriminant_1b0c  = f['discriminant_1b0c'][:]
    discriminant_2b0c  = f['discriminant_2b0c'][:]
    discriminant_1b1c  = f['discriminant_1b1c'][:]
    discriminant_0b1c  = f['discriminant_0b1c'][:]
    discriminant_0b2c  = f['discriminant_0b2c'][:]
    discriminant_0b0c  = f['discriminant_0b0c'][:]
    mc_weights_1b0c  = f['mc_weights_1b0c'][:] 
    mc_weights_2b0c  = f['mc_weights_2b0c'][:]
    mc_weights_1b1c  = f['mc_weights_1b1c'][:]
    mc_weights_0b1c  = f['mc_weights_0b1c'][:] 
    mc_weights_0b2c  = f['mc_weights_0b2c'][:] 
    mc_weights_0b0c  = f['mc_weights_0b0c'][:] 
    pt_values_1b0c  = f['pt_values_1b0c'][:] 
    pt_values_2b0c  = f['pt_values_2b0c'][:]
    pt_values_1b1c  = f['pt_values_1b1c'][:]
    pt_values_0b1c  = f['pt_values_0b1c'][:]
    pt_values_0b2c  = f['pt_values_0b2c'][:]
    pt_values_0b0c  = f['pt_values_0b0c'][:]
    total_w_trkd05 = f['total_weight'][:]

weights_trkd05_1b0c = mc_weights_1b0c*weights_slice5/total_w_trkd05
weights_trkd05_2b0c = mc_weights_2b0c*weights_slice5/total_w_trkd05
weights_trkd05_1b1c = mc_weights_1b1c*weights_slice5/total_w_trkd05
weights_trkd05_0b1c = mc_weights_0b1c*weights_slice5/total_w_trkd05
weights_trkd05_0b2c = mc_weights_0b2c*weights_slice5/total_w_trkd05
weights_trkd05_0b0c = mc_weights_0b0c*weights_slice5/total_w_trkd05

discr_w_trkd05_1b0c = discriminant_1b0c 
discr_w_trkd05_2b0c = discriminant_2b0c 
discr_w_trkd05_1b1c = discriminant_1b1c 
discr_w_trkd05_0b1c = discriminant_0b1c 
discr_w_trkd05_0b2c = discriminant_0b2c 
discr_w_trkd05_0b0c = discriminant_0b0c 


pt_values_trkd05_0b0c = pt_values_0b0c 
pt_values_trkd05_1b0c = pt_values_1b0c 
pt_values_trkd05_2b0c = pt_values_2b0c 
pt_values_trkd05_1b1c = pt_values_1b1c 
pt_values_trkd05_0b1c = pt_values_0b1c 
pt_values_trkd05_0b2c = pt_values_0b2c 


discriminant_0b0c_trkd0 = np.concatenate((discr_w_trkd03_0b0c, discr_w_trkd04_0b0c, discr_w_trkd05_0b0c))
discriminant_1b0c_trkd0 = np.concatenate((discr_w_trkd03_1b0c, discr_w_trkd04_1b0c, discr_w_trkd05_1b0c))
discriminant_2b0c_trkd0 = np.concatenate((discr_w_trkd03_2b0c, discr_w_trkd04_2b0c, discr_w_trkd05_2b0c))
discriminant_1b1c_trkd0 = np.concatenate((discr_w_trkd03_1b1c, discr_w_trkd04_1b1c, discr_w_trkd05_1b1c))
discriminant_0b1c_trkd0 = np.concatenate((discr_w_trkd03_0b1c, discr_w_trkd04_0b1c, discr_w_trkd05_0b1c))
discriminant_0b2c_trkd0 = np.concatenate((discr_w_trkd03_0b2c, discr_w_trkd04_0b2c, discr_w_trkd05_0b2c))

weights_0b0c_trkd0 = np.concatenate((weights_trkd03_0b0c, weights_trkd04_0b0c, weights_trkd05_0b0c))
weights_1b0c_trkd0 = np.concatenate((weights_trkd03_1b0c, weights_trkd04_1b0c, weights_trkd05_1b0c))
weights_2b0c_trkd0 = np.concatenate((weights_trkd03_2b0c, weights_trkd04_2b0c, weights_trkd05_2b0c))
weights_1b1c_trkd0 = np.concatenate((weights_trkd03_1b1c, weights_trkd04_1b1c, weights_trkd05_1b1c))
weights_0b1c_trkd0 = np.concatenate((weights_trkd03_0b1c, weights_trkd04_0b1c, weights_trkd05_0b1c))
weights_0b2c_trkd0 = np.concatenate((weights_trkd03_0b2c, weights_trkd04_0b2c, weights_trkd05_0b2c))

pt_values_0b0c_trkd0 = np.concatenate((pt_values_trkd03_0b0c, pt_values_trkd04_0b0c, pt_values_trkd05_0b0c))
pt_values_1b0c_trkd0 = np.concatenate((pt_values_trkd03_1b0c, pt_values_trkd04_1b0c, pt_values_trkd05_1b0c))
pt_values_2b0c_trkd0 = np.concatenate((pt_values_trkd03_2b0c, pt_values_trkd04_2b0c, pt_values_trkd05_2b0c))
pt_values_1b1c_trkd0 = np.concatenate((pt_values_trkd03_1b1c, pt_values_trkd04_1b1c, pt_values_trkd05_1b1c))
pt_values_0b1c_trkd0 = np.concatenate((pt_values_trkd03_0b1c, pt_values_trkd04_0b1c, pt_values_trkd05_0b1c))
pt_values_0b2c_trkd0 = np.concatenate((pt_values_trkd03_0b2c, pt_values_trkd04_0b2c, pt_values_trkd05_0b2c))


with h5py.File('../files/discriminant_values_trkd0up3.h5', 'r') as f:
    discriminant_0b0c = f['discriminant_0b0c'][:]
    discriminant_1b0c = f['discriminant_1b0c'][:]
    discriminant_2b0c = f['discriminant_2b0c'][:]
    discriminant_1b1c = f['discriminant_1b1c'][:]
    discriminant_0b1c = f['discriminant_0b1c'][:]
    discriminant_0b2c = f['discriminant_0b2c'][:]
    mc_weights_0b0c = f['mc_weights_0b0c'][:]
    mc_weights_1b0c = f['mc_weights_1b0c'][:]
    mc_weights_2b0c = f['mc_weights_2b0c'][:]
    mc_weights_1b1c = f['mc_weights_1b1c'][:]
    mc_weights_0b1c = f['mc_weights_0b1c'][:]
    mc_weights_0b2c = f['mc_weights_0b2c'][:]
    pt_values_0b0c = f['pt_values_0b0c'][:]
    pt_values_1b0c = f['pt_values_1b0c'][:]
    pt_values_2b0c = f['pt_values_2b0c'][:]
    pt_values_1b1c = f['pt_values_1b1c'][:]
    pt_values_0b1c = f['pt_values_0b1c'][:]
    pt_values_0b2c = f['pt_values_0b2c'][:]
    total_w_trkd0up3 = f['total_weight'][:]

weights_trkd0up3_0b0c = mc_weights_0b0c*weights_slice3/total_w_trkd0up3
weights_trkd0up3_1b0c = mc_weights_1b0c*weights_slice3/total_w_trkd0up3
weights_trkd0up3_2b0c = mc_weights_2b0c*weights_slice3/total_w_trkd0up3
weights_trkd0up3_1b1c = mc_weights_1b1c*weights_slice3/total_w_trkd0up3
weights_trkd0up3_0b1c = mc_weights_0b1c*weights_slice3/total_w_trkd0up3
weights_trkd0up3_0b2c = mc_weights_0b2c*weights_slice3/total_w_trkd0up3

discr_w_trkd0up3_0b0c = discriminant_0b0c
discr_w_trkd0up3_1b0c = discriminant_1b0c
discr_w_trkd0up3_2b0c = discriminant_2b0c
discr_w_trkd0up3_1b1c = discriminant_1b1c
discr_w_trkd0up3_0b1c = discriminant_0b1c
discr_w_trkd0up3_0b2c = discriminant_0b2c

pt_values_trkd0up3_0b0c = pt_values_0b0c
pt_values_trkd0up3_1b0c = pt_values_1b0c
pt_values_trkd0up3_2b0c = pt_values_2b0c
pt_values_trkd0up3_1b1c = pt_values_1b1c
pt_values_trkd0up3_0b1c = pt_values_0b1c
pt_values_trkd0up3_0b2c = pt_values_0b2c

with h5py.File('../files/discriminant_values_trkd0up4.h5', 'r') as f:
    discriminant_0b0c = f['discriminant_0b0c'][:]
    discriminant_1b0c = f['discriminant_1b0c'][:]
    discriminant_2b0c = f['discriminant_2b0c'][:]
    discriminant_1b1c = f['discriminant_1b1c'][:]
    discriminant_0b1c = f['discriminant_0b1c'][:]
    discriminant_0b2c = f['discriminant_0b2c'][:]
    mc_weights_0b0c = f['mc_weights_0b0c'][:]
    mc_weights_1b0c = f['mc_weights_1b0c'][:]
    mc_weights_2b0c = f['mc_weights_2b0c'][:]
    mc_weights_1b1c = f['mc_weights_1b1c'][:]
    mc_weights_0b1c = f['mc_weights_0b1c'][:]
    mc_weights_0b2c = f['mc_weights_0b2c'][:]
    pt_values_0b0c = f['pt_values_0b0c'][:]
    pt_values_1b0c = f['pt_values_1b0c'][:]
    pt_values_2b0c = f['pt_values_2b0c'][:]
    pt_values_1b1c = f['pt_values_1b1c'][:]
    pt_values_0b1c = f['pt_values_0b1c'][:]
    pt_values_0b2c = f['pt_values_0b2c'][:]
    total_w_trkd0up4 = f['total_weight'][:]

weights_trkd0up4_0b0c = mc_weights_0b0c*weights_slice4/total_w_trkd0up4
weights_trkd0up4_1b0c = mc_weights_1b0c*weights_slice4/total_w_trkd0up4
weights_trkd0up4_2b0c = mc_weights_2b0c*weights_slice4/total_w_trkd0up4
weights_trkd0up4_1b1c = mc_weights_1b1c*weights_slice4/total_w_trkd0up4
weights_trkd0up4_0b1c = mc_weights_0b1c*weights_slice4/total_w_trkd0up4
weights_trkd0up4_0b2c = mc_weights_0b2c*weights_slice4/total_w_trkd0up4

discr_w_trkd0up4_0b0c = discriminant_0b0c
discr_w_trkd0up4_1b0c = discriminant_1b0c
discr_w_trkd0up4_2b0c = discriminant_2b0c
discr_w_trkd0up4_1b1c = discriminant_1b1c
discr_w_trkd0up4_0b1c = discriminant_0b1c
discr_w_trkd0up4_0b2c = discriminant_0b2c

pt_values_trkd0up4_0b0c = pt_values_0b0c
pt_values_trkd0up4_1b0c = pt_values_1b0c
pt_values_trkd0up4_2b0c = pt_values_2b0c
pt_values_trkd0up4_1b1c = pt_values_1b1c
pt_values_trkd0up4_0b1c = pt_values_0b1c
pt_values_trkd0up4_0b2c = pt_values_0b2c

with h5py.File('../files/discriminant_values_trkd0up5.h5', 'r') as f:
    discriminant_0b0c = f['discriminant_0b0c'][:]
    discriminant_1b0c = f['discriminant_1b0c'][:]
    discriminant_2b0c = f['discriminant_2b0c'][:]
    discriminant_1b1c = f['discriminant_1b1c'][:]
    discriminant_0b1c = f['discriminant_0b1c'][:]
    discriminant_0b2c = f['discriminant_0b2c'][:]
    mc_weights_0b0c = f['mc_weights_0b0c'][:]
    mc_weights_1b0c = f['mc_weights_1b0c'][:]
    mc_weights_2b0c = f['mc_weights_2b0c'][:]
    mc_weights_1b1c = f['mc_weights_1b1c'][:]
    mc_weights_0b1c = f['mc_weights_0b1c'][:]
    mc_weights_0b2c = f['mc_weights_0b2c'][:]
    pt_values_0b0c = f['pt_values_0b0c'][:]
    pt_values_1b0c = f['pt_values_1b0c'][:]
    pt_values_2b0c = f['pt_values_2b0c'][:]
    pt_values_1b1c = f['pt_values_1b1c'][:]
    pt_values_0b1c = f['pt_values_0b1c'][:]
    pt_values_0b2c = f['pt_values_0b2c'][:]
    total_w_trkd0up5 = f['total_weight'][:]

weights_trkd0up5_0b0c = mc_weights_0b0c*weights_slice5/total_w_trkd0up5                     
weights_trkd0up5_1b0c = mc_weights_1b0c*weights_slice5/total_w_trkd0up5
weights_trkd0up5_2b0c = mc_weights_2b0c*weights_slice5/total_w_trkd0up5
weights_trkd0up5_1b1c = mc_weights_1b1c*weights_slice5/total_w_trkd0up5
weights_trkd0up5_0b1c = mc_weights_0b1c*weights_slice5/total_w_trkd0up5
weights_trkd0up5_0b2c = mc_weights_0b2c*weights_slice5/total_w_trkd0up5

discr_w_trkd0up5_0b0c = discriminant_0b0c
discr_w_trkd0up5_1b0c = discriminant_1b0c
discr_w_trkd0up5_2b0c = discriminant_2b0c
discr_w_trkd0up5_1b1c = discriminant_1b1c
discr_w_trkd0up5_0b1c = discriminant_0b1c
discr_w_trkd0up5_0b2c = discriminant_0b2c

pt_values_trkd0up5_0b0c = pt_values_0b0c
pt_values_trkd0up5_1b0c = pt_values_1b0c
pt_values_trkd0up5_2b0c = pt_values_2b0c
pt_values_trkd0up5_1b1c = pt_values_1b1c
pt_values_trkd0up5_0b1c = pt_values_0b1c
pt_values_trkd0up5_0b2c = pt_values_0b2c

discriminant_0b0c_trkd0up = np.concatenate((discr_w_trkd0up3_0b0c, discr_w_trkd0up4_0b0c, discr_w_trkd0up5_0b0c))
discriminant_1b0c_trkd0up = np.concatenate((discr_w_trkd0up3_1b0c, discr_w_trkd0up4_1b0c, discr_w_trkd0up5_1b0c))
discriminant_2b0c_trkd0up = np.concatenate((discr_w_trkd0up3_2b0c, discr_w_trkd0up4_2b0c, discr_w_trkd0up5_2b0c))
discriminant_1b1c_trkd0up = np.concatenate((discr_w_trkd0up3_1b1c, discr_w_trkd0up4_1b1c, discr_w_trkd0up5_1b1c))
discriminant_0b1c_trkd0up = np.concatenate((discr_w_trkd0up3_0b1c, discr_w_trkd0up4_0b1c, discr_w_trkd0up5_0b1c))
discriminant_0b2c_trkd0up = np.concatenate((discr_w_trkd0up3_0b2c, discr_w_trkd0up4_0b2c, discr_w_trkd0up5_0b2c))

weights_0b0c_trkd0up = np.concatenate((weights_trkd0up3_0b0c, weights_trkd0up4_0b0c, weights_trkd0up5_0b0c))
weights_1b0c_trkd0up = np.concatenate((weights_trkd0up3_1b0c, weights_trkd0up4_1b0c, weights_trkd0up5_1b0c))
weights_2b0c_trkd0up = np.concatenate((weights_trkd0up3_2b0c, weights_trkd0up4_2b0c, weights_trkd0up5_2b0c))
weights_1b1c_trkd0up = np.concatenate((weights_trkd0up3_1b1c, weights_trkd0up4_1b1c, weights_trkd0up5_1b1c))
weights_0b1c_trkd0up = np.concatenate((weights_trkd0up3_0b1c, weights_trkd0up4_0b1c, weights_trkd0up5_0b1c))
weights_0b2c_trkd0up = np.concatenate((weights_trkd0up3_0b2c, weights_trkd0up4_0b2c, weights_trkd0up5_0b2c))

pt_values_0b0c_trkd0up = np.concatenate((pt_values_trkd0up3_0b0c, pt_values_trkd0up4_0b0c, pt_values_trkd0up5_0b0c))
pt_values_1b0c_trkd0up = np.concatenate((pt_values_trkd0up3_1b0c, pt_values_trkd0up4_1b0c, pt_values_trkd0up5_1b0c))
pt_values_2b0c_trkd0up = np.concatenate((pt_values_trkd0up3_2b0c, pt_values_trkd0up4_2b0c, pt_values_trkd0up5_2b0c))
pt_values_1b1c_trkd0up = np.concatenate((pt_values_trkd0up3_1b1c, pt_values_trkd0up4_1b1c, pt_values_trkd0up5_1b1c))
pt_values_0b1c_trkd0up = np.concatenate((pt_values_trkd0up3_0b1c, pt_values_trkd0up4_0b1c, pt_values_trkd0up5_0b1c))
pt_values_0b2c_trkd0up = np.concatenate((pt_values_trkd0up3_0b2c, pt_values_trkd0up4_0b2c, pt_values_trkd0up5_0b2c))


with h5py.File('../files/discriminant_values_trkd0down3.h5', 'r') as f:
    discriminant_0b0c = f['discriminant_0b0c'][:]
    discriminant_1b0c = f['discriminant_1b0c'][:]
    discriminant_2b0c = f['discriminant_2b0c'][:]
    discriminant_1b1c = f['discriminant_1b1c'][:]
    discriminant_0b1c = f['discriminant_0b1c'][:]
    discriminant_0b2c = f['discriminant_0b2c'][:]
    mc_weights_0b0c = f['mc_weights_0b0c'][:]
    mc_weights_1b0c = f['mc_weights_1b0c'][:]
    mc_weights_2b0c = f['mc_weights_2b0c'][:]
    mc_weights_1b1c = f['mc_weights_1b1c'][:]
    mc_weights_0b1c = f['mc_weights_0b1c'][:]
    mc_weights_0b2c = f['mc_weights_0b2c'][:]
    pt_values_0b0c = f['pt_values_0b0c'][:]
    pt_values_1b0c = f['pt_values_1b0c'][:]
    pt_values_2b0c = f['pt_values_2b0c'][:]
    pt_values_1b1c = f['pt_values_1b1c'][:]
    pt_values_0b1c = f['pt_values_0b1c'][:]
    pt_values_0b2c = f['pt_values_0b2c'][:]
    total_w_trkd0down3 = f['total_weight'][:]

weights_trkd0down3_0b0c = mc_weights_0b0c*weights_slice3/total_w_trkd0down3
weights_trkd0down3_1b0c = mc_weights_1b0c*weights_slice3/total_w_trkd0down3
weights_trkd0down3_2b0c = mc_weights_2b0c*weights_slice3/total_w_trkd0down3
weights_trkd0down3_1b1c = mc_weights_1b1c*weights_slice3/total_w_trkd0down3
weights_trkd0down3_0b1c = mc_weights_0b1c*weights_slice3/total_w_trkd0down3
weights_trkd0down3_0b2c = mc_weights_0b2c*weights_slice3/total_w_trkd0down3

discr_w_trkd0down3_0b0c = discriminant_0b0c
discr_w_trkd0down3_1b0c = discriminant_1b0c
discr_w_trkd0down3_2b0c = discriminant_2b0c
discr_w_trkd0down3_1b1c = discriminant_1b1c
discr_w_trkd0down3_0b1c = discriminant_0b1c
discr_w_trkd0down3_0b2c = discriminant_0b2c

pt_values_trkd0down3_0b0c = pt_values_0b0c
pt_values_trkd0down3_1b0c = pt_values_1b0c
pt_values_trkd0down3_2b0c = pt_values_2b0c
pt_values_trkd0down3_1b1c = pt_values_1b1c
pt_values_trkd0down3_0b1c = pt_values_0b1c
pt_values_trkd0down3_0b2c = pt_values_0b2c

with h5py.File('../files/discriminant_values_trkd0down4.h5', 'r') as f:
    discriminant_0b0c = f['discriminant_0b0c'][:]
    discriminant_1b0c = f['discriminant_1b0c'][:]
    discriminant_2b0c = f['discriminant_2b0c'][:]
    discriminant_1b1c = f['discriminant_1b1c'][:]
    discriminant_0b1c = f['discriminant_0b1c'][:]
    discriminant_0b2c = f['discriminant_0b2c'][:]
    mc_weights_0b0c = f['mc_weights_0b0c'][:]
    mc_weights_1b0c = f['mc_weights_1b0c'][:]
    mc_weights_2b0c = f['mc_weights_2b0c'][:]
    mc_weights_1b1c = f['mc_weights_1b1c'][:]
    mc_weights_0b1c = f['mc_weights_0b1c'][:]
    mc_weights_0b2c = f['mc_weights_0b2c'][:]
    pt_values_0b0c = f['pt_values_0b0c'][:]
    pt_values_1b0c = f['pt_values_1b0c'][:]
    pt_values_2b0c = f['pt_values_2b0c'][:]
    pt_values_1b1c = f['pt_values_1b1c'][:]
    pt_values_0b1c = f['pt_values_0b1c'][:]
    pt_values_0b2c = f['pt_values_0b2c'][:]
    total_w_trkd0down4 = f['total_weight'][:]

weights_trkd0down4_0b0c = mc_weights_0b0c*weights_slice4/total_w_trkd0down4
weights_trkd0down4_1b0c = mc_weights_1b0c*weights_slice4/total_w_trkd0down4
weights_trkd0down4_2b0c = mc_weights_2b0c*weights_slice4/total_w_trkd0down4
weights_trkd0down4_1b1c = mc_weights_1b1c*weights_slice4/total_w_trkd0down4
weights_trkd0down4_0b1c = mc_weights_0b1c*weights_slice4/total_w_trkd0down4
weights_trkd0down4_0b2c = mc_weights_0b2c*weights_slice4/total_w_trkd0down4

discr_w_trkd0down4_0b0c = discriminant_0b0c
discr_w_trkd0down4_1b0c = discriminant_1b0c
discr_w_trkd0down4_2b0c = discriminant_2b0c
discr_w_trkd0down4_1b1c = discriminant_1b1c
discr_w_trkd0down4_0b1c = discriminant_0b1c
discr_w_trkd0down4_0b2c = discriminant_0b2c

pt_values_trkd0down4_0b0c = pt_values_0b0c
pt_values_trkd0down4_1b0c = pt_values_1b0c
pt_values_trkd0down4_2b0c = pt_values_2b0c
pt_values_trkd0down4_1b1c = pt_values_1b1c
pt_values_trkd0down4_0b1c = pt_values_0b1c
pt_values_trkd0down4_0b2c = pt_values_0b2c

with h5py.File('../files/discriminant_values_trkd0down5.h5', 'r') as f:
    discriminant_0b0c = f['discriminant_0b0c'][:]
    discriminant_1b0c = f['discriminant_1b0c'][:]
    discriminant_2b0c = f['discriminant_2b0c'][:]
    discriminant_1b1c = f['discriminant_1b1c'][:]
    discriminant_0b1c = f['discriminant_0b1c'][:]
    discriminant_0b2c = f['discriminant_0b2c'][:]
    mc_weights_0b0c = f['mc_weights_0b0c'][:]
    mc_weights_1b0c = f['mc_weights_1b0c'][:]
    mc_weights_2b0c = f['mc_weights_2b0c'][:]
    mc_weights_1b1c = f['mc_weights_1b1c'][:]
    mc_weights_0b1c = f['mc_weights_0b1c'][:]
    mc_weights_0b2c = f['mc_weights_0b2c'][:]
    pt_values_0b0c = f['pt_values_0b0c'][:]
    pt_values_1b0c = f['pt_values_1b0c'][:]
    pt_values_2b0c = f['pt_values_2b0c'][:]
    pt_values_1b1c = f['pt_values_1b1c'][:]
    pt_values_0b1c = f['pt_values_0b1c'][:]
    pt_values_0b2c = f['pt_values_0b2c'][:]
    total_w_trkd0down5 = f['total_weight'][:]

weights_trkd0down5_0b0c = mc_weights_0b0c*weights_slice5/total_w_trkd0down5
weights_trkd0down5_1b0c = mc_weights_1b0c*weights_slice5/total_w_trkd0down5
weights_trkd0down5_2b0c = mc_weights_2b0c*weights_slice5/total_w_trkd0down5
weights_trkd0down5_1b1c = mc_weights_1b1c*weights_slice5/total_w_trkd0down5
weights_trkd0down5_0b1c = mc_weights_0b1c*weights_slice5/total_w_trkd0down5
weights_trkd0down5_0b2c = mc_weights_0b2c*weights_slice5/total_w_trkd0down5

discr_w_trkd0down5_0b0c = discriminant_0b0c
discr_w_trkd0down5_1b0c = discriminant_1b0c
discr_w_trkd0down5_2b0c = discriminant_2b0c
discr_w_trkd0down5_1b1c = discriminant_1b1c
discr_w_trkd0down5_0b1c = discriminant_0b1c
discr_w_trkd0down5_0b2c = discriminant_0b2c

pt_values_trkd0down5_0b0c = pt_values_0b0c
pt_values_trkd0down5_1b0c = pt_values_1b0c
pt_values_trkd0down5_2b0c = pt_values_2b0c
pt_values_trkd0down5_1b1c = pt_values_1b1c
pt_values_trkd0down5_0b1c = pt_values_0b1c
pt_values_trkd0down5_0b2c = pt_values_0b2c

discriminant_0b0c_trkd0down = np.concatenate((discr_w_trkd0down3_0b0c, discr_w_trkd0down4_0b0c, discr_w_trkd0down5_0b0c))
discriminant_1b0c_trkd0down = np.concatenate((discr_w_trkd0down3_1b0c, discr_w_trkd0down4_1b0c, discr_w_trkd0down5_1b0c))
discriminant_2b0c_trkd0down = np.concatenate((discr_w_trkd0down3_2b0c, discr_w_trkd0down4_2b0c, discr_w_trkd0down5_2b0c))
discriminant_1b1c_trkd0down = np.concatenate((discr_w_trkd0down3_1b1c, discr_w_trkd0down4_1b1c, discr_w_trkd0down5_1b1c))
discriminant_0b1c_trkd0down = np.concatenate((discr_w_trkd0down3_0b1c, discr_w_trkd0down4_0b1c, discr_w_trkd0down5_0b1c))
discriminant_0b2c_trkd0down = np.concatenate((discr_w_trkd0down3_0b2c, discr_w_trkd0down4_0b2c, discr_w_trkd0down5_0b2c))

weights_0b0c_trkd0down = np.concatenate((weights_trkd0down3_0b0c, weights_trkd0down4_0b0c, weights_trkd0down5_0b0c))
weights_1b0c_trkd0down = np.concatenate((weights_trkd0down3_1b0c, weights_trkd0down4_1b0c, weights_trkd0down5_1b0c))
weights_2b0c_trkd0down = np.concatenate((weights_trkd0down3_2b0c, weights_trkd0down4_2b0c, weights_trkd0down5_2b0c))
weights_1b1c_trkd0down = np.concatenate((weights_trkd0down3_1b1c, weights_trkd0down4_1b1c, weights_trkd0down5_1b1c))
weights_0b1c_trkd0down = np.concatenate((weights_trkd0down3_0b1c, weights_trkd0down4_0b1c, weights_trkd0down5_0b1c))
weights_0b2c_trkd0down = np.concatenate((weights_trkd0down3_0b2c, weights_trkd0down4_0b2c, weights_trkd0down5_0b2c))

pt_values_0b0c_trkd0down = np.concatenate((pt_values_trkd0down3_0b0c, pt_values_trkd0down4_0b0c, pt_values_trkd0down5_0b0c))
pt_values_1b0c_trkd0down = np.concatenate((pt_values_trkd0down3_1b0c, pt_values_trkd0down4_1b0c, pt_values_trkd0down5_1b0c))
pt_values_2b0c_trkd0down = np.concatenate((pt_values_trkd0down3_2b0c, pt_values_trkd0down4_2b0c, pt_values_trkd0down5_2b0c))
pt_values_1b1c_trkd0down = np.concatenate((pt_values_trkd0down3_1b1c, pt_values_trkd0down4_1b1c, pt_values_trkd0down5_1b1c))
pt_values_0b1c_trkd0down = np.concatenate((pt_values_trkd0down3_0b1c, pt_values_trkd0down4_0b1c, pt_values_trkd0down5_0b1c))
pt_values_0b2c_trkd0down = np.concatenate((pt_values_trkd0down3_0b2c, pt_values_trkd0down4_0b2c, pt_values_trkd0down5_0b2c))



################# NOMINAL 50% CUT ############################


pt_values = np.asarray([250,300, 350, 450 ,600,800,1000,1500])
pt_bins = (pt_values[1:] + pt_values[:-1])/2
pt_bin_width = pt_values[1:] - pt_values[:-1]

pt_values_0b0c_nominal_50 = pt_values_0b0c_nominal[discriminant_0b0c_nominal>discr_cut]
pt_values_1b0c_nominal_50 = pt_values_1b0c_nominal[discriminant_1b0c_nominal>discr_cut]
pt_values_2b0c_nominal_50 = pt_values_2b0c_nominal[discriminant_2b0c_nominal>discr_cut]
pt_values_1b1c_nominal_50 = pt_values_1b1c_nominal[discriminant_1b1c_nominal>discr_cut]
pt_values_0b1c_nominal_50 = pt_values_0b1c_nominal[discriminant_0b1c_nominal>discr_cut]
pt_values_0b2c_nominal_50 = pt_values_0b2c_nominal[discriminant_0b2c_nominal>discr_cut]

weights_0b0c_nominal_50 = weights_0b0c_nominal[discriminant_0b0c_nominal>discr_cut]
weights_1b0c_nominal_50 = weights_1b0c_nominal[discriminant_1b0c_nominal>discr_cut]
weights_2b0c_nominal_50 = weights_2b0c_nominal[discriminant_2b0c_nominal>discr_cut]
weights_1b1c_nominal_50 = weights_1b1c_nominal[discriminant_1b1c_nominal>discr_cut]
weights_0b1c_nominal_50 = weights_0b1c_nominal[discriminant_0b1c_nominal>discr_cut]
weights_0b2c_nominal_50 = weights_0b2c_nominal[discriminant_0b2c_nominal>discr_cut]

pt_values_0b0c_nominal_nontag50 = pt_values_0b0c_nominal[discriminant_0b0c_nominal<=discr_cut]
pt_values_1b0c_nominal_nontag50 = pt_values_1b0c_nominal[discriminant_1b0c_nominal<=discr_cut]
pt_values_2b0c_nominal_nontag50 = pt_values_2b0c_nominal[discriminant_2b0c_nominal<=discr_cut]
pt_values_1b1c_nominal_nontag50 = pt_values_1b1c_nominal[discriminant_1b1c_nominal<=discr_cut]
pt_values_0b1c_nominal_nontag50 = pt_values_0b1c_nominal[discriminant_0b1c_nominal<=discr_cut]
pt_values_0b2c_nominal_nontag50 = pt_values_0b2c_nominal[discriminant_0b2c_nominal<=discr_cut]

weights_0b0c_nominal_nontag50 = weights_0b0c_nominal[discriminant_0b0c_nominal<=discr_cut]
weights_1b0c_nominal_nontag50 = weights_1b0c_nominal[discriminant_1b0c_nominal<=discr_cut]
weights_2b0c_nominal_nontag50 = weights_2b0c_nominal[discriminant_2b0c_nominal<=discr_cut]
weights_1b1c_nominal_nontag50 = weights_1b1c_nominal[discriminant_1b1c_nominal<=discr_cut]
weights_0b1c_nominal_nontag50 = weights_0b1c_nominal[discriminant_0b1c_nominal<=discr_cut]
weights_0b2c_nominal_nontag50 = weights_0b2c_nominal[discriminant_0b2c_nominal<=discr_cut]

pt_bin_indices_0b0c_50 = np.digitize(pt_values_0b0c_nominal_50, pt_bins)
pt_bin_indices_1b0c_50 = np.digitize(pt_values_1b0c_nominal_50, pt_bins)
pt_bin_indices_2b0c_50 = np.digitize(pt_values_2b0c_nominal_50, pt_bins)
pt_bin_indices_1b1c_50 = np.digitize(pt_values_1b1c_nominal_50, pt_bins)
pt_bin_indices_0b1c_50 = np.digitize(pt_values_0b1c_nominal_50, pt_bins)
pt_bin_indices_0b2c_50 = np.digitize(pt_values_0b2c_nominal_50, pt_bins)

pt_bin_indices_0b0c_nontag50 = np.digitize(pt_values_0b0c_nominal_nontag50, pt_bins)
pt_bin_indices_1b0c_nontag50 = np.digitize(pt_values_1b0c_nominal_nontag50, pt_bins)
pt_bin_indices_2b0c_nontag50 = np.digitize(pt_values_2b0c_nominal_nontag50, pt_bins)
pt_bin_indices_1b1c_nontag50 = np.digitize(pt_values_1b1c_nominal_nontag50, pt_bins)
pt_bin_indices_0b1c_nontag50 = np.digitize(pt_values_0b1c_nominal_nontag50, pt_bins)
pt_bin_indices_0b2c_nontag50 = np.digitize(pt_values_0b2c_nominal_nontag50, pt_bins)


nsel_0b0c_nominal = []
nsel_0b1c_nominal = []
nsel_0b2c_nominal = []
nsel_1b0c_nominal = []
nsel_1b1c_nominal = []
nsel_2b0c_nominal = []

nunsel_0b0c_nominal = []
nunsel_0b1c_nominal = []
nunsel_0b2c_nominal = []
nunsel_1b0c_nominal = []
nunsel_1b1c_nominal = []
nunsel_2b0c_nominal = []

err_nsel_0b0c_nominal_50 = []
err_nsel_0b1c_nominal_50 = []
err_nsel_0b2c_nominal_50 = []
err_nsel_1b0c_nominal_50 = []
err_nsel_1b1c_nominal_50 = []
err_nsel_2b0c_nominal_50 = []

err_nunsel_0b0c_nominal_50 = []
err_nunsel_0b1c_nominal_50 = []
err_nunsel_0b2c_nominal_50 = []
err_nunsel_1b0c_nominal_50 = []
err_nunsel_1b1c_nominal_50 = []
err_nunsel_2b0c_nominal_50 = []

def compute_weightedcount_error(weights_array):
    squared_weights = weights_array**2
    err_weighted_square = np.sum(squared_weights)
    return np.sqrt(err_weighted_square)

for i in range(0, len(pt_bins)):
    
    event_in_bin_0b0c_sel = weights_0b0c_nominal_50[pt_bin_indices_0b0c_50 == i]
    event_in_bin_0b0c_unsel = weights_0b0c_nominal_nontag50[pt_bin_indices_0b0c_nontag50 == i]

    event_in_bin_0b1c_sel = weights_0b1c_nominal_50[pt_bin_indices_0b1c_50 == i]
    event_in_bin_0b1c_unsel = weights_0b1c_nominal_nontag50[pt_bin_indices_0b1c_nontag50 == i]

    event_in_bin_0b2c_sel = weights_0b2c_nominal_50[pt_bin_indices_0b2c_50 == i]
    event_in_bin_0b2c_unsel = weights_0b2c_nominal_nontag50[pt_bin_indices_0b2c_nontag50 == i]

    event_in_bin_1b0c_sel = weights_1b0c_nominal_50[pt_bin_indices_1b0c_50 == i]
    event_in_bin_1b0c_unsel = weights_1b0c_nominal_nontag50[pt_bin_indices_1b0c_nontag50 == i]

    event_in_bin_1b1c_sel = weights_1b1c_nominal_50[pt_bin_indices_1b1c_50 == i]
    event_in_bin_1b1c_unsel = weights_1b1c_nominal_nontag50[pt_bin_indices_1b1c_nontag50 == i]

    event_in_bin_2b0c_sel = weights_2b0c_nominal_50[pt_bin_indices_2b0c_50 == i]
    event_in_bin_2b0c_unsel = weights_2b0c_nominal_nontag50[pt_bin_indices_2b0c_nontag50 == i]

    nsel_0b0c_nominal.append(np.sum(event_in_bin_0b0c_sel))
    nsel_0b1c_nominal.append(np.sum(event_in_bin_0b1c_sel))
    nsel_0b2c_nominal.append(np.sum(event_in_bin_0b2c_sel))
    nsel_1b0c_nominal.append(np.sum(event_in_bin_1b0c_sel))
    nsel_1b1c_nominal.append(np.sum(event_in_bin_1b1c_sel))
    nsel_2b0c_nominal.append(np.sum(event_in_bin_2b0c_sel))

    nunsel_0b0c_nominal.append(np.sum(event_in_bin_0b0c_unsel))
    nunsel_0b1c_nominal.append(np.sum(event_in_bin_0b1c_unsel))
    nunsel_0b2c_nominal.append(np.sum(event_in_bin_0b2c_unsel))
    nunsel_1b0c_nominal.append(np.sum(event_in_bin_1b0c_unsel))
    nunsel_1b1c_nominal.append(np.sum(event_in_bin_1b1c_unsel))
    nunsel_2b0c_nominal.append(np.sum(event_in_bin_2b0c_unsel))

    err_nsel_0b0c_nominal_50.append(compute_weightedcount_error(event_in_bin_0b0c_sel))
    err_nsel_0b1c_nominal_50.append(compute_weightedcount_error(event_in_bin_0b1c_sel))
    err_nsel_0b2c_nominal_50.append(compute_weightedcount_error(event_in_bin_0b2c_sel))
    err_nsel_1b0c_nominal_50.append(compute_weightedcount_error(event_in_bin_1b0c_sel))
    err_nsel_1b1c_nominal_50.append(compute_weightedcount_error(event_in_bin_1b1c_sel))
    err_nsel_2b0c_nominal_50.append(compute_weightedcount_error(event_in_bin_2b0c_sel))

    err_nunsel_0b0c_nominal_50.append(compute_weightedcount_error(event_in_bin_0b0c_unsel))
    err_nunsel_0b1c_nominal_50.append(compute_weightedcount_error(event_in_bin_0b1c_unsel))
    err_nunsel_0b2c_nominal_50.append(compute_weightedcount_error(event_in_bin_0b2c_unsel))
    err_nunsel_1b0c_nominal_50.append(compute_weightedcount_error(event_in_bin_1b0c_unsel))
    err_nunsel_1b1c_nominal_50.append(compute_weightedcount_error(event_in_bin_1b1c_unsel))
    err_nunsel_2b0c_nominal_50.append(compute_weightedcount_error(event_in_bin_2b0c_unsel))
    


def compute_weightedcount_error2(weights_array):
    mean_weight = np.mean(weights_array)
    devstd_weight = np.std(weights_array)
    Nevents = len(weights_array)
    Nevents_weighted = np.sum(weights_array)

    err_weighted_square = Nevents_weighted**2 * np.sqrt((1+devstd_weight**2/mean_weight**2)/Nevents)
    return np.sqrt(err_weighted_square)

nsel_0b0c_nominal = np.asarray(nsel_0b0c_nominal)
nsel_1b0c_nominal = np.asarray(nsel_1b0c_nominal)
nsel_2b0c_nominal = np.asarray(nsel_2b0c_nominal)
nsel_1b1c_nominal = np.asarray(nsel_1b1c_nominal)
nsel_0b1c_nominal = np.asarray(nsel_0b1c_nominal)
nsel_0b2c_nominal = np.asarray(nsel_0b2c_nominal)

err_nsel_0b0c_nominal_50 = np.asarray(err_nsel_0b0c_nominal_50)
err_nsel_0b1c_nominal_50 = np.asarray(err_nsel_0b1c_nominal_50)
err_nsel_0b2c_nominal_50 = np.asarray(err_nsel_0b2c_nominal_50)
err_nsel_1b0c_nominal_50 = np.asarray(err_nsel_1b0c_nominal_50)
err_nsel_1b1c_nominal_50 = np.asarray(err_nsel_1b1c_nominal_50)
err_nsel_2b0c_nominal_50 = np.asarray(err_nsel_2b0c_nominal_50)

nunsel_0b0c_nominal = np.asarray(nunsel_0b0c_nominal)
nunsel_0b1c_nominal = np.asarray(nunsel_0b1c_nominal)
nunsel_0b2c_nominal = np.asarray(nunsel_0b2c_nominal)
nunsel_1b0c_nominal = np.asarray(nunsel_1b0c_nominal)
nunsel_1b1c_nominal = np.asarray(nunsel_1b1c_nominal)
nunsel_2b0c_nominal = np.asarray(nunsel_2b0c_nominal)

err_nunsel_0b0c_nominal_50 = np.asarray(err_nunsel_0b0c_nominal_50)
err_nunsel_0b1c_nominal_50 = np.asarray(err_nunsel_0b1c_nominal_50)
err_nunsel_0b2c_nominal_50 = np.asarray(err_nunsel_0b2c_nominal_50)
err_nunsel_1b0c_nominal_50 = np.asarray(err_nunsel_1b0c_nominal_50)
err_nunsel_1b1c_nominal_50 = np.asarray(err_nunsel_1b1c_nominal_50)
err_nunsel_2b0c_nominal_50 = np.asarray(err_nunsel_2b0c_nominal_50)



eff_0b0c_nominal_50 = nsel_0b0c_nominal/(nsel_0b0c_nominal+nunsel_0b0c_nominal)
eff_0b1c_nominal_50 = nsel_0b1c_nominal/(nsel_0b1c_nominal+nunsel_0b1c_nominal)
eff_0b2c_nominal_50 = nsel_0b2c_nominal/(nsel_0b2c_nominal+nunsel_0b2c_nominal)
eff_1b0c_nominal_50 = nsel_1b0c_nominal/(nsel_1b0c_nominal+nunsel_1b0c_nominal)
eff_1b1c_nominal_50 = nsel_1b1c_nominal/(nsel_1b1c_nominal+nunsel_1b1c_nominal)
eff_2b0c_nominal_50 = nsel_2b0c_nominal/(nsel_2b0c_nominal+nunsel_2b0c_nominal)

print('Selected events 0b0c:', nsel_0b0c_nominal)
print('Selected events 1b0c:', nsel_1b0c_nominal)
print('Selected events 2b0c:', nsel_2b0c_nominal)
print('Selected events 1b1c:', nsel_1b1c_nominal)
print('Selected events 0b1c:', nsel_0b1c_nominal)
print('Selected events 0b2c:', nsel_0b2c_nominal)

print('Unselected events 0b0c:', nunsel_0b0c_nominal)
print('Unselected events 1b0c:', nunsel_1b0c_nominal)
print('Unselected events 2b0c:', nunsel_2b0c_nominal)
print('Unselected events 1b1c:', nunsel_1b1c_nominal)
print('Unselected events 0b1c:', nunsel_0b1c_nominal)
print('Unselected events 0b2c:', nunsel_0b2c_nominal)

print('Error on selected events 0b0c:', err_nsel_0b0c_nominal_50)
print('Error on selected events 1b0c:', err_nsel_1b0c_nominal_50)
print('Error on selected events 2b0c:', err_nsel_2b0c_nominal_50)
print('Error on selected events 1b1c:', err_nsel_1b1c_nominal_50)
print('Error on selected events 0b1c:', err_nsel_0b1c_nominal_50)
print('Error on selected events 0b2c:', err_nsel_0b2c_nominal_50)

print('Error on unselected events 0b0c:', err_nunsel_0b0c_nominal_50)
print('Error on unselected events 1b0c:', err_nunsel_1b0c_nominal_50)
print('Error on unselected events 2b0c:', err_nunsel_2b0c_nominal_50)
print('Error on unselected events 1b1c:', err_nunsel_1b1c_nominal_50)
print('Error on unselected events 0b1c:', err_nunsel_0b1c_nominal_50)
print('Error on unselected events 0b2c:', err_nunsel_0b2c_nominal_50)




err_eff_0b0c_nominal_50 = np.sqrt(((nunsel_0b0c_nominal*err_nsel_0b0c_nominal_50)**2 + (nsel_0b0c_nominal*err_nunsel_0b0c_nominal_50)**2)/(nsel_0b0c_nominal+nunsel_0b0c_nominal)**4)
err_eff_1b0c_nominal_50 = np.sqrt(((nunsel_1b0c_nominal*err_nsel_1b0c_nominal_50)**2 + (nsel_1b0c_nominal*err_nunsel_1b0c_nominal_50)**2)/(nsel_1b0c_nominal+nunsel_1b0c_nominal)**4)
err_eff_2b0c_nominal_50 = np.sqrt(((nunsel_2b0c_nominal*err_nsel_2b0c_nominal_50)**2 + (nsel_2b0c_nominal*err_nunsel_2b0c_nominal_50)**2)/(nsel_2b0c_nominal+nunsel_2b0c_nominal)**4)
err_eff_1b1c_nominal_50 = np.sqrt(((nunsel_1b1c_nominal*err_nsel_1b1c_nominal_50)**2 + (nsel_1b1c_nominal*err_nunsel_1b1c_nominal_50)**2)/(nsel_1b1c_nominal+nunsel_1b1c_nominal)**4)
err_eff_0b1c_nominal_50 = np.sqrt(((nunsel_0b1c_nominal*err_nsel_0b1c_nominal_50)**2 + (nsel_0b1c_nominal*err_nunsel_0b1c_nominal_50)**2)/(nsel_0b1c_nominal+nunsel_0b1c_nominal)**4)
err_eff_0b2c_nominal_50 = np.sqrt(((nunsel_0b2c_nominal*err_nsel_0b2c_nominal_50)**2 + (nsel_0b2c_nominal*err_nunsel_0b2c_nominal_50)**2)/(nsel_0b2c_nominal+nunsel_0b2c_nominal)**4)


print('Efficiency 0b0c:', eff_0b0c_nominal_50, 'Error:', err_eff_0b0c_nominal_50)
print('Efficiency 1b0c:', eff_1b0c_nominal_50, 'Error:', err_eff_1b0c_nominal_50)
print('Efficiency 2b0c:', eff_2b0c_nominal_50, 'Error:', err_eff_2b0c_nominal_50)
print('Efficiency 1b1c:', eff_1b1c_nominal_50, 'Error:', err_eff_1b1c_nominal_50)
print('Efficiency 0b1c:', eff_0b1c_nominal_50, 'Error:', err_eff_0b1c_nominal_50)
print('Efficiency 0b2c:', eff_0b2c_nominal_50, 'Error:', err_eff_0b2c_nominal_50)



# plot efficiency vs pt for 50% working point for each category in a different subplot
# setting xerror as the bin width
fig, ax = plt.subplots(3, 2, figsize=(20, 15))
ax[0, 0].errorbar(pt_bins, eff_0b0c_nominal_50, xerr=pt_bin_width/2, yerr=err_eff_0b0c_nominal_50, fmt='o', label='0b0c')
ax[0, 0].set_title('0b0c')
ax[0, 0].set_xlabel('pT [GeV]')
ax[0, 0].set_ylabel('Efficiency')
ax[0, 0].set_yscale('log')
ax[0, 0].legend()

ax[0, 1].errorbar(pt_bins, eff_1b0c_nominal_50, xerr=pt_bin_width/2, yerr=err_eff_1b0c_nominal_50, fmt='o', label='1b0c')
ax[0, 1].set_title('1b0c')
ax[0, 1].set_xlabel('pT [GeV]')
ax[0, 1].set_ylabel('Efficiency')
ax[0, 1].set_yscale('log')
ax[0, 1].legend()

ax[1, 0].errorbar(pt_bins, eff_2b0c_nominal_50, xerr=pt_bin_width/2, yerr=err_eff_2b0c_nominal_50, fmt='o', label='2b0c')
ax[1, 0].set_title('2b0c')
ax[1, 0].set_xlabel('pT [GeV]')
ax[1, 0].set_ylabel('Efficiency')
ax[1, 0].set_yscale('log')
ax[1, 0].legend()

ax[1, 1].errorbar(pt_bins, eff_1b1c_nominal_50, xerr=pt_bin_width/2, yerr=err_eff_1b1c_nominal_50, fmt='o', label='1b1c')
ax[1, 1].set_title('1b1c')
ax[1, 1].set_xlabel('pT [GeV]')
ax[1, 1].set_ylabel('Efficiency')
ax[1, 1].set_yscale('log')
ax[1, 1].legend()

ax[2, 0].errorbar(pt_bins, eff_0b1c_nominal_50, xerr=pt_bin_width/2, yerr=err_eff_0b1c_nominal_50, fmt='o', label='0b1c')
ax[2, 0].set_title('0b1c')
ax[2, 0].set_xlabel('pT [GeV]')
ax[2, 0].set_ylabel('Efficiency')
ax[2, 0].set_yscale('log')
ax[2, 0].legend()

ax[2, 1].errorbar(pt_bins, eff_0b2c_nominal_50, xerr=pt_bin_width/2, yerr=err_eff_0b2c_nominal_50, fmt='o', label='0b2c')
ax[2, 1].set_title('0b2c')
ax[2, 1].set_xlabel('pT [GeV]')
ax[2, 1].set_ylabel('Efficiency')
ax[2, 1].set_yscale('log')
ax[2, 1].legend()

plt.tight_layout()
plt.savefig(efficiency_nominal_filename)
                                  

#################### TRKD0 50% CUT ############################
pt_values_0b0c_trkd0_50 = pt_values_0b0c_trkd0[discriminant_0b0c_trkd0>discr_cut]
pt_values_1b0c_trkd0_50 = pt_values_1b0c_trkd0[discriminant_1b0c_trkd0>discr_cut]
pt_values_2b0c_trkd0_50 = pt_values_2b0c_trkd0[discriminant_2b0c_trkd0>discr_cut]
pt_values_1b1c_trkd0_50 = pt_values_1b1c_trkd0[discriminant_1b1c_trkd0>discr_cut]
pt_values_0b1c_trkd0_50 = pt_values_0b1c_trkd0[discriminant_0b1c_trkd0>discr_cut]
pt_values_0b2c_trkd0_50 = pt_values_0b2c_trkd0[discriminant_0b2c_trkd0>discr_cut]

weights_0b0c_trkd0_50 = weights_0b0c_trkd0[discriminant_0b0c_trkd0>discr_cut]
weights_1b0c_trkd0_50 = weights_1b0c_trkd0[discriminant_1b0c_trkd0>discr_cut]
weights_2b0c_trkd0_50 = weights_2b0c_trkd0[discriminant_2b0c_trkd0>discr_cut]
weights_1b1c_trkd0_50 = weights_1b1c_trkd0[discriminant_1b1c_trkd0>discr_cut]
weights_0b1c_trkd0_50 = weights_0b1c_trkd0[discriminant_0b1c_trkd0>discr_cut]
weights_0b2c_trkd0_50 = weights_0b2c_trkd0[discriminant_0b2c_trkd0>discr_cut]

pt_values_0b0c_trkd0_nontag50 = pt_values_0b0c_trkd0[discriminant_0b0c_trkd0<=discr_cut]
pt_values_1b0c_trkd0_nontag50 = pt_values_1b0c_trkd0[discriminant_1b0c_trkd0<=discr_cut]
pt_values_2b0c_trkd0_nontag50 = pt_values_2b0c_trkd0[discriminant_2b0c_trkd0<=discr_cut]
pt_values_1b1c_trkd0_nontag50 = pt_values_1b1c_trkd0[discriminant_1b1c_trkd0<=discr_cut]
pt_values_0b1c_trkd0_nontag50 = pt_values_0b1c_trkd0[discriminant_0b1c_trkd0<=discr_cut]
pt_values_0b2c_trkd0_nontag50 = pt_values_0b2c_trkd0[discriminant_0b2c_trkd0<=discr_cut]

weights_0b0c_trkd0_nontag50 = weights_0b0c_trkd0[discriminant_0b0c_trkd0<=discr_cut]
weights_1b0c_trkd0_nontag50 = weights_1b0c_trkd0[discriminant_1b0c_trkd0<=discr_cut]
weights_2b0c_trkd0_nontag50 = weights_2b0c_trkd0[discriminant_2b0c_trkd0<=discr_cut]
weights_1b1c_trkd0_nontag50 = weights_1b1c_trkd0[discriminant_1b1c_trkd0<=discr_cut]
weights_0b1c_trkd0_nontag50 = weights_0b1c_trkd0[discriminant_0b1c_trkd0<=discr_cut]
weights_0b2c_trkd0_nontag50 = weights_0b2c_trkd0[discriminant_0b2c_trkd0<=discr_cut]

pt_bin_indices_0b0c_50 = np.digitize(pt_values_0b0c_trkd0_50, pt_bins)
pt_bin_indices_1b0c_50 = np.digitize(pt_values_1b0c_trkd0_50, pt_bins)
pt_bin_indices_2b0c_50 = np.digitize(pt_values_2b0c_trkd0_50, pt_bins)
pt_bin_indices_1b1c_50 = np.digitize(pt_values_1b1c_trkd0_50, pt_bins)
pt_bin_indices_0b1c_50 = np.digitize(pt_values_0b1c_trkd0_50, pt_bins)
pt_bin_indices_0b2c_50 = np.digitize(pt_values_0b2c_trkd0_50, pt_bins)

pt_bin_indices_0b0c_nontag50 = np.digitize(pt_values_0b0c_trkd0_nontag50, pt_bins)
pt_bin_indices_1b0c_nontag50 = np.digitize(pt_values_1b0c_trkd0_nontag50, pt_bins)
pt_bin_indices_2b0c_nontag50 = np.digitize(pt_values_2b0c_trkd0_nontag50, pt_bins)
pt_bin_indices_1b1c_nontag50 = np.digitize(pt_values_1b1c_trkd0_nontag50, pt_bins)
pt_bin_indices_0b1c_nontag50 = np.digitize(pt_values_0b1c_trkd0_nontag50, pt_bins)
pt_bin_indices_0b2c_nontag50 = np.digitize(pt_values_0b2c_trkd0_nontag50, pt_bins)


nsel_0b0c_trkd0 = []
nsel_0b1c_trkd0 = []
nsel_0b2c_trkd0 = []
nsel_1b0c_trkd0 = []
nsel_1b1c_trkd0 = []
nsel_2b0c_trkd0 = []

nunsel_0b0c_trkd0 = []
nunsel_0b1c_trkd0 = []
nunsel_0b2c_trkd0 = []
nunsel_1b0c_trkd0 = []
nunsel_1b1c_trkd0 = []
nunsel_2b0c_trkd0 = []

err_nsel_0b0c_trkd0_50 = []
err_nsel_0b1c_trkd0_50 = []
err_nsel_0b2c_trkd0_50 = []
err_nsel_1b0c_trkd0_50 = []
err_nsel_1b1c_trkd0_50 = []
err_nsel_2b0c_trkd0_50 = []

err_nunsel_0b0c_trkd0_50 = []
err_nunsel_0b1c_trkd0_50 = []
err_nunsel_0b2c_trkd0_50 = []
err_nunsel_1b0c_trkd0_50 = []
err_nunsel_1b1c_trkd0_50 = []
err_nunsel_2b0c_trkd0_50 = []

def compute_weightedcount_error(weights_array):
    squared_weights = weights_array**2
    err_weighted_square = np.sum(squared_weights)
    return np.sqrt(err_weighted_square)

for i in range(0, len(pt_bins)):
    
 event_in_bin_0b0c_sel = weights_0b0c_trkd0_50[pt_bin_indices_0b0c_50 == i]
 event_in_bin_0b0c_unsel = weights_0b0c_trkd0_nontag50[pt_bin_indices_0b0c_nontag50 == i]

 event_in_bin_0b1c_sel = weights_0b1c_trkd0_50[pt_bin_indices_0b1c_50 == i]
 event_in_bin_0b1c_unsel = weights_0b1c_trkd0_nontag50[pt_bin_indices_0b1c_nontag50 == i]

 event_in_bin_0b2c_sel = weights_0b2c_trkd0_50[pt_bin_indices_0b2c_50 == i]
 event_in_bin_0b2c_unsel = weights_0b2c_trkd0_nontag50[pt_bin_indices_0b2c_nontag50 == i]

 event_in_bin_1b0c_sel = weights_1b0c_trkd0_50[pt_bin_indices_1b0c_50 == i]
 event_in_bin_1b0c_unsel = weights_1b0c_trkd0_nontag50[pt_bin_indices_1b0c_nontag50 == i]

 event_in_bin_1b1c_sel = weights_1b1c_trkd0_50[pt_bin_indices_1b1c_50 == i]
 event_in_bin_1b1c_unsel = weights_1b1c_trkd0_nontag50[pt_bin_indices_1b1c_nontag50 == i]

 event_in_bin_2b0c_sel = weights_2b0c_trkd0_50[pt_bin_indices_2b0c_50 == i]
 event_in_bin_2b0c_unsel = weights_2b0c_trkd0_nontag50[pt_bin_indices_2b0c_nontag50 == i]

 nsel_0b0c_trkd0.append(np.sum(event_in_bin_0b0c_sel))
 nsel_0b1c_trkd0.append(np.sum(event_in_bin_0b1c_sel))
 nsel_0b2c_trkd0.append(np.sum(event_in_bin_0b2c_sel))
 nsel_1b0c_trkd0.append(np.sum(event_in_bin_1b0c_sel))
 nsel_1b1c_trkd0.append(np.sum(event_in_bin_1b1c_sel))
 nsel_2b0c_trkd0.append(np.sum(event_in_bin_2b0c_sel))

 nunsel_0b0c_trkd0.append(np.sum(event_in_bin_0b0c_unsel))
 nunsel_0b1c_trkd0.append(np.sum(event_in_bin_0b1c_unsel))
 nunsel_0b2c_trkd0.append(np.sum(event_in_bin_0b2c_unsel))
 nunsel_1b0c_trkd0.append(np.sum(event_in_bin_1b0c_unsel))
 nunsel_1b1c_trkd0.append(np.sum(event_in_bin_1b1c_unsel))
 nunsel_2b0c_trkd0.append(np.sum(event_in_bin_2b0c_unsel))

 err_nsel_0b0c_trkd0_50.append(compute_weightedcount_error(event_in_bin_0b0c_sel))
 err_nsel_0b1c_trkd0_50.append(compute_weightedcount_error(event_in_bin_0b1c_sel))
 err_nsel_0b2c_trkd0_50.append(compute_weightedcount_error(event_in_bin_0b2c_sel))
 err_nsel_1b0c_trkd0_50.append(compute_weightedcount_error(event_in_bin_1b0c_sel))
 err_nsel_1b1c_trkd0_50.append(compute_weightedcount_error(event_in_bin_1b1c_sel))
 err_nsel_2b0c_trkd0_50.append(compute_weightedcount_error(event_in_bin_2b0c_sel))

 err_nunsel_0b0c_trkd0_50.append(compute_weightedcount_error(event_in_bin_0b0c_unsel))
 err_nunsel_0b1c_trkd0_50.append(compute_weightedcount_error(event_in_bin_0b1c_unsel))
 err_nunsel_0b2c_trkd0_50.append(compute_weightedcount_error(event_in_bin_0b2c_unsel))
 err_nunsel_1b0c_trkd0_50.append(compute_weightedcount_error(event_in_bin_1b0c_unsel))
 err_nunsel_1b1c_trkd0_50.append(compute_weightedcount_error(event_in_bin_1b1c_unsel))
 err_nunsel_2b0c_trkd0_50.append(compute_weightedcount_error(event_in_bin_2b0c_unsel))
    


def compute_weightedcount_error2(weights_array):
    mean_weight = np.mean(weights_array)
    devstd_weight = np.std(weights_array)
    Nevents = len(weights_array)
    Nevents_weighted = np.sum(weights_array)

    err_weighted_square = Nevents_weighted**2 * np.sqrt((1+devstd_weight**2/mean_weight**2)/Nevents)
    return np.sqrt(err_weighted_square)

nunsel_0b0c_trkd0 = np.asarray(nunsel_0b0c_trkd0)
nunsel_0b1c_trkd0 = np.asarray(nunsel_0b1c_trkd0)
nunsel_0b2c_trkd0 = np.asarray(nunsel_0b2c_trkd0)
nunsel_1b0c_trkd0 = np.asarray(nunsel_1b0c_trkd0)
nunsel_1b1c_trkd0 = np.asarray(nunsel_1b1c_trkd0)
nunsel_2b0c_trkd0 = np.asarray(nunsel_2b0c_trkd0)

nsel_0b0c_trkd0 = np.asarray(nsel_0b0c_trkd0)
nsel_0b1c_trkd0 = np.asarray(nsel_0b1c_trkd0)
nsel_0b2c_trkd0 = np.asarray(nsel_0b2c_trkd0)
nsel_1b0c_trkd0 = np.asarray(nsel_1b0c_trkd0)
nsel_1b1c_trkd0 = np.asarray(nsel_1b1c_trkd0)
nsel_2b0c_trkd0 = np.asarray(nsel_2b0c_trkd0)

err_nsel_0b0c_trkd0_50 = np.asarray(err_nsel_0b0c_trkd0_50)
err_nsel_0b1c_trkd0_50 = np.asarray(err_nsel_0b1c_trkd0_50)
err_nsel_0b2c_trkd0_50 = np.asarray(err_nsel_0b2c_trkd0_50)
err_nsel_1b0c_trkd0_50 = np.asarray(err_nsel_1b0c_trkd0_50)
err_nsel_1b1c_trkd0_50 = np.asarray(err_nsel_1b1c_trkd0_50)
err_nsel_2b0c_trkd0_50 = np.asarray(err_nsel_2b0c_trkd0_50)

err_nunsel_0b0c_trkd0_50 = np.asarray(err_nunsel_0b0c_trkd0_50)
err_nunsel_0b1c_trkd0_50 = np.asarray(err_nunsel_0b1c_trkd0_50)
err_nunsel_0b2c_trkd0_50 = np.asarray(err_nunsel_0b2c_trkd0_50)
err_nunsel_1b0c_trkd0_50 = np.asarray(err_nunsel_1b0c_trkd0_50)
err_nunsel_1b1c_trkd0_50 = np.asarray(err_nunsel_1b1c_trkd0_50)
err_nunsel_2b0c_trkd0_50 = np.asarray(err_nunsel_2b0c_trkd0_50)

eff_0b0c_trkd0_50 = nsel_0b0c_trkd0/(nsel_0b0c_trkd0+nunsel_0b0c_trkd0)
eff_0b1c_trkd0_50 = nsel_0b1c_trkd0/(nsel_0b1c_trkd0+nunsel_0b1c_trkd0)
eff_0b2c_trkd0_50 = nsel_0b2c_trkd0/(nsel_0b2c_trkd0+nunsel_0b2c_trkd0)
eff_1b0c_trkd0_50 = nsel_1b0c_trkd0/(nsel_1b0c_trkd0+nunsel_1b0c_trkd0)
eff_1b1c_trkd0_50 = nsel_1b1c_trkd0/(nsel_1b1c_trkd0+nunsel_1b1c_trkd0)
eff_2b0c_trkd0_50 = nsel_2b0c_trkd0/(nsel_2b0c_trkd0+nunsel_2b0c_trkd0)

print('Selected events 0b0c:', nsel_0b0c_trkd0)
print('Selected events 1b0c:', nsel_1b0c_trkd0)
print('Selected events 2b0c:', nsel_2b0c_trkd0)
print('Selected events 1b1c:', nsel_1b1c_trkd0)
print('Selected events 0b1c:', nsel_0b1c_trkd0)
print('Selected events 0b2c:', nsel_0b2c_trkd0)

print('Unselected events 0b0c:', nunsel_0b0c_trkd0)
print('Unselected events 1b0c:', nunsel_1b0c_trkd0)
print('Unselected events 2b0c:', nunsel_2b0c_trkd0)
print('Unselected events 1b1c:', nunsel_1b1c_trkd0)
print('Unselected events 0b1c:', nunsel_0b1c_trkd0)
print('Unselected events 0b2c:', nunsel_0b2c_trkd0)

print('Error on selected events 0b0c:', err_nsel_0b0c_trkd0_50)
print('Error on selected events 1b0c:', err_nsel_1b0c_trkd0_50)
print('Error on selected events 2b0c:', err_nsel_2b0c_trkd0_50)
print('Error on selected events 1b1c:', err_nsel_1b1c_trkd0_50)
print('Error on selected events 0b1c:', err_nsel_0b1c_trkd0_50)
print('Error on selected events 0b2c:', err_nsel_0b2c_trkd0_50)

print('Error on unselected events 0b0c:', err_nunsel_0b0c_trkd0_50)
print('Error on unselected events 1b0c:', err_nunsel_1b0c_trkd0_50)
print('Error on unselected events 2b0c:', err_nunsel_2b0c_trkd0_50)
print('Error on unselected events 1b1c:', err_nunsel_1b1c_trkd0_50)
print('Error on unselected events 0b1c:', err_nunsel_0b1c_trkd0_50)
print('Error on unselected events 0b2c:', err_nunsel_0b2c_trkd0_50)




err_eff_0b0c_trkd0_50 = np.sqrt(((nunsel_0b0c_trkd0*err_nsel_0b0c_trkd0_50)**2 + (nsel_0b0c_trkd0*err_nunsel_0b0c_trkd0_50)**2)/(nsel_0b0c_trkd0+nunsel_0b0c_trkd0)**4)
err_eff_1b0c_trkd0_50 = np.sqrt(((nunsel_1b0c_trkd0*err_nsel_1b0c_trkd0_50)**2 + (nsel_1b0c_trkd0*err_nunsel_1b0c_trkd0_50)**2)/(nsel_1b0c_trkd0+nunsel_1b0c_trkd0)**4)
err_eff_2b0c_trkd0_50 = np.sqrt(((nunsel_2b0c_trkd0*err_nsel_2b0c_trkd0_50)**2 + (nsel_2b0c_trkd0*err_nunsel_2b0c_trkd0_50)**2)/(nsel_2b0c_trkd0+nunsel_2b0c_trkd0)**4)
err_eff_1b1c_trkd0_50 = np.sqrt(((nunsel_1b1c_trkd0*err_nsel_1b1c_trkd0_50)**2 + (nsel_1b1c_trkd0*err_nunsel_1b1c_trkd0_50)**2)/(nsel_1b1c_trkd0+nunsel_1b1c_trkd0)**4)
err_eff_0b1c_trkd0_50 = np.sqrt(((nunsel_0b1c_trkd0*err_nsel_0b1c_trkd0_50)**2 + (nsel_0b1c_trkd0*err_nunsel_0b1c_trkd0_50)**2)/(nsel_0b1c_trkd0+nunsel_0b1c_trkd0)**4)
err_eff_0b2c_trkd0_50 = np.sqrt(((nunsel_0b2c_trkd0*err_nsel_0b2c_trkd0_50)**2 + (nsel_0b2c_trkd0*err_nunsel_0b2c_trkd0_50)**2)/(nsel_0b2c_trkd0+nunsel_0b2c_trkd0)**4)


print('Efficiency 0b0c:', eff_0b0c_trkd0_50, 'Error:', err_eff_0b0c_trkd0_50)
print('Efficiency 1b0c:', eff_1b0c_trkd0_50, 'Error:', err_eff_1b0c_trkd0_50)
print('Efficiency 2b0c:', eff_2b0c_trkd0_50, 'Error:', err_eff_2b0c_trkd0_50)
print('Efficiency 1b1c:', eff_1b1c_trkd0_50, 'Error:', err_eff_1b1c_trkd0_50)
print('Efficiency 0b1c:', eff_0b1c_trkd0_50, 'Error:', err_eff_0b1c_trkd0_50)
print('Efficiency 0b2c:', eff_0b2c_trkd0_50, 'Error:', err_eff_0b2c_trkd0_50)



# plot efficiency vs pt for 50% working point for each category in a different subplot
# setting xerror as the bin width
fig, ax = plt.subplots(3, 2, figsize=(20, 15))
ax[0, 0].errorbar(pt_bins, eff_0b0c_trkd0_50, xerr=pt_bin_width/2, yerr=err_eff_0b0c_trkd0_50, fmt='o', label='0b0c')
ax[0, 0].set_title('0b0c')
ax[0, 0].set_xlabel('pT [GeV]')
ax[0, 0].set_ylabel('Efficiency')
ax[0, 0].set_yscale('log')
ax[0, 0].legend()

ax[0, 1].errorbar(pt_bins, eff_1b0c_trkd0_50, xerr=pt_bin_width/2, yerr=err_eff_1b0c_trkd0_50, fmt='o', label='1b0c')
ax[0, 1].set_title('1b0c')
ax[0, 1].set_xlabel('pT [GeV]')
ax[0, 1].set_ylabel('Efficiency')
ax[0, 1].set_yscale('log')
ax[0, 1].legend()

ax[1, 0].errorbar(pt_bins, eff_2b0c_trkd0_50, xerr=pt_bin_width/2, yerr=err_eff_2b0c_trkd0_50, fmt='o', label='2b0c')
ax[1, 0].set_title('2b0c')
ax[1, 0].set_xlabel('pT [GeV]')
ax[1, 0].set_ylabel('Efficiency')
ax[1, 0].set_yscale('log')
ax[1, 0].legend()

ax[1, 1].errorbar(pt_bins, eff_1b1c_trkd0_50, xerr=pt_bin_width/2, yerr=err_eff_1b1c_trkd0_50, fmt='o', label='1b1c')
ax[1, 1].set_title('1b1c')
ax[1, 1].set_xlabel('pT [GeV]')
ax[1, 1].set_ylabel('Efficiency')
ax[1, 1].set_yscale('log')
ax[1, 1].legend()

ax[2, 0].errorbar(pt_bins, eff_0b1c_trkd0_50, xerr=pt_bin_width/2, yerr=err_eff_0b1c_trkd0_50, fmt='o', label='0b1c')
ax[2, 0].set_title('0b1c')
ax[2, 0].set_xlabel('pT [GeV]')
ax[2, 0].set_ylabel('Efficiency')
ax[2, 0].set_yscale('log')
ax[2, 0].legend()

ax[2, 1].errorbar(pt_bins, eff_0b2c_trkd0_50, xerr=pt_bin_width/2, yerr=err_eff_0b2c_trkd0_50, fmt='o', label='0b2c')
ax[2, 1].set_title('0b2c')
ax[2, 1].set_xlabel('pT [GeV]')
ax[2, 1].set_ylabel('Efficiency')
ax[2, 1].set_yscale('log')
ax[2, 1].legend()

plt.tight_layout()
plt.savefig(efficiency_trkd0_filename)

############### TRKD0 UP 50% CUT ############################
pt_values_0b0c_trkd0up_50 = pt_values_0b0c_trkd0up[discriminant_0b0c_trkd0up>discr_cut]
pt_values_1b0c_trkd0up_50 = pt_values_1b0c_trkd0up[discriminant_1b0c_trkd0up>discr_cut]
pt_values_2b0c_trkd0up_50 = pt_values_2b0c_trkd0up[discriminant_2b0c_trkd0up>discr_cut]
pt_values_1b1c_trkd0up_50 = pt_values_1b1c_trkd0up[discriminant_1b1c_trkd0up>discr_cut]
pt_values_0b1c_trkd0up_50 = pt_values_0b1c_trkd0up[discriminant_0b1c_trkd0up>discr_cut]
pt_values_0b2c_trkd0up_50 = pt_values_0b2c_trkd0up[discriminant_0b2c_trkd0up>discr_cut]

weights_0b0c_trkd0up_50 = weights_0b0c_trkd0up[discriminant_0b0c_trkd0up>discr_cut]
weights_1b0c_trkd0up_50 = weights_1b0c_trkd0up[discriminant_1b0c_trkd0up>discr_cut]
weights_2b0c_trkd0up_50 = weights_2b0c_trkd0up[discriminant_2b0c_trkd0up>discr_cut]
weights_1b1c_trkd0up_50 = weights_1b1c_trkd0up[discriminant_1b1c_trkd0up>discr_cut]
weights_0b1c_trkd0up_50 = weights_0b1c_trkd0up[discriminant_0b1c_trkd0up>discr_cut]
weights_0b2c_trkd0up_50 = weights_0b2c_trkd0up[discriminant_0b2c_trkd0up>discr_cut]

pt_values_0b0c_trkd0up_nontag50 = pt_values_0b0c_trkd0up[discriminant_0b0c_trkd0up<=discr_cut]
pt_values_1b0c_trkd0up_nontag50 = pt_values_1b0c_trkd0up[discriminant_1b0c_trkd0up<=discr_cut]
pt_values_2b0c_trkd0up_nontag50 = pt_values_2b0c_trkd0up[discriminant_2b0c_trkd0up<=discr_cut]
pt_values_1b1c_trkd0up_nontag50 = pt_values_1b1c_trkd0up[discriminant_1b1c_trkd0up<=discr_cut]
pt_values_0b1c_trkd0up_nontag50 = pt_values_0b1c_trkd0up[discriminant_0b1c_trkd0up<=discr_cut]
pt_values_0b2c_trkd0up_nontag50 = pt_values_0b2c_trkd0up[discriminant_0b2c_trkd0up<=discr_cut]

weights_0b0c_trkd0up_nontag50 = weights_0b0c_trkd0up[discriminant_0b0c_trkd0up<=discr_cut]
weights_1b0c_trkd0up_nontag50 = weights_1b0c_trkd0up[discriminant_1b0c_trkd0up<=discr_cut]
weights_2b0c_trkd0up_nontag50 = weights_2b0c_trkd0up[discriminant_2b0c_trkd0up<=discr_cut]
weights_1b1c_trkd0up_nontag50 = weights_1b1c_trkd0up[discriminant_1b1c_trkd0up<=discr_cut]
weights_0b1c_trkd0up_nontag50 = weights_0b1c_trkd0up[discriminant_0b1c_trkd0up<=discr_cut]
weights_0b2c_trkd0up_nontag50 = weights_0b2c_trkd0up[discriminant_0b2c_trkd0up<=discr_cut]

pt_bin_indices_0b0c_50 = np.digitize(pt_values_0b0c_trkd0up_50, pt_bins)
pt_bin_indices_1b0c_50 = np.digitize(pt_values_1b0c_trkd0up_50, pt_bins)
pt_bin_indices_2b0c_50 = np.digitize(pt_values_2b0c_trkd0up_50, pt_bins)
pt_bin_indices_1b1c_50 = np.digitize(pt_values_1b1c_trkd0up_50, pt_bins)
pt_bin_indices_0b1c_50 = np.digitize(pt_values_0b1c_trkd0up_50, pt_bins)
pt_bin_indices_0b2c_50 = np.digitize(pt_values_0b2c_trkd0up_50, pt_bins)

pt_bin_indices_0b0c_nontag50 = np.digitize(pt_values_0b0c_trkd0up_nontag50, pt_bins)
pt_bin_indices_1b0c_nontag50 = np.digitize(pt_values_1b0c_trkd0up_nontag50, pt_bins)
pt_bin_indices_2b0c_nontag50 = np.digitize(pt_values_2b0c_trkd0up_nontag50, pt_bins)
pt_bin_indices_1b1c_nontag50 = np.digitize(pt_values_1b1c_trkd0up_nontag50, pt_bins)
pt_bin_indices_0b1c_nontag50 = np.digitize(pt_values_0b1c_trkd0up_nontag50, pt_bins)
pt_bin_indices_0b2c_nontag50 = np.digitize(pt_values_0b2c_trkd0up_nontag50, pt_bins)


nsel_0b0c_trkd0up = []
nsel_0b1c_trkd0up = []
nsel_0b2c_trkd0up = []
nsel_1b0c_trkd0up = []
nsel_1b1c_trkd0up = []
nsel_2b0c_trkd0up = []

nunsel_0b0c_trkd0up = []
nunsel_0b1c_trkd0up = []
nunsel_0b2c_trkd0up = []
nunsel_1b0c_trkd0up = []
nunsel_1b1c_trkd0up = []
nunsel_2b0c_trkd0up = []

err_nsel_0b0c_trkd0up_50 = []
err_nsel_0b1c_trkd0up_50 = []
err_nsel_0b2c_trkd0up_50 = []
err_nsel_1b0c_trkd0up_50 = []
err_nsel_1b1c_trkd0up_50 = []
err_nsel_2b0c_trkd0up_50 = []

err_nunsel_0b0c_trkd0up_50 = []
err_nunsel_0b1c_trkd0up_50 = []
err_nunsel_0b2c_trkd0up_50 = []
err_nunsel_1b0c_trkd0up_50 = []
err_nunsel_1b1c_trkd0up_50 = []
err_nunsel_2b0c_trkd0up_50 = []


for i in range(0, len(pt_bins)):
    
 event_in_bin_0b0c_sel = weights_0b0c_trkd0up_50[pt_bin_indices_0b0c_50 == i]
 event_in_bin_0b0c_unsel = weights_0b0c_trkd0up_nontag50[pt_bin_indices_0b0c_nontag50 == i]

 event_in_bin_0b1c_sel = weights_0b1c_trkd0up_50[pt_bin_indices_0b1c_50 == i]
 event_in_bin_0b1c_unsel = weights_0b1c_trkd0up_nontag50[pt_bin_indices_0b1c_nontag50 == i]

 event_in_bin_0b2c_sel = weights_0b2c_trkd0up_50[pt_bin_indices_0b2c_50 == i]
 event_in_bin_0b2c_unsel = weights_0b2c_trkd0up_nontag50[pt_bin_indices_0b2c_nontag50 == i]

 event_in_bin_1b0c_sel = weights_1b0c_trkd0up_50[pt_bin_indices_1b0c_50 == i]
 event_in_bin_1b0c_unsel = weights_1b0c_trkd0up_nontag50[pt_bin_indices_1b0c_nontag50 == i]

 event_in_bin_1b1c_sel = weights_1b1c_trkd0up_50[pt_bin_indices_1b1c_50 == i]
 event_in_bin_1b1c_unsel = weights_1b1c_trkd0up_nontag50[pt_bin_indices_1b1c_nontag50 == i]

 event_in_bin_2b0c_sel = weights_2b0c_trkd0up_50[pt_bin_indices_2b0c_50 == i]
 event_in_bin_2b0c_unsel = weights_2b0c_trkd0up_nontag50[pt_bin_indices_2b0c_nontag50 == i]

 nsel_0b0c_trkd0up.append(np.sum(event_in_bin_0b0c_sel))
 nsel_0b1c_trkd0up.append(np.sum(event_in_bin_0b1c_sel))
 nsel_0b2c_trkd0up.append(np.sum(event_in_bin_0b2c_sel))
 nsel_1b0c_trkd0up.append(np.sum(event_in_bin_1b0c_sel))
 nsel_1b1c_trkd0up.append(np.sum(event_in_bin_1b1c_sel))
 nsel_2b0c_trkd0up.append(np.sum(event_in_bin_2b0c_sel))

 nunsel_0b0c_trkd0up.append(np.sum(event_in_bin_0b0c_unsel))
 nunsel_0b1c_trkd0up.append(np.sum(event_in_bin_0b1c_unsel))
 nunsel_0b2c_trkd0up.append(np.sum(event_in_bin_0b2c_unsel))
 nunsel_1b0c_trkd0up.append(np.sum(event_in_bin_1b0c_unsel))
 nunsel_1b1c_trkd0up.append(np.sum(event_in_bin_1b1c_unsel))
 nunsel_2b0c_trkd0up.append(np.sum(event_in_bin_2b0c_unsel))

 err_nsel_0b0c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_0b0c_sel))
 err_nsel_0b1c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_0b1c_sel))
 err_nsel_0b2c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_0b2c_sel))
 err_nsel_1b0c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_1b0c_sel))
 err_nsel_1b1c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_1b1c_sel))
 err_nsel_2b0c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_2b0c_sel))

 err_nunsel_0b0c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_0b0c_unsel))
 err_nunsel_0b1c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_0b1c_unsel))
 err_nunsel_0b2c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_0b2c_unsel))
 err_nunsel_1b0c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_1b0c_unsel))
 err_nunsel_1b1c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_1b1c_unsel))
 err_nunsel_2b0c_trkd0up_50.append(compute_weightedcount_error(event_in_bin_2b0c_unsel))
    

nunsel_0b0c_trkd0up = np.asarray(nunsel_0b0c_trkd0up)
nunsel_0b1c_trkd0up = np.asarray(nunsel_0b1c_trkd0up)
nunsel_0b2c_trkd0up = np.asarray(nunsel_0b2c_trkd0up)
nunsel_1b0c_trkd0up = np.asarray(nunsel_1b0c_trkd0up)
nunsel_1b1c_trkd0up = np.asarray(nunsel_1b1c_trkd0up)
nunsel_2b0c_trkd0up = np.asarray(nunsel_2b0c_trkd0up)

nsel_0b0c_trkd0up = np.asarray(nsel_0b0c_trkd0up)
nsel_0b1c_trkd0up = np.asarray(nsel_0b1c_trkd0up)
nsel_0b2c_trkd0up = np.asarray(nsel_0b2c_trkd0up)
nsel_1b0c_trkd0up = np.asarray(nsel_1b0c_trkd0up)
nsel_1b1c_trkd0up = np.asarray(nsel_1b1c_trkd0up)
nsel_2b0c_trkd0up = np.asarray(nsel_2b0c_trkd0up)

err_nsel_0b0c_trkd0up_50 = np.asarray(err_nsel_0b0c_trkd0up_50)
err_nsel_0b1c_trkd0up_50 = np.asarray(err_nsel_0b1c_trkd0up_50)
err_nsel_0b2c_trkd0up_50 = np.asarray(err_nsel_0b2c_trkd0up_50)
err_nsel_1b0c_trkd0up_50 = np.asarray(err_nsel_1b0c_trkd0up_50)
err_nsel_1b1c_trkd0up_50 = np.asarray(err_nsel_1b1c_trkd0up_50)
err_nsel_2b0c_trkd0up_50 = np.asarray(err_nsel_2b0c_trkd0up_50)

err_nunsel_0b0c_trkd0up_50 = np.asarray(err_nunsel_0b0c_trkd0up_50)
err_nunsel_0b1c_trkd0up_50 = np.asarray(err_nunsel_0b1c_trkd0up_50)
err_nunsel_0b2c_trkd0up_50 = np.asarray(err_nunsel_0b2c_trkd0up_50)
err_nunsel_1b0c_trkd0up_50 = np.asarray(err_nunsel_1b0c_trkd0up_50)
err_nunsel_1b1c_trkd0up_50 = np.asarray(err_nunsel_1b1c_trkd0up_50)
err_nunsel_2b0c_trkd0up_50 = np.asarray(err_nunsel_2b0c_trkd0up_50)



eff_0b0c_trkd0up_50 = nsel_0b0c_trkd0up/(nsel_0b0c_trkd0up+nunsel_0b0c_trkd0up)
eff_0b1c_trkd0up_50 = nsel_0b1c_trkd0up/(nsel_0b1c_trkd0up+nunsel_0b1c_trkd0up)
eff_0b2c_trkd0up_50 = nsel_0b2c_trkd0up/(nsel_0b2c_trkd0up+nunsel_0b2c_trkd0up)
eff_1b0c_trkd0up_50 = nsel_1b0c_trkd0up/(nsel_1b0c_trkd0up+nunsel_1b0c_trkd0up)
eff_1b1c_trkd0up_50 = nsel_1b1c_trkd0up/(nsel_1b1c_trkd0up+nunsel_1b1c_trkd0up)
eff_2b0c_trkd0up_50 = nsel_2b0c_trkd0up/(nsel_2b0c_trkd0up+nunsel_2b0c_trkd0up)

err_eff_0b0c_trkd0up_50 = np.sqrt(((nunsel_0b0c_trkd0up*err_nsel_0b0c_trkd0up_50)**2 + (nsel_0b0c_trkd0up*err_nunsel_0b0c_trkd0up_50)**2)/(nsel_0b0c_trkd0up+nunsel_0b0c_trkd0up)**4)
err_eff_1b0c_trkd0up_50 = np.sqrt(((nunsel_1b0c_trkd0up*err_nsel_1b0c_trkd0up_50)**2 + (nsel_1b0c_trkd0up*err_nunsel_1b0c_trkd0up_50)**2)/(nsel_1b0c_trkd0up+nunsel_1b0c_trkd0up)**4)
err_eff_2b0c_trkd0up_50 = np.sqrt(((nunsel_2b0c_trkd0up*err_nsel_2b0c_trkd0up_50)**2 + (nsel_2b0c_trkd0up*err_nunsel_2b0c_trkd0up_50)**2)/(nsel_2b0c_trkd0up+nunsel_2b0c_trkd0up)**4)
err_eff_1b1c_trkd0up_50 = np.sqrt(((nunsel_1b1c_trkd0up*err_nsel_1b1c_trkd0up_50)**2 + (nsel_1b1c_trkd0up*err_nunsel_1b1c_trkd0up_50)**2)/(nsel_1b1c_trkd0up+nunsel_1b1c_trkd0up)**4)
err_eff_0b1c_trkd0up_50 = np.sqrt(((nunsel_0b1c_trkd0up*err_nsel_0b1c_trkd0up_50)**2 + (nsel_0b1c_trkd0up*err_nunsel_0b1c_trkd0up_50)**2)/(nsel_0b1c_trkd0up+nunsel_0b1c_trkd0up)**4)
err_eff_0b2c_trkd0up_50 = np.sqrt(((nunsel_0b2c_trkd0up*err_nsel_0b2c_trkd0up_50)**2 + (nsel_0b2c_trkd0up*err_nunsel_0b2c_trkd0up_50)**2)/(nsel_0b2c_trkd0up+nunsel_0b2c_trkd0up)**4)


fig, ax = plt.subplots(3, 2, figsize=(20, 15))
ax[0, 0].errorbar(pt_bins, eff_0b0c_trkd0up_50, xerr=pt_bin_width/2, yerr=err_eff_0b0c_trkd0up_50, fmt='o', label='0b0c')
ax[0, 0].set_title('0b0c')
ax[0, 0].set_xlabel('pT [GeV]')
ax[0, 0].set_ylabel('Efficiency')
ax[0, 0].set_yscale('log')
ax[0, 0].legend()

ax[0, 1].errorbar(pt_bins, eff_1b0c_trkd0up_50, xerr=pt_bin_width/2, yerr=err_eff_1b0c_trkd0up_50, fmt='o', label='1b0c')
ax[0, 1].set_title('1b0c')
ax[0, 1].set_xlabel('pT [GeV]')
ax[0, 1].set_ylabel('Efficiency')
ax[0, 1].set_yscale('log')
ax[0, 1].legend()

ax[1, 0].errorbar(pt_bins, eff_2b0c_trkd0up_50, xerr=pt_bin_width/2, yerr=err_eff_2b0c_trkd0up_50, fmt='o', label='2b0c')
ax[1, 0].set_title('2b0c')
ax[1, 0].set_xlabel('pT [GeV]')
ax[1, 0].set_ylabel('Efficiency')
ax[1, 0].set_yscale('log')
ax[1, 0].legend()

ax[1, 1].errorbar(pt_bins, eff_1b1c_trkd0up_50, xerr=pt_bin_width/2, yerr=err_eff_1b1c_trkd0up_50, fmt='o', label='1b1c')
ax[1, 1].set_title('1b1c')
ax[1, 1].set_xlabel('pT [GeV]')
ax[1, 1].set_ylabel('Efficiency')
ax[1, 1].set_yscale('log')
ax[1, 1].legend()

ax[2, 0].errorbar(pt_bins, eff_0b1c_trkd0up_50, xerr=pt_bin_width/2, yerr=err_eff_0b1c_trkd0up_50, fmt='o', label='0b1c')
ax[2, 0].set_title('0b1c')
ax[2, 0].set_xlabel('pT [GeV]')
ax[2, 0].set_ylabel('Efficiency')
ax[2, 0].set_yscale('log')
ax[2, 0].legend()

ax[2, 1].errorbar(pt_bins, eff_0b2c_trkd0up_50, xerr=pt_bin_width/2, yerr=err_eff_0b2c_trkd0up_50, fmt='o', label='0b2c')
ax[2, 1].set_title('0b2c')
ax[2, 1].set_xlabel('pT [GeV]')
ax[2, 1].set_ylabel('Efficiency')
ax[2, 1].set_yscale('log')
ax[2, 1].legend()

plt.tight_layout()
plt.savefig(efficiency_trkd0up_filename)

############ TRKD0 DOWN 50% CUT ############################
pt_values_0b0c_trkd0down_50 = pt_values_0b0c_trkd0down[discriminant_0b0c_trkd0down>discr_cut]
pt_values_1b0c_trkd0down_50 = pt_values_1b0c_trkd0down[discriminant_1b0c_trkd0down>discr_cut]
pt_values_2b0c_trkd0down_50 = pt_values_2b0c_trkd0down[discriminant_2b0c_trkd0down>discr_cut]
pt_values_1b1c_trkd0down_50 = pt_values_1b1c_trkd0down[discriminant_1b1c_trkd0down>discr_cut]
pt_values_0b1c_trkd0down_50 = pt_values_0b1c_trkd0down[discriminant_0b1c_trkd0down>discr_cut]
pt_values_0b2c_trkd0down_50 = pt_values_0b2c_trkd0down[discriminant_0b2c_trkd0down>discr_cut]

weights_0b0c_trkd0down_50 = weights_0b0c_trkd0down[discriminant_0b0c_trkd0down>discr_cut]
weights_1b0c_trkd0down_50 = weights_1b0c_trkd0down[discriminant_1b0c_trkd0down>discr_cut]
weights_2b0c_trkd0down_50 = weights_2b0c_trkd0down[discriminant_2b0c_trkd0down>discr_cut]
weights_1b1c_trkd0down_50 = weights_1b1c_trkd0down[discriminant_1b1c_trkd0down>discr_cut]
weights_0b1c_trkd0down_50 = weights_0b1c_trkd0down[discriminant_0b1c_trkd0down>discr_cut]
weights_0b2c_trkd0down_50 = weights_0b2c_trkd0down[discriminant_0b2c_trkd0down>discr_cut]

pt_values_0b0c_trkd0down_nontag50 = pt_values_0b0c_trkd0down[discriminant_0b0c_trkd0down<=discr_cut]
pt_values_1b0c_trkd0down_nontag50 = pt_values_1b0c_trkd0down[discriminant_1b0c_trkd0down<=discr_cut]
pt_values_2b0c_trkd0down_nontag50 = pt_values_2b0c_trkd0down[discriminant_2b0c_trkd0down<=discr_cut]
pt_values_1b1c_trkd0down_nontag50 = pt_values_1b1c_trkd0down[discriminant_1b1c_trkd0down<=discr_cut]
pt_values_0b1c_trkd0down_nontag50 = pt_values_0b1c_trkd0down[discriminant_0b1c_trkd0down<=discr_cut]
pt_values_0b2c_trkd0down_nontag50 = pt_values_0b2c_trkd0down[discriminant_0b2c_trkd0down<=discr_cut]

weights_0b0c_trkd0down_nontag50 = weights_0b0c_trkd0down[discriminant_0b0c_trkd0down<=discr_cut]
weights_1b0c_trkd0down_nontag50 = weights_1b0c_trkd0down[discriminant_1b0c_trkd0down<=discr_cut]
weights_2b0c_trkd0down_nontag50 = weights_2b0c_trkd0down[discriminant_2b0c_trkd0down<=discr_cut]
weights_1b1c_trkd0down_nontag50 = weights_1b1c_trkd0down[discriminant_1b1c_trkd0down<=discr_cut]
weights_0b1c_trkd0down_nontag50 = weights_0b1c_trkd0down[discriminant_0b1c_trkd0down<=discr_cut]
weights_0b2c_trkd0down_nontag50 = weights_0b2c_trkd0down[discriminant_0b2c_trkd0down<=discr_cut]

pt_bin_indices_0b0c_50 = np.digitize(pt_values_0b0c_trkd0down_50, pt_bins)
pt_bin_indices_1b0c_50 = np.digitize(pt_values_1b0c_trkd0down_50, pt_bins)
pt_bin_indices_2b0c_50 = np.digitize(pt_values_2b0c_trkd0down_50, pt_bins)
pt_bin_indices_1b1c_50 = np.digitize(pt_values_1b1c_trkd0down_50, pt_bins)
pt_bin_indices_0b1c_50 = np.digitize(pt_values_0b1c_trkd0down_50, pt_bins)
pt_bin_indices_0b2c_50 = np.digitize(pt_values_0b2c_trkd0down_50, pt_bins)

pt_bin_indices_0b0c_nontag50 = np.digitize(pt_values_0b0c_trkd0down_nontag50, pt_bins)
pt_bin_indices_1b0c_nontag50 = np.digitize(pt_values_1b0c_trkd0down_nontag50, pt_bins)
pt_bin_indices_2b0c_nontag50 = np.digitize(pt_values_2b0c_trkd0down_nontag50, pt_bins)
pt_bin_indices_1b1c_nontag50 = np.digitize(pt_values_1b1c_trkd0down_nontag50, pt_bins)
pt_bin_indices_0b1c_nontag50 = np.digitize(pt_values_0b1c_trkd0down_nontag50, pt_bins)
pt_bin_indices_0b2c_nontag50 = np.digitize(pt_values_0b2c_trkd0down_nontag50, pt_bins)


nsel_0b0c_trkd0down = []
nsel_0b1c_trkd0down = []
nsel_0b2c_trkd0down = []
nsel_1b0c_trkd0down = []
nsel_1b1c_trkd0down = []
nsel_2b0c_trkd0down = []

nunsel_0b0c_trkd0down = []
nunsel_0b1c_trkd0down = []
nunsel_0b2c_trkd0down = []
nunsel_1b0c_trkd0down = []
nunsel_1b1c_trkd0down = []
nunsel_2b0c_trkd0down = []

err_nsel_0b0c_trkd0down_50 = []
err_nsel_0b1c_trkd0down_50 = []
err_nsel_0b2c_trkd0down_50 = []
err_nsel_1b0c_trkd0down_50 = []
err_nsel_1b1c_trkd0down_50 = []
err_nsel_2b0c_trkd0down_50 = []

err_nunsel_0b0c_trkd0down_50 = []
err_nunsel_0b1c_trkd0down_50 = []
err_nunsel_0b2c_trkd0down_50 = []
err_nunsel_1b0c_trkd0down_50 = []
err_nunsel_1b1c_trkd0down_50 = []
err_nunsel_2b0c_trkd0down_50 = []


for i in range(0, len(pt_bins)):
    
 event_in_bin_0b0c_sel = weights_0b0c_trkd0down_50[pt_bin_indices_0b0c_50 == i]
 event_in_bin_0b0c_unsel = weights_0b0c_trkd0down_nontag50[pt_bin_indices_0b0c_nontag50 == i]

 event_in_bin_0b1c_sel = weights_0b1c_trkd0down_50[pt_bin_indices_0b1c_50 == i]
 event_in_bin_0b1c_unsel = weights_0b1c_trkd0down_nontag50[pt_bin_indices_0b1c_nontag50 == i]

 event_in_bin_0b2c_sel = weights_0b2c_trkd0down_50[pt_bin_indices_0b2c_50 == i]
 event_in_bin_0b2c_unsel = weights_0b2c_trkd0down_nontag50[pt_bin_indices_0b2c_nontag50 == i]

 event_in_bin_1b0c_sel = weights_1b0c_trkd0down_50[pt_bin_indices_1b0c_50 == i]
 event_in_bin_1b0c_unsel = weights_1b0c_trkd0down_nontag50[pt_bin_indices_1b0c_nontag50 == i]

 event_in_bin_1b1c_sel = weights_1b1c_trkd0down_50[pt_bin_indices_1b1c_50 == i]
 event_in_bin_1b1c_unsel = weights_1b1c_trkd0down_nontag50[pt_bin_indices_1b1c_nontag50 == i]

 event_in_bin_2b0c_sel = weights_2b0c_trkd0down_50[pt_bin_indices_2b0c_50 == i]
 event_in_bin_2b0c_unsel = weights_2b0c_trkd0down_nontag50[pt_bin_indices_2b0c_nontag50 == i]

 nsel_0b0c_trkd0down.append(np.sum(event_in_bin_0b0c_sel))
 nsel_0b1c_trkd0down.append(np.sum(event_in_bin_0b1c_sel))
 nsel_0b2c_trkd0down.append(np.sum(event_in_bin_0b2c_sel))
 nsel_1b0c_trkd0down.append(np.sum(event_in_bin_1b0c_sel))
 nsel_1b1c_trkd0down.append(np.sum(event_in_bin_1b1c_sel))
 nsel_2b0c_trkd0down.append(np.sum(event_in_bin_2b0c_sel))

 nunsel_0b0c_trkd0down.append(np.sum(event_in_bin_0b0c_unsel))
 nunsel_0b1c_trkd0down.append(np.sum(event_in_bin_0b1c_unsel))
 nunsel_0b2c_trkd0down.append(np.sum(event_in_bin_0b2c_unsel))
 nunsel_1b0c_trkd0down.append(np.sum(event_in_bin_1b0c_unsel))
 nunsel_1b1c_trkd0down.append(np.sum(event_in_bin_1b1c_unsel))
 nunsel_2b0c_trkd0down.append(np.sum(event_in_bin_2b0c_unsel))

 err_nsel_0b0c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_0b0c_sel))
 err_nsel_0b1c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_0b1c_sel))
 err_nsel_0b2c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_0b2c_sel))
 err_nsel_1b0c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_1b0c_sel))
 err_nsel_1b1c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_1b1c_sel))
 err_nsel_2b0c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_2b0c_sel))

 err_nunsel_0b0c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_0b0c_unsel))
 err_nunsel_0b1c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_0b1c_unsel))
 err_nunsel_0b2c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_0b2c_unsel))
 err_nunsel_1b0c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_1b0c_unsel))
 err_nunsel_1b1c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_1b1c_unsel))
 err_nunsel_2b0c_trkd0down_50.append(compute_weightedcount_error(event_in_bin_2b0c_unsel))

nunsel_0b0c_trkd0down = np.asarray(nunsel_0b0c_trkd0down)
nunsel_0b1c_trkd0down = np.asarray(nunsel_0b1c_trkd0down)
nunsel_0b2c_trkd0down = np.asarray(nunsel_0b2c_trkd0down)
nunsel_1b0c_trkd0down = np.asarray(nunsel_1b0c_trkd0down)
nunsel_1b1c_trkd0down = np.asarray(nunsel_1b1c_trkd0down)
nunsel_2b0c_trkd0down = np.asarray(nunsel_2b0c_trkd0down)

nsel_0b0c_trkd0down = np.asarray(nsel_0b0c_trkd0down)
nsel_0b1c_trkd0down = np.asarray(nsel_0b1c_trkd0down)
nsel_0b2c_trkd0down = np.asarray(nsel_0b2c_trkd0down)
nsel_1b0c_trkd0down = np.asarray(nsel_1b0c_trkd0down)
nsel_1b1c_trkd0down = np.asarray(nsel_1b1c_trkd0down)
nsel_2b0c_trkd0down = np.asarray(nsel_2b0c_trkd0down)

err_nsel_0b0c_trkd0down_50 = np.asarray(err_nsel_0b0c_trkd0down_50)
err_nsel_0b1c_trkd0down_50 = np.asarray(err_nsel_0b1c_trkd0down_50)
err_nsel_0b2c_trkd0down_50 = np.asarray(err_nsel_0b2c_trkd0down_50)
err_nsel_1b0c_trkd0down_50 = np.asarray(err_nsel_1b0c_trkd0down_50)
err_nsel_1b1c_trkd0down_50 = np.asarray(err_nsel_1b1c_trkd0down_50)
err_nsel_2b0c_trkd0down_50 = np.asarray(err_nsel_2b0c_trkd0down_50)


err_nunsel_0b0c_trkd0down_50 = np.asarray(err_nunsel_0b0c_trkd0down_50)
err_nunsel_0b1c_trkd0down_50 = np.asarray(err_nunsel_0b1c_trkd0down_50)
err_nunsel_0b2c_trkd0down_50 = np.asarray(err_nunsel_0b2c_trkd0down_50)
err_nunsel_1b0c_trkd0down_50 = np.asarray(err_nunsel_1b0c_trkd0down_50)
err_nunsel_1b1c_trkd0down_50 = np.asarray(err_nunsel_1b1c_trkd0down_50)
err_nunsel_2b0c_trkd0down_50 = np.asarray(err_nunsel_2b0c_trkd0down_50)


eff_0b0c_trkd0down_50 = nsel_0b0c_trkd0down/(nsel_0b0c_trkd0down+nunsel_0b0c_trkd0down)
eff_0b1c_trkd0down_50 = nsel_0b1c_trkd0down/(nsel_0b1c_trkd0down+nunsel_0b1c_trkd0down)
eff_0b2c_trkd0down_50 = nsel_0b2c_trkd0down/(nsel_0b2c_trkd0down+nunsel_0b2c_trkd0down)
eff_1b0c_trkd0down_50 = nsel_1b0c_trkd0down/(nsel_1b0c_trkd0down+nunsel_1b0c_trkd0down)
eff_1b1c_trkd0down_50 = nsel_1b1c_trkd0down/(nsel_1b1c_trkd0down+nunsel_1b1c_trkd0down)
eff_2b0c_trkd0down_50 = nsel_2b0c_trkd0down/(nsel_2b0c_trkd0down+nunsel_2b0c_trkd0down)


err_eff_0b0c_trkd0down_50 = np.sqrt(((nunsel_0b0c_trkd0down*err_nsel_0b0c_trkd0down_50)**2 + (nsel_0b0c_trkd0down*err_nunsel_0b0c_trkd0down_50)**2)/(nsel_0b0c_trkd0down+nunsel_0b0c_trkd0down)**4)
err_eff_1b0c_trkd0down_50 = np.sqrt(((nunsel_1b0c_trkd0down*err_nsel_1b0c_trkd0down_50)**2 + (nsel_1b0c_trkd0down*err_nunsel_1b0c_trkd0down_50)**2)/(nsel_1b0c_trkd0down+nunsel_1b0c_trkd0down)**4)
err_eff_2b0c_trkd0down_50 = np.sqrt(((nunsel_2b0c_trkd0down*err_nsel_2b0c_trkd0down_50)**2 + (nsel_2b0c_trkd0down*err_nunsel_2b0c_trkd0down_50)**2)/(nsel_2b0c_trkd0down+nunsel_2b0c_trkd0down)**4)
err_eff_1b1c_trkd0down_50 = np.sqrt(((nunsel_1b1c_trkd0down*err_nsel_1b1c_trkd0down_50)**2 + (nsel_1b1c_trkd0down*err_nunsel_1b1c_trkd0down_50)**2)/(nsel_1b1c_trkd0down+nunsel_1b1c_trkd0down)**4)
err_eff_0b1c_trkd0down_50 = np.sqrt(((nunsel_0b1c_trkd0down*err_nsel_0b1c_trkd0down_50)**2 + (nsel_0b1c_trkd0down*err_nunsel_0b1c_trkd0down_50)**2)/(nsel_0b1c_trkd0down+nunsel_0b1c_trkd0down)**4)
err_eff_0b2c_trkd0down_50 = np.sqrt(((nunsel_0b2c_trkd0down*err_nsel_0b2c_trkd0down_50)**2 + (nsel_0b2c_trkd0down*err_nunsel_0b2c_trkd0down_50)**2)/(nsel_0b2c_trkd0down+nunsel_0b2c_trkd0down)**4)

fig, ax = plt.subplots(3, 2, figsize=(20, 15))
ax[0, 0].errorbar(pt_bins, eff_0b0c_trkd0down_50, xerr=pt_bin_width/2, yerr=err_eff_0b0c_trkd0down_50, fmt='o', label='0b0c')
ax[0, 0].set_title('0b0c')
ax[0, 0].set_xlabel('pT [GeV]')
ax[0, 0].set_ylabel('Efficiency')
ax[0, 0].set_yscale('log')
ax[0, 0].legend()

ax[0, 1].errorbar(pt_bins, eff_1b0c_trkd0down_50, xerr=pt_bin_width/2, yerr=err_eff_1b0c_trkd0down_50, fmt='o', label='1b0c')
ax[0, 1].set_title('1b0c')
ax[0, 1].set_xlabel('pT [GeV]')
ax[0, 1].set_ylabel('Efficiency')
ax[0, 1].set_yscale('log')
ax[0, 1].legend()

ax[1, 0].errorbar(pt_bins, eff_2b0c_trkd0down_50, xerr=pt_bin_width/2, yerr=err_eff_2b0c_trkd0down_50, fmt='o', label='2b0c')
ax[1, 0].set_title('2b0c')
ax[1, 0].set_xlabel('pT [GeV]')
ax[1, 0].set_ylabel('Efficiency')
ax[1, 0].set_yscale('log')
ax[1, 0].legend()

ax[1, 1].errorbar(pt_bins, eff_1b1c_trkd0down_50, xerr=pt_bin_width/2, yerr=err_eff_1b1c_trkd0down_50, fmt='o', label='1b1c')
ax[1, 1].set_title('1b1c')
ax[1, 1].set_xlabel('pT [GeV]')
ax[1, 1].set_ylabel('Efficiency')
ax[1, 1].set_yscale('log')
ax[1, 1].legend()

ax[2, 0].errorbar(pt_bins, eff_0b1c_trkd0down_50, xerr=pt_bin_width/2, yerr=err_eff_0b1c_trkd0down_50, fmt='o', label='0b1c')
ax[2, 0].set_title('0b1c')
ax[2, 0].set_xlabel('pT [GeV]')
ax[2, 0].set_ylabel('Efficiency')
ax[2, 0].set_yscale('log')
ax[2, 0].legend()

ax[2, 1].errorbar(pt_bins, eff_0b2c_trkd0down_50, xerr=pt_bin_width/2, yerr=err_eff_0b2c_trkd0down_50, fmt='o', label='0b2c')
ax[2, 1].set_title('0b2c')
ax[2, 1].set_xlabel('pT [GeV]')
ax[2, 1].set_ylabel('Efficiency')
ax[2, 1].set_yscale('log')
ax[2, 1].legend()

plt.tight_layout()
plt.savefig(efficiency_trkd0down_filename)

######### COMPUTE SCALE FACTORS WITH ERRORS #############

sf_0b0c_trkd0_50 = eff_0b0c_trkd0_50/eff_0b0c_nominal_50    
sf_0b1c_trkd0_50 = eff_0b1c_trkd0_50/eff_0b1c_nominal_50
sf_0b2c_trkd0_50 = eff_0b2c_trkd0_50/eff_0b2c_nominal_50
sf_1b0c_trkd0_50 = eff_1b0c_trkd0_50/eff_1b0c_nominal_50
sf_1b1c_trkd0_50 = eff_1b1c_trkd0_50/eff_1b1c_nominal_50
sf_2b0c_trkd0_50 = eff_2b0c_trkd0_50/eff_2b0c_nominal_50

err_sf_0b0c_trkd0_50 = np.sqrt((err_eff_0b0c_trkd0_50/eff_0b0c_nominal_50)**2 + (err_eff_0b0c_nominal_50 * eff_0b0c_trkd0_50/eff_0b0c_nominal_50**2)**2)
err_sf_0b1c_trkd0_50 = np.sqrt((err_eff_0b1c_trkd0_50/eff_0b1c_nominal_50)**2 + (err_eff_0b1c_nominal_50 * eff_0b1c_trkd0_50/eff_0b1c_nominal_50**2)**2)
err_sf_0b2c_trkd0_50 = np.sqrt((err_eff_0b2c_trkd0_50/eff_0b2c_nominal_50)**2 + (err_eff_0b2c_nominal_50 * eff_0b2c_trkd0_50/eff_0b2c_nominal_50**2)**2)
err_sf_1b0c_trkd0_50 = np.sqrt((err_eff_1b0c_trkd0_50/eff_1b0c_nominal_50)**2 + (err_eff_1b0c_nominal_50 * eff_1b0c_trkd0_50/eff_1b0c_nominal_50**2)**2)
err_sf_1b1c_trkd0_50 = np.sqrt((err_eff_1b1c_trkd0_50/eff_1b1c_nominal_50)**2 + (err_eff_1b1c_nominal_50 * eff_1b1c_trkd0_50/eff_1b1c_nominal_50**2)**2)
err_sf_2b0c_trkd0_50 = np.sqrt((err_eff_2b0c_trkd0_50/eff_2b0c_nominal_50)**2 + (err_eff_2b0c_nominal_50 * eff_2b0c_trkd0_50/eff_2b0c_nominal_50**2)**2)

sf_0b0c_trkd0up_50 = eff_0b0c_trkd0up_50/eff_0b0c_nominal_50
sf_0b1c_trkd0up_50 = eff_0b1c_trkd0up_50/eff_0b1c_nominal_50
sf_0b2c_trkd0up_50 = eff_0b2c_trkd0up_50/eff_0b2c_nominal_50
sf_1b0c_trkd0up_50 = eff_1b0c_trkd0up_50/eff_1b0c_nominal_50
sf_1b1c_trkd0up_50 = eff_1b1c_trkd0up_50/eff_1b1c_nominal_50
sf_2b0c_trkd0up_50 = eff_2b0c_trkd0up_50/eff_2b0c_nominal_50

err_sf_0b0c_trkd0up_50 = np.sqrt((err_eff_0b0c_trkd0up_50/eff_0b0c_nominal_50)**2 + (err_eff_0b0c_nominal_50 * eff_0b0c_trkd0up_50/eff_0b0c_nominal_50**2)**2)
err_sf_0b1c_trkd0up_50 = np.sqrt((err_eff_0b1c_trkd0up_50/eff_0b1c_nominal_50)**2 + (err_eff_0b1c_nominal_50 * eff_0b1c_trkd0up_50/eff_0b1c_nominal_50**2)**2)
err_sf_0b2c_trkd0up_50 = np.sqrt((err_eff_0b2c_trkd0up_50/eff_0b2c_nominal_50)**2 + (err_eff_0b2c_nominal_50 * eff_0b2c_trkd0up_50/eff_0b2c_nominal_50**2)**2)
err_sf_1b0c_trkd0up_50 = np.sqrt((err_eff_1b0c_trkd0up_50/eff_1b0c_nominal_50)**2 + (err_eff_1b0c_nominal_50 * eff_1b0c_trkd0up_50/eff_1b0c_nominal_50**2)**2)
err_sf_1b1c_trkd0up_50 = np.sqrt((err_eff_1b1c_trkd0up_50/eff_1b1c_nominal_50)**2 + (err_eff_1b1c_nominal_50 * eff_1b1c_trkd0up_50/eff_1b1c_nominal_50**2)**2)
err_sf_2b0c_trkd0up_50 = np.sqrt((err_eff_2b0c_trkd0up_50/eff_2b0c_nominal_50)**2 + (err_eff_2b0c_nominal_50 * eff_2b0c_trkd0up_50/eff_2b0c_nominal_50**2)**2)

sf_0b0c_trkd0down_50 = eff_0b0c_trkd0down_50/eff_0b0c_nominal_50
sf_0b1c_trkd0down_50 = eff_0b1c_trkd0down_50/eff_0b1c_nominal_50
sf_0b2c_trkd0down_50 = eff_0b2c_trkd0down_50/eff_0b2c_nominal_50
sf_1b0c_trkd0down_50 = eff_1b0c_trkd0down_50/eff_1b0c_nominal_50
sf_1b1c_trkd0down_50 = eff_1b1c_trkd0down_50/eff_1b1c_nominal_50
sf_2b0c_trkd0down_50 = eff_2b0c_trkd0down_50/eff_2b0c_nominal_50

err_sf_0b0c_trkd0down_50 = np.sqrt((err_eff_0b0c_trkd0down_50/eff_0b0c_nominal_50)**2 + (err_eff_0b0c_nominal_50 * eff_0b0c_trkd0down_50/eff_0b0c_nominal_50**2)**2)
err_sf_0b1c_trkd0down_50 = np.sqrt((err_eff_0b1c_trkd0down_50/eff_0b1c_nominal_50)**2 + (err_eff_0b1c_nominal_50 * eff_0b1c_trkd0down_50/eff_0b1c_nominal_50**2)**2)
err_sf_0b2c_trkd0down_50 = np.sqrt((err_eff_0b2c_trkd0down_50/eff_0b2c_nominal_50)**2 + (err_eff_0b2c_nominal_50 * eff_0b2c_trkd0down_50/eff_0b2c_nominal_50**2)**2)
err_sf_1b0c_trkd0down_50 = np.sqrt((err_eff_1b0c_trkd0down_50/eff_1b0c_nominal_50)**2 + (err_eff_1b0c_nominal_50 * eff_1b0c_trkd0down_50/eff_1b0c_nominal_50**2)**2)
err_sf_1b1c_trkd0down_50 = np.sqrt((err_eff_1b1c_trkd0down_50/eff_1b1c_nominal_50)**2 + (err_eff_1b1c_nominal_50 * eff_1b1c_trkd0down_50/eff_1b1c_nominal_50**2)**2)
err_sf_2b0c_trkd0down_50 = np.sqrt((err_eff_2b0c_trkd0down_50/eff_2b0c_nominal_50)**2 + (err_eff_2b0c_nominal_50 * eff_2b0c_trkd0down_50/eff_2b0c_nominal_50**2)**2)

#plot in a 3x2 grid in each plot the scale factor for a certain category and for each systematic variation
#x axis is pt, y axis is scale factor, error bars are the statistical uncertainty

fig, ax = plt.subplots(3,2, figsize=(30, 20))
ax[0,0].errorbar(pt_bins, sf_0b0c_trkd0_50, xerr=pt_bin_width/2, yerr = err_sf_0b0c_trkd0_50, fmt='o', label='TRK_RES_D0_MEAS')
ax[0,0].errorbar(pt_bins, sf_0b0c_trkd0up_50, xerr=pt_bin_width/2, yerr = err_sf_0b0c_trkd0up_50, fmt='o', label='TRK_RES_D0_MEAS_UP')
ax[0,0].errorbar(pt_bins, sf_0b0c_trkd0down_50, xerr=pt_bin_width/2, yerr = err_sf_0b0c_trkd0down_50, fmt='o', label='TRK_RES_D0_MEAS_DOWN')
ax[0,0].set_title('0b + 0c')
ax[0,0].set_xlabel(r"$p_{T}$ [GeV]")
ax[0,0].set_ylabel('Scale factor')
ax[0,0].legend()
ax[0,0].set_ylim(0.5, 1.5)
ax[0,0].grid(True)

ax[0,1].errorbar(pt_bins, sf_0b1c_trkd0_50, xerr=pt_bin_width/2, yerr = err_sf_0b1c_trkd0_50, fmt='o', label='TRK_RES_D0_MEAS')
ax[0,1].errorbar(pt_bins, sf_0b1c_trkd0up_50, xerr=pt_bin_width/2, yerr = err_sf_0b1c_trkd0up_50, fmt='o', label='TRK_RES_D0_MEAS_UP')
ax[0,1].errorbar(pt_bins, sf_0b1c_trkd0down_50, xerr=pt_bin_width/2, yerr = err_sf_0b1c_trkd0down_50, fmt='o', label='TRK_RES_D0_MEAS_DOWN')
ax[0,1].set_title('0b + >=1c')
ax[0,1].set_xlabel(r"$p_{T}$ [GeV]")
ax[0,1].set_ylabel('Scale factor')
ax[0,1].legend()
ax[0,1].set_ylim(0.5, 1.5)
ax[0,1].grid(True)

ax[1,0].errorbar(pt_bins, sf_0b2c_trkd0_50, xerr=pt_bin_width/2, yerr = err_sf_0b2c_trkd0_50, fmt='o', label='TRK_RES_D0_MEAS')
ax[1,0].errorbar(pt_bins, sf_0b2c_trkd0up_50, xerr=pt_bin_width/2, yerr = err_sf_0b2c_trkd0up_50, fmt='o', label='TRK_RES_D0_MEAS_UP')
ax[1,0].errorbar(pt_bins, sf_0b2c_trkd0down_50, xerr=pt_bin_width/2, yerr = err_sf_0b2c_trkd0down_50, fmt='o', label='TRK_RES_D0_MEAS_DOWN')
ax[1,0].set_title('0b + >=2c')
ax[1,0].set_xlabel(r"$p_{T}$ [GeV]")
ax[1,0].set_ylabel('Scale factor')
ax[1,0].legend()
ax[1,0].set_ylim(0.5, 1.5)
ax[1,0].grid(True)

ax[1,1].errorbar(pt_bins, sf_1b0c_trkd0_50, xerr=pt_bin_width/2, yerr = err_sf_1b0c_trkd0_50, fmt='o', label='TRK_RES_D0_MEAS')
ax[1,1].errorbar(pt_bins, sf_1b0c_trkd0up_50, xerr=pt_bin_width/2, yerr = err_sf_1b0c_trkd0up_50, fmt='o', label='TRK_RES_D0_MEAS_UP')
ax[1,1].errorbar(pt_bins, sf_1b0c_trkd0down_50, xerr=pt_bin_width/2, yerr = err_sf_1b0c_trkd0down_50, fmt='o', label='TRK_RES_D0_MEAS_DOWN')
ax[1,1].set_title('1b + 0c')
ax[1,1].set_xlabel(r"$p_{T}$ [GeV]")
ax[1,1].set_ylabel('Scale factor')
ax[1,1].legend()
ax[1,1].set_ylim(0.5, 1.5)
ax[1,1].grid(True)

ax[2,0].errorbar(pt_bins, sf_1b1c_trkd0_50, xerr=pt_bin_width/2, yerr = err_sf_1b1c_trkd0_50, fmt='o', label='TRK_RES_D0_MEAS')
ax[2,0].errorbar(pt_bins, sf_1b1c_trkd0up_50, xerr=pt_bin_width/2, yerr = err_sf_1b1c_trkd0up_50, fmt='o', label='TRK_RES_D0_MEAS_UP')
ax[2,0].errorbar(pt_bins, sf_1b1c_trkd0down_50, xerr=pt_bin_width/2, yerr = err_sf_1b1c_trkd0down_50, fmt='o', label='TRK_RES_D0_MEAS_DOWN')
ax[2,0].set_title('1b + >=1c')
ax[2,0].set_xlabel(r"$p_{T}$ [GeV]")
ax[2,0].set_ylabel('Scale factor')
ax[2,0].legend()
ax[2,0].set_ylim(0.5, 1.5)
ax[2,0].grid(True)

ax[2,1].errorbar(pt_bins, sf_2b0c_trkd0_50, xerr=pt_bin_width/2, yerr = err_sf_2b0c_trkd0_50, fmt='o', label='TRK_RES_D0_MEAS')
ax[2,1].errorbar(pt_bins, sf_2b0c_trkd0up_50, xerr=pt_bin_width/2, yerr = err_sf_2b0c_trkd0up_50, fmt='o', label='TRK_RES_D0_MEAS_UP')
ax[2,1].errorbar(pt_bins, sf_2b0c_trkd0down_50, xerr=pt_bin_width/2, yerr = err_sf_2b0c_trkd0down_50, fmt='o', label='TRK_RES_D0_MEAS_DOWN')
ax[2,1].set_title('>=2b + 0c')
ax[2,1].set_xlabel(r"$p_{T}$ [GeV]")
ax[2,1].set_ylabel('Scale factor')
ax[2,1].legend()
ax[2,1].set_ylim(0.5, 1.5)
ax[2,1].grid(True)

plt.tight_layout()
plt.savefig(sf_filename)

# saving in an h5 file all the sf_values and their errors for each category and for each systematic variation
hf = h5py.File(sf_h5file_filename, 'w')
hf.create_dataset('sf_0b0c_trkd0_50', data=sf_0b0c_trkd0_50)
hf.create_dataset('sf_0b1c_trkd0_50', data=sf_0b1c_trkd0_50)
hf.create_dataset('sf_0b2c_trkd0_50', data=sf_0b2c_trkd0_50)
hf.create_dataset('sf_1b0c_trkd0_50', data=sf_1b0c_trkd0_50)
hf.create_dataset('sf_1b1c_trkd0_50', data=sf_1b1c_trkd0_50)
hf.create_dataset('sf_2b0c_trkd0_50', data=sf_2b0c_trkd0_50)
hf.create_dataset('err_sf_0b0c_trkd0_50', data=err_sf_0b0c_trkd0_50)
hf.create_dataset('err_sf_0b1c_trkd0_50', data=err_sf_0b1c_trkd0_50)
hf.create_dataset('err_sf_0b2c_trkd0_50', data=err_sf_0b2c_trkd0_50)
hf.create_dataset('err_sf_1b0c_trkd0_50', data=err_sf_1b0c_trkd0_50)
hf.create_dataset('err_sf_1b1c_trkd0_50', data=err_sf_1b1c_trkd0_50)
hf.create_dataset('err_sf_2b0c_trkd0_50', data=err_sf_2b0c_trkd0_50)
hf.create_dataset('sf_0b0c_trkd0up_50', data=sf_0b0c_trkd0up_50)
hf.create_dataset('sf_0b1c_trkd0up_50', data=sf_0b1c_trkd0up_50)
hf.create_dataset('sf_0b2c_trkd0up_50', data=sf_0b2c_trkd0up_50)
hf.create_dataset('sf_1b0c_trkd0up_50', data=sf_1b0c_trkd0up_50)
hf.create_dataset('sf_1b1c_trkd0up_50', data=sf_1b1c_trkd0up_50)
hf.create_dataset('sf_2b0c_trkd0up_50', data=sf_2b0c_trkd0up_50)
hf.create_dataset('err_sf_0b0c_trkd0up_50', data=err_sf_0b0c_trkd0up_50)
hf.create_dataset('err_sf_0b1c_trkd0up_50', data=err_sf_0b1c_trkd0up_50)
hf.create_dataset('err_sf_0b2c_trkd0up_50', data=err_sf_0b2c_trkd0up_50)
hf.create_dataset('err_sf_1b0c_trkd0up_50', data=err_sf_1b0c_trkd0up_50)
hf.create_dataset('err_sf_1b1c_trkd0up_50', data=err_sf_1b1c_trkd0up_50)
hf.create_dataset('err_sf_2b0c_trkd0up_50', data=err_sf_2b0c_trkd0up_50)
hf.create_dataset('sf_0b0c_trkd0down_50', data=sf_0b0c_trkd0down_50)
hf.create_dataset('sf_0b1c_trkd0down_50', data=sf_0b1c_trkd0down_50)
hf.create_dataset('sf_0b2c_trkd0down_50', data=sf_0b2c_trkd0down_50)
hf.create_dataset('sf_1b0c_trkd0down_50', data=sf_1b0c_trkd0down_50)
hf.create_dataset('sf_1b1c_trkd0down_50', data=sf_1b1c_trkd0down_50)
hf.create_dataset('sf_2b0c_trkd0down_50', data=sf_2b0c_trkd0down_50)
hf.create_dataset('err_sf_0b0c_trkd0down_50', data=err_sf_0b0c_trkd0down_50)
hf.create_dataset('err_sf_0b1c_trkd0down_50', data=err_sf_0b1c_trkd0down_50)
hf.create_dataset('err_sf_0b2c_trkd0down_50', data=err_sf_0b2c_trkd0down_50)
hf.create_dataset('err_sf_1b0c_trkd0down_50', data=err_sf_1b0c_trkd0down_50)
hf.create_dataset('err_sf_1b1c_trkd0down_50', data=err_sf_1b1c_trkd0down_50)
hf.create_dataset('err_sf_2b0c_trkd0down_50', data=err_sf_2b0c_trkd0down_50)
hf.close()


