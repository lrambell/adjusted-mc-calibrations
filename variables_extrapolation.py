import os
import h5py
from concurrent.futures import ThreadPoolExecutor
import numpy as np
from methods import SubCatSplit

directories = [
     '/project/gruppo1/atlas_gen/rambeluc/FTAG1_p5981/deltaR/slice3/user.lrambell'  
]

saved_h5_filename = 'prova.h5'

fc = 0.02
ft = 0.25

def process_file(file_name):
    file_path = os.path.join(directory, file_name)
         
    # Open the h5 file
    with h5py.File(file_path, 'r') as f:

        #interesting jet variables extraction and discriminant computation
        jets = f['jets']
        pt = jets['pt'][:] * 0.001
        mc_weights = jets['mcEventWeight'][:]
        hbb_values = jets['GN2Xv01_phbb']
        hcc_values = jets['GN2Xv01_phcc']
        top_values = jets['GN2Xv01_ptop']
        qcd_values = jets['GN2Xv01_pqcd']


        discr = np.log(hbb_values / (ft * top_values + fc * hcc_values + (1 - fc - ft) * qcd_values))      

        #use subjet variables for splitting events in different categories
        subjets = f['subjets']
        # >=2b + 0c
        bb_index = SubCatSplit(subjets, "bb")
        bbb_index = SubCatSplit(subjets, "bbb")
        bbl_index = SubCatSplit(subjets, "bbl")
        bbt_index = SubCatSplit(subjets, "bbt")

        # 1b + >=1c
        bct_index = SubCatSplit(subjets, "bct")
        bcc_index = SubCatSplit(subjets, "bcc")
        bcl_index = SubCatSplit(subjets, "bcl")
        bc_index = SubCatSplit(subjets, "bc")

        # 0b + >=2c
        cc_index = SubCatSplit(subjets, "cc")
        ccc_index = SubCatSplit(subjets, "ccc")
        ccl_index = SubCatSplit(subjets, "ccl")
        cct_index = SubCatSplit(subjets, "cct")

        # 1b + 0c        
        b_index = SubCatSplit(subjets, "b")
        bl_index = SubCatSplit(subjets, "bl")
        bll_index = SubCatSplit(subjets, "bll")
        bt_index = SubCatSplit(subjets, "bt")
        btl_index = SubCatSplit(subjets, "btl")
        btt_index = SubCatSplit(subjets, "btt")

        # 0b + 1c
        c_index = SubCatSplit(subjets, "c")
        cl_index = SubCatSplit(subjets, "cl")
        cll_index = SubCatSplit(subjets, "cll")
        ct_index = SubCatSplit(subjets, "ct")
        ctl_index = SubCatSplit(subjets, "ctl")
        ctt_index = SubCatSplit(subjets, "ctt")

        # 0b + 0c        
        l_index = SubCatSplit(subjets, "l")
        ll_index = SubCatSplit(subjets, "ll")     
        lll_index = SubCatSplit(subjets, "lll")
        lt_index = SubCatSplit(subjets, "lt")
        ltt_index = SubCatSplit(subjets, "ltt")
        t_index = SubCatSplit(subjets, "t")
        tt_index = SubCatSplit(subjets, "tt")
        ttt_index = SubCatSplit(subjets, "ttt")
        tll_index = SubCatSplit(subjets, "tll")
        
        # 1b + 0c
        btt_index = SubCatSplit(subjets, "btt")
        btl_index = SubCatSplit(subjets, "btl")
        bll_index = SubCatSplit(subjets, "bll")
        bt_index = SubCatSplit(subjets, "bt")
        bl_index = SubCatSplit(subjets, "bl")
        b_index = SubCatSplit(subjets, "b")
        
        discr_bb = discr[bb_index]
        discr_bbb = discr[bbb_index]
        discr_bbl = discr[bbl_index]
        discr_bbt = discr[bbt_index]

        discr_bct = discr[bct_index]
        discr_bcc = discr[bcc_index]
        discr_bcl = discr[bcl_index]
        discr_bc = discr[bc_index]

        discr_cc = discr[cc_index]
        discr_ccc = discr[ccc_index]
        discr_ccl = discr[ccl_index]
        discr_cct = discr[cct_index]

        discr_b = discr[b_index]
        discr_bl = discr[bl_index]
        discr_bll = discr[bll_index]
        discr_bt = discr[bt_index]
        discr_btl = discr[btl_index]
        discr_btt = discr[btt_index]

        discr_c = discr[c_index]
        discr_cl = discr[cl_index]
        discr_cll = discr[cll_index]
        discr_ct = discr[ct_index]
        discr_ctl = discr[ctl_index]
        discr_ctt = discr[ctt_index]

        discr_l = discr[l_index]
        discr_ll = discr[ll_index]
        discr_lll = discr[lll_index]
        discr_lt = discr[lt_index]
        discr_ltt = discr[ltt_index]
        discr_t = discr[t_index]
        discr_tt = discr[tt_index]
        discr_ttt = discr[ttt_index]
        discr_tll = discr[tll_index]

        discr_2b0c = np.concatenate((discr_bb, discr_bbb, discr_bbl, discr_bbt))
        discr_1b0c = np.concatenate((discr_b, discr_bl, discr_bll, discr_bt, discr_btl, discr_btt))
        discr_1b1c = np.concatenate((discr_bc, discr_bcc, discr_bcl, discr_bct))
        discr_0b1c = np.concatenate((discr_c, discr_cl, discr_cll, discr_ct, discr_ctl, discr_ctt))
        discr_0b2c = np.concatenate((discr_cc, discr_ccc, discr_ccl, discr_cct))
        discr_0b0c = np.concatenate((discr_l, discr_ll, discr_lll, discr_lt, discr_ltt, discr_t, discr_tt, discr_ttt, discr_tll))


        w_bb = mc_weights[bb_index]
        w_bbb = mc_weights[bbb_index]
        w_bbl = mc_weights[bbl_index]
        w_bbt = mc_weights[bbt_index]

        w_bct = mc_weights[bct_index]
        w_bcc = mc_weights[bcc_index]
        w_bcl = mc_weights[bcl_index]
        w_bc = mc_weights[bc_index]

        w_cc = mc_weights[cc_index]
        w_ccc = mc_weights[ccc_index]
        w_ccl = mc_weights[ccl_index]
        w_cct = mc_weights[cct_index]

        w_b = mc_weights[b_index]
        w_bl = mc_weights[bl_index]
        w_bll = mc_weights[bll_index]
        w_bt = mc_weights[bt_index]
        w_btl = mc_weights[btl_index]
        w_btt = mc_weights[btt_index]

        w_c = mc_weights[c_index]
        w_cl = mc_weights[cl_index]
        w_cll = mc_weights[cll_index]
        w_ct = mc_weights[ct_index]
        w_ctl = mc_weights[ctl_index]
        w_ctt = mc_weights[ctt_index]
        
        w_l = mc_weights[l_index]
        w_ll = mc_weights[ll_index]
        w_lll = mc_weights[lll_index]
        w_lt = mc_weights[lt_index]
        w_ltt = mc_weights[ltt_index]
        w_t = mc_weights[t_index]
        w_tt = mc_weights[tt_index]
        w_ttt = mc_weights[ttt_index]
        w_tll = mc_weights[tll_index]

        w_2b0c = np.concatenate((w_bb, w_bbb, w_bbl, w_bbt))
        w_1b0c = np.concatenate((w_b, w_bl, w_bll, w_bt, w_btl, w_btt))
        w_1b1c = np.concatenate((w_bc, w_bcc, w_bcl, w_bct))
        w_0b1c = np.concatenate((w_c, w_cl, w_cll, w_ct, w_ctl, w_ctt))
        w_0b2c = np.concatenate((w_cc, w_ccc, w_ccl, w_cct))
        w_0b0c = np.concatenate((w_l, w_ll, w_lll, w_lt, w_ltt, w_t, w_tt, w_ttt, w_tll))

        pt_bb = pt[bb_index]
        pt_bbb = pt[bbb_index]
        pt_bbl = pt[bbl_index]
        pt_bbt = pt[bbt_index]

        pt_bct = pt[bct_index]
        pt_bcc = pt[bcc_index]
        pt_bcl = pt[bcl_index]
        pt_bc = pt[bc_index]

        pt_cc = pt[cc_index]
        pt_ccc = pt[ccc_index]
        pt_ccl = pt[ccl_index]
        pt_cct = pt[cct_index]

        pt_b = pt[b_index]
        pt_bl = pt[bl_index]
        pt_bll = pt[bll_index]
        pt_bt = pt[bt_index]
        pt_btl = pt[btl_index]
        pt_btt = pt[btt_index]

        pt_c = pt[c_index]
        pt_cl = pt[cl_index]
        pt_cll = pt[cll_index]
        pt_ct = pt[ct_index]
        pt_ctl = pt[ctl_index]
        pt_ctt = pt[ctt_index]

        pt_l = pt[l_index]
        pt_ll = pt[ll_index]
        pt_lll = pt[lll_index]
        pt_lt = pt[lt_index]
        pt_ltt = pt[ltt_index]
        pt_t = pt[t_index]
        pt_tt = pt[tt_index]
        pt_ttt = pt[ttt_index]
        pt_tll = pt[tll_index]

        pt_2b0c = np.concatenate((pt_bb, pt_bbb, pt_bbl, pt_bbt))
        pt_1b0c = np.concatenate((pt_b, pt_bl, pt_bll, pt_bt, pt_btl, pt_btt))
        pt_1b1c = np.concatenate((pt_bc, pt_bcc, pt_bcl, pt_bct))
        pt_0b1c = np.concatenate((pt_c, pt_cl, pt_cll, pt_ct, pt_ctl, pt_ctt))
        pt_0b2c = np.concatenate((pt_cc, pt_ccc, pt_ccl, pt_cct))
        pt_0b0c = np.concatenate((pt_l, pt_ll, pt_lll, pt_lt, pt_ltt, pt_t, pt_tt, pt_ttt, pt_tll))

    

        return mc_weights, discr_1b0c, discr_2b0c, discr_1b1c, discr_0b1c, discr_0b2c, discr_0b0c, w_1b0c, w_2b0c, w_1b1c, w_0b1c, w_0b2c, w_0b0c, pt_1b0c, pt_2b0c, pt_1b1c, pt_0b1c, pt_0b2c, pt_0b0c
        


def process_directory(directory, max_files=None):
    

    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".h5"):
                file_path = os.path.join(root, file)
                directory_name = os.path.basename(directory)

                mc_weights, discr_1b0c, discr_2b0c, discr_1b1c, discr_0b1c, discr_0b2c, discr_0b0c, w_1b0c, w_2b0c, w_1b1c, w_0b1c, w_0b2c, w_0b0c, pt_1b0c, pt_2b0c, pt_1b1c, pt_0b1c, pt_0b2c, pt_0b0c = process_file(file_path)
                
                discriminant_1b0c.extend(discr_1b0c)
                discriminant_2b0c.extend(discr_2b0c)
                discriminant_0b1c.extend(discr_0b1c)
                discriminant_0b2c.extend(discr_0b2c)
                discriminant_1b1c.extend(discr_1b1c)
                discriminant_0b0c.extend(discr_0b0c)

                mc_weights_1b0c.extend(w_1b0c)
                mc_weights_2b0c.extend(w_2b0c)
                mc_weights_1b1c.extend(w_1b1c)
                mc_weights_0b1c.extend(w_0b1c)
                mc_weights_0b2c.extend(w_0b2c)
                mc_weights_0b0c.extend(w_0b0c)

                pt_values_2b0c.extend(pt_2b0c)
                pt_values_1b0c.extend(pt_1b0c)
                pt_values_1b1c.extend(pt_1b1c)
                pt_values_0b1c.extend(pt_0b1c)
                pt_values_0b2c.extend(pt_0b2c)
                pt_values_0b0c.extend(pt_0b0c)

                #saving all the slice weights for the final weighting computation
                total_w.extend(mc_weights)                

                del mc_weights, discr_1b0c, discr_2b0c, discr_1b1c, discr_0b1c, discr_0b2c, discr_0b0c, w_1b0c, w_2b0c, w_1b1c, w_0b1c, w_0b2c, w_0b0c, pt_1b0c, pt_2b0c, pt_1b1c, pt_0b1c, pt_0b2c, pt_0b0c
    

for directory in directories:

    discriminant_2b0c = []
    discriminant_1b0c = []
    discriminant_1b1c = []
    discriminant_0b1c = []
    discriminant_0b2c = []
    discriminant_0b0c = []

    mc_weights_2b0c = []
    mc_weights_1b0c = []
    mc_weights_1b1c = []
    mc_weights_0b1c = []
    mc_weights_0b2c = []
    mc_weights_0b0c = []

    pt_values_2b0c = []
    pt_values_1b0c = []
    pt_values_1b1c = []
    pt_values_0b1c = []
    pt_values_0b2c = []
    pt_values_0b0c = []

    total_w = []
    process_directory(directory)  
    

    #save all the discriminant values and the weights in a single h5 file
    with h5py.File(saved_h5_filename, 'w') as f:
        f.create_dataset('discriminant_1b0c', data=discriminant_1b0c)
        f.create_dataset('discriminant_2b0c', data=discriminant_2b0c)
        f.create_dataset('discriminant_1b1c', data=discriminant_1b1c)
        f.create_dataset('discriminant_0b1c', data=discriminant_0b1c)
        f.create_dataset('discriminant_0b2c', data=discriminant_0b2c)
        f.create_dataset('discriminant_0b0c', data=discriminant_0b0c)
        f.create_dataset('mc_weights_1b0c', data=mc_weights_1b0c)
        f.create_dataset('mc_weights_2b0c', data=mc_weights_2b0c)
        f.create_dataset('mc_weights_1b1c', data=mc_weights_1b1c)
        f.create_dataset('mc_weights_0b1c', data=mc_weights_0b1c)
        f.create_dataset('mc_weights_0b2c', data=mc_weights_0b2c)
        f.create_dataset('mc_weights_0b0c', data=mc_weights_0b0c)
        f.create_dataset('pt_values_1b0c', data=pt_values_1b0c)
        f.create_dataset('pt_values_2b0c', data=pt_values_2b0c)
        f.create_dataset('pt_values_1b1c', data=pt_values_1b1c)
        f.create_dataset('pt_values_0b1c', data=pt_values_0b1c)
        f.create_dataset('pt_values_0b2c', data=pt_values_0b2c)
        f.create_dataset('pt_values_0b0c', data=pt_values_0b0c)
        f.create_dataset('total_weights', data=np.sum(total_w))

    print('Processed files in directory: ', directories)
    print('Sum of all the weights of the slice: ', np.sum(total_w))
